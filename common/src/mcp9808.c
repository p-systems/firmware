/**
 * @brief Simplified MCP9808 access library
 * @copyright 2023 Pomegranate Systems LLC
 * @copyright 2016 Michael Blouin <michaelblouin.ca>
 * @license Apache 2.0
 * @see https://github.com/Mobius5150/mcp9808
 */

#include "mcp9808.h"

#include <stdbool.h>

#include "stm32_util.h"

bool MCP9808_Readout(I2C_TypeDef *i2c,
                     uint8_t      address_7_bit,
                     float       *output) {
  const uint32_t mcp_timeout_ms = 100;
  uint8_t        buffer[2];

  if (!I2C_Read_Register(i2c,
                         address_7_bit,
                         MCP9808_REGISTER_T,
                         sizeof(buffer),
                         buffer,
                         mcp_timeout_ms)) {
    return false;
  }

  buffer[0] &= 0x1F;

  if (buffer[0] & 0x10) {
    buffer[0] &= 0x0F;
    *output = (256 - ((float)buffer[0] * 16.0f + (float)buffer[1] / 16.0f));
  } else {
    *output = ((float)buffer[0] * 16.0f + (float)buffer[1] / 16.0f);
  }
  return true;
}
