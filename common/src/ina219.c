/**
 * @file
 * @brief stm32 interface to INA219
 * @author Stou Sandalski <stou@p-systems.io>
 * @license MIT
 */


#include "ina219.h"
#include "stm32_util.h"

const uint32_t ina219_timeout_ms = 100;

bool INA219_Config(I2C_TypeDef *i2c,
                   uint16_t     address_7_bit,
                   uint16_t     config) {

  uint8_t buffer[3] = {
      INA219_REGISTER_CONFIGURATION,
      (uint8_t) (0xFF & (config >> 8)),
      (uint8_t) (0xFF & config)};

  return I2C_Write(i2c,
                   address_7_bit,
                   sizeof(buffer),
                   buffer,
                   ina219_timeout_ms);
}

bool INA219_Readout(I2C_TypeDef *i2c,
                    uint16_t     address_7_bit,
                    int32_t     *V_shunt,
                    int32_t     *V_bus) {

  uint8_t buffer[2];

  // There is no auto-increment support so registers have to be read separately.

  // Read Shunt voltage
  if (!I2C_Read_Register(i2c,
                         address_7_bit,
                         INA219_REGISTER_VOLTAGE_SHUNT,
                         sizeof(buffer),
                         buffer,
                         ina219_timeout_ms)) {
    return false;
  }

  *V_shunt = buffer[1] | (buffer[0] << 8);

  // Shunt can be negative.
  if (*V_shunt & 0x8000) {
    *V_shunt = -(uint16_t) (0x1U + ~*V_shunt);
  }

  // Read Bus Voltage
  if (!I2C_Read_Register(i2c,
                         address_7_bit,
                         INA219_REGISTER_VOLTAGE_BUS,
                         sizeof(buffer),
                         buffer,
                         ina219_timeout_ms)) {
    return false;
  }

  *V_bus = (buffer[1] >> 3 | (buffer[0] << 5));

  return true;
}

bool INA219_Test(I2C_TypeDef *i2c,
                 uint16_t     address_7_bit) {
  uint8_t buffer[2];

  // Read configuration but don't worry about the contents
  // reset values are not as described in manual
  return I2C_Read_Register(i2c,
                           address_7_bit,
                           INA219_REGISTER_CONFIGURATION,
                           sizeof(buffer),
                           buffer,
                           ina219_timeout_ms);
}
