/**
 * @file
 * @brief Definitions for Automation interface
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @copyright (c) 2019 - STMicroelectronics
 * @copyright (c) 2017 - UAVCAN Team
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 *
 * @warning Update `method_signatures` with any multi-messages you want to process
 */

#include "uavcan_v0.h"

#include <string.h>
#include <stdint.h>

#if defined(STM32F3)

#include "stm32f3xx_hal_flash.h"

#elif defined(STM32F4)
//#include "stm32f4xx_hal_flash.h"
#endif

#include "stm32_util.h"

#if ENABLE_PSYSTEMS_AUTOMATION_REGISTER
#include "automation.h"
#endif

extern uint32_t System_Tick;

// CAN device
extern CAN_HANDLE *CAN_device;

// Interface Stats (at the call level)
extern iface_stats_t interface_stats[CAN_IFACE_COUNT];

typedef struct {
  uint16_t id;
  uint64_t signature;
} method_sig_t;

// Add signatures for any multi-packet data types
static const method_sig_t method_signatures[] = {
    {UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_ID, UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_SIGNATURE},
    {UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID, UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_SIGNATURE},
    {UAVCAN_PROTOCOL_FILE_READ_ID, UAVCAN_PROTOCOL_FILE_READ_SIGNATURE},
    {UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_ID, UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_SIGNATURE},
    {UAVCAN_PROTOCOL_PARAM_GET_SET_ID, UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE},
    {UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_ID, UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_SIGNATURE},
    {IO_P_SYSTEMS_DEVICE_I2C_READ_ID, IO_P_SYSTEMS_DEVICE_I2C_READ_SIGNATURE},
    {IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID, IO_P_SYSTEMS_DEVICE_I2C_WRITE_SIGNATURE},
};

packet_reassembly_t reassembly_buffer[REASSEMBLY_BUFFERS] = {};

/**
 * Low-level UAVCAN can_id decoding routines
 */
static inline uint8_t destination_id_from_can_id(uint32_t can_id) {
  return (uint8_t) ((can_id >> 8U) & 0x7FU);
}

static inline uint16_t msg_type_from_can_id(uint32_t can_id) {
  return (uint16_t) ((can_id >> 8U) & 0xFFFFU);
}

static inline uint8_t priority_from_can_id(uint32_t can_id) {
  return (uint8_t) ((can_id >> 24U) & 0x1FU);
}

static inline uint8_t service_type_from_can_id(uint32_t can_id) {
  return (uint8_t) ((can_id >> 16U) & 0xFFU);
}

static inline uint8_t source_id_from_can_id(uint32_t can_id) {
  return (uint8_t) (can_id & 0x7FU);
}

static inline bool is_service_from_can_id(uint32_t can_id) {
  return (bool) ((can_id >> 7U) & 0x1U);
}

static inline bool is_start_of_transfer(uint8_t tail_byte) {
  return (bool) ((tail_byte >> 7U) & 0x1U);
}

static inline bool is_end_of_transfer(uint8_t tail_byte) {
  return (bool) ((tail_byte >> 6U) & 0x1U);
}

static inline uint8_t transfer_id_from_tail_byte(uint8_t tail_byte) {
  return ((uint8_t) (tail_byte & 0x1FU));
}

/**
 * Low Level data manipulation functions
 */

void uintN_to_buffer(uint8_t N_bytes,
                     uint64_t val,
                     uint8_t *buffer) {

  for (int i = 0; i < N_bytes; ++i) {
    buffer[i] = (uint8_t) (0xFFU & val);
    val >>= 8U;
  }

}

union FP32 {
  uint32_t u;
  float f;
};

uint16_t float16_to_buffer(float value,
                           uint8_t *buffer) {

  const union FP32 f32inf = {255UL << 23U};
  const union FP32 f16inf = {31UL << 23U};
  const union FP32 magic = {15UL << 23U};
  const uint32_t sign_mask = 0x80000000UL;
  const uint32_t round_mask = ~0xFFFUL;

  union FP32 in;
  in.f = value;
  uint32_t sign = in.u & sign_mask;
  in.u ^= sign;

  uint16_t out = 0;

  if (in.u >= f32inf.u) {
    out = (in.u > f32inf.u) ? (uint16_t) 0x7FFFU : (uint16_t) 0x7C00U;
  } else {
    in.u &= round_mask;
    in.f *= magic.f;
    in.u -= round_mask;
    if (in.u > f16inf.u) {
      in.u = f16inf.u;
    }
    out = (uint16_t) (in.u >> 13U);
  }

  out |= (uint16_t) (sign >> 16U);

  buffer[0] = (uint8_t) (0xFF & out);
  buffer[1] = (uint8_t) (0xFF & out >> 8);

  return out;
}

float buffer_to_float16(const uint8_t *buffer) {

  uint16_t value = (uint16_t) (buffer[0] | (buffer[1] << 8U));

  const union FP32 magic = {(254UL - 15UL) << 23U};
  const union FP32 was_inf_nan = {(127UL + 16UL) << 23U};
  union FP32 out;

  out.u = (value & 0x7FFFU) << 13U;
  out.f *= magic.f;
  if (out.f >= was_inf_nan.f) {
    out.u |= 255UL << 23U;
  }
  out.u |= (value & 0x8000UL) << 16U;

  return out.f;
}

void float32_to_buffer(float value_f,
                       uint8_t *buffer) {

  union FP32 value = {.f = value_f};

  for (int i = 0; i < 4; ++i) {
    buffer[i] = (uint8_t) (0xFFU & value.u);
    value.u >>= 8U;
  }

}

float buffer_to_float32(const uint8_t *buffer) {

  union FP32 val = {.u = 0};

  int8_t shift = 0;

  for (int i = 0; i < 4; ++i) {
    val.u |= (((uint32_t) buffer[i]) << shift);
    shift += 8;
  }

  return val.f;
}

int64_t buffer_to_int64(const uint8_t *buffer) {

  int64_t val = 0;
  int8_t shift = 0;

  for (int i = 0; i < 8; ++i) {
    val |= (int64_t) buffer[i] << shift;
    shift += 8;
  }

  return val;
}

// Enable these by default.
#ifndef ENABLE_AUTO_BAUD
#define ENABLE_AUTO_BAUD                                              1
#endif

#ifndef ENABLE_DYNAMIC_ID_ALLOCATION
#define ENABLE_DYNAMIC_ID_ALLOCATION                                  1
#endif

bool UAVCAN_Init(boot_info_t *boot_info,
                 node_info_t *node,
                 can_params_t *params,
                 uavcan_init_t *can_init) {

#if ENABLE_AUTO_BAUD
  static uint32_t CAN_valid_bauds_kbs[] = {1000, 500, 250, 125};
  const size_t baud_rate_count = sizeof(CAN_valid_bauds_kbs)/sizeof(CAN_valid_bauds_kbs[0]);
#endif

  if (node->can_status==CAN_STATUS_RESET) {
#if ENABLE_AUTO_BAUD
    // Just started and have to figure out where to go to next
    if (params->auto_baud_enable) {
      node->can_status = CAN_STATUS_AUTO_BAUD;
    } else {
      boot_info->can_baud = params->default_baud_kbps;
      node->can_status = CAN_STATUS_SEND_ONLY;
    }
#else
    node->can_status = CAN_STATUS_SEND_ONLY;
    boot_info->can_baud = params->default_baud_kbps;
#endif
  }

#if ENABLE_AUTO_BAUD
  if (node->can_status==CAN_STATUS_AUTO_BAUD) {
    // Cycling through the auto-baud rate

    // Node status received so the baud is correct
    if (can_init->node_status_received) {
      node->can_status = CAN_STATUS_SEND_ONLY;
      return true;
    }

    // Too many retries fail to default
    if ((params->auto_baud_max_retries > 0)
        && (can_init->auto_baud_retry_count > params->auto_baud_max_retries)) {
      node->can_status = CAN_STATUS_SEND_ONLY;
      boot_info->can_baud = params->default_baud_kbps;
      return true;
    }

    // Not time for a new retry.
    if ((can_init->auto_baud_retry_timestamp > 0) &&
        (System_Tick - can_init->auto_baud_retry_timestamp) < params->auto_baud_retry_time_ms) {
      return true;
    }

    // Cycle-through the different baud rates
    boot_info->can_baud = CAN_valid_bauds_kbs[can_init->next_baud_ix];
    can_init->node_status_received = false;

    if (bxCAN_Init(can_init->can_if, boot_info->can_baud, CANBUS_TIMEOUT_MS, false)) {
      // Setup the filter to only let in node-status packets
      uint32_t filters[2] = {UAVCAN_PROTOCOL_NODE_STATUS_FILTER_ID,
                             UAVCAN_PROTOCOL_NODE_STATUS_FILTER_MASK};
      bxCAN_Config_Filters(can_init->can_filter_if, 1, filters);
    }

    can_init->next_baud_ix = (can_init->next_baud_ix + 1)%baud_rate_count;
    can_init->auto_baud_retry_count += 1;
    can_init->auto_baud_retry_timestamp = System_Tick;

  }
#endif
  if (node->can_status==CAN_STATUS_SEND_ONLY) {
#if FDCAN == 1
    // We know the correct baud rate either from auto-baud or default
    if (!FDCAN_Init(can_init->can_if,
                    FDCAN_MODE_NORMAL,
                    boot_info->can_baud,
                    CANBUS_TIMEOUT_MS)) {
      // TODO: Set some kind of error state?
      return false;
    }
#else
    // We know the correct baud rate either from auto-baud or default
    if (!bxCAN_Init(can_init->can_if, boot_info->can_baud, CANBUS_TIMEOUT_MS, params->auto_retransmit_enable)) {
      // TODO: Set some kind of error state?
      return false;
    }
#endif


#if ENABLE_DYNAMIC_ID_ALLOCATION

    if (!params->dynamic_id_allocation_enable) {
      node->id_valid = true;
    }

    if (!node->id_valid) {

      // Set up the filter to only let in id allocation packets
      uint32_t filters[2] = {UAVCAN_PROTOCOL_DYNAMIC_NODE_ID_ALLOCATION_FILTER_ID,
                             UAVCAN_PROTOCOL_DYNAMIC_NODE_ID_ALLOCATION_FILTER_MASK};
#if FDCAN==1
      FDCAN_Config_Filters(can_init->can_filter_if,
                           1,filters);
#else
      bxCAN_Config_Filters(can_init->can_filter_if, 1, filters);
#endif
    }
#else
    node->id_valid = true;
#endif
    node->can_status = CAN_STATUS_OPERATIONAL;
  }

  if (node->can_status==CAN_STATUS_OPERATIONAL) {
    // CAN interface is initialized but do we have a node id
    if (node->id_valid) {
      // Setup normal reception filters
#if FDCAN == 1
      FDCAN_Config_Filters(can_init->can_filter_if, can_init->filter_count, can_init->can_filters);
#else
      bxCAN_Config_Filters(can_init->can_filter_if, can_init->filter_count, can_init->can_filters);
#endif
      node->can_status = CAN_STATUS_ONLINE;
      return true;
    }
#if ENABLE_DYNAMIC_ID_ALLOCATION
    if ((can_init->dynamic_id_allocation_timestamp==0) ||
        ((can_init->dynamic_id_allocation_timestamp > 0) &&
            (System_Tick - can_init->dynamic_id_allocation_timestamp >
                params->dynamic_id_allocation_retry_time_ms))) {

      // If there was no response and we are at max retries, fail to default id
      if (can_init->dynamic_id_allocation_retry_count < params->dynamic_id_allocation_max_retries) {
        // Otherwise send the first message in the allocation sequence
        Send_Node_ID_Allocation_Request(node->uuid, 6, 0, true);
        can_init->dynamic_id_allocation_timestamp = System_Tick;
        ++can_init->dynamic_id_allocation_retry_count;
      } else {
        node->id_valid = true;
      }
    }
#endif
  }
  return true;
}

parameter_getset_t param_get_set_;

// Registers
#if ENABLE_PSYSTEMS_AUTOMATION_REGISTER
register_request_t register_operation_request_;
#endif

uint8_t error_code_;

/**
 *
 * @param data_type_id
 * @param is_service
 * @param source_node_id
 * @param priority
 * @param transfer_id
 * @param payload
 * @param payload_len
 */
void Process_UAVCAN_Message(const uint16_t data_type_id,
                            const bool is_service,
                            const uint8_t source_node_id,
                            const uint8_t priority,
                            uint8_t transfer_id,
                            uint8_t *payload,
                            uint32_t payload_len,
                            node_info_t *node,
                            uavcan_init_t *uavcan_init,
                            request_queue_t *queue,
                            boot_info_t *boot_info) {
  size_t firmware_file_path_len;

  // Dispatch data
  switch (data_type_id) {
    {

      case 1: // UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_ID and UAVCAN_PROTOCOL_GET_NODE_INFO_ID

        // If we have no name this message is actually dynamic id resolution
        if (!node->id_valid) {
#if ENABLE_DYNAMIC_ID_ALLOCATION
          // uavcan.protocol.dynamic_id.Allocation

          // Need to see if this message is meant for this node
          // by checking how many UUID bytes match
          int uuid_matches = 0;
          for (int i = 0; i < payload_len - 1; ++i) {
            if (payload[1 + uuid_matches]!=node->uuid[uuid_matches]) {
              break;
            }
            ++uuid_matches;
          }

          // Decode message
          if (uuid_matches==6) {
            // Message 2
            Send_Node_ID_Allocation_Request(&node->uuid[6], 6, 0, false);
          } else if (uuid_matches==12) {
            // Message 3
            Send_Node_ID_Allocation_Request(&node->uuid[12], 4, 0, false);
          } else if (uuid_matches==16) {
            node->id = (payload[0] >> 1U);
            node->id_valid = true;
          } else {
            // Something is wrong so reset back to start
            uavcan_init->dynamic_id_allocation_timestamp = 0;
          }
#endif
        } else {
          Request_Queue_Add(queue,
                            data_type_id,
                            source_node_id,
                            transfer_id,
                            priority,
                            0);
        }

      break;

#if ENABLE_OPTION_BYTE_WRITE
      case IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID:
      // io.p-systems.OptionByteWrite
      Request_Queue_Add_Unique(&queue,
                               data_type_id,
                               source_node_id,
                               transfer_id,
                               priority,
                               0);

      obwrite_data[0] = payload[0];
      obwrite_data[1] = payload[1];

      break;
#endif

#if ENABLE_AUTO_BAUD
      case UAVCAN_PROTOCOL_NODE_STATUS_ID:uavcan_init->node_status_received = true;
      break;
#endif

#if ENABLE_TRANSPORT_STATS
      case UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_ID:
        // uavcan.protocol.GetTransportStats / uavcan.protocol.GlobalTimeSync

        if (is_service) {
          Request_Queue_Add(queue,
                            data_type_id,
                            source_node_id,
                            transfer_id,
                            priority,
                            0);
        }
      break;
#endif

      case UAVCAN_PROTOCOL_RESTART_NODE_ID:
        // uavcan.protocol.RestartNode

        if (Check_NodeRestart_Request(payload, payload_len)) {
          Request_Queue_Add(queue,
                            data_type_id,
                            source_node_id,
                            transfer_id,
                            priority,
                            0);
        }
      break;

      case UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID:

        Deserialize_BeginFirmwareUpdate_Request(payload,
                                                payload_len,
                                                &boot_info->server_node_id,
                                                &firmware_file_path_len,
                                                (char *) boot_info->firmware_file_path);

      boot_info->server_node_id = (boot_info->server_node_id == 0) ? source_node_id : boot_info->server_node_id;
      boot_info->firmware_file_path_len = firmware_file_path_len;

      Request_Queue_Add_Unique(queue,
                               data_type_id,
                               source_node_id,
                               transfer_id,
                               priority,
                               0);

      break;

#if ENABLE_PARAMS
      case UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_ID:

        // First byte is the opcode, the rest is a reserved future parameter...
        Request_Queue_Add_Unique(queue,
                                 data_type_id,
                                 source_node_id,
                                 transfer_id,
                                 priority,
                                 payload[0]);

      break;

      case UAVCAN_PROTOCOL_PARAM_GET_SET_ID:

        Request_Queue_Add_Unique(queue,
                                 data_type_id,
                                 source_node_id,
                                 transfer_id,
                                 priority,
                                 0);

      Deserialize_GetSet_Request(payload,
                                 payload_len,
                                 &param_get_set_);
      break;
#endif

#if ENABLE_PSYSTEMS_I2C
      case IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID:
      // io.psystems.i2c.Write

      case IO_P_SYSTEMS_DEVICE_I2C_READ_ID:
        // io.psystems.i2c.Read/.Write

        if(Decode_Device_I2C_ReadWrite(payload, payload_len)){
          Request_Queue_Add_Unique(queue,
                                   data_type_id,
                                   source_node_id,
                                   transfer_id,
                                   priority,
                                   0);
        }
      break;
#endif

#if ENABLE_PSYSTEMS_AUTOMATION_SYNCHRONIZE
      case IO_P_SYSTEMS_AUTOMATION_SYNCHRONIZE_ID:
        Handle_Psystems_Automation_Synchronize();
        break;
#endif

#if ENABLE_PSYSTEMS_AUTOMATION_REGISTER
      case IO_P_SYSTEMS_AUTOMATION_OPERATION_ID:
        // io.p-systems.automation.Operation
        Deserialize_Register_Operation_Request(payload,
                                               payload_len,
                                               &register_operation_request_);
        Request_Queue_Add_Unique(queue,
                                 data_type_id,
                                 source_node_id,
                                 transfer_id,
                                 priority,
                                 0);

        break;
#endif

      default:
        // TODO: Flag unknown message?
        break;
    }
  }
}

void Service_UAVCAN_Requests(node_info_t *node,
                             uavcan_init_t *uavcan_init,
                             request_queue_t *queue,
                             boot_info_t *boot_info,
                             bool enable_firmware_updates,
                             can_params_t *can) {

#if ENABLE_PARAMS
  bool param_ok = false;
#endif

  // If there are no requests pending, return
  if (!queue->request[queue->top].pending) {
    return;
  }

  // Dispatch message
  switch (queue->request[queue->top].type_id) {

    case UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID:
      Service_Request_Begin_Firmware_Update(&queue->request[queue->top]);
      break;

#if ENABLE_HANDLE_FILE_READ
      case UAVCAN_PROTOCOL_FILE_READ_ID:
        // uavcan.protocol.file.Read
        Service_Request_File_Read();
        break;
#endif

    case UAVCAN_PROTOCOL_GET_NODE_INFO_ID:
      // uavcan.protocol.GetNodeInfo
      Send_GetNodeInfo_Response(node, queue->request[queue->top]);
      break;

#if ENABLE_TRANSPORT_STATS
    case UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_ID:
      // uavcan.protocol.GetTransportStat
      Send_GetTransportStats_Response(node, queue->request[queue->top]);
      break;
#endif

#if ENABLE_PARAMS
      case UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_ID:
        // uavcan.protocol.param.ExecuteOpcode

        switch(queue->request[queue->top].data){

          case UAVCAN_PROTOCOL_PARAM_OPCODE_ERASE:
            // Load defaults and fall-through to save
            Params_Load_Default_Values();

          case UAVCAN_PROTOCOL_PARAM_OPCODE_SAVE:
            param_ok = Params_Save();
            break;

          default:
            param_ok = false;
            break;
        }
        Send_ExecuteOpcode_Response(node->id, queue->request[queue->top], 0, param_ok);
        break;

      case UAVCAN_PROTOCOL_PARAM_GET_SET_ID:
        // uavcan.protocol.param.GetSet

        if(Params_Call_Handle_GetSet(node->id,
                                     &param_get_set_,
                                     queue->request[queue->top])){

          Handle_Param_GetSet_Special(param_get_set_.name);
        }

        break;
#endif

    case UAVCAN_PROTOCOL_RESTART_NODE_ID:
      // uavcan.protocol.RestartNode
      Send_NodeRestart_Response(node->id,
                                true,
                                queue->request[queue->top]);
      Busy_Wait(10);
      NVIC_SystemReset();

#if ENABLE_PSYSTEMS_I2C
      case IO_P_SYSTEMS_DEVICE_I2C_READ_ID:
        // io.p-systems.i2c.Read
        Handle_Device_I2C_Read(&queue->request[queue->top]);
        break;

      case IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID:
        // io.p-systems.i2c.Write
        Handle_Device_I2C_Write(&queue->request[queue->top]);
        break;
#endif

#if ENABLE_PSYSTEMS_AUTOMATION_REGISTER

      case IO_P_SYSTEMS_AUTOMATION_OPERATION_ID:
        error_code_ = Handle_Register_Operation(&register_operation_request_);
        Send_Register_Operation_Response(node->id,
                                         &register_operation_request_,
                                         &queue->request[queue->top],
                                         error_code_);
        break;
#endif

#if ENABLE_OPTION_BYTE_WRITE
      case IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID:

        Handle_Write_Option_Bytes_Request(node.id,
                                          queue.request[queue.top],
                                          enable_option_byte_writer,
                                          obwrite_data);

        Busy_Wait(100);
        NVIC_SystemReset();
#endif

  }

  // Update queue pointers
  Request_Queue_Pop(queue);
}

/**
 * Transmit a UAVCAN packet using stm32 bxCAN
 * https://uavcan.org/Specification/4._CAN_bus_transport_layer/
 *
 * @param can_id
 * @param data_type_signature
 * @param transfer_id
 * @param payload
 * @param payload_len
 * @return
 */
bool UAVCAN_TX_Frames(CAN_HANDLE *can,
                      uint32_t can_id,
                      uint64_t data_type_signature,
                      uint8_t transfer_id,
                      const uint8_t *payload,
                      uint16_t payload_len) {

  CAN_TX_HEADER can_header = {};

#if USE_FDCAN
  can_header.Identifier = can_id & 0x1FFFFFFFU;
  can_header.IdType = FDCAN_EXTENDED_ID;
  can_header.FDFormat = FDCAN_CLASSIC_CAN;
  can_header.TxFrameType = FDCAN_DATA_FRAME;
  can_header.TxEventFifoControl = FDCAN_NO_TX_EVENTS;
  can_header.BitRateSwitch = FDCAN_BRS_OFF;
  can_header.ErrorStateIndicator = FDCAN_ESI_ACTIVE;
  can_header.MessageMarker = 0;
#else
  can_header.ExtId = can_id & 0x1FFFFFFFU;
  can_header.IDE = CAN_ID_EXT;
  can_header.RTR = 0;
#endif
  uint8_t processed_payload[UAVCAN_CAN_FRAME_MAX_DATA_LEN];

  if (payload_len < UAVCAN_CAN_FRAME_MAX_DATA_LEN) {
    // Single frame transfer
    memset(processed_payload, 0, sizeof(processed_payload));
    memcpy(processed_payload, payload, payload_len);

    processed_payload[payload_len] = (uint8_t) (0xC0U | (transfer_id & 0x1FU));

#if USE_FDCAN
    can_header.DataLength = (uint8_t) (payload_len + 1);

    if (!FDCAN_QueueTransmitMessage(can, &can_header, processed_payload)) {
      return false;
    }
#else
    can_header.DLC = (uint8_t) (payload_len + 1);

    if (!bxCAN_Transmit(can, &can_header, processed_payload, CANBUS_TIMEOUT_MS)) {
      return false;
    }
#endif
  } else {

    // Round up (payload is actually payload_len+2 and we add (UAVCAN_CAN_FRAME_MAX_DATA_LEN - 2) to round up
    int frame_count = (int) ((payload_len + UAVCAN_CAN_FRAME_MAX_DATA_LEN)/(UAVCAN_CAN_FRAME_MAX_DATA_LEN - 1U));

    int payload_offset = 0;

    uint8_t toggle = 0x00;

    // Send all the blocks
    for (int i = 0; i < frame_count; ++i) {
      memset(processed_payload, 0, sizeof(processed_payload));

      int bytes_in_block = MIN(payload_len - payload_offset, UAVCAN_CAN_FRAME_MAX_DATA_LEN - 1);

      if (i==0) {
        uint16_t crc = CRC_16_Compute(CRC_16_Add_Signature(0xFFFFU, data_type_signature),
                                      payload,
                                      payload_len);

        // First block so add the CRC... this should be last tho
        processed_payload[0] = (uint8_t) (crc);
        processed_payload[1] = (uint8_t) (crc >> 8U);
      }

      for (int j = (i > 0) ? 0 : 2; j < bytes_in_block; ++j) {
        processed_payload[j] = (uint8_t) payload[payload_offset++];
      }

      // Generate the last byte.
      uint8_t last_byte = (uint8_t) toggle | (uint8_t) (transfer_id & 0x1FU);

      // Toggle the bit
      toggle = toggle ^ (uint8_t) 0x20U;

      // Set the start/end of transfer bits
      if (i==0) {
        last_byte |= 0x80U;
      } else if (i==(frame_count - 1)) {
        last_byte |= 0x40U;
      }

      processed_payload[bytes_in_block] = last_byte;

#if USE_FDCAN
      can_header.DataLength = (uint8_t) (bytes_in_block + 1);

      if (!FDCAN_QueueTransmitMessage(can, &can_header, processed_payload)) {
        return false;
      }
#else
      can_header.DLC = (uint8_t) (bytes_in_block + 1);

      if (!bxCAN_Transmit(can, &can_header, processed_payload, CANBUS_TIMEOUT_MS)) {
        return false;
      }
#endif
    }
  }

  return true;
}

bool UAVCAN_Send_Anonymous_Frame(uint16_t data_type_id,
                                 uint8_t priority,
                                 uint8_t transfer_id,
                                 const uint8_t *payload,
                                 uint16_t payload_len) {

  // anonymous transfer, random discriminator
  const uint16_t discriminator = (uint16_t) ((CRC_16_Compute(0xFFFFU, payload, payload_len)) & 0x7FFEU);

  const uint32_t can_id = ((uint32_t) priority << 24U) |
      ((uint32_t) discriminator << 9U) |
      ((uint32_t) (data_type_id & 0x03U) << 8U);

  return UAVCAN_TX_Frames(CAN_device,
                          can_id,
                          0,
                          transfer_id,
                          payload,
                          payload_len);
}

/**
 * Broadcast frame
 *
 * @param source_node_id
 * @param data_type_signature
 * @param data_type_id
 * @param transfer_id
 * @param priority
 * @param payload
 * @param payload_len
 * @return
 */
bool UAVCAN_Send_Broadcast_Frame(uint8_t source_node_id,
                                 uint64_t data_type_signature,
                                 uint16_t data_type_id,
                                 uint8_t transfer_id,
                                 uint8_t priority,
                                 const uint8_t *payload,
                                 uint16_t payload_len) {

  uint32_t can_id = ((uint32_t) priority << 24U) |
      ((uint32_t) data_type_id << 8U) |
      (uint32_t) (source_node_id);

  return UAVCAN_TX_Frames(CAN_device,
                          can_id,
                          data_type_signature,
                          transfer_id,
                          payload,
                          payload_len);
}

bool UAVCAN_Send_Service_Frame(bool request,
                               uint8_t source_node_id,
                               uint8_t destination_node_id,
                               uint64_t data_type_signature,
                               uint16_t data_type_id,
                               uint8_t transfer_id,
                               uint8_t priority,
                               const void *payload,
                               uint16_t payload_len) {

  const uint32_t can_id = ((uint16_t) (request ? 0x8080 : 0x0080)) |
      ((uint32_t) priority << 24U) |
      ((uint32_t) data_type_id << 16U) |
      ((uint32_t) destination_node_id << 8U) |
      (uint32_t) source_node_id;

  return UAVCAN_TX_Frames(CAN_device,
                          can_id,
                          data_type_signature,
                          transfer_id,
                          payload,
                          payload_len);
}

/**
 * CAN callback processor
 *
 * @param node
 */
void UAVCAN_Process_Frame(uint8_t node_id_local,
                          uint32_t can_id,
                          uint8_t *payload,
                          const size_t payload_len) {

  const bool is_service = is_service_from_can_id(can_id);
  const uint8_t destination_node_id = is_service ? destination_id_from_can_id(can_id) : (uint8_t) 0;
  const uint8_t source_node_id = source_id_from_can_id(can_id);

  // If it's not a broadcast but is not addressed to this node or it's from this node so exit handler.
  if (((destination_node_id!=0)
      && (destination_node_id!=node_id_local))
      || (source_node_id==node_id_local)) {
    return;
  }

  const uint8_t priority = priority_from_can_id(can_id);
  const uint16_t data_type_id = is_service_from_can_id(can_id) ?
                                service_type_from_can_id(can_id) :
                                msg_type_from_can_id(can_id);

  const uint8_t tail_byte = payload[payload_len - 1];
  uint8_t transfer_id = transfer_id_from_tail_byte(tail_byte);

  const bool first_frame_flag = is_start_of_transfer(tail_byte);
  const bool last_frame_flag = is_end_of_transfer(tail_byte);

  int buffer_ix = REASSEMBLY_BUFFERS;
  int payload_start = 0;

  if (first_frame_flag && last_frame_flag) {

    // Single frame transfer dispatch frame directly
    UAVCAN_Handle_Message(data_type_id,
                          is_service,
                          source_node_id,
                          priority,
                          transfer_id,
                          payload,
                          payload_len - 1);
    return;
  } else if (first_frame_flag) {

    // First frame of a multi-frame transfer

    // Find the data type signature
    uint64_t signature = 0;
    uint8_t signature_count = sizeof(method_signatures)/sizeof(method_sig_t);
    bool signature_found = false;

    for (int i = 0; i < signature_count; ++i) {
      if (method_signatures[i].id==data_type_id) {
        signature = method_signatures[i].signature;
        signature_found = true;
        break;
      }
    }

    // If it's unknown the packet is rejected
    if (!signature_found) {
      return;
    }

    // Find an empty reassembly buffer
    for (int i = 0; i < REASSEMBLY_BUFFERS; ++i) {
      if (!reassembly_buffer[i].in_use || (reassembly_buffer[i].timestamp < System_Tick)) {
        buffer_ix = i;
        break;
      }
    }

    // No empty buffer so drop packet
    if (buffer_ix >= REASSEMBLY_BUFFERS) {
      return;
    }

    // Initialize reassembly buffer
    reassembly_buffer[buffer_ix].in_use = true;
    reassembly_buffer[buffer_ix].timestamp = System_Tick + REASSEMBLY_TIMEOUT_MS;
    reassembly_buffer[buffer_ix].transfer_id = transfer_id;
    reassembly_buffer[buffer_ix].data_type_id = data_type_id;
    reassembly_buffer[buffer_ix].source_node_id = source_node_id;
    reassembly_buffer[buffer_ix].crc_computed = CRC_16_Add_Signature(0xFFFF, signature);
    reassembly_buffer[buffer_ix].crc_payload = ((uint16_t) payload[0]) | (uint16_t) payload[1] << 8U;
    reassembly_buffer[buffer_ix].payload_len = 0;

    payload_start = 2;

  } else {
    // Find the reassembly buffer containing the rest of the packet
    for (int i = 0; i < REASSEMBLY_BUFFERS; ++i) {
      if ((reassembly_buffer[i].data_type_id==data_type_id)
          && (reassembly_buffer[i].source_node_id==source_node_id)
          && (reassembly_buffer[i].transfer_id==transfer_id)) {
        buffer_ix = i;
        break;
      }
    }

    // Packet is not in the reassembly buffer.
    if (buffer_ix >= REASSEMBLY_BUFFERS) {
      return;
    }
  }

  // Check for overflows
  if (reassembly_buffer[buffer_ix].payload_len + (payload_len - payload_start - 1) > UAVCAN_MAX_PAYLOAD_SIZE) {
    return;
  }

  // Copy data
  for (int i = payload_start; i < payload_len - 1; ++i) {
    reassembly_buffer[buffer_ix].payload[reassembly_buffer[buffer_ix].payload_len++] = payload[i];
  }

  // Last frame of transfer checks CRC and dispatches packet
  if (last_frame_flag) {

    // Then the rest of the packet
    for (int i = 0; i < reassembly_buffer[buffer_ix].payload_len; ++i) {
      reassembly_buffer[buffer_ix].crc_computed = CRC_16_Add_Byte(reassembly_buffer[buffer_ix].crc_computed,
                                                                  reassembly_buffer[buffer_ix].payload[i]);
    }

    // Dispatch message only if the CRC
    if (reassembly_buffer[buffer_ix].crc_computed==reassembly_buffer[buffer_ix].crc_payload) {
      UAVCAN_Handle_Message(data_type_id,
                            is_service,
                            source_node_id,
                            priority,
                            transfer_id,
                            reassembly_buffer[buffer_ix].payload,
                            reassembly_buffer[buffer_ix].payload_len);
    } else {
      // TODO: Increment message disassembly error count
    }

    // Clear the packet to avoid confusion
    reassembly_buffer[buffer_ix].in_use = false;
  }
}


/**
 * Message-Specific Encoding / Decoding and Transmission Routines
 */

/**
 *
 * The commands are 32bit long, 8-bit id, 8-bit type and 16-bit value
 *
 * uavcan.equipment.actuator.ArrayCommand
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#arraycommand
 *
 *
 * @param buffer
 * @param buffer_len
 * @param actuator_id
 * @param command_type
 * @param command_value
 * @return
 */

bool Deserialize_ActuatorArrayCommand_Message(const uint8_t *buffer,
                                              const uint32_t buffer_len,
                                              const uint8_t actuator_id,
                                              uint8_t *command_type,
                                              float *command_value) {

  int offset = 0;

  for (int i = 0; i < buffer_len/4; ++i) {

    if (buffer[offset]==actuator_id) {
      *command_type = buffer[offset + 1];
      *command_value = buffer_to_float16(&buffer[offset + 2]);
      return true;
    }

    offset += 4;
  }
  return false;
}

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#status
 *
 * uavcan.equipment.actuator.Status
 *
 *
 */
bool Send_ActuatorStatus_Message(uint8_t node_id,
                                 uint8_t actuator_id,
                                 float position,
                                 float force,
                                 float speed,
                                 uint8_t power_rating_pct) {

  uint8_t buffer[UAVCAN_EQUIPMENT_ACTUATOR_STATUS_MAX_SIZE];

  buffer[0] = actuator_id;
  float16_to_buffer(position, &buffer[1]);
  float16_to_buffer(force, &buffer[3]);
  float16_to_buffer(speed, &buffer[5]);
  buffer[7] = (uint8_t) (0x7F & power_rating_pct);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_EQUIPMENT_ACTUATOR_STATUS_SIGNATURE,
                                     UAVCAN_EQUIPMENT_ACTUATOR_STATUS_ID,
                                     0,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     buffer,
                                     UAVCAN_EQUIPMENT_ACTUATOR_STATUS_MAX_SIZE);

}

/**
 * uavcan.equipment.air_data.StaticPressure
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#staticpressure
 *
 * @param source_node_id
 * @param pressure
 * @param variance
 * @return
 */
bool Send_StaticPressure_Message(uint8_t node_id,
                                 float pressure,
                                 float variance) {

  uint8_t buffer[UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_MAX_SIZE];

  float32_to_buffer(pressure, &buffer[0]);
  float16_to_buffer(variance, &buffer[4]);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_SIGNATURE,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_ID,
                                     0,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     buffer,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_MAX_SIZE);

}

/**
 * uavcan.equipment.air_data.StaticTemperature
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#statictemperature
 *
 * @param source_node_id
 * @param pressure
 * @param variance
 * @return
 */
bool Send_StaticTemperature_Message(uint8_t node_id,
                                    float temperature,
                                    float variance) {

  uint8_t buffer[UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_MAX_SIZE];

  float16_to_buffer(temperature, &buffer[0]);
  float16_to_buffer(variance, &buffer[2]);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_SIGNATURE,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_ID,
                                     0,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     buffer,
                                     UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_MAX_SIZE);

}

/**
 * uavcan.equipment.device.Temperature
 *
 * @param source_node_id
 * @param pressure
 * @param variance
 * @return
 */
bool Send_DeviceTemperature_Message(uint8_t node_id,
                                    device_temperature_t *device_temperature) {

  uint8_t buffer[UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  buffer[0] = (uint8_t) (device_temperature->device_id & 0xFFU);
  buffer[1] = (uint8_t) (device_temperature->device_id >> 8U & 0xFFU);

  float16_to_buffer(device_temperature->temperature_K, &buffer[2]);

  buffer[4] = device_temperature->error_flags;

  bool result = UAVCAN_Send_Broadcast_Frame(node_id,
                                            UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_SIGNATURE,
                                            UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_ID,
                                            device_temperature->transfer_id,
                                            UAVCAN_TRANSFER_PRIORITY_HIGH,
                                            buffer,
                                            UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_MAX_SIZE);

  // Increment transfer ID
  device_temperature->transfer_id = (device_temperature->transfer_id + 0x1U) & 0xFU;
  device_temperature->timestamp = System_Tick;

  return result;
}

void DeviceTemperature_Set(float temperature_C,
                           float temp_limit_min_C,
                           float temp_limit_max_C,
                           device_temperature_t *device_temperature) {

  device_temperature->temperature_K = temperature_C + C_TO_K_OFFSET;
  device_temperature->error_flags = 0;

  if (temperature_C > temp_limit_max_C) {
    device_temperature->error_flags = UAVCAN_DEVICE_TEMPERATURE_ERROR_FLAG_OVERHEATING;
  } else if (temperature_C < temp_limit_min_C) {
    device_temperature->error_flags = UAVCAN_DEVICE_TEMPERATURE_ERROR_FLAG_OVERCOOLING;
  }
}

/**
 * uavcan.equipment.esc
 */

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#rawcommand
 *
 *
 * @param buffer
 * @param buffer_len (must be at least length 7)
 * @param esc_vals (at least length 4)
 * @return Number of ESC values decoded
 */

int Deserialize_ESC_RawCommand_Message(const uint8_t *buffer,
                                       const uint32_t buffer_len,
                                       int16_t *esc_vals) {

/*
  A(1) A(2)     A(3) A(4) A(5) A(6)     A(7) A(8) A(9) A(10)     A(11) A(12) A(13) A(14)
  B(1) B(2)     B(3) B(4) B(5) B(6)     B(7) B(8) B(9) B(10)     B(11) B(12) B(13) B(14)
  C(1) C(2)     C(3) C(4) C(5) C(6)     C(7) C(8) C(9) C(10)     C(11) C(12) C(13) C(14)
  D(1) D(2)     D(3) D(4) D(5) D(6)     D(7) D(8) D(9) D(10)     D(11) D(12) D(13) D(14)

  0> A(7) A(8) A(9) A(10)      A(11) A(12) A(13) A(14)

  1> A(1) A(2) A(3) A(4)       A(5)  A(6)  B(7)  B(8)

  2> B(9) B(10) B(11) B(12)    B(13) B(14) B(1)  B(2)

  3> B(3) B(4)  B(5)  B(6)     C(7)  C(8)  C(9)  C(10)

  4> C(11) C(12) C(13) C(14)   C(1) C(2)  C(3)   C(4)

  5> C(5)  C(6)  D(7)  D(8)    D(9) D(10) D(11)  D(12)

  6> D(13) D(14) D(1)  D(2)    D(3) D(4)  D(5)   D(6)
*/

  // The data is a bunch of tightly packed 14 bit integers
  int count = (buffer_len*8)/14;

  for (int i = 0; i < count; ++i) {
    esc_vals[i] = 0;
  }

  // Ok
  esc_vals[0] = (0x7C & (uint16_t) buffer[1]) << 6
      | buffer[0];

  // Ok
  esc_vals[1] = (uint16_t) (0xF0 & buffer[3]) << 4
      | ((uint16_t) buffer[2]) >> 2
      | ((uint16_t) (0x03 & buffer[1])) << 6
      | ((uint16_t) (0x03 & buffer[2])) << 12;

  // Ok
  esc_vals[2] = ((uint16_t) (0xC0 & buffer[5]) << 2)
      | ((uint16_t) (0x0F & buffer[3]) << 4)
      | ((uint16_t) (0x0F & buffer[4]) << 10)
      | ((uint16_t) (0xF0 & buffer[4]) >> 4);

  // Ok
  esc_vals[3] = ((uint16_t) (0xC0 & buffer[6])) >> 6
      | ((uint16_t) (0x3F & buffer[5])) << 2
      | ((uint16_t) (0x3F & buffer[6])) << 8;

//  // Convert from two's complement
//  for(int i = 0; i < count; ++i){
//    if((esc_vals[i] & 0x2000)){
//      esc_vals[i] = -(int16_t) ((~(esc_vals[i] | 0xC000)) + 1);
//    }
//  }

#if 0
  int j = 0;

  // The
  for (int i = 0; i < buffer_len; ++i) {

    // From 0-6
    const int i_mod = i % 7;
    const uint16_t value = (uint16_t) buffer[i];

    switch (i_mod) {

      case 0:
        esc_vals[0] |= buffer[i];
        break;

      case 1:
        esc_vals[0] |= (0x7C & value) << 6;
        esc_vals[1] |= (0x03 & value) << 6;
        break;

      case 2:
        esc_vals[1] |= (0xFC & value) >> 2;
        esc_vals[1] |= (0x03 & value) << 12;
        break;

      case 3:
        esc_vals[1] |= (0xF0 & value) << 4;
        esc_vals[2] |= (0x0F & value) << 4;
        break;

      case 4:
        esc_vals[2] |= (0x0F & value) << 10;
        esc_vals[2] |= (0xF0 & value) >> 4;
        break;

      case 5:
        esc_vals[2] |= (0xC0 & value) << 2;
        esc_vals[3] |= (0x3F & value) << 2;
        break;

      case 6:
        esc_vals[3] |= (0xC0 & value) >> 6;
        esc_vals[3] |= (0x3F & value) << 8;
        break;

      default:
        break;
    }
  }
#endif

  return count;
}

int Deserialize_ESC_RpmCommand_Message(const uint8_t *buffer,
                                       const uint32_t buffer_len,
                                       int32_t *esc_vals) {

//  // Each element is 18 bits long (20 max)
//  byte[0] << 10, byte[1] << 2, (byte[2] >> 6)
//  byte[2] << 12, byte[3] << 4, (byte[4] >> 4)
//  byte[4] << 14, byte[5] << 6, (byte[6] >> 2)
//  byte[6] << 16, byte[7] << 8, (byte[8] >> 0)

//  // Each element is 18 bits long (20 max)
//  byte[0] << 10, byte[1] << 2, (byte[2] >> 6)
//  byte[2] << 12, byte[3] << 4, (byte[4] >> 4)
//  byte[4] << 14, byte[5] << 6, (byte[6] >> 2)
//  byte[6] << 16, byte[7] << 8, (byte[8] >> 0)

  int count = 0;

  int32_t left = 0;
  int32_t middle = 0;
  int32_t right = 0;

  size_t left_shift = 10;
  size_t middle_shift = 2;

  // Do first element here
  left = buffer[0] << left_shift;

  for (int i = 1; i < buffer_len; ++i) {

    if (i%2==0) {

      // Build third piece
      right = buffer[i] >> (8 - middle_shift);

      // Piece together the value (TODO: bitmask)
      esc_vals[count++] = left | middle | right;

      left_shift = (left_shift < 16) ? left_shift + 2 : 10;

      // New left piece
      left = buffer[i] << left_shift;

      middle_shift = (middle_shift < 8) ? middle_shift + 2 : 2;

    } else {
      middle = buffer[i] << middle_shift;
    }
  }

#if 0
  // Convert from two's complement
  for(int i = 0; i < 20; ++i){
    if((esc_vals[i] & 0x2000)){
      esc_vals[i] = -(int16_t) ((~(esc_vals[i] | 0xC000)) + 1);
    }
  }
#endif

  return count;
}

bool Deserialize_LightsCommand_Message(const uint8_t *buffer,
                                       const uint32_t buffer_len,
                                       const uint8_t light_id,
                                       uint16_t *rgb565) {
  uint16_t single;
  bool res = Deserialize_LightsCommand_Message_Single(buffer, buffer_len, light_id, &single);

  rgb565[0] = (single >> 11) & 0x1FU;
  rgb565[1] = (single >> 5) & 0x3FU;
  rgb565[2] = single& 0x1FU;
  return res;
}

bool Deserialize_LightsCommand_Message_Single(const uint8_t *buffer,
                                              const uint32_t buffer_len,
                                              const uint8_t light_id,
                                              uint16_t *single) {

  int offset = 0;

  for (int i = 0; i < buffer_len/3; ++i) {

    if (buffer[offset]==light_id) {
      uint16_t tmp = (buffer[offset + 1] << 8U) | buffer[offset + 2];
      *single = tmp;
      return true;
    }

    offset += 3;
  }
  return false;
}

/**
 * uavcan.equipment.hardpoint.Command
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#command-1
 *
 */
void Deserialize_Hardpoint_Command_Message(const uint8_t *buffer,
                                           uint8_t *hardpoint_id,
                                           uint16_t *command) {

  *hardpoint_id = buffer[0];
  *command = (buffer[1] | (buffer[2] << 8U));
}

/**
 * uavcan.equipment.hardpoint.Status
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#status-3
 *
 * uint8 hardpoint_id
 * float16 payload_weight
 * float16 payload_weight_variance
 * uint16 status
 *
 */
bool Send_Hardpoint_Status_Message(uint8_t node_id,
                                   uint8_t transfer_id,
                                   uint8_t hardpoint_id,
                                   float payload_weight,
                                   float payload_weight_variance,
                                   uint16_t status) {

  uint8_t buffer[UAVCAN_EQUIPMENT_HARDPOINT_STATUS_MAX_SIZE];
  memset(buffer, 0, UAVCAN_EQUIPMENT_HARDPOINT_STATUS_MAX_SIZE);

  buffer[0] = hardpoint_id;
  float16_to_buffer(payload_weight, &buffer[1]);
  float16_to_buffer(payload_weight_variance, &buffer[3]);

  buffer[5] = (uint8_t) (status & 0xFFU);
  buffer[6] = (uint8_t) (status >> 8U & 0xFFU);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_EQUIPMENT_HARDPOINT_STATUS_SIGNATURE,
                                     UAVCAN_EQUIPMENT_HARDPOINT_STATUS_ID,
                                     transfer_id,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     buffer,
                                     7);
}


// uavcan.equipment.power

/**
 * uavcan.equipment.power.PrimaryPowerSupplyStatus
 *
 * float16 hours_to_empty_at_10sec_avg_power
 * float16 hours_to_empty_at_10sec_avg_power_variance
 * bool external_power_available
 * uint7 remaining_energy_pct
 * uint7 remaining_energy_pct_stdev
 *
 *
 */
bool Send_PrimaryPowerSupplyStatus_Message(uint8_t node_id,
                                           uint8_t transfer_id,
                                           psu_info_t *psu_status) {

  uint8_t buffer[UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_MAX_SIZE];
  memset(buffer, 0, UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_MAX_SIZE);

  float16_to_buffer(psu_status->hours_to_empty_at_10sec_avg_power, &buffer[0]);
  float16_to_buffer(psu_status->hours_to_empty_at_10sec_avg_power_variance, &buffer[2]);

  buffer[4] = (uint8_t) (psu_status->external_power_available << 8U)
      | (uint8_t) (psu_status->remaining_energy_pct & 0x7FU);
  buffer[5] = (uint8_t) (psu_status->remaining_energy_pct_stdev << 1U & 0x7FU);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_SIGNATURE,
                                     UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_ID,
                                     transfer_id,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     buffer,
                                     6);

}

/**
 * uavcan.equipment.power.CircuitStatus
 *
 *
 */
bool Send_CircuitStatus_Message(uint8_t node_id,
                                circuit_status_t *circuit_status) {

  uint8_t buffer[UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_MAX_SIZE];
  memset(buffer, 0, UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_MAX_SIZE);

  buffer[0] = (uint8_t) (circuit_status->circuit_id & 0xFFU);
  buffer[1] = (uint8_t) (circuit_status->circuit_id >> 8U & 0xFFU);
  float16_to_buffer(circuit_status->voltage, &buffer[2]);
  float16_to_buffer(circuit_status->current, &buffer[4]);
  buffer[6] = circuit_status->error_flags;

  bool result = UAVCAN_Send_Broadcast_Frame(node_id,
                                            UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_SIGNATURE,
                                            UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_ID,
                                            circuit_status->transfer_id,
                                            UAVCAN_TRANSFER_PRIORITY_LOW,
                                            buffer,
                                            7);

  // Increment transfer ID
  circuit_status->transfer_id = (circuit_status->transfer_id + 1) & 0xF;
  circuit_status->timestamp = System_Tick;
  return result;
}

/**
 * uavcan.equipment.power.BatteryInfo (Tested)
 *
 * float16 temperature             # [Kelvin]
 * float16 voltage                 # [Volt]
 * float16 current                 # [Ampere]
 * float16 average_power_10sec     # [Watt]        Average power consumption over the last 10 seconds
 * float16 remaining_capacity_wh   # [Watt hours]  Will be increasing during charging
 * float16 full_charge_capacity_wh # [Watt hours]  Predicted battery capacity when it is fully charged. Falls with aging
 * float16 hours_to_full_charge    # [Hours]       Charging is expected to complete in this time; zero if not charging
 * uint11 status_flags
 * uint7 state_of_health_pct
 * uint7 state_of_charge_pct               # Percent of the full charge [0, 100]. This field is required
 * uint7 state_of_charge_pct_stdev         # SOC error standard deviation; use best guess if unknown
 * uint8 battery_id                        # Identifies the battery within this vehicle, e.g. 0 - primary battery
 * uint32 model_instance_id                # Set to zero if not applicable
 * uint8[<32] model_name                   # Battery model name
 *
 */
bool Send_BatteryInfo_Message(uint8_t node_id,
                              battery_info_t *info) {

  uint8_t buffer[UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_MAX_SIZE];
  memset(buffer, 0, UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_MAX_SIZE);

  float16_to_buffer(info->temperature, &buffer[0]);
  float16_to_buffer(info->voltage_V, &buffer[2]);
  float16_to_buffer(info->current_A, &buffer[4]);
  float16_to_buffer(info->average_power_10sec, &buffer[6]);
  float16_to_buffer(info->remaining_capacity_Wh, &buffer[8]);
  float16_to_buffer(info->full_charge_capacity_Wh, &buffer[10]);
  float16_to_buffer(info->hours_to_full_charge, &buffer[12]);

  buffer[14] = (uint8_t) (info->status_flags & 0xFFU);
  buffer[15] = (uint8_t) (info->status_flags >> 3U & 0xE0U);

  buffer[15] |= (uint8_t) (info->state_of_health_pct >> 2U & 0x1FU);
  buffer[16] = (uint8_t) (info->state_of_health_pct << 5U & 0xC0U);

  buffer[16] |= (uint8_t) (info->state_of_charge_pct >> 1U & 0x3FU);
  buffer[17] = (uint8_t) (info->state_of_charge_pct << 7U);

  buffer[17] |= (uint8_t) (info->state_of_charge_pct_stdev & 0x7FU);
  buffer[18] = info->battery_id;

  buffer[19] = (uint8_t) (info->model_instance_id & 0xFFU);
  buffer[20] = (uint8_t) (info->model_instance_id >> (8U & 0xFFU));
  buffer[21] = (uint8_t) (info->model_instance_id >> 16U & 0xFFU);
  buffer[22] = (uint8_t) (info->model_instance_id >> 24U & 0xFFU);

  const size_t model_name_len = MIN(strlen(info->model_name), 32);

  memcpy(&buffer[23], info->model_name, (size_t) model_name_len);

  const uint16_t total_size = (uint16_t) (23 + model_name_len);

  bool result = UAVCAN_Send_Broadcast_Frame(node_id,
                                            UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_SIGNATURE,
                                            UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_ID,
                                            info->transfer_id,
                                            UAVCAN_TRANSFER_PRIORITY_LOW,
                                            buffer,
                                            total_size);

  // Increment transfer ID
  info->transfer_id = (info->transfer_id + 1) & 0xF;
  info->timestamp = System_Tick;

  return result;
}

/**
 * Serialize the data for a node status message
 *
 *   uavcan.protocol.NodeStatus
 *
 * @param uptime_sec
 * @param node_mode
 * @param node_health
 * @param buffer
 */
void Serialize_NodeStatus_Message(const uint32_t uptime_sec,
                                  const uint8_t node_health,
                                  const uint8_t node_mode,
                                  const uint16_t vendor_specific_status_code,
                                  uint8_t *buffer) {

  // uptime [32]
  uintN_to_buffer(4, uptime_sec, buffer);

  // health [2], mode [3], sub-mode [3] (unused)
  buffer[4] = (uint8_t) (node_health << 6) | (uint8_t) (node_mode << 3);

  // mode [3]
  uintN_to_buffer(2, vendor_specific_status_code, &buffer[5]);
}

/**
 * Send out a node-status message
 */
bool Send_NodeStatus_Message(node_info_t *node) {

  uint8_t buffer[UAVCAN_PROTOCOL_NODE_STATUS_MAX_SIZE];

  Serialize_NodeStatus_Message(node->uptime_sec,
                               node->health,
                               node->mode,
                               node->vendor_status_code,
                               buffer);

  bool result = UAVCAN_Send_Broadcast_Frame(node->id,
                                            UAVCAN_PROTOCOL_NODE_STATUS_SIGNATURE,
                                            UAVCAN_PROTOCOL_NODE_STATUS_ID,
                                            node->transfer_id,
                                            UAVCAN_TRANSFER_PRIORITY_LOW,
                                            buffer,
                                            UAVCAN_PROTOCOL_NODE_STATUS_MAX_SIZE);

  // Increment transfer ID
  node->transfer_id = (node->transfer_id + 1) & 0xF;
  node->timestamp = System_Tick;
  return result;
}


/**
 *
 *
 *
 */
bool Send_GetNodeInfo_Response(node_info_t *node,
                               request_t request) {

  uint8_t buffer[UAVCAN_PROTOCOL_GET_NODE_INFO_MAX_SIZE];
  memset(buffer, 0, UAVCAN_PROTOCOL_GET_NODE_INFO_MAX_SIZE);

  // GetNodeInfo starts with a node status packet
  Serialize_NodeStatus_Message(node->uptime_sec,
                               node->health,
                               node->mode,
                               node->vendor_status_code,
                               buffer);

  // SoftwareVersion structure
  buffer[7] = node->sw_version_major;
  buffer[8] = node->sw_version_minor;

  // Optional field flags indicating we have VCS hash and CRC image
  buffer[9] = 0x03;

  // VCS Hash
  uintN_to_buffer(4, node->vcs_commit, &buffer[10]);

  // Image CRC
  uintN_to_buffer(8, node->image_crc, &buffer[14]);

  // HardwareVersion
  buffer[22] = node->hw_version_major;
  buffer[23] = node->hw_version_minor;

  // Unique Hardware ID
  for (int i = 0; i < 16; ++i) {
    buffer[24 + i] = node->uuid[i];
  }

#if 0
  // Hardware Certificate
  for(int i = 0; i < 255; ++i){
    buffer[40+i] = node->hw_cert[i];
  }
#endif

  // Name
  const size_t name_len = strlen(node->app_name);
  memcpy((char *) &buffer[41], node->app_name, name_len + 1U);

  const uint16_t total_size = (uint16_t) (41 + name_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node->id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_GET_NODE_INFO_SIGNATURE,
                                   UAVCAN_PROTOCOL_GET_NODE_INFO_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   total_size);
}

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#gettransportstats
 *
 * uavcan.protocol.GetTransportStats
 *
 * @param node
 * @param ok
 * @param priority
 * @param destination_node_id
 * @param transfer_id
 */
bool Send_GetTransportStats_Response(node_info_t *node,
                                     request_t request) {

  uint8_t buffer[UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_MAX_SIZE];
  memset(buffer, 0, UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_MAX_SIZE);

  uint64_t total_frames_tx = 0;
  uint64_t total_frames_rx = 0;
  uint64_t total_errors = 0;

  uint16_t offset = 18;

  for (int i = 0; i < CAN_IFACE_COUNT; ++i) {
    uintN_to_buffer(6, interface_stats[i].frames_tx, &buffer[offset]);
    uintN_to_buffer(6, interface_stats[i].frames_rx, &buffer[offset + 6]);
    uintN_to_buffer(6, interface_stats[i].errors, &buffer[offset + 12]);

    total_frames_tx += interface_stats[i].frames_tx;
    total_frames_rx += interface_stats[i].frames_rx;
    total_errors += interface_stats[i].errors;
    offset += 18;
  }

  uintN_to_buffer(6, total_frames_tx, &buffer[0]);
  uintN_to_buffer(6, total_frames_rx, &buffer[6]);
  uintN_to_buffer(6, total_errors, &buffer[12]);

  return UAVCAN_Send_Service_Frame(false,
                                   node->id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_SIGNATURE,
                                   UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_ID,
                                   request.transfer_id,
                                   request.priority,
                                   &buffer,
                                   offset);
}


/**
 * uavcan.protocol.RestartNode
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#restartnode
 *
 * Verify the magic number 0xACCE551B1E
 *
 * @param buffer
 * @param buffer_len
 * @return
 */
bool Check_NodeRestart_Request(const uint8_t *buffer,
                               uint32_t buffer_len) {

  uint64_t magic = UAVCAN_PROTOCOL_RESTART_NODE_MAGIC;

  if (buffer_len < 5) {
    return false;
  }

  for (int i = 0; i < 5; ++i) {
    if (buffer[i]!=(magic & 0xFFU)) {
      return false;
    }

    magic >>= 8U;
  }

  return true;
}

bool Send_NodeRestart_Response(uint8_t node_id,
                               bool ok,
                               request_t request) {

  uint8_t buffer = (uint8_t) ok;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_RESTART_NODE_SIGNATURE,
                                   UAVCAN_PROTOCOL_RESTART_NODE_ID,
                                   request.transfer_id,
                                   request.priority,
                                   &buffer,
                                   1);
}

/**
 * uavcan.protocol.debug.KeyValue
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#keyvalue
 *
 * @param node_id
 * @param key
 * @param text
 */
bool Send_DebugKeyValue_Message(uint8_t node_id,
                                const char *key,
                                const float value) {

  const size_t key_len = MIN(strlen(key), 58);
  const size_t buffer_len = 4 + key_len;

  uint8_t buffer[buffer_len];
  memset(buffer, 0, sizeof(buffer));

  float32_to_buffer(value, &buffer[0]);

  memcpy(&buffer[4], key, key_len);

  UAVCAN_Send_Broadcast_Frame(node_id,
                              UAVCAN_PROTOCOL_DEBUG_KEY_VALUE_SIGNATURE,
                              UAVCAN_PROTOCOL_DEBUG_KEY_VALUE_ID,
                              0,
                              UAVCAN_TRANSFER_PRIORITY_LOWEST,
                              buffer,
                              buffer_len);
  return false;
}

/**
 * uavcan.protocol.debug.LogMessage
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#logmessage
 *
 * @param node_id
 * @param level
 * @param source
 * @param text
 * @return
 */
bool Send_DebugLog_Message(uint8_t node_id,
                           const LogLevel level,
                           const char *source,
                           const char *text) {

  uint8_t transfer_id = 0;

  const size_t source_len = MIN(strlen(source), 31);
  const size_t text_len = MIN(strlen(text), 90);

  const size_t buffer_len = source_len + text_len + 1;

  uint8_t buffer[buffer_len];
  memset(buffer, 0, sizeof(buffer));

  buffer[0] = level << 5;

  // Encoded source length (https://uavcan.org/Specification/3._Data_structure_description_language/)
  buffer[0] |= (uint8_t) (0x1FU & source_len);

  memcpy(&buffer[1], source, source_len);
  memcpy(&buffer[1 + source_len], text, text_len);

  return UAVCAN_Send_Broadcast_Frame(node_id,
                                     UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_SIGNATURE,
                                     UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_ID,
                                     transfer_id,
                                     UAVCAN_TRANSFER_PRIORITY_LOWEST,
                                     buffer,
                                     buffer_len);

}


// uavcan.protocol.dynamic_node_id.Allocation
bool Send_Node_ID_Allocation_Request(const uint8_t *uuid,
                                     uint8_t uuid_len,
                                     uint8_t transfer_id,
                                     bool first) {

  uint8_t buffer[7];

  buffer[0] = (uint8_t) (first);

  for (int i = 0; i < uuid_len; ++i) {
    buffer[1 + i] = uuid[i];
  }

  // Send message
  return UAVCAN_Send_Anonymous_Frame(UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_ID,
                                     UAVCAN_TRANSFER_PRIORITY_LOW,
                                     transfer_id,
                                     buffer,
                                     (uint16_t) (uuid_len + 1));
}

/**
 * Deserialize the BeginFirmwareUpdate request payload
 *
 * uavcan.protocol.file.BeginFirmwareUpdate
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#beginfirmwareupdate
 *
 * @param buffer
 * @param buffer_len
 * @param node_id
 * @param path
 */

void Deserialize_BeginFirmwareUpdate_Request(const uint8_t *buffer,
                                             uint32_t buffer_len,
                                             uint8_t *node_id,
                                             size_t *path_len,
                                             char *path) {
  // Node_id
  (*node_id) = buffer[0];

  // Copy the rest of the path
  *path_len = buffer_len - 1;
  memcpy(path, &buffer[1], *path_len);
}

/**
 * uavcan.protocol.file.BeginFirmwareUpdate
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#beginfirmwareupdate
 *
 * @param node
 * @param destination_node_id
 * @param transfer_id
 * @param priority
 * @param error_code
 * @param error_message
 */

bool Send_BeginFirmwareUpdate_Response(uint8_t node_id,
                                       request_t request,
                                       uint8_t error_code,
                                       const char *error_message) {

  uint8_t buffer[UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  size_t error_len = MIN(127, strlen(error_message));

  buffer[0] = error_code;

  memcpy(&buffer[1], error_message, error_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_SIGNATURE,
                                   UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID,
                                   request.transfer_id,
                                   UAVCAN_TRANSFER_PRIORITY_HIGHEST,
                                   buffer,
                                   (uint16_t) (error_len + 1));

}

/**
 * Send a file read request
 *
 *  uavcan.protocol.file.Read
 *
 *  https://uavcan.org/Specification/7._List_of_standard_data_types/#read
 *
 * Packet format:
 *
 * - uint40 offset
 * - uint8 path[<=200]
 *
 * @param node_id  this node id
 * @param inout_transfer_id
 * @param priority
 * @param offset
 * @param path
 */
bool Send_FileRead_Request(uint8_t node_id,
                           uint8_t server_node_id,
                           uint8_t transfer_id,
                           uint64_t offset,
                           size_t path_len,
                           const char *path) {

  uint8_t buffer[UAVCAN_PROTOCOL_FILE_READ_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  // uint40 offset
  for (int i = 0; i < 5; ++i) {
    buffer[i] = (uint8_t) (0xFFU & offset);
    offset >>= 8U;
  }

  // path
  memcpy((char *) &buffer[5], path, path_len);

  return UAVCAN_Send_Service_Frame(true,
                                   node_id,
                                   server_node_id,
                                   UAVCAN_PROTOCOL_FILE_READ_SIGNATURE,
                                   UAVCAN_PROTOCOL_FILE_READ_ID,
                                   transfer_id,
                                   UAVCAN_TRANSFER_PRIORITY_HIGHEST,
                                   buffer,
                                   (uint16_t) (path_len + 5));
}

// uavcan.protocol.param - https://uavcan.org/Specification/7._List_of_standard_data_types/#uavcanprotocolparam

//
//

/**
 * uavcan.protocol.param.ExecuteOpcode
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#executeopcode
 *
 */

bool Send_ExecuteOpcode_Response(uint8_t node_id,
                                 request_t request,
                                 int32_t error_code,
                                 bool ok) {

  uint8_t buffer[UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  buffer[0] = (uint8_t) error_code;
  buffer[6] = (uint8_t) ok << 7U;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_MAX_SIZE);

}

/**
 * uavcan.protocol.param.GetSet
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#getset
 *
 * @param node
 * @param buffer
 * @param buffer_len
 * @param index
 * @param name
 * @return value type
 */
void Deserialize_GetSet_Request(const uint8_t *buffer,
                                const uint32_t buffer_len,
                                parameter_getset_t *param) {

  uint8_t offset = 2;

  // Decode the index which is the first 13bits
  param->index = buffer[0] | (uint16_t) (0xFF00U & buffer[1] << 5U);

  // Then check the tag
  param->type = (uint8_t) (0x07U & buffer[1]);

  // Decode the value
  switch (param->type) {
    case PARAM_VALUE_INT:param->value.i = buffer_to_int64(&buffer[offset]);
      offset += 8;
      break;

    case PARAM_VALUE_REAL:param->value.f = buffer_to_float32(&buffer[offset]);
      offset += 4;
      break;

    case PARAM_VALUE_BOOL:param->value.b = buffer[offset];
      offset += 1;
      break;

    case PARAM_VALUE_STRING:
      // First element of the array is the length
      param->value_len = MIN(buffer[offset], 127);
      offset += 1;
      memset(param->value.s, 0, sizeof(param->value.s));
      memcpy(param->value.s, (const char *) &buffer[offset], param->value_len);
      offset += param->value_len;
      break;
  }

  // Set the value name
  param->name_len = (uint8_t) MAX(0, MIN(buffer_len - offset, UAVCAN_PROTOCOL_PARAM_NAME_MAX_SIZE));
  memset(param->name, 0, UAVCAN_PROTOCOL_PARAM_NAME_MAX_SIZE);
  memcpy(param->name, (const char *) &buffer[offset], param->name_len);
}

/**
 * uavcan.protocol.param.GetSet
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#getset
 *
 */

bool Send_GetSet_Empty_Response(uint8_t node_id,
                                request_t request) {

  uint8_t buffer[5];
  memset(buffer, 0, sizeof(buffer));

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   4);
}

bool Send_GetSet_Bool_Response(uint8_t node_id,
                               request_t request,
                               bool value,
                               bool default_value,
                               const char *name) {

  uint8_t buffer[UAVCAN_PROTOCOL_PARAM_GET_SET_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  buffer[0] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_BOOL;
  buffer[1] = (uint8_t) value;
  buffer[2] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_BOOL;
  buffer[3] = (uint8_t) default_value;
  buffer[4] = 0;
  buffer[5] = 0;

  size_t name_len = strlen(name);
  memcpy((char *) &buffer[6], name, name_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   (uint16_t) (6 + name_len));
}

bool Send_GetSet_Int_Response(uint8_t node_id,
                              request_t request,
                              int64_t value,
                              int64_t default_value,
                              int64_t min_value,
                              int64_t max_value,
                              const char *name) {

  uint8_t buffer[UAVCAN_PROTOCOL_PARAM_GET_SET_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  buffer[0] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_INT;
  buffer[9] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_INT;
  buffer[18] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_INT;
  buffer[27] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_INT;

  for (int i = 0; i < 8; ++i) {

    buffer[1 + i] = (uint8_t) (0xFFU & value);
    buffer[10 + i] = (uint8_t) (0xFFU & default_value);
    buffer[19 + i] = (uint8_t) (0xFFU & max_value);
    buffer[28 + i] = (uint8_t) (0xFFU & min_value);

    value >>= 8U;
    default_value >>= 8U;
    max_value >>= 8U;
    min_value >>= 8U;
  }

  size_t name_len = strlen(name);
  memcpy((char *) &buffer[36], name, name_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   (uint16_t) (36 + name_len));
}

bool Send_GetSet_Real_Response(uint8_t node_id,
                               request_t request,
                               float value,
                               float default_value,
                               float min_value,
                               float max_value,
                               const char *name) {

  uint8_t buffer[UAVCAN_PROTOCOL_PARAM_GET_SET_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  union {
    float f;
    uint32_t i;
  } value_union, default_value_union, min_value_union, max_value_union;

  value_union.f = value;
  default_value_union.f = default_value;
  min_value_union.f = min_value;
  max_value_union.f = max_value;

  buffer[0] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_REAL;
  buffer[5] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_REAL;
  buffer[10] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_REAL;
  buffer[15] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_REAL;

  for (int i = 0; i < 4; ++i) {
    buffer[1 + i] = (uint8_t) (0xFF & value_union.i);
    buffer[6 + i] = (uint8_t) (0xFF & default_value_union.i);
    buffer[11 + i] = (uint8_t) (0xFF & max_value_union.i);
    buffer[16 + i] = (uint8_t) (0xFF & min_value_union.i);

    value_union.i >>= 8;
    default_value_union.i >>= 8;
    max_value_union.i >>= 8;
    min_value_union.i >>= 8;
  }

  size_t name_len = strlen(name);
  memcpy((char *) &buffer[20], name, name_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   (uint16_t) (20 + name_len));

}

bool Send_GetSet_String_Response(uint8_t node_id,
                                 request_t request,
                                 size_t value_len,
                                 const uint8_t *value,
                                 size_t default_value_len,
                                 const uint8_t *default_value,
                                 const char *name) {

  uint8_t buffer[UAVCAN_PROTOCOL_PARAM_GET_SET_MAX_SIZE];
  memset(buffer, 0, sizeof(buffer));

  size_t offset = 0;

  //
  buffer[offset++] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_STRING;
  buffer[offset++] = value_len;

  // Value
  for (int i = 0; i < MIN(127, value_len); ++i) {
    buffer[offset++] = value[i];
  }

  // Default
  buffer[offset++] = UAVCAN_PROTOCOL_PARAM_VALUE_TAG_STRING;
  buffer[offset++] = default_value_len;
  for (int i = 0; i < MIN(127, default_value_len); ++i) {
    buffer[offset++] = default_value[i];
  }

  // Min / Max
  buffer[offset++] = 0;
  buffer[offset++] = 0;

  size_t name_len = strlen(name);
  memcpy((char *) &buffer[offset], name, name_len);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE,
                                   UAVCAN_PROTOCOL_PARAM_GET_SET_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   (uint16_t) (offset + name_len));
}

bool Params_Handle_GetSet(uint8_t node_id,
                          uint32_t parameter_count,
                          const parameter_info_t *info_table,
                          const uint8_t *factory_info_base,
                          bool *dirty_table,
                          parameter_getset_t *param,
                          request_t request,
                          uint8_t *param_base) {

  int64_t int_value = 0;
  int64_t int_default = 0;
  float real_default = 0;

  uint8_t *default_pointer = 0;

  if (param->name_len > 0) {
    // Find the parameter index for the given name
    for (int i = 0; i < parameter_count; ++i) {
      if (strncmp(param->name,
                  info_table[i].name,
                  UAVCAN_PROTOCOL_PARAM_NAME_MAX_SIZE)==0) {
        param->index = i;
        break;
      }
    }
  }

  if ((factory_info_base!=0) && (info_table[param->index].calibration_offset!=0)) {
    default_pointer = ((uint8_t *) factory_info_base + info_table[param->index].calibration_offset);
  }

  if (param->index < parameter_count) {

    switch (info_table[param->index].type) {

      case PARAM_VALUE_BOOL:

        if (param->type==PARAM_VALUE_BOOL) {
          *(bool *) ((uint8_t *) param_base + info_table[param->index].struct_offset) = param->value.b;
          dirty_table[param->index] = true;
        }

        Send_GetSet_Bool_Response(node_id,
                                  request,
                                  *(bool *) ((uint8_t *) param_base + info_table[param->index].struct_offset),
                                  (bool) info_table[param->index].i_default,
                                  info_table[param->index].name);
        break;

      case PARAM_VALUE_INT:
        if (param->type==PARAM_VALUE_INT) {
          memcpy(((uint8_t *) param_base + info_table[param->index].struct_offset),
                 &param->value.i,
                 info_table[param->index].variable_size);
          dirty_table[param->index] = true;
        }

        // Memcpy because ints can be 8, 16, or 32 bits wide and signed / unsigned

        // Copy the value
        memcpy(&int_value,
               ((uint8_t *) param_base + info_table[param->index].struct_offset),
               info_table[param->index].variable_size);

        // Copy the default because it might be from the factory info
        memcpy(&int_default,
               default_pointer ? default_pointer : (uint8_t *) &info_table[param->index].i_default,
               info_table[param->index].variable_size);

        Send_GetSet_Int_Response(node_id,
                                 request,
                                 int_value,
                                 int_default,
                                 info_table[param->index].i_min,
                                 info_table[param->index].i_max,
                                 info_table[param->index].name);
        break;

      case PARAM_VALUE_REAL:

        if (param->type==PARAM_VALUE_REAL) {
          *(float *) ((uint8_t *) param_base + info_table[param->index].struct_offset) = param->value.f;
          dirty_table[param->index] = true;
        }

        // Copy the default either from the fixed table or the factory info block
        memcpy(&real_default,
               default_pointer ? default_pointer : (uint8_t *) &info_table[param->index].f_default,
               info_table[param->index].variable_size);

        Send_GetSet_Real_Response(node_id,
                                  request,
                                  *(float *) ((uint8_t *) param_base + info_table[param->index].struct_offset),
                                  real_default,
                                  info_table[param->index].f_min,
                                  info_table[param->index].f_max,
                                  info_table[param->index].name);

        break;

      case PARAM_VALUE_STRING:
        // This needs special handling and so is not done here.
        break;
    }
    return true;
  } else {
    Send_GetSet_Empty_Response(node_id, request);
    return false;
  }
}

/**
 * Vendor-Specific Functions
 */


/**
 *
 * io.p-systems.device.WriteOptionBytes
 *  > uint8_t bytes[2]
 *  ---
 *  > uint8_t error_code
 */
bool Handle_Write_Option_Bytes_Request(uint8_t node_id,
                                       request_t request,
                                       bool enabled,
                                       const uint8_t *payload) {

  uint8_t buffer[1] = {0};
  uint8_t error_code = enabled ? 0 : 100;

  if (enabled) {
    // Unlock the flash
    while ((FLASH->SR & FLASH_SR_BSY)!=0) {}

    if ((FLASH->CR & FLASH_CR_LOCK)!=0) {
      FLASH->KEYR = FLASH_KEY1;
      FLASH->KEYR = FLASH_KEY2;
    }

#if defined(STM32F3)
    // Unlock the option registers
    if ((FLASH->CR & FLASH_CR_OPTWRE)==0) {
      FLASH->OPTKEYR = FLASH_OPTKEY1;
      FLASH->OPTKEYR = FLASH_OPTKEY2;
    }

    // Erase Option Bytes
    FLASH->CR |= FLASH_CR_OPTER;
    FLASH->CR |= FLASH_CR_STRT;

    // Wait for it to finish
    while ((FLASH->SR & FLASH_SR_BSY)!=0) {}

    if ((FLASH->SR & FLASH_SR_EOP)!=0) {
      FLASH->SR = FLASH_SR_EOP;
    }

    // Disable erase
    FLASH->CR &= ~FLASH_CR_OPTER;

    // Enable programming
    FLASH->CR |= FLASH_CR_OPTPG;

    // User Config
    OB->USER = (OB_IWDG_SW | OB_STOP_NO_RST | OB_STDBY_NO_RST | OB_VDDA_ANALOG_ON | 0x88U);

    // Disable read protection
    OB->RDP = OB_RDP_LEVEL_0;

    // Program the user data
    *(__IO
    uint16_t *) OB_DATA_ADDRESS_DATA0 = payload[0];
    *(__IO
    uint16_t *) OB_DATA_ADDRESS_DATA1 = payload[1];

    // Wait for operation to finish
    while ((FLASH->SR & FLASH_SR_BSY)!=0) {}

    if ((FLASH->SR & FLASH_SR_EOP)!=0) {
      FLASH->SR = FLASH_SR_EOP;
    }

    // Lock everything
    FLASH->CR &= ~FLASH_CR_OPTPG;
#endif

  }

  // Send response
  buffer[0] = error_code;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_SIGNATURE,
                                   IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_MAX_SIZE);
}


/**
 * io.p-systems.i2c.Read
 *  > uint8_t bus_id
 *  > uint8_t read_length
 *  > uint16_t device_address
 *  > uint16_t device_register
 *  ---
 *  > uint8_t error_code
 *  > uint8_t[<=255] data
 *
 * @param node
 * @param request
 * @param hi2c           Array of I2C_TypeDef
 * @param i2c_but_count  N
 * @param payload
 * @param payload_len
 * @return
 */
bool Handle_I2C_Read_Request(uint8_t node_id,
                             request_t request,
                             I2C_TypeDef *hi2c,
                             uint8_t i2c_address,
                             uint8_t register_address,
                             uint8_t read_length,
                             uint32_t timeout_ms) {
  uint8_t buffer[IO_P_SYSTEMS_DEVICE_I2C_READ_MAX_SIZE + 1];
  memset(buffer, 0, sizeof(buffer));

  uint8_t error_code = 0;
  read_length = MIN(sizeof(buffer) - 1, read_length);

  if (!I2C_Read_Register(hi2c,
                         (i2c_address << 1U),
                         register_address,
                         read_length,
                         &buffer[1],
                         timeout_ms)) {
    error_code = 1;
  }

  buffer[0] = (uint8_t) error_code;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_DEVICE_I2C_READ_SIGNATURE,
                                   IO_P_SYSTEMS_DEVICE_I2C_READ_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   (uint16_t) (read_length + 1));

}

/**
 * io.p-systems.i2c.Write
 *  > uint8_t bus_id
 *  > uint16_t address
 *  > uint16_t register
 *  > uint8_t[<=255] data
 *  ---
 *  > uint8_t error_code
 *
 * @param node
 * @param request
 * @param payload
 * @param payload_len
 * @return
 */
bool Handle_I2C_Write_Request(uint8_t node_id,
                              request_t request,
                              I2C_TypeDef *hi2c,
                              uint8_t i2c_address,
                              uint8_t register_address,
                              const uint8_t *payload,
                              const size_t payload_len,
                              uint32_t timeout_ms) {

  uint8_t error_code = 0;

  if (!I2C_Write_Register(hi2c,
                          (i2c_address << 1U),
                          register_address,
                          (uint8_t) MAX(payload_len, 0),
                          payload,
                          timeout_ms)) {
    error_code = 1;
  }

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_DEVICE_I2C_WRITE_SIGNATURE,
                                   IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID,
                                   request.transfer_id,
                                   request.priority,
                                   &error_code,
                                   1);

}

/**
 * Universal Test Fixture
 */

/**
 *
 * @param buffer
 * @param pin_id
 * @param mode
 * @param push_pull
 */

void Deserialize_ConfigPin_Request(const uint8_t *buffer,
                                   uint16_t *pin_id,
                                   uint8_t *mode,
                                   uint8_t *push_pull) {

  *pin_id = (buffer[0] | (buffer[1] << 8U));

  // Is the pin being configured
  *mode = ((buffer[2] >> 4U) & 0xFU);
  *push_pull = (buffer[2] & 0xFU);
}

bool Send_ConfigPin_Response(uint8_t node_id,
                             request_t request,
                             uint16_t pin_id,
                             uint8_t error_code) {

  uint8_t buffer[IO_P_SYSTEMS_UTF_CONFIG_PIN_REQUEST_MAX_SIZE];

  buffer[0] = (uint8_t) (pin_id & 0xFFU);
  buffer[1] = (uint8_t) (pin_id >> 8U & 0xFFU);
  buffer[2] = error_code;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_UTF_CONFIG_PIN_SIGNATURE,
                                   IO_P_SYSTEMS_UTF_CONFIG_PIN_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   3);
}

/**
 *
 * @param buffer
 * @param pin_id
 * @return
 */

void Deserialize_SetPinState_Request(const uint8_t *buffer,
                                     uint16_t *pin_id,
                                     bool *pin_state) {

  *pin_id = (buffer[0] | (buffer[1] << 8U));
  *pin_state = (buffer[2] & 0x1U)==1;
}

bool Send_SetPinState_Response(uint8_t node_id,
                               request_t request,
                               uint16_t pin_id,
                               uint8_t error_code,
                               bool state) {

  uint8_t buffer[IO_P_SYSTEMS_UTF_SET_PIN_STATE_REQUEST_MAX_SIZE];

  buffer[0] = (uint8_t) (pin_id & 0xFFU);
  buffer[1] = (uint8_t) (pin_id >> 8U & 0xFFU);
  buffer[2] = (error_code << 4U) | (0x0FU & (uint8_t) state);

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_UTF_SET_PIN_STATE_SIGNATURE,
                                   IO_P_SYSTEMS_UTF_SET_PIN_STATE_ID,
                                   request.transfer_id,
                                   request.priority,
                                   buffer,
                                   3);
}


/**
 *
 * @param buffer
 * @param pin_id
 * @return true if the signature passes
 */
bool Check_ResetDUT_Request(const uint8_t *buffer) {

  uint64_t magic = IO_P_SYSTEMS_UTF_RESTART_DUT_MAGIC;

  for (int i = 0; i < 5; ++i) {
    if (buffer[i]!=(magic & 0xFFU)) {
      return false;
    }

    magic >>= 8U;
  }
  return true;
}

bool Send_ResetDUT_Response(uint8_t node_id,
                            bool ok,
                            request_t request) {

  uint8_t buffer = (uint8_t) ok;

  return UAVCAN_Send_Service_Frame(false,
                                   node_id,
                                   request.node_id,
                                   IO_P_SYSTEMS_UTF_RESTART_DUT_SIGNATURE,
                                   IO_P_SYSTEMS_UTF_RESTART_DUT_ID,
                                   request.transfer_id,
                                   request.priority,
                                   &buffer,
                                   1);
}

/**
 * Misc Routines
 */

void Check_Circuit_Limits(circuit_status_t *circuit, float V_min, float V_max, float I_min, float I_max) {

  circuit->error_flags = 0;

  if (circuit->voltage > V_max) {
    circuit->error_flags |= UAVCAN_CIRCUIT_STATUS_FLAG_OVER_VOLTAGE;
  } else if (circuit->voltage < V_min) {
    circuit->error_flags |= UAVCAN_CIRCUIT_STATUS_FLAG_UNDER_VOLTAGE;
  }

  if (circuit->current > I_max) {
    circuit->error_flags |= UAVCAN_CIRCUIT_STATUS_FLAG_OVER_CURRENT;
  } else if (circuit->current > I_max) {
    circuit->error_flags |= UAVCAN_CIRCUIT_STATUS_FLAG_UNDER_CURRENT;
  }
}
