/**
 * @file
 * @brief STM32 Utilities
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @copyright (c) 2019 - STMicroelectronics
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 *
 * @details :
 *
 *   This file is a collection of routines for dealing with GPIO, bxCAN, and Timers for several STM32 MCUs. The
 *   code mostly comes from ST's HAL and Reference Manuals for F0, F3, F4 but has been specialized and flattened out
 *   for simplicity. This is not a library and only supports a small number of MCUs that share similar hardware.
 *
 *   It also contains routines for interacting with the Pomegranate Systems STM32 bootloader.
 *
 * Quick Start:
 *  - Setup and define uint32_t System_Tick that is incremented every ms
 *  - Setup CAN device
 *  - For F0 pins PB8 and PB9 are used unless REMAP_PA11_PA12 is defined then PA11 and PA12 are used
 */

#include "stm32_util.h"

#include <stdint.h>
#include <string.h>

/**
 * Hardware Specific Headers and Hacks
 */
#if defined(STM32F0)

#include "stm32f0xx_ll_gpio.h"
#include "stm32f0xx_ll_i2c.h"
#include "stm32f0xx_ll_rcc.h"

#elif defined(STM32F3)

#include "stm32f3xx_ll_i2c.h"
//#include "stm32f3xx_ll_rcc.h"
#include "stm32f3xx_hal_flash.h"

#elif defined(STM32F4)
// clang-format off
#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_rcc.h"

#define GPIO_NUMBER           16U

#elif defined(STM32G4)

#include "stm32g4xx_ll_i2c.h"
//#include "stm32g4xx_ll_rcc.h"

#include "stm32g4xx_hal_fdcan.h"

// Private defines from _fdcan.c
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/** @addtogroup FDCAN_Private_Constants
  * @{
  */
#define FDCAN_TIMEOUT_VALUE 10U

#define FDCAN_TX_EVENT_FIFO_MASK (FDCAN_IR_TEFL | FDCAN_IR_TEFF | FDCAN_IR_TEFN)
#define FDCAN_RX_FIFO0_MASK (FDCAN_IR_RF0L | FDCAN_IR_RF0F | FDCAN_IR_RF0N)
#define FDCAN_RX_FIFO1_MASK (FDCAN_IR_RF1L | FDCAN_IR_RF1F | FDCAN_IR_RF1N)
#define FDCAN_ERROR_MASK (FDCAN_IR_ELO | FDCAN_IR_WDI | FDCAN_IR_PEA | FDCAN_IR_PED | FDCAN_IR_ARA)
#define FDCAN_ERROR_STATUS_MASK (FDCAN_IR_EP | FDCAN_IR_EW | FDCAN_IR_BO)

#define FDCAN_ELEMENT_MASK_STDID ((uint32_t)0x1FFC0000U) /* Standard Identifier         */
#define FDCAN_ELEMENT_MASK_EXTID ((uint32_t)0x1FFFFFFFU) /* Extended Identifier         */
#define FDCAN_ELEMENT_MASK_RTR   ((uint32_t)0x20000000U) /* Remote Transmission Request */
#define FDCAN_ELEMENT_MASK_XTD   ((uint32_t)0x40000000U) /* Extended Identifier         */
#define FDCAN_ELEMENT_MASK_ESI   ((uint32_t)0x80000000U) /* Error State Indicator       */
#define FDCAN_ELEMENT_MASK_TS    ((uint32_t)0x0000FFFFU) /* Timestamp                   */
#define FDCAN_ELEMENT_MASK_DLC   ((uint32_t)0x000F0000U) /* Data Length Code            */
#define FDCAN_ELEMENT_MASK_BRS   ((uint32_t)0x00100000U) /* Bit Rate Switch             */
#define FDCAN_ELEMENT_MASK_FDF   ((uint32_t)0x00200000U) /* FD Format                   */
#define FDCAN_ELEMENT_MASK_EFC   ((uint32_t)0x00800000U) /* Event FIFO Control          */
#define FDCAN_ELEMENT_MASK_MM    ((uint32_t)0xFF000000U) /* Message Marker              */
#define FDCAN_ELEMENT_MASK_FIDX  ((uint32_t)0x7F000000U) /* Filter Index                */
#define FDCAN_ELEMENT_MASK_ANMF  ((uint32_t)0x80000000U) /* Accepted Non-matching Frame */
#define FDCAN_ELEMENT_MASK_ET    ((uint32_t)0x00C00000U) /* Event type                  */

#define SRAMCAN_FLS_NBR                  (28U)         /* Max. Filter List Standard Number      */
#define SRAMCAN_FLE_NBR                  ( 8U)         /* Max. Filter List Extended Number      */
#define SRAMCAN_RF0_NBR                  ( 3U)         /* RX FIFO 0 Elements Number             */
#define SRAMCAN_RF1_NBR                  ( 3U)         /* RX FIFO 1 Elements Number             */
#define SRAMCAN_TEF_NBR                  ( 3U)         /* TX Event FIFO Elements Number         */
#define SRAMCAN_TFQ_NBR                  ( 3U)         /* TX FIFO/Queue Elements Number         */

#define SRAMCAN_FLS_SIZE            ( 1U * 4U)         /* Filter Standard Element Size in bytes */
#define SRAMCAN_FLE_SIZE            ( 2U * 4U)         /* Filter Extended Element Size in bytes */
#define SRAMCAN_RF0_SIZE            (18U * 4U)         /* RX FIFO 0 Elements Size in bytes      */
#define SRAMCAN_RF1_SIZE            (18U * 4U)         /* RX FIFO 1 Elements Size in bytes      */
#define SRAMCAN_TEF_SIZE            ( 2U * 4U)         /* TX Event FIFO Elements Size in bytes  */
#define SRAMCAN_TFQ_SIZE            (18U * 4U)         /* TX FIFO/Queue Elements Size in bytes  */

#define SRAMCAN_FLSSA ((uint32_t)0)                                                      /* Filter List Standard Start
                                                                                            Address                  */
#define SRAMCAN_FLESA ((uint32_t)(SRAMCAN_FLSSA + (SRAMCAN_FLS_NBR * SRAMCAN_FLS_SIZE))) /* Filter List Extended Start
                                                                                            Address                  */
#define SRAMCAN_RF0SA ((uint32_t)(SRAMCAN_FLESA + (SRAMCAN_FLE_NBR * SRAMCAN_FLE_SIZE))) /* Rx FIFO 0 Start Address  */
#define SRAMCAN_RF1SA ((uint32_t)(SRAMCAN_RF0SA + (SRAMCAN_RF0_NBR * SRAMCAN_RF0_SIZE))) /* Rx FIFO 1 Start Address  */
#define SRAMCAN_TEFSA ((uint32_t)(SRAMCAN_RF1SA + (SRAMCAN_RF1_NBR * SRAMCAN_RF1_SIZE))) /* Tx Event FIFO Start
                                                                                            Address */
#define SRAMCAN_TFQSA ((uint32_t)(SRAMCAN_TEFSA + (SRAMCAN_TEF_NBR * SRAMCAN_TEF_SIZE))) /* Tx FIFO/Queue Start
                                                                                            Address                  */
#define SRAMCAN_SIZE  ((uint32_t)(SRAMCAN_TFQSA + (SRAMCAN_TFQ_NBR * SRAMCAN_TFQ_SIZE))) /* Message RAM size         */

static const uint8_t DLCtoBytes[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 12, 16, 20, 24, 32, 48, 64};
// clang-format on
#endif

const uint32_t eeprom_timeout_ms = 100;

volatile extern uint32_t System_Tick;

#if ENABLE_TRANSPORT_STATS
iface_stats_t interface_stats[CAN_IFACE_COUNT] = {};
#endif

/**
 * Low Level Data Manipulation Routines
 */

/**
 * @param b
 * @return
 */
float compute_average(rolling_buffer_t b) {

  float total = 0;

  for (int i = 0; i < b.count; ++i) {
    total += (float) b.data[i];
  }

  if (b.count == 0) {
    return 0;
  } else {
    return total / (float) b.count;
  }
}

void rolling_buffer_add(rolling_buffer_t *b, int32_t value) {

  if (b->count > 0) {
    b->index_prev = b->index;
    b->index      = (b->index + 1) % ROLLING_BUFFER_MAX_SIZE;
  }

  b->data[b->index]      = value;
  b->timestamp[b->index] = System_Tick;

  if (b->count < ROLLING_BUFFER_MAX_SIZE - 1) {
    ++b->count;
  }
}

void Busy_Wait(uint32_t delay_ms) {
  uint32_t tickstart = System_Tick;

  delay_ms = MAX(1, delay_ms);

  while ((System_Tick - tickstart) < delay_ms) {
    __asm__ volatile("nop");
  }
}

/*
 * CRC functions
 */

uint16_t CRC_16_Add_Byte(uint16_t crc_val,
                         uint8_t  byte) {

  crc_val ^= (uint16_t) ((uint16_t) (byte) << 8U);

  for (uint8_t j = 0; j < 8; j++) {
    crc_val = (crc_val & 0x8000U) ? (crc_val << 1U) ^ 0x1021U : crc_val << 1U;
  }

  return crc_val;
}

uint16_t CRC_16_Add_Signature(uint16_t crc,
                              uint64_t data_type_signature) {

  for (size_t i = 0; i < 8; ++i) {
    crc                 = CRC_16_Add_Byte(crc, (uint8_t) data_type_signature);
    data_type_signature = data_type_signature >> 8;
  }

  return crc;
}

uint16_t CRC_16_Compute(uint16_t    crc,
                        const void *ptr,
                        size_t      count) {

  const uint8_t *buffer = ptr;

  for (int i = 0; i < count; ++i) {
    crc = CRC_16_Add_Byte(crc, *(buffer + i));
  }

  return crc;
}

/**
 * http://reveng.sourceforge.net/crc-catalogue/17plus.htm#crc.cat.crc-64-we
 *
 * Compute CRC-64 using table
 *
 * @param table
 * @param crc
 * @param length
 * @param data
 *
 * @return
 */

uint64_t CRC_64_Compute(const uint64_t *table,
                        uint64_t        crc,
                        size_t          length,
                        const uint8_t  *data) {

  for (int i = 0; i < length; ++i) {
    crc ^= ((0xFFU & (uint64_t) data[i]) << 56);
    uint8_t ix = 0xFFU & (crc >> 56);

    crc <<= 8;
    crc ^= table[ix];
  }

  return crc;
}

/**
 * Generate Table for the given CRC-64 Polynomial. Table has 256 entries.
 *
 * Tested and compared to:
 *
 *  http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
 *
 * @param polynomial
 * @param output_table
 */
void CRC_64_Generate_Table(uint64_t  polynomial,
                           uint64_t *output_table) {

  for (int i = 0; i < 256; ++i) {

    uint64_t crc = ((uint64_t) i) << 56;

    for (int j = 0; j < 8; ++j) {
      crc = (crc & 0x8000000000000000) ? (crc << 1) ^ polynomial : crc << 1;
    }
    output_table[i] = crc;
  }
}

/**
 * Initialize a node_info_t structure
 *
 *
 *
 * @param node
 * @param default_node_id
 * @param version_major
 * @param version_minor
 * @param app_name
 */
void Node_Init(node_info_t           *node,
               uint8_t                health,
               uint8_t                mode,
               uint8_t                default_node_id,
               uint8_t                sw_ver_major,
               uint8_t                sw_ver_minor,
               uint32_t               vcs_commit,
               uint64_t               image_crc,
               const char            *app_name,
               factory_info_header_t *factory_info) {

  // Initialize node data
  memset(node, 0, sizeof(node_info_t));

  node->health           = health;
  node->mode             = mode;
  node->id               = default_node_id;
  node->sw_version_major = sw_ver_major;
  node->sw_version_minor = sw_ver_minor;
  strncpy((char *) node->app_name, app_name, strlen(app_name));

  uint8_t device_type = 0;

  // Simple hash for basic random number generation
  const uint8_t p1    = 7;
  const uint8_t p2    = 31;
  node->hardware_hash = p1;

  // Load stm32 UUID
#if defined(STM32F0) || defined(STM32F3)
  // For F0: Section 33.1 of STM32F0 Reference Manual RM0091 (Page 925)
  volatile uint32_t *stm32_uuid = ((uint32_t *) 0x1FFFF7AC);
#elif defined(STM32F4)
  // Chapter 35 of STM32F413/423 Reference Manual RM0430 (Page 1315)
  volatile uint32_t *stm32_uuid = ((uint32_t *) 0x1FFF7A10);
#elif defined(STM32G4)
  // Section 48.1 of STM32G4 Reference Manual RM0440 (Page 2114)
  volatile uint32_t *stm32_uuid = ((uint32_t *) 0x1FFF7590);
#endif

  for (int j = 0; j < 3; ++j) {

    uint32_t uuid_seg = stm32_uuid[j];

    for (int i = 0; i < 4; ++i) {
      node->uuid[4 * j + i] = (uint8_t) (0xFFU & (uint8_t) uuid_seg);
      uuid_seg              = uuid_seg >> 8U;

      node->hardware_hash = node->hardware_hash * p2 + node->uuid[4 * j + i];
    }
  }

  // Load the hardware version from the option bytes if available
#if defined(STM32F3)
  uint32_t ob_data = *(__IO uint32_t *) OB_DATA_ADDRESS_DATA0;

  device_type           = 0xFF & ob_data;
  uint8_t version_major = 0x1F & (ob_data >> 16);
  uint8_t version_minor = 0x7 & (ob_data >> 21);

  node->hw_version_major = version_major;
  node->hw_version_minor = version_minor;
#else

#if defined(HARDWARE_VERSION_MAJOR)
  node->hw_version_major = HARDWARE_VERSION_MAJOR;
#endif
#if defined(HARDWARE_VERSION_MINOR)
  node->hw_version_minor = HARDWARE_VERSION_MINOR;
#endif

#endif

  // If factory info is valid use it to set the hardware version
  if (factory_info->magic == FACTORY_INFO_MAGIC) {
    //    node->hw_version_major = factory_info->device_version_major;
    //    node->hw_version_minor = factory_info->device_version_minor;
    device_type = factory_info->device_type;
  }

  // Add more to the UUID
  node->uuid[13] = node->hw_version_minor;
  node->uuid[14] = node->hw_version_major;
  node->uuid[15] = device_type;

  // If default node ID is 0 it is set to the low 6 bits of the hardware hash
  if (default_node_id == 0) {
    node->id = (uint8_t) (0x3F & node->hardware_hash);
  }

  // Interface stats
  node->image_crc  = image_crc;
  node->vcs_commit = vcs_commit;

#if 0
  // Copy over the hardware certificate (it will be blank if it's invalid)
  for(int i = 0; i < MIN(sizeof(factory_info->crypto_hash), UAVCAN_HW_CERT_LENGTH_BYTES); ++i){
    node->hw_cert[i] = factory_info->crypto_hash[i];
  }
#endif
}

/**
 * Hardware Functions
 */

/**
 * bxCAN
 *
 */


uint32_t bxCAN_Get_Prescalar_8(uint32_t speed_kbs) {

  switch (speed_kbs) {

    case 1000:
      return 1;

    case 500:
      return 2;

    case 250:
      return 4;

    case 125:
      return 8;

    case 20:
      return 50;

    case 10:
      return 100;

    default:
      return 0;
  }
}

uint32_t bxCAN_Get_Prescalar_16(uint32_t speed_kbs) {

  switch (speed_kbs) {

    case 1000:
      return 1;

    case 500:
      return 2;

    case 250:
      return 4;

    case 125:
      return 8;

    case 100:
      return 10;

    case 50:
      return 20;

    case 20:
      return 50;

    case 10:
      return 100;

    default:
      return 0;
  }
}

uint32_t bxCAN_Get_Prescalar_48(uint32_t speed_kbs) {

  switch (speed_kbs) {

    case 1000:
      return 3;

    case 500:
      return 6;

    case 250:
      return 12;

    case 125:
      return 24;

    case 100:
      return 30;

    case 50:
      return 60;

    case 20:
      return 150;

    case 10:
      return 300;

    default:
      return 0;
  }
}

/**
 *
 * If using 8, 16, 48Mhz
 *
 * From: http://www.bittiming.can-wiki.info/
 *
 * @param speed_kbs
 * @param prescaler
 * @param seg
 */
uint32_t bxCAN_Get_Prescalar(uint32_t speed_kbs) {

#if CANBUS_PRESCALAR == CANBUS_PRESCALAR_8MHZ
  return bxCAN_Get_Prescalar_8(speed_kbs);
#elif CANBUS_PRESCALAR == CANBUS_PRESCALAR_16MHZ
  return bxCAN_Get_Prescalar_16(speed_kbs);
#elif CANBUS_PRESCALAR == CANBUS_PRESCALAR_48MHZ
  return bxCAN_Get_Prescalar_48(speed_kbs);
#else
  return 0;
#endif
}

#if FDCAN == 1
FDCAN_MsgRamAddressTypeDef msgRam;
uint32_t                   LatestTxFifoQRequest;

/**
 *
 *
 * @param can
 * @warning The FDCAN device hardware and clocks must be initialized first!
 * @return
 */
bool FDCAN_Init(FDCAN_GlobalTypeDef *can,
                fdcan_init_mode      mode,
                uint32_t             baud_kbps,
                bool                 disable_automatic_retransmission,
                bool                 transmit_pause,
                bool                 protocol_exception_handling,
                uint32_t             timeout) {

  bool     fifo_op_mode = true;
  uint32_t time_sjw     = 0;
  uint32_t time_seg_1   = 13;
  uint32_t time_seg_2   = 2;
  uint32_t prescaler    = 1;

  uint32_t standard_filter_count = 0;
  uint32_t extended_filter_count = 8;

  /* Exit from Sleep mode */
  CLEAR_BIT(can->CCCR, FDCAN_CCCR_CSR);

  uint32_t tickstart = System_Tick;
  while ((can->CCCR & FDCAN_CCCR_CSA) == FDCAN_CCCR_CSA) {
    if ((System_Tick - tickstart) > timeout) {
      // TODO: Handle error?
      return false;
    }
  }

  // Request initialisation */
  SET_BIT(can->CCCR, FDCAN_CCCR_INIT);

  tickstart = System_Tick;

  while ((can->CCCR & FDCAN_CCCR_INIT) == 0U) {
    if ((System_Tick - tickstart) > timeout) {
      // TODO: Handle error?
      return false;
    }
  }

  // Enable configuration change
  SET_BIT(can->CCCR, FDCAN_CCCR_CCE);

  if (can == FDCAN1) {
    FDCAN_CONFIG->CKDIV = FDCAN_CLOCK_DIV1;
  } else {
    return false;
  }

  if (disable_automatic_retransmission) {
    SET_BIT(can->CCCR, FDCAN_CCCR_DAR);
  } else {
    CLEAR_BIT(can->CCCR, FDCAN_CCCR_DAR);
  }

  if (transmit_pause) {
    SET_BIT(can->CCCR, FDCAN_CCCR_TXP);
  } else {
    CLEAR_BIT(can->CCCR, FDCAN_CCCR_TXP);
  }

  if (protocol_exception_handling) {
    CLEAR_BIT(can->CCCR, FDCAN_CCCR_PXHD);
  } else {
    SET_BIT(can->CCCR, FDCAN_CCCR_PXHD);
  }

  // Classic mode only
  MODIFY_REG(can->CCCR, FDCAN_FRAME_FD_BRS, FDCAN_FRAME_CLASSIC);

  // Operating mode
  CLEAR_BIT(can->CCCR, (FDCAN_CCCR_TEST | FDCAN_CCCR_MON | FDCAN_CCCR_ASM));
  CLEAR_BIT(can->TEST, FDCAN_TEST_LBCK);

  switch (mode) {
    case FDCAN_INIT_MODE_BUS_MONITORING:
      SET_BIT(can->CCCR, FDCAN_CCCR_MON);
      break;
    case FFDCAN_INIT_MODE_EXTERNAL_LOOPBACK:
      SET_BIT(can->CCCR, FDCAN_CCCR_TEST);
      SET_BIT(can->TEST, FDCAN_TEST_LBCK);
      break;
    case FDCAN_INIT_MODE_INTERNAL_LOOPBACK:
      SET_BIT(can->CCCR, FDCAN_CCCR_TEST);
      SET_BIT(can->TEST, FDCAN_TEST_LBCK);
      SET_BIT(can->CCCR, FDCAN_CCCR_MON);
      break;
    case FDCAN_INIT_MODE_NORMAL:
      break;
    case FDCAN_INIT_MODE_RESTRICTED_OPERATION:
      SET_BIT(can->CCCR, FDCAN_CCCR_ASM);
      break;
    default:
      return false;
  };

  // Setup Bus Timing
  can->NBTP = (((time_sjw - 1U) << FDCAN_NBTP_NSJW_Pos)
               | ((time_seg_1 - 1U) << FDCAN_NBTP_NTSEG1_Pos)
               | ((time_seg_2 - 1U) << FDCAN_NBTP_NTSEG2_Pos)
               | ((prescaler - 1U) << FDCAN_NBTP_NBRP_Pos));


  /* Select between Tx FIFO and Tx Queue operation modes */
  SET_BIT(can->TXBC, (fifo_op_mode) ? FDCAN_TX_FIFO_OPERATION : FDCAN_TXBC_TFQM);

  /* Calculate each RAM block address */
  uint32_t RAMcounter;
  uint32_t SramCanInstanceBase = SRAMCAN_BASE;

#if defined(FDCAN2)
  if (can == FDCAN2) {
    SramCanInstanceBase += SRAMCAN_SIZE;
  }
#endif /* FDCAN2 */
#if defined(FDCAN3)
  if (can == FDCAN3) {
    SramCanInstanceBase += SRAMCAN_SIZE * 2U;
  }
#endif /* FDCAN3 */

  // Standard filter list start address
  msgRam.StandardFilterSA = SramCanInstanceBase + SRAMCAN_FLSSA;

  // Standard filter elements number
  MODIFY_REG(can->RXGFC, FDCAN_RXGFC_LSS, (standard_filter_count << FDCAN_RXGFC_LSS_Pos));

  // Extended filter list start address
  msgRam.ExtendedFilterSA = SramCanInstanceBase + SRAMCAN_FLESA;

  // Extended filter elements number
  MODIFY_REG(can->RXGFC, FDCAN_RXGFC_LSE, (extended_filter_count << FDCAN_RXGFC_LSE_Pos));

  // Rx FIFO 0 start address
  msgRam.RxFIFO0SA = SramCanInstanceBase + SRAMCAN_RF0SA;

  // Rx FIFO 1 start address
  msgRam.RxFIFO1SA = SramCanInstanceBase + SRAMCAN_RF1SA;

  // Tx event FIFO start address
  msgRam.TxEventFIFOSA = SramCanInstanceBase + SRAMCAN_TEFSA;

  // Tx FIFO/queue start address
  msgRam.TxFIFOQSA = SramCanInstanceBase + SRAMCAN_TFQSA;

  /* Flush the allocated Message RAM area */
  for (RAMcounter = SramCanInstanceBase; RAMcounter < (SramCanInstanceBase + SRAMCAN_SIZE); RAMcounter += 4U) {
    *(uint32_t *) (RAMcounter) = 0x00000000U;
  }

  LatestTxFifoQRequest = 0U;

  // Start
  CLEAR_BIT(can->CCCR, FDCAN_CCCR_INIT);

  // Enable Interrupts
  // Enable line 0
  SET_BIT(can->ILE, FDCAN_INTERRUPT_LINE0);
  can->IE |= FDCAN_IE_RF0NE;

  return true;
}

bool FDCAN_GetReceivedMessage(FDCAN_GlobalTypeDef *can,
                              uint32_t             fifo,
                              CAN_RX_HEADER       *header,
                              uint8_t              payload[]) {

  if (((fifo == FDCAN_RX_FIFO0) && ((can->RXF0S & FDCAN_RXF0S_F0FL) == 0U))
      || ((fifo == FDCAN_RX_FIFO1) && ((can->RXF1S & FDCAN_RXF1S_F1FL) == 0U))) {
    return false;
  }

  // Load Header
  uint32_t *RxAddress;
  uint8_t  *pData;
  uint32_t  ByteCounter;
  uint32_t  GetIndex;

  if (fifo == FDCAN_RX_FIFO0) {
    GetIndex  = ((can->RXF0S & FDCAN_RXF0S_F0GI) >> FDCAN_RXF0S_F0GI_Pos);
    RxAddress = (uint32_t *) (msgRam.RxFIFO0SA + (GetIndex * SRAMCAN_RF0_SIZE));
  } else {
    /* Calculate Rx FIFO 1 element address */
    GetIndex  = ((can->RXF1S & FDCAN_RXF1S_F1GI) >> FDCAN_RXF1S_F1GI_Pos);
    RxAddress = (uint32_t *) (msgRam.RxFIFO1SA + (GetIndex * SRAMCAN_RF1_SIZE));
  }

  header->IdType = *RxAddress & FDCAN_ELEMENT_MASK_XTD;

  // Identifier
  if (header->IdType == FDCAN_STANDARD_ID) {
    header->Identifier = ((*RxAddress & FDCAN_ELEMENT_MASK_STDID) >> 18U);
  } else {
    header->Identifier = (*RxAddress & FDCAN_ELEMENT_MASK_EXTID);
  }

  header->RxFrameType         = (*RxAddress & FDCAN_ELEMENT_MASK_RTR);
  header->ErrorStateIndicator = (*RxAddress & FDCAN_ELEMENT_MASK_ESI);

  /* Increment RxAddress pointer to second word of Rx FIFO element */
  RxAddress++;

  // More header data
  header->RxTimestamp           = (*RxAddress & FDCAN_ELEMENT_MASK_TS);
  header->DataLength            = (*RxAddress & FDCAN_ELEMENT_MASK_DLC);
  header->BitRateSwitch         = (*RxAddress & FDCAN_ELEMENT_MASK_BRS);
  header->FDFormat              = (*RxAddress & FDCAN_ELEMENT_MASK_FDF);
  header->FilterIndex           = ((*RxAddress & FDCAN_ELEMENT_MASK_FIDX) >> 24U);
  header->IsFilterMatchingFrame = ((*RxAddress & FDCAN_ELEMENT_MASK_ANMF) >> 31U);

  /* Increment RxAddress pointer to payload of Rx FIFO element */
  RxAddress++;

  /* Retrieve Rx payload */
  pData = (uint8_t *) RxAddress;
  for (ByteCounter = 0; ByteCounter < DLCtoBytes[header->DataLength >> 16U]; ByteCounter++) {
    payload[ByteCounter] = pData[ByteCounter];
  }

  // Acknowledge that the oldest element is read out in order to increment the GetIndex
  if (fifo == FDCAN_RX_FIFO0) {
    can->RXF0A = GetIndex;
  } else {
    can->RXF1A = GetIndex;
  }

#if ENABLE_TRANSPORT_STATS

  uint8_t can_unit = 0;

#if defined(FDCAN2)
  if (can == FDCAN2) {
    can_unit = 1;
  }
#endif

#if defined(FDCAN3)
  if (can == FDCAN3) {
    can_unit = 2;
  }
#endif

  ++interface_stats[can_unit].frames_rx;
#endif

  return true;
}

bool FDCAN_QueueTransmitMessage(FDCAN_GlobalTypeDef *can,
                                uint32_t             can_id,
                                const uint8_t        payload[],
                                uint32_t             payload_len) {

  /* Check that the Tx FIFO/Queue is not full */
  if ((can->TXFQS & FDCAN_TXFQS_TFQF) != 0U) {
    return false;
  }

  /* Retrieve the Tx FIFO PutIndex */
  uint32_t BufferIndex = ((can->TXFQS & FDCAN_TXFQS_TFQPI) >> FDCAN_TXFQS_TFQPI_Pos);

  /* Calculate Tx element address */
  uint32_t *TxAddress = (uint32_t *) ((SRAMCAN_BASE + SRAMCAN_TFQSA) + (BufferIndex * SRAMCAN_TFQ_SIZE));

  /* Write Tx element header to the message RAM */
  *TxAddress = (FDCAN_EXTENDED_ID | FDCAN_DATA_FRAME | can_id);
  TxAddress++;
  *TxAddress = (FDCAN_CLASSIC_CAN | (payload_len << 16));
  TxAddress++;

  /* Write Tx payload to the message RAM */
  for (uint32_t ByteCounter = 0; ByteCounter < DLCtoBytes[payload_len]; ByteCounter += 4U) {
    *TxAddress = (((uint32_t)payload[ByteCounter + 3U] << 24U) |
                  ((uint32_t)payload[ByteCounter + 2U] << 16U) |
                  ((uint32_t)payload[ByteCounter + 1U] << 8U)  |
                  (uint32_t)payload[ByteCounter]);
    TxAddress++;
  }

  /* Activate the corresponding transmission request */
  can->TXBAR = ((uint32_t) 1 << BufferIndex);

  /* Store the Latest Tx FIFO/Queue Request Buffer Index */
  LatestTxFifoQRequest = ((uint32_t) 1 << BufferIndex);

#if ENABLE_TRANSPORT_STATS
  uint8_t can_unit = 0;

#if defined(FDCAN2)
  can_unit = (can == FDCAN2) ? 1 : can_unit;
#endif

#if defined(FDCAN3)
  can_unit = (can == FDCAN3) ? 2 : can_unit;
#endif

  ++interface_stats[can_unit].frames_tx;
#endif

  return true;
}

void FDCAN_Config_Filter(FDCAN_GlobalTypeDef *can,
                         uint8_t              FilterIndex,
                         uint32_t             FilterType,
                         uint32_t             FilterConfig,
                         uint32_t             FilterId1,
                         uint32_t             FilterId2) {
  uint32_t  FilterElementW1;
  uint32_t  FilterElementW2;
  uint32_t *FilterAddress;

  // We only support extended CAN IDs
  /* Build first word of filter element */
  FilterElementW1 = ((FilterConfig << 29U) | FilterId1);

  /* Build second word of filter element */
  FilterElementW2 = ((FilterType << 30U) | FilterId2);

  /* Calculate filter address */
  FilterAddress = (uint32_t *) (msgRam.ExtendedFilterSA + (FilterIndex * SRAMCAN_FLE_SIZE));

  /* Write filter element to the message RAM */
  *FilterAddress = FilterElementW1;
  FilterAddress++;
  *FilterAddress = FilterElementW2;
}

void FDCAN_Config_Filters(FDCAN_GlobalTypeDef *can,
                          uint8_t              filter_count,
                          const uint32_t      *filters) {

  for (int i = 0; i < filter_count; ++i) {
    uint32_t filter_id   = *(filters++);
    uint32_t filter_mask = *(filters++);

    FDCAN_Config_Filter(can, i, FDCAN_FILTER_MASK, FDCAN_FILTER_TO_RXFIFO0, filter_id, filter_mask);
  }
}

#else

bool bxCAN_Init(CAN_HANDLE *can,
                uint32_t    baud_kbps,
                uint32_t    timeout_ms,
                bool        enable_ART) {

  uint32_t prescaler = bxCAN_Get_Prescalar(baud_kbps);
  uint32_t tickstamp;

  // Automatic Bus-Off Management
  bool enable_ABOM = true;

  // Automatic Wake-Up Management
  bool enable_AWMU = false;

  // Time Triggered Communication Mode
  bool enable_TTCM = false;

  // Transmit Chronologically
  bool enable_TXFP = true;

  // Receive FIFO Locked Mode
  bool enable_RFLM = false;

  // Initialize the CAN bus
  uint32_t Mode = CAN_MODE_NORMAL;

  // Configure Timing (maybe should be defines?)
  uint32_t time_sjw   = CAN_SJW_1TQ;
  uint32_t time_seg_1 = CAN_BS1_13TQ;
  uint32_t time_seg_2 = CAN_BS2_2TQ;

#if defined(STM32F0)

#elif defined(STM32F3)
  time_seg_1 = CAN_BS1_6TQ;
  time_seg_2 = CAN_BS2_1TQ;
#elif defined(STM32F4)

#endif

  // Exit Sleep Mode
  CLEAR_BIT(can->MCR, CAN_MCR_SLEEP);

  // Wait to exit Sleep mode
  tickstamp = System_Tick + timeout_ms;
  while (READ_BIT(can->MSR, CAN_MSR_SLAK)) {
    if (System_Tick > tickstamp) {
      return false;
    }
  }

  // Enter Initialization Mode
  SET_BIT(can->MCR, CAN_MCR_INRQ);

  tickstamp = System_Tick + timeout_ms;
  while (!READ_BIT(can->MSR, CAN_MSR_INAK)) {
    if (System_Tick > tickstamp) {
      return false;
    }
  }

  // Automatic bus-off management
  if (enable_ABOM) {
    SET_BIT(can->MCR, CAN_MCR_ABOM);
  } else {
    CLEAR_BIT(can->MCR, CAN_MCR_ABOM);
  }

  // Automatic Wake-Up Mode
  if (enable_AWMU) {
    SET_BIT(can->MCR, CAN_MCR_AWUM);
  } else {
    CLEAR_BIT(can->MCR, CAN_MCR_AWUM);
  }

  // Automatic Retransmission
  if (enable_ART) {
    CLEAR_BIT(can->MCR, CAN_MCR_NART);
  } else {
    SET_BIT(can->MCR, CAN_MCR_NART);
  }

  // Receive FIFO Locked Mode
  if (enable_RFLM) {
    SET_BIT(can->MCR, CAN_MCR_RFLM);
  } else {
    CLEAR_BIT(can->MCR, CAN_MCR_RFLM);
  }

  // Time Triggered Communication Mode
  if (enable_TTCM) {
    SET_BIT(can->MCR, CAN_MCR_TTCM);
  } else {
    CLEAR_BIT(can->MCR, CAN_MCR_TTCM);
  }

  // Transmit FIFO priority
  if (enable_TXFP) {
    SET_BIT(can->MCR, CAN_MCR_TXFP);
  } else {
    CLEAR_BIT(can->MCR, CAN_MCR_TXFP);
  }

  // Setup Bus Timing
  WRITE_REG(can->BTR, (uint32_t) (Mode
                                  | time_sjw
                                  | time_seg_1
                                  | time_seg_2
                                  | (prescaler - 1U)));

  // Leave initialization mode
  CLEAR_BIT(can->MCR, CAN_MCR_INRQ);

  // Wait until device is ready
  tickstamp = System_Tick + timeout_ms;
  while (READ_BIT(can->MSR, CAN_MSR_INAK)) {
    if (System_Tick > tickstamp) {
      return false;
    }
  }

#if defined(STM32F0)
  can->IER |= CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_ERROR;
#elif defined(STM32F3)
  can->IER |= CAN_IT_RX_FIFO0_MSG_PENDING;
#elif defined(STM32F4)
  // Enable Interrupts
  can->IER |= CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_ERROR;
#endif
  //  can->IER |= (CAN_IT_ERROR_WARNING | CAN_IT_ERROR_PASSIVE
  //               | CAN_IT_LAST_ERROR_CODE | CAN_IT_ERROR
  //               | CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_RX_FIFO0_OVERRUN);
  return true;
}

bool bxCAN_Receive(CAN_HANDLE    *can,
                   uint32_t       fifo,
                   CAN_RX_HEADER *header,
                   uint8_t        payload[]) {

  if (((fifo == CAN_RX_FIFO0) && ((can->RF0R & CAN_RF0R_FMP0) == RESET))
      || ((fifo == CAN_RX_FIFO1) && ((can->RF1R & CAN_RF1R_FMP1) == RESET))) {
    return false;
  }

  // Load Header
  header->IDE = CAN_RI0R_IDE & can->sFIFOMailBox[fifo].RIR;
  if (header->IDE == CAN_ID_STD) {
    header->StdId = (CAN_RI0R_STID & can->sFIFOMailBox[fifo].RIR) >> CAN_TI0R_STID_Pos;
  } else {
    header->ExtId = ((CAN_RI0R_EXID | CAN_RI0R_STID) & can->sFIFOMailBox[fifo].RIR) >> CAN_RI0R_EXID_Pos;
  }

  header->RTR              = (CAN_RI0R_RTR & can->sFIFOMailBox[fifo].RIR) >> CAN_RI0R_RTR_Pos;
  header->DLC              = (CAN_RDT0R_DLC & can->sFIFOMailBox[fifo].RDTR) >> CAN_RDT0R_DLC_Pos;
  header->FilterMatchIndex = (CAN_RDT0R_FMI & can->sFIFOMailBox[fifo].RDTR) >> CAN_RDT0R_FMI_Pos;
  header->Timestamp        = (CAN_RDT0R_TIME & can->sFIFOMailBox[fifo].RDTR) >> CAN_RDT0R_TIME_Pos;

  // And the data
  payload[0] = (CAN_RDL0R_DATA0 & can->sFIFOMailBox[fifo].RDLR) >> CAN_RDL0R_DATA0_Pos;
  payload[1] = (CAN_RDL0R_DATA1 & can->sFIFOMailBox[fifo].RDLR) >> CAN_RDL0R_DATA1_Pos;
  payload[2] = (CAN_RDL0R_DATA2 & can->sFIFOMailBox[fifo].RDLR) >> CAN_RDL0R_DATA2_Pos;
  payload[3] = (CAN_RDL0R_DATA3 & can->sFIFOMailBox[fifo].RDLR) >> CAN_RDL0R_DATA3_Pos;
  payload[4] = (CAN_RDH0R_DATA4 & can->sFIFOMailBox[fifo].RDHR) >> CAN_RDH0R_DATA4_Pos;
  payload[5] = (CAN_RDH0R_DATA5 & can->sFIFOMailBox[fifo].RDHR) >> CAN_RDH0R_DATA5_Pos;
  payload[6] = (CAN_RDH0R_DATA6 & can->sFIFOMailBox[fifo].RDHR) >> CAN_RDH0R_DATA6_Pos;
  payload[7] = (CAN_RDH0R_DATA7 & can->sFIFOMailBox[fifo].RDHR) >> CAN_RDH0R_DATA7_Pos;

#if defined(STM32F0)

  if (fifo == CAN_RX_FIFO0) {
    SET_BIT(can->RF0R, CAN_RF0R_RFOM0);
    CLEAR_BIT(CAN->IER, CAN_FLAG_FOV0);
  } else if (fifo == CAN_RX_FIFO1) {
    SET_BIT(can->RF1R, CAN_RF1R_RFOM1);
    CLEAR_BIT(CAN->IER, CAN_FLAG_FOV1);
  }

#elif defined(STM32F3) || defined(STM32F4)
  if (fifo == CAN_RX_FIFO0) {
    SET_BIT(can->RF0R, CAN_RF0R_RFOM0);
  } else if (fifo == CAN_RX_FIFO1) {
    SET_BIT(can->RF1R, CAN_RF1R_RFOM1);
  }
#endif

#if ENABLE_TRANSPORT_STATS

  uint8_t can_unit = 0;

#if defined(CAN2)
  if (can == CAN2) {
    can_unit = 1;
  }
#endif

#if defined(CAN3)
  if (can == CAN3) {
    can_unit = 2;
  }
#endif

  ++interface_stats[can_unit].frames_rx;
#endif

  return true;
}

bool bxCAN_Transmit(CAN_HANDLE    *can,
                    CAN_TX_HEADER *header,
                    const uint8_t  payload[],
                    uint32_t       timeout_ms) {

  uint32_t mailbox = 0;

  // Find an empty mailbox
  bool mailbox_found = false;

  uint32_t ts_end_ms = System_Tick + timeout_ms;

  // Find an empty mailbox
  while (System_Tick < ts_end_ms) {

    //#if defined(STM32F0)
    //    if(((can->TSR & CAN_TSR_TME0) != 0U) ||
    //       ((can->TSR & CAN_TSR_TME1) != 0U) ||
    //       ((can->TSR & CAN_TSR_TME2) != 0U)){
    //#elif defined(STM32F3) || defined(STM32F4)
    //      if(READ_BIT(can->TSR, CAN_TSR_TME0)
    //       || READ_BIT(can->TSR, CAN_TSR_TME1)
    //       || READ_BIT(can->TSR, CAN_TSR_TME2)){
    //#endif

    if (READ_BIT(can->TSR, CAN_TSR_TME0)
        || READ_BIT(can->TSR, CAN_TSR_TME1)
        || READ_BIT(can->TSR, CAN_TSR_TME2)) {

      mailbox       = (can->TSR & CAN_TSR_CODE) >> CAN_TSR_CODE_Pos;
      mailbox_found = true;
      break;
    }
  }

  if (!mailbox_found) {
    return false;
  }

  // ID
#if defined(STM32F0) // TODO: Cleanup?
  if (header->IDE == CAN_ID_STD) {
    can->sTxMailBox[mailbox].TIR = ((header->StdId << CAN_TI0R_STID_Pos) | header->RTR);
  } else {
    can->sTxMailBox[mailbox].TIR = ((header->ExtId << CAN_TI0R_EXID_Pos) | header->IDE | header->RTR);
  }

#elif defined(STM32F3) || defined(STM32F4)
  can->sTxMailBox[mailbox].TIR &= CAN_TI0R_TXRQ;
  if (header->IDE == CAN_ID_STD) {
    can->sTxMailBox[mailbox].TIR |= ((header->StdId << CAN_TI0R_STID_Pos)
                                     | header->RTR);
  } else {
    can->sTxMailBox[mailbox].TIR |= ((header->ExtId << CAN_TI0R_EXID_Pos)
                                     | header->IDE
                                     | header->RTR);
  }
#else
#error Unimplemented
#endif

  // Data Length
  can->sTxMailBox[mailbox].TDTR = header->DLC;

#if 0
#if defined(STM32F0) || defined(STM32F4)
  if(header->TransmitGlobalTime == ENABLE){
    SET_BIT(can->sTxMailBox[mailbox].TDTR, CAN_TDT0R_TGT);
  }
#endif
#endif

  WRITE_REG(can->sTxMailBox[mailbox].TDHR,
            ((uint32_t) payload[7] << CAN_TDH0R_DATA7_Pos)
                | ((uint32_t) payload[6] << CAN_TDH0R_DATA6_Pos)
                | ((uint32_t) payload[5] << CAN_TDH0R_DATA5_Pos)
                | ((uint32_t) payload[4] << CAN_TDH0R_DATA4_Pos));

  /* Set up the data field */
  WRITE_REG(can->sTxMailBox[mailbox].TDLR,
            ((uint32_t) payload[3] << CAN_TDL0R_DATA3_Pos)
                | ((uint32_t) payload[2] << CAN_TDL0R_DATA2_Pos)
                | ((uint32_t) payload[1] << CAN_TDL0R_DATA1_Pos)
                | ((uint32_t) payload[0] << CAN_TDL0R_DATA0_Pos));

  // Queue-Up Transmission
  SET_BIT(can->sTxMailBox[mailbox].TIR, CAN_TI0R_TXRQ);

#if ENABLE_TRANSPORT_STATS

  uint8_t can_unit = 0;

#if defined(CAN2)
  if (can == CAN2) {
    can_unit = 1;
  }
#endif

#if defined(CAN3)
  if (can == CAN3) {
    can_unit = 2;
  }
#endif
  ++interface_stats[can_unit].frames_tx;
#endif

  return true;
}

/**
 * Setup id-mask filtering
 *
 * @param can
 * @param FilterNumber
 * @param FilterId
 * @param FilterMask
 * @return
 */
void bxCAN_Config_Filter(CAN_HANDLE *can,
                         uint8_t     FilterNumber,
                         uint32_t    FilterIdHigh,
                         uint32_t    FilterIdLow,
                         uint32_t    FilterMaskIdHigh,
                         uint32_t    FilterMaskIdLow) {

  uint32_t filter_bit = 1U << FilterNumber;

  SET_BIT(can->FMR, CAN_FMR_FINIT);

#if defined(STM32F3)

#elif defined(STM32F4)
  // Either 28 filter banks shared between CAN1, CAN2. Or 14 banks for single interface
  // We only care about multi-interface STM32F4s.
  // This parameter sets the first index of filters for the slave address. Setting it to
  // zero and only activating one CAN interface assigns all of the filters to that interface
  CLEAR_BIT(can->FMR, CAN_FMR_CAN2SB);
#endif

  // Deactivate Filter
  CLEAR_BIT(can->FA1R, filter_bit);

  // 32-bit scale
  SET_BIT(can->FS1R, filter_bit);

  can->sFilterRegister[FilterNumber].FR1 = ((0x0000FFFFU & (uint32_t) FilterIdHigh) << 16U)
                                           | (0x0000FFFFU & (uint32_t) FilterIdLow);

  can->sFilterRegister[FilterNumber].FR2 = ((0x0000FFFFU & (uint32_t) FilterMaskIdHigh) << 16U)
                                           | (0x0000FFFFU & (uint32_t) FilterMaskIdLow);

  // Id/Mask mode
  CLEAR_BIT(can->FM1R, filter_bit);

  // Send to FIFO-0
  CLEAR_BIT(can->FFA1R, filter_bit);

  // Activate filter
  SET_BIT(can->FA1R, filter_bit);

  // Leave Init mode
  CLEAR_BIT(can->FMR, ((uint32_t) CAN_FMR_FINIT));
}

void bxCAN_Config_Filters(CAN_HANDLE     *can,
                          uint8_t         filter_count,
                          const uint32_t *filters) {

  for (int i = 0; i < filter_count; ++i) {
    uint32_t filter_id   = *(filters++);
    uint32_t filter_mask = *(filters++);

    uint32_t id_high   = (uint32_t) (filter_id >> 13U);
    uint32_t id_low    = (uint32_t) ((0x00FFU & (filter_id << 3U)) | 0x04U);
    uint32_t mask_high = (uint32_t) (filter_mask >> 13U);
    uint32_t mask_low  = (uint32_t) ((0x00FFU & (filter_mask << 3U)) | 0x04U);

    bxCAN_Config_Filter(can, i, id_high, id_low, mask_high, mask_low);
  }
}
#endif

/**
 *  FLASH
 */

#if defined(FIRMWARE_PAGE_COUNT)

/**
 * Erase the entire firmware block
 * @return
 */
bool Flash_Erase_Pages(uint32_t base_address,
                       uint8_t  page_count) {

  // Unlock
  WRITE_REG(FLASH->KEYR, FLASH_KEY1);
  WRITE_REG(FLASH->KEYR, FLASH_KEY2);

  // Wait
  while ((FLASH->SR & FLASH_SR_BSY) != 0)
    ;

  FLASH->SR = (FLASH_FLAG_EOP);

  for (int i = 0; i < page_count; ++i) {

    // Erase
    SET_BIT(FLASH->CR, FLASH_CR_PER);
    WRITE_REG(FLASH->AR, base_address);
    SET_BIT(FLASH->CR, FLASH_CR_STRT);

    // Wait
    while ((FLASH->SR & FLASH_SR_BSY) != 0)
      ;

    // End of Op (works on F0, F3, F4)
    FLASH->SR = FLASH_FLAG_EOP;

    CLEAR_BIT(FLASH->CR, FLASH_CR_PER);

    base_address += FLASH_PAGE_SIZE;
  }

  // Disable page write and lock flash
  FLASH->CR &= ~FLASH_CR_PER;
  SET_BIT(FLASH->CR, FLASH_CR_LOCK);

  return true;
}

#endif

#if defined(FIRMWARE_SECTOR_COUNT)

#if defined(STM32F4)

bool Flash_Wait_For_Operations(uint32_t timeout) {
  uint32_t tickstart = System_Tick;
  timeout            = MIN(timeout, 0xFFFFFFFFU);

  while (FLASH->SR & FLASH_SR_BSY) {
    if ((timeout == 0U) || ((System_Tick - tickstart) > timeout)) {
      return false;
    }
  }

  if (FLASH->SR & FLASH_SR_EOP) {
    FLASH->SR = FLASH_SR_EOP;
  }

#if defined(FLASH_SR_RDERR)
  if (FLASH->SR & (FLASH_SR_SOP
      | FLASH_SR_WRPERR
      | FLASH_SR_PGAERR
      | FLASH_SR_PGPERR
      | FLASH_SR_PGSERR
      | FLASH_SR_RDERR))
#else
  if (FLASH->SR & (FLASH_SR_SOP | FLASH_SR_WRPERR | FLASH_SR_PGAERR | FLASH_SR_PGPERR | FLASH_SR_PGSERR))
#endif
  {
    return false;
  }

  return true;
}

#endif

uint32_t Flash_Get_Sector_Base_Address(uint32_t sector) {

  const uint32_t addresses[] = {0x08000000U,
                                0x08004000U,
                                0x08008000U,
                                0x0800C000U,
                                0x08010000U,
                                0x08020000U,
                                0x08040000U,
                                0x08060000U,
                                0x08080000U,
                                0x080A0000U,
                                0x080C0000U,
                                0x080E0000U,
                                0x08100000U,
                                0x08120000U,
                                0x08140000U,
                                0x08160000U};

  return addresses[sector];
}

uint32_t Flash_Get_Sector_Size(uint32_t sector) {
  if (sector < 4) {
    return 16 * 1024;
  } else if (sector == 4) {
    return 64 * 1024;
  } else {
    return 128 * 1024;
  }
}

bool Flash_Is_Sector_Empty(uint32_t sector) {
  uint32_t sector_start_address = Flash_Get_Sector_Base_Address(sector);
  uint32_t end_address          = sector_start_address + Flash_Get_Sector_Size(sector);

  bool empty = true;

  for (uint32_t address = sector_start_address; address < end_address; address += 4) {
    if (*((uint32_t *) address) != 0xFFFFFFFF) {
      return false;
    }
  }

  return empty;
}

bool Flash_Erase_Sectors(uint32_t sector_start,
                         uint8_t  sector_count) {

  __disable_irq();

  Flash_Wait_For_Operations(50000U);

  for (uint32_t sector = sector_start; sector < sector_start + sector_count; ++sector) {

    // No point in erasing empty sectors
    if (Flash_Is_Sector_Empty(sector)) {
      continue;
    }

    // Unlock
    FLASH->KEYR = FLASH_KEY1;
    FLASH->KEYR = FLASH_KEY2;

    // Program the parallelism size
    FLASH->CR &= ~FLASH_CR_PSIZE;

#if STM32F4_FLASH_VOLTAGE_RANGE == 1
    // 1.8V to 2.1V
    FLASH->CR |= FLASH_PSIZE_BYTE;
#elif STM32F4_FLASH_VOLTAGE_RANGE == 2
    // 2.1V to 2.7V
    FLASH->CR |= FLASH_PSIZE_HALF_WORD;
#elif STM32F4_FLASH_VOLTAGE_RANGE == 3
    // 2.7V to 3.6V
    FLASH->CR |= FLASH_PSIZE_WORD;
#elif STM32F4_FLASH_VOLTAGE_RANGE == 4
    // 2.7V to 3.6V + external Vpp (from stm32f4xx_hal_flash_ex.h)
    FLASH->CR |= FLASH_PSIZE_DOUBLE_WORD;
#endif

    FLASH->CR &= ~FLASH_CR_SNB;

    FLASH->CR |= FLASH_CR_SER | (sector << FLASH_CR_SNB_Pos);
    FLASH->CR |= FLASH_CR_STRT;

    Flash_Wait_For_Operations(50000U);

    FLASH->CR &= ~(FLASH_CR_SER | FLASH_CR_SNB);

    // Lock the flash
    FLASH->CR |= FLASH_CR_LOCK;
  }

  // Flush Instruction Caches
  if (READ_BIT(FLASH->ACR, FLASH_ACR_ICEN) != RESET) {
    // Disable instruction cache
    FLASH->ACR &= ~FLASH_ACR_ICEN;

    // Reset instruction cache
    FLASH->ACR |= FLASH_ACR_ICRST;
    FLASH->ACR &= ~FLASH_ACR_ICRST;

    // Enable instruction cache
    FLASH->ACR |= FLASH_ACR_ICEN;
  }

  // Flush Data Cache
  if (READ_BIT(FLASH->ACR, FLASH_ACR_DCEN) != RESET) {
    // Disable data cache
    FLASH->ACR &= ~FLASH_ACR_DCEN;
    // Reset data cache
    FLASH->ACR |= FLASH_ACR_DCRST;
    FLASH->ACR &= ~FLASH_ACR_DCRST;
    // Enable data cache
    FLASH->ACR |= FLASH_ACR_DCEN;
  }

  __enable_irq();

  return true;
}

#endif

/**
 * Write data in blocks of 1k using half-words
 *
 * @param data
 * @param data_len
 */
bool Flash_Write_Block(uint32_t       memory_offset,
                       const uint8_t *buffer,
                       uint32_t       buffer_len) {
  __disable_irq();

  // Wait for any pending operations
  while ((FLASH->SR & FLASH_SR_BSY) != 0)
    ;

#if !defined(STM32F0)
  FLASH->SR = FLASH_SR_EOP;
#endif

  // Unlock
  FLASH->KEYR = FLASH_KEY1;
  FLASH->KEYR = FLASH_KEY2;

#if defined(STM32F3)
  FLASH->SR = (FLASH_SR_EOP | FLASH_SR_WRPERR | FLASH_SR_PGERR);
#elif defined(STM32F4)
  FLASH->SR = FLASH_SR_EOP | FLASH_SR_SOP | FLASH_SR_WRPERR | FLASH_SR_PGAERR
              | FLASH_SR_PGPERR | FLASH_SR_PGSERR;

  // Program the parallelism size
  CLEAR_BIT(FLASH->CR, FLASH_CR_PSIZE);

#if STM32F4_FLASH_VOLTAGE_RANGE < 2
#error "Only 2.1 - 3.67 range is supported, but we write HALF_WORDs"
#endif

  FLASH->CR |= FLASH_PSIZE_HALF_WORD;
#endif

  // We can go past the end of the buffer here, size of buffer should be even.
  uint32_t buffer_hword_count = (buffer_len + 1) / 2;

  for (int i = 0; i < buffer_hword_count; ++i) {

    uint16_t data = (uint16_t) buffer[2 * i + 1] << 8U
                    | ((uint16_t) buffer[2 * i + 0]);

    FLASH->CR |= FLASH_CR_PG;

    *(volatile uint16_t *) (memory_offset + 2 * i) = data;

    // Wait for operation to finish
    do {
      __ASM volatile("nop");
    } while ((FLASH->SR & FLASH_SR_BSY) != 0);
  }

  // Lock flash
  FLASH->CR |= FLASH_CR_LOCK;

  do {
    __ASM volatile("nop");
  } while ((FLASH->SR & FLASH_SR_BSY) != 0);

  bool verified = true;

  for (int i = 0; i < buffer_hword_count; ++i) {

    uint16_t data = ((uint16_t) buffer[2 * i + 1]) << 8U
                    | ((uint16_t) buffer[2 * i + 0]);

    if (*(__IO uint16_t *) (memory_offset + 2 * i) != data) {
      verified = false;
      break;
    }
  }

  __enable_irq();

  return verified;
}

/**
 * GPIO
 */

// clang-format off
#define GPIO_MODE             (0x00000003U)
#define EXTI_MODE             (0x10000000U)
#define GPIO_MODE_IT          (0x00010000U)
#define GPIO_MODE_EVT         (0x00020000U)
#define RISING_EDGE           (0x00100000U)
#define FALLING_EDGE          (0x00200000U)
#define GPIO_OUTPUT_TYPE      (0x00000010U)
// clang-format on

#if !defined(USE_LL_GPIO)
void GPIO_Config_Pins(GPIO_TypeDef *GPIOx,
                      uint32_t      Pin,
                      uint32_t      Mode,
                      uint32_t      Type,
                      uint32_t      Pull,
                      uint32_t      Speed) {
  uint32_t position  = 0x00U;
  uint32_t iocurrent = 0x00U;

#if defined(STM32F0)

  while (((Pin) >> position) != 0x00u) {
    iocurrent = (Pin) & (0x00000001uL << position);

    if (iocurrent != 0x00u) {
      LL_GPIO_SetPinMode(GPIOx, iocurrent, Mode);

      if ((Mode == LL_GPIO_MODE_OUTPUT) || (Mode == LL_GPIO_MODE_ALTERNATE)) {
        LL_GPIO_SetPinSpeed(GPIOx, iocurrent, Speed);
      }

      LL_GPIO_SetPinPull(GPIOx, iocurrent, Pull);

#if 0
      if(Mode == LL_GPIO_MODE_ALTERNATE){
        if(iocurrent < LL_GPIO_PIN_8){
          LL_GPIO_SetAFPin_0_7(GPIOx, iocurrent, Alternate);
        }else{
          LL_GPIO_SetAFPin_8_15(GPIOx, iocurrent, Alternate);
        }
      }
#endif
    }
    position++;
  }

  if ((Mode == LL_GPIO_MODE_OUTPUT) || (Mode == LL_GPIO_MODE_ALTERNATE)) {
    LL_GPIO_SetPinOutputType(GPIOx, Pin, Type);
  }

#elif defined(STM32F3)
  uint32_t temp = 0x00U;

  while ((Pin >> position)) {
    iocurrent = Pin & (1U << position);

    if (iocurrent) {
      /* In case of Alternate function mode selection */
      if ((Mode == GPIO_MODE_AF_PP) || (Mode == GPIO_MODE_AF_OD)) {

        // Configure Alternate function
        temp = GPIOx->AFR[position >> 3U];
        CLEAR_BIT(temp, 0xFU << ((uint32_t) (position & 0x07U) * 4U));
        //        SET_BIT(temp, (uint32_t) (Alternate) << (((uint32_t) position & 0x07U) * 4U));
        GPIOx->AFR[position >> 3U] = temp;
      }

      // Configure IO Direction mode (Input, Output, Alternate or Analog)
      CLEAR_BIT(GPIOx->MODER, GPIO_MODER_MODER0 << (position * 2U));
      SET_BIT(GPIOx->MODER, (Mode & GPIO_MODE) << (position * 2U));

      // Output or Alternate function mode selection
      if ((Mode == GPIO_MODE_OUTPUT_PP)
          || (Mode == GPIO_MODE_AF_PP)
          || (Mode == GPIO_MODE_OUTPUT_OD)
          || (Mode == GPIO_MODE_AF_OD)) {

        CLEAR_BIT(GPIOx->OSPEEDR, GPIO_OSPEEDER_OSPEEDR0 << (position * 2U));
        SET_BIT(GPIOx->OSPEEDR, Speed << (position * 2U));

        CLEAR_BIT(GPIOx->OTYPER, GPIO_OTYPER_OT_0 << position);
        SET_BIT(GPIOx->OTYPER, ((Mode & GPIO_OUTPUT_TYPE) >> 4U) << position);
      }

      // Activate the Pull-up or Pull down resistor for the current IO */
      CLEAR_BIT(GPIOx->PUPDR, GPIO_PUPDR_PUPDR0 << (position * 2U));
      SET_BIT(GPIOx->PUPDR, (Pull) << (position * 2U));
    }

    position++;
  }
#elif defined(STM32F4)
  uint32_t temp       = 0x00U;
  uint32_t ioposition = 0x00U;

  /* Configure the port pins */
  for (position = 0U; position < GPIO_NUMBER; position++) {
    /* Get the IO position */
    ioposition = 0x01U << position;
    /* Get the current IO position */
    iocurrent = (uint32_t) (Pin) &ioposition;

    if (iocurrent != ioposition) {
      continue;
    }

    /*--------------------- GPIO Mode Configuration ------------------------*/
    /* In case of Alternate function mode selection */
    if ((Mode == GPIO_MODE_AF_PP) || (Mode == GPIO_MODE_AF_OD)) {
      /* Configure Alternate function mapped with the current IO */
      temp = GPIOx->AFR[position >> 3U];
      temp &= ~(0xFU << ((uint32_t) (position & 0x07U) * 4U));
      //      temp |= ((uint32_t) (Alternate) << (((uint32_t) position & 0x07U) * 4U));
      GPIOx->AFR[position >> 3U] = temp;
    }

    /* Configure IO Direction mode (Input, Output, Alternate or Analog) */
    temp = GPIOx->MODER;
    temp &= ~(GPIO_MODER_MODER0 << (position * 2U));
    temp |= ((Mode & GPIO_MODE) << (position * 2U));
    GPIOx->MODER = temp;

    /* In case of Output or Alternate function mode selection */
    if ((Mode == GPIO_MODE_OUTPUT_PP)
        || (Mode == GPIO_MODE_AF_PP)
        || (Mode == GPIO_MODE_OUTPUT_OD)
        || (Mode == GPIO_MODE_AF_OD)) {

      /* Configure the IO Speed */
      temp = GPIOx->OSPEEDR;
      temp &= ~(GPIO_OSPEEDER_OSPEEDR0 << (position * 2U));
      temp |= (Speed << (position * 2U));
      GPIOx->OSPEEDR = temp;

      /* Configure the IO Output Type */
      temp = GPIOx->OTYPER;
      temp &= ~(GPIO_OTYPER_OT_0 << position);
      temp |= (((Mode & GPIO_OUTPUT_TYPE) >> 4U) << position);
      GPIOx->OTYPER = temp;
    }

    /* Activate the Pull-up or Pull down resistor for the current IO */
    temp = GPIOx->PUPDR;
    temp &= ~(GPIO_PUPDR_PUPDR0 << (position * 2U));
    temp |= ((Pull) << (position * 2U));
    GPIOx->PUPDR = temp;
  }
#endif
}

// clang-format off
#if !defined(GPIO_GET_INDEX)
#if defined(GPIOE)
#define GPIO_GET_INDEX(__GPIOx__)    (((__GPIOx__) == (GPIOA))? 0U :\
                                      ((__GPIOx__) == (GPIOB))? 1U :\
                                      ((__GPIOx__) == (GPIOC))? 2U :\
                                      ((__GPIOx__) == (GPIOD))? 3U :\
                                      ((__GPIOx__) == (GPIOE))? 4U : 5U)
#else
#define GPIO_GET_INDEX(__GPIOx__)    (((__GPIOx__) == (GPIOA))? 0U :\
                                      ((__GPIOx__) == (GPIOB))? 1U :\
                                      ((__GPIOx__) == (GPIOC))? 2U :\
                                      ((__GPIOx__) == (GPIOD))? 3U : 4U)
#endif
#endif
// clang-format on

void GPIO_Config_Pins_EXTI(GPIO_TypeDef *GPIOx,
                           uint32_t      Pin,
                           uint32_t      Mode) {
#if defined(STM32F4)

  for (uint32_t position = 0U; position < GPIO_NUMBER; position++) {

    uint32_t ioposition = 0x01U << position;
    uint32_t iocurrent  = (uint32_t) (Pin) &ioposition;

    if (iocurrent == ioposition) {

      uint32_t temp = SYSCFG->EXTICR[position >> 2U];
      temp &= ~(0x0FU << (4U * (position & 0x03U)));
      temp |= ((uint32_t) (GPIO_GET_INDEX(GPIOx)) << (4U * (position & 0x03U)));
      SYSCFG->EXTICR[position >> 2U] = temp;

      EXTI->IMR &= ~((uint32_t) iocurrent);
      if ((Mode & GPIO_MODE_IT) == GPIO_MODE_IT) {
        EXTI->IMR |= iocurrent;
      }

      EXTI->EMR &= ~((uint32_t) iocurrent);
      if ((Mode & GPIO_MODE_EVT) == GPIO_MODE_EVT) {
        EXTI->EMR |= iocurrent;
      }

      EXTI->RTSR &= ~((uint32_t) iocurrent);
      if ((Mode & RISING_EDGE) == RISING_EDGE) {
        EXTI->RTSR |= iocurrent;
      }

      EXTI->FTSR &= ~((uint32_t) iocurrent);
      if ((Mode & FALLING_EDGE) == FALLING_EDGE) {
        EXTI->FTSR |= iocurrent;
      }
    }
  }
#elif defined(STM32G4)

#error("Implement IO routines")

#else

  uint32_t position = 0x00U;

  SET_BIT(RCC->APB2ENR, RCC_APB2ENR_SYSCFGEN);

  while ((Pin >> position)) {

    uint32_t iocurrent = Pin & (1U << position);

    if (iocurrent) {

      uint32_t temp = SYSCFG->EXTICR[position >> 2U];
      CLEAR_BIT(temp, (0x0FU) << (4U * (position & 0x03U)));
      SET_BIT(temp, (GPIO_GET_INDEX(GPIOx)) << (4U * (position & 0x03U)));
      SYSCFG->EXTICR[position >> 2U] = temp;

      CLEAR_BIT(EXTI->IMR, (uint32_t) iocurrent);
      if ((Mode & GPIO_MODE_IT) == GPIO_MODE_IT) {
        SET_BIT(EXTI->IMR, iocurrent);
      }

      CLEAR_BIT(EXTI->EMR, (uint32_t) iocurrent);
      if ((Mode & GPIO_MODE_EVT) == GPIO_MODE_EVT) {
        SET_BIT(EXTI->EMR, iocurrent);
      }

      CLEAR_BIT(EXTI->RTSR, (uint32_t) iocurrent);
      if ((Mode & RISING_EDGE) == RISING_EDGE) {
        SET_BIT(EXTI->RTSR, iocurrent);
      }

      CLEAR_BIT(EXTI->FTSR, (uint32_t) iocurrent);
      if ((Mode & FALLING_EDGE) == FALLING_EDGE) {
        SET_BIT(EXTI->FTSR, iocurrent);
      }
    }
    position++;
  }
#endif
}

void GPIO_DeConfig_Pins(GPIO_TypeDef *GPIOx,
                        uint32_t      GPIO_Pin) {

  uint32_t position = 0;

  while ((GPIO_Pin >> position) != RESET) {

    uint32_t iocurrent = GPIO_Pin & (1U << position);

    if (iocurrent) {
      // Configure IO Direction in Input Floating Mode
      CLEAR_BIT(GPIOx->MODER, GPIO_MODER_MODER0 << (position * 2U));

      // Configure the default Alternate Function
      CLEAR_BIT(GPIOx->AFR[position >> 3U], 0xFU << ((uint32_t) (position & 0x07U) * 4U));

      // Clear IO Speed
      CLEAR_BIT(GPIOx->OSPEEDR, GPIO_OSPEEDER_OSPEEDR0 << (position * 2U));

      // Clear IO Output Type
      CLEAR_BIT(GPIOx->OTYPER, GPIO_OTYPER_OT_0 << position);

      // Deactivate the Pull-up and Pull-down resistor
      CLEAR_BIT(GPIOx->PUPDR, GPIO_PUPDR_PUPDR0 << (position * 2U));

      // Clear EXTI Configuration
      uint32_t tmp = SYSCFG->EXTICR[position >> 2U];
      tmp &= ((0x0FU) << (4U * (position & 0x03U)));
      if (tmp == (GPIO_GET_INDEX(GPIOx) << (4U * (position & 0x03U)))) {
        tmp = (0x0FU) << (4U * (position & 0x03U));
        CLEAR_BIT(SYSCFG->EXTICR[position >> 2U], tmp);

        /* Clear EXTI line configuration */
        CLEAR_BIT(EXTI->IMR, (uint32_t) iocurrent);
        CLEAR_BIT(EXTI->EMR, (uint32_t) iocurrent);

        /* Clear Rising Falling edge configuration */
        CLEAR_BIT(EXTI->RTSR, (uint32_t) iocurrent);
        CLEAR_BIT(EXTI->FTSR, (uint32_t) iocurrent);
      }
    }

    position++;
  }
}

#endif
/**
 * I2C
 */
// clang-format off
#define MAX_NBYTE_SIZE                                                255
/** @defgroup I2C_RELOAD_END_MODE I2C Reload End Mode
  * @{
  */
#define  I2C_RELOAD_MODE                I2C_CR2_RELOAD
#define  I2C_AUTOEND_MODE               I2C_CR2_AUTOEND
#define  I2C_SOFTEND_MODE               (0x00000000U)
/** @defgroup I2C_START_STOP_MODE I2C Start or Stop Mode
  * @{
  */
#define  I2C_NO_STARTSTOP               (0x00000000U)
#define  I2C_GENERATE_STOP              (uint32_t)(0x80000000U | I2C_CR2_STOP)
#define  I2C_GENERATE_START_READ        (uint32_t)(0x80000000U | I2C_CR2_START | I2C_CR2_RD_WRN)
#define  I2C_GENERATE_START_WRITE       (uint32_t)(0x80000000U | I2C_CR2_START)

// clang-format on


bool I2C_Read(I2C_TypeDef  *hi2c,
              uint8_t       address_7bit,
              size_t        register_address_length,
              const uint8_t register_address[],
              size_t        data_length,
              uint8_t      *data,
              uint32_t      timeout_ms) {

#if defined(STM32F3)

  // Setup I2C transfer
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        register_address_length,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_WRITE);

  // Dummy write of the target register address
  for (int i = 0; i < register_address_length; ++i) {

    // Wait for TX buffer to be empty
    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    }

    // Send some data
    LL_I2C_TransmitData8(hi2c, register_address[i]);
  }

  // Wait for TX buffer to be empty
  const uint32_t ts_end_tx_empty = System_Tick + timeout_ms;

  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_tx_empty) {
      return false;
    }
    __asm__ volatile("nop");
  }

  // SETUP Read
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        data_length,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_READ);

  // READ data
  for (int j = 0; j < data_length; ++j) {

    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_RXNE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    }
    data[j] = LL_I2C_ReceiveData8(hi2c);
  }

  const uint32_t ts_end_stop = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_STOP(hi2c)) {
    if (System_Tick > ts_end_stop) {
      return false;
    }
    __asm__ volatile("nop");
  }

  LL_I2C_ClearFlag_STOP(hi2c);

#elif defined(STM32F4)

  //  LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_ACK);

  LL_I2C_GenerateStartCondition(hi2c);

  // Wait for SB flag
  for (const int64_t ts_end = System_Tick + timeout_ms;
       !LL_I2C_IsActiveFlag_SB(hi2c);) {
    if (System_Tick > ts_end) {
      return false;
    }
  }

  LL_I2C_TransmitData8(hi2c, address_7bit);

  for (const int64_t ts_end = System_Tick + timeout_ms;
       !LL_I2C_IsActiveFlag_ADDR(hi2c);) {
    if ((int64_t) System_Tick > ts_end) {
      return false;
    }
  }

  LL_I2C_ClearFlag_ADDR(hi2c);

  // Write the register address
  for (int i = 0; i < register_address_length; ++i) {

    // Wait for TX buffer to be empty
    for (const int64_t ts_end = System_Tick + timeout_ms;
         !LL_I2C_IsActiveFlag_TXE(hi2c);) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    }

    // Send some data
    LL_I2C_TransmitData8(hi2c, register_address[i]);
  }

  /**
   * Read the data
   */

  // Send Start
  LL_I2C_GenerateStartCondition(hi2c);

  // Enable Acknowledge
  LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_ACK);

  // Wait for SB flag to be set
  for (const int64_t ts_end = System_Tick + timeout_ms;
       !LL_I2C_IsActiveFlag_SB(hi2c);) {
    if (System_Tick > ts_end) {
      return false;
    }
  }

  // Send target address (+ Read bit)
  LL_I2C_TransmitData8(hi2c, address_7bit | 0x01);

  for (const int64_t ts_end = System_Tick + timeout_ms;
       !LL_I2C_IsActiveFlag_ADDR(hi2c);) {
    if (System_Tick > ts_end) {
      return false;
    }
  };

  LL_I2C_ClearFlag_ADDR(hi2c);

  // Start Reading
  size_t offset = 0;

  // TODO: Kinda of a hack...
  if (data_length == 1) {
    LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_NACK);
    LL_I2C_GenerateStopCondition(hi2c);
  }

  while (data_length > 0) {

    // If there is something in the RX buffer
    // Or if the transfer is ending
    if (LL_I2C_IsActiveFlag_RXNE(hi2c) || LL_I2C_IsActiveFlag_BTF(hi2c)) {

      // No. fucking. clue.
      if (data_length == 2) {
        LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_NACK);
        LL_I2C_GenerateStopCondition(hi2c);
      }

      data[offset++] = LL_I2C_ReceiveData8(hi2c);
      --data_length;
    }
  }

#elif defined(STM32G4)

  // Setup I2C transfer
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        register_address_length,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_WRITE);

  // Dummy write of the target register address
  for (int i = 0; i < register_address_length; ++i) {

    // Wait for TX buffer to be empty
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      __asm__ volatile("nop");
    };

    // Send some data
    LL_I2C_TransmitData8(hi2c, register_address[i]);
  }

  // Wait for TX buffer to be empty
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    __asm__ volatile("nop");
  };

  // SETUP Read
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        data_length,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_READ);

  // READ data
  for (int j = 0; j < data_length; ++j) {
    while (!LL_I2C_IsActiveFlag_RXNE(hi2c)) {
      __asm__ volatile("nop");
    }
    data[j] = LL_I2C_ReceiveData8(hi2c);
  }

  while (!LL_I2C_IsActiveFlag_STOP(hi2c)) {
    __asm__ volatile("nop");
  }

  LL_I2C_ClearFlag_STOP(hi2c);

#endif
  return true;
}

bool I2C_Read_Register(I2C_TypeDef *hi2c,
                       uint8_t      address_7bit,
                       uint8_t      register_address,
                       size_t       data_length,
                       uint8_t     *data,
                       uint32_t     timeout_ms) {

  return I2C_Read(hi2c,
                  address_7bit,
                  1,
                  &register_address,
                  data_length,
                  data,
                  timeout_ms);
}

bool I2C_Write(I2C_TypeDef   *hi2c,
               uint8_t        address_7bit,
               size_t         data_length,
               const uint8_t *data,
               uint32_t       timeout_ms) {

#if defined(STM32F3)

  // Time out
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        data_length,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_WRITE);

  // Write the data
  for (int i = 0; i < data_length; ++i) {

    // Wait for TX buffer to be empty
    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    }

    // Send some data
    LL_I2C_TransmitData8(hi2c, data[i]);
  }

  // Wait for TX buffer to be empty
  const uint32_t ts_end = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end) {
      return false;
    }
    __asm__ volatile("nop");
  }

#elif defined(STM32F4)

  //  LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_ACK);

  LL_I2C_GenerateStartCondition(hi2c);

  // Wait for SB flag
  const uint32_t ts_end_sb = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_SB(hi2c)) {

    if (System_Tick > ts_end_sb) {
      return false;
    }
    __asm__ volatile("nop");
  }

  // Dummy write of address + write bit
  LL_I2C_TransmitData8(hi2c, address_7bit | 0x00);

  // Wait for ADDR flag
  const uint32_t ts_end_addr = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_ADDR(hi2c)) {

    if (System_Tick > ts_end_addr) {
      return false;
    }
    __asm__ volatile("nop");
  };

  LL_I2C_ClearFlag_ADDR(hi2c);

  // Write the data
  for (int i = 0; i < data_length; ++i) {

    // Wait for TX buffer to be empty
    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    };

    // Send some data
    LL_I2C_TransmitData8(hi2c, data[i]);
  }

  const uint32_t ts_end_txe = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_txe) {
      return false;
    }
    __asm__ volatile("nop");
  };

  LL_I2C_GenerateStopCondition(hi2c);

#endif
  return true;
}

bool I2C_Write_Register(I2C_TypeDef   *hi2c,
                        uint8_t        address_7bit,
                        uint8_t        register_address,
                        size_t         data_length,
                        const uint8_t *data,
                        uint32_t       timeout_ms) {

#if defined(STM32F3)

  // Time out
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        data_length + 1,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_WRITE);

  // Wait for TX buffer to be empty
  const uint32_t ts_end_txe_a = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_txe_a) {
      return false;
    }
  };

  // Write the register
  LL_I2C_TransmitData8(hi2c, register_address);

  // Write the data
  for (int i = 0; i < data_length; ++i) {

    // Wait for TX buffer to be empty
    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    }

    // Send some data
    LL_I2C_TransmitData8(hi2c, data[i]);
  }

  // Wait for TX buffer to be empty
  const uint32_t ts_end_txe = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_txe) {
      return false;
    }
    __asm__ volatile("nop");
  }

  //  LL_I2C_GenerateStopCondition(hi2c);

#elif defined(STM32F4)

  //  LL_I2C_AcknowledgeNextData(hi2c, LL_I2C_ACK);

  LL_I2C_GenerateStartCondition(hi2c);

  // Wait for SB flag
  const uint32_t ts_end_sb = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_SB(hi2c)) {
    if (System_Tick > ts_end_sb) {
      return false;
    }
    __asm__ volatile("nop");
  }

  // Dummy write of address + write bit
  LL_I2C_TransmitData8(hi2c, address_7bit | 0x00);

  // Wait for ADDR flag (in master mode this waits for the address to be sent)
  const uint32_t ts_end_addr = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_ADDR(hi2c)) {
    if (System_Tick > ts_end_addr) {
      return false;
    }
    __asm__ volatile("nop");
  };

  LL_I2C_ClearFlag_ADDR(hi2c);

  // Wait for TX buffer to be empty
  const uint32_t ts_end_txe = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_txe) {
      return false;
    }
    __asm__ volatile("nop");
  };

  // Write the register
  LL_I2C_TransmitData8(hi2c, register_address);

  // Write the data
  for (int i = 0; i < data_length; ++i) {

    // Wait for TX buffer to be empty
    const uint32_t ts_end = System_Tick + timeout_ms;
    while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
      if (System_Tick > ts_end) {
        return false;
      }
      __asm__ volatile("nop");
    };

    // Send some data
    LL_I2C_TransmitData8(hi2c, data[i]);
  }

  const uint32_t ts_end_txe2 = System_Tick + timeout_ms;
  while (!LL_I2C_IsActiveFlag_TXE(hi2c)) {
    if (System_Tick > ts_end_txe2) {
      return false;
    }
    __asm__ volatile("nop");
  };

  LL_I2C_GenerateStopCondition(hi2c);
#elif defined(STM32G4)
  LL_I2C_HandleTransfer(hi2c,
                        address_7bit,
                        LL_I2C_ADDRSLAVE_7BIT,
                        data_length + 1,
                        LL_I2C_MODE_AUTOEND,
                        LL_I2C_GENERATE_START_WRITE);
  // Wait for SB flag
  while (!LL_I2C_IsActiveFlag_BUSY(hi2c)) {
    __asm__ volatile("nop");
  }

  // Wait for TX buffer to be empty
  while (!LL_I2C_IsActiveFlag_TXIS(hi2c)) {
    __asm__ volatile("nop");
  };

  // Write the register
  LL_I2C_TransmitData8(hi2c, register_address);

  // Write the data
  for (int i = 0; i < data_length; ++i) {

    // Wait for TX buffer to be empty
    while (!LL_I2C_IsActiveFlag_TXIS(hi2c)) {
      __asm__ volatile("nop");
    };

    // Send some data
    LL_I2C_TransmitData8(hi2c, data[i]);
  }

#endif
  return true;
}

/**
 * EEPROM
 */

bool EEPROM_Page_Read(I2C_TypeDef *hi2c,
                      uint8_t      address_7bit,
                      uint8_t      start_page,
                      size_t       byte_count,
                      uint8_t     *p) {

  // Temporary buffer
  uint8_t buffer[EEPROM_PAGE_SIZE];
  uint8_t register_address[EEPROM_ADDRESS_SIZE] = {0};
  size_t  page_count                            = (byte_count + EEPROM_PAGE_SIZE - 1) / EEPROM_PAGE_SIZE;

  // Read in pages
  for (int i = 0; i < page_count; ++i) {
    size_t bytes_in_page = MIN(byte_count, EEPROM_PAGE_SIZE);
    size_t eeprom_offset = (start_page + i) * EEPROM_PAGE_SIZE;

    // Get the "register" address (TODO: Fix this, we don't have access to the entire chip)

#if EEPROM_ADDRESS_SIZE == 1
    // AT24C changes i2c address based on memory location, otherwise can't access full chip.
    address_7bit |= ((0x300 & eeprom_offset) >> 7);
#elif EEPROM_ADDRESS_SIZE == 2
    register_address[0] = 0xFF & (eeprom_offset >> 8);
#endif
    register_address[EEPROM_ADDRESS_SIZE - 1] = 0xFF & eeprom_offset;

    // Read the page data
    if (!I2C_Read(hi2c,
                  address_7bit,
                  EEPROM_ADDRESS_SIZE,
                  register_address,
                  bytes_in_page,
                  buffer,
                  eeprom_timeout_ms)) {
      return false;
    }

    // Copy data over
    for (int j = 0; j < bytes_in_page; ++j) {
      *(p++) = buffer[j];
    }
    byte_count -= bytes_in_page;

    // If this isn't here, things don't work...
    Busy_Wait(1);
  }

  return true;
}

/**
 *
 * This writes a series of pages to the EEPROM
 *
 * @param hi2c
 * @param chip_address_7bit
 * @param start_page
 * @param byte_count
 * @param p
 * @return
 */
bool EEPROM_Page_Write(I2C_TypeDef *i2c,
                       uint8_t      address_7bit,
                       uint8_t      start_page,
                       size_t       byte_count,
                       uint8_t     *data) {

  uint8_t buffer[EEPROM_PAGE_SIZE + EEPROM_ADDRESS_SIZE];

  // Can write to the EEPROM in pages only
  size_t page_count = (byte_count + EEPROM_PAGE_SIZE - 1) / EEPROM_PAGE_SIZE;

  // Write loop
  for (int i = 0; i < page_count; ++i) {
    size_t bytes_in_page = MIN(byte_count, EEPROM_PAGE_SIZE);
    size_t eeprom_offset = (start_page + i) * EEPROM_PAGE_SIZE;

#if !(defined(ENABLE_BOOTLOADER_FUNCTIONS) && ENABLE_BOOTLOADER_FUNCTIONS == 1)
    // Only bootloader is allowed to write to the first 256
    if (eeprom_offset < 256) {
      // This should never happen!! so when it does things have gone really bad...
      // so just 'exit' and hope things work themselves out.
      return false;
    }
#endif

#if EEPROM_ADDRESS_SIZE == 1
    // AT24C8C uses the 2 MSBs of the memory address as the 2 LSB of the device address.
    // We shift by seven here because we are going to add it to a 7bit address
    address_7bit |= (0x300 & eeprom_offset) >> 7;
#elif EEPROM_ADDRESS_SIZE == 2
    buffer[0] = 0xFF & (eeprom_offset >> 8);
#endif

    buffer[EEPROM_ADDRESS_SIZE - 1] = 0xFF & eeprom_offset;

    for (int j = 0; j < bytes_in_page; ++j) {
      buffer[EEPROM_ADDRESS_SIZE + j] = *(data++);
    }

    if (!I2C_Write(i2c,
                   address_7bit,
                   bytes_in_page + EEPROM_ADDRESS_SIZE,
                   buffer,
                   eeprom_timeout_ms)) {
      return false;
    }

    byte_count -= bytes_in_page;

    // Write cycle for AT24C0x is 5ms
    Busy_Wait(5);
  }
  return true;
}

/**
 *
 * These can only read 1 page or less.
 *
 * @param i2c
 * @param chip_address_7bit
 * @param address
 * @param byte_count
 * @param p
 * @return
 */
bool EEPROM_Short_Read(I2C_TypeDef *i2c,
                       uint8_t      address_7bit,
                       uint16_t     address,
                       size_t       byte_count,
                       uint8_t     *p) {

  uint8_t register_address[EEPROM_ADDRESS_SIZE] = {
#if EEPROM_ADDRESS_SIZE == 2
    0xFF & (address >> 8),
#endif
    0xFF & address
  };

#if EEPROM_ADDRESS_SIZE == 1
  address_7bit |= ((0x300 & address) >> 7);
#endif

  return I2C_Read(i2c,
                  address_7bit,
                  EEPROM_ADDRESS_SIZE,
                  register_address,
                  byte_count,
                  p,
                  eeprom_timeout_ms);
}

bool EEPROM_Short_Write(I2C_TypeDef   *i2c,
                        uint8_t        address_7bit,
                        uint16_t       eeprom_address,
                        size_t         data_size,
                        const uint8_t *p) {

  const uint8_t page_size    = EEPROM_PAGE_SIZE;
  const uint8_t address_size = EEPROM_ADDRESS_SIZE;
  uint8_t       buffer[page_size + address_size];
  size_t        byte_count = MIN(data_size, page_size);

#if !(defined(ENABLE_BOOTLOADER_FUNCTIONS) && ENABLE_BOOTLOADER_FUNCTIONS == 1)
  // Only bootloader is allowed to write to the first 256 bytes
  if (eeprom_address < 256) {
    // This should never happen!! so when it does things have gone really bad...
    // so just 'exit' and hope things work themselves out.
    return false;
  }
#endif

#if EEPROM_ADDRESS_SIZE == 1
  // AT24C changes i2c address based on memory location, otherwise can't access full chip.
  address_7bit |= ((0x300 & eeprom_address) >> 7);
#elif EEPROM_ADDRESS_SIZE == 2
  buffer[0] = 0xFF & (eeprom_address >> 8);
#endif

  buffer[EEPROM_ADDRESS_SIZE - 1] = 0xFF & eeprom_address;

  // Copy data over
  for (int j = 0; j < byte_count; ++j) {
    buffer[address_size + j] = *(p++);
  }

  // Write it
  bool results = I2C_Write(i2c,
                           address_7bit,
                           byte_count + address_size,
                           buffer,
                           eeprom_timeout_ms);

  // Write cycle for AT24C0x is 5ms
  Busy_Wait(5);
  return results;
}

/**
 * Bootloader
 */

/**
 * Jump into the bootloader.
 *
 * This assume the bootloader is at FLASH_BASE (0x8000000)
 *
 * Places information at the first 256 bytes of program memory.
 *
 * Make sure to disable all interrupts, dma transfers, and devices before jumping or it will crash.
 */
void Bootloader_Jump(uint32_t flash_base) {

  // Get pointer to bootloader reset handler
  void (*bootloader_main)(void) = (void (*)(void))(*((uint32_t *) (flash_base + 4)));

#if defined(STM32F0)
  //  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x4000)
  // Remap to flash
  SYSCFG->CFGR1 &= ~SYSCFG_CFGR1_MEM_MODE;

#elif defined(STM32F3) || defined(STM32F4)
  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL  = 0;

  SCB->VTOR = 0x8000000;
#endif

  // Set stack pointer
  __set_MSP(*(uint32_t *) flash_base);

  // Start
  bootloader_main();
}

/**
 * Read from I2C in pages (EEPROM)
 */

/**
 * Parameters
 *
 * @returns true if the value was loaded (and therefore this parameter is dirty)
 */
bool Params_Load_Default(int                     index,
                         const parameter_info_t *info_table,
                         const uint8_t          *factory_info_base,
                         uint8_t                *param_base) {

  bool     dirty               = false;
  uint8_t *element_pointer     = ((uint8_t *) param_base + info_table[index].struct_offset);
  uint8_t *calibration_pointer = 0;

  // Check for a factory default
  if (info_table[index].calibration_offset != 0) {
    calibration_pointer = ((uint8_t *) factory_info_base + info_table[index].calibration_offset);
  }

  switch (info_table[index].type) {
    case PARAM_VALUE_BOOL:
      dirty                     = (*(bool *) element_pointer != (bool) info_table[index].i_default);
      *(bool *) element_pointer = (bool) info_table[index].i_default;
      break;

    case PARAM_VALUE_INT:
      dirty               = (*(int32_t *) element_pointer != info_table[index].i_default);
      calibration_pointer = calibration_pointer ? calibration_pointer : (uint8_t *) &info_table[index].i_default;
      // Memcpy because ints can be 8, 16, or 32 bits wide and signed / unsigned
      memcpy(element_pointer,
             calibration_pointer,
             info_table[index].variable_size);
      break;

    case PARAM_VALUE_REAL:
      dirty                      = (*(float *) element_pointer != info_table[index].f_default);
      *(float *) element_pointer = calibration_pointer ? *(float *) calibration_pointer : info_table[index].f_default;

      break;
  }
  return dirty;
}

void Params_Load_Defaults(size_t                  parameter_count,
                          const parameter_info_t *info_table,
                          const uint8_t          *factory_info_base,
                          bool                   *dirty_table,
                          uint8_t                *param_base) {

  for (int i = 0; i < parameter_count; ++i) {
    dirty_table[i] = dirty_table[i] | Params_Load_Default(i, info_table, factory_info_base, param_base);
  }
}

/**
 * This subroutine will load parameters from EEPROM if they are available
 * and it will also upgrade any parameters.
 *
 * @param i2c bus to use (e.g. I2C1, I2C2, I2C3, ... )
 * @param address_7bit
 * @param parameter_version Parameter version used by the current firmware
 * @param page_size EEPROM page size (e.g. 16)
 * @param header_start_page
 * @param parameter_start_page
 * @param parameter_count
 * @param info_table
 * @param param_base
 * @return 0 if everything loaded cleanly, otherwise status info.
 */
enum EepromLoadState Params_EEPROM_Load(I2C_TypeDef            *i2c,
                                        uint8_t                 address_7bit,
                                        uint8_t                 header_start_page,
                                        uint8_t                 parameter_start_page,
                                        int                     parameter_version,
                                        size_t                  parameter_count,
                                        const parameter_info_t *info_table,
                                        const uint8_t          *factory_info_base,
                                        bool                   *dirty_table,
                                        uint8_t                *param_base) {

  const size_t page_size        = EEPROM_PAGE_SIZE;
  const size_t parameter_offset = parameter_start_page * page_size;

  uint8_t ret_val = EEPROM_LOAD_STATE_ALL_OK;

  parameter_header_t header = {};

  // First, read the header
  if (!EEPROM_Page_Read(i2c,
                        address_7bit,
                        header_start_page,
                        sizeof(header),
                        (uint8_t *) &header)) {
    return EEPROM_LOAD_STATE_DEVICE_ERROR;
  }


  // Check if there is something valid in the EEPROM
  if (header.magic != PARAMETER_HEADER_MAGIC) {
    // If not, let the next loop load all the defaults
    header.version = 0;
  }

  // We are loading one page at a time
  uint8_t buffer[16];

  for (int i = 0; i < parameter_count; ++i) {

    // If this parameter is newer than the parameters
    // in the EEPROM then the parameter is new
    if (header.version < info_table[i].version) {
      Params_Load_Default(i, info_table, factory_info_base, param_base);
      dirty_table[i] = true;
      ret_val        = EEPROM_LOAD_STATE_SOME_PARAMS_UPGRADED;
      continue;
    }

    memset(buffer, 0, sizeof(buffer));
    EEPROM_Short_Read(i2c,
                      address_7bit,
                      info_table[i].eeprom_offset + parameter_offset,
                      info_table[i].eeprom_size,
                      buffer);

    // Copy the data into memory
    for (int j = 0; j < info_table[i].eeprom_size; ++j) {
      *(param_base + info_table[i].struct_offset + j) = buffer[j];
    }
  }

  if (header.magic != PARAMETER_HEADER_MAGIC) {
    ret_val = EEPROM_LOAD_STATE_ALL_DEFAULTS_LOADED;
  }

  return ret_val;
}

/**
 *
 * This will only save parameters that have changed in the EEPROM
 *
 * Note: Variables can not cross a page boundary!
 *
 */
bool Params_EEPROM_Save(I2C_TypeDef            *i2c,
                        uint8_t                 address_7bit,
                        uint8_t                 header_start_page,
                        uint8_t                 parameter_start_page,
                        int                     parameter_version,
                        size_t                  parameter_count,
                        const parameter_info_t *info_table,
                        bool                   *dirty_table,
                        const uint8_t          *param_base) {

  const size_t page_size        = EEPROM_PAGE_SIZE;
  const size_t parameter_offset = parameter_start_page * page_size;

  parameter_header_t header = {PARAMETER_HEADER_MAGIC,
                               parameter_version};

  uint8_t buffer[page_size];

  // Save parameters that haven't changed
  for (int i = 0; i < parameter_count; ++i) {

    if (!dirty_table[i]) {
      continue;
    }

    size_t eeprom_offset = info_table[i].eeprom_offset + parameter_offset;
    size_t eeprom_size   = info_table[i].eeprom_size;
    size_t memory_offset = info_table[i].struct_offset;

    // Copy the data
    for (int j = 0; j < eeprom_size; ++j) {
      buffer[j] = param_base[memory_offset + j];
    }

    if (!EEPROM_Short_Write(i2c,
                            address_7bit,
                            eeprom_offset,
                            eeprom_size,
                            buffer)) {
      return false;
    }
  }

  // Finally, write the header
  if (!EEPROM_Page_Write(i2c,
                         address_7bit,
                         header_start_page,
                         sizeof(header),
                         (uint8_t *) &header)) {
    return false;
  }

  // Clear out the 'dirty table'
  memset(dirty_table, 0, parameter_count * sizeof(bool));
  return true;
}

/**
 *
 * @param i2c  I2C device
 * @param address_7bit EEPROM address already shifted by 1
 * @param struct_size sizeof(factory_info_t)
 * @param param_base base of factory structure
 * @return true if the structure was loaded (header magic and crc verified)
 */
enum EepromLoadState FactoryInfo_Load(I2C_TypeDef *i2c,
                         uint8_t      address_7bit,
                         size_t       struct_size,
                         uint8_t     *param_base) {

  // EEPROM structure version and size can be different so we clear out
  // the current structure version here and later only load up to .
  for (int i = 0; i < struct_size; ++i) {
    *(param_base + i) = 0;
  }

  factory_info_header_t *header = (factory_info_header_t *) param_base;

  // Read 1 page from EEPROM
  if (!EEPROM_Page_Read(i2c,
                        address_7bit,
                        FACTORY_INFO_START_PAGE,
                        EEPROM_PAGE_SIZE,
                        param_base)) {
    return EEPROM_LOAD_STATE_DEVICE_ERROR;
  }

  if ((header->magic != FACTORY_INFO_MAGIC)
      || (struct_size < header->struct_size)) {
    return EEPROM_LOAD_STATE_ALL_DEFAULTS_LOADED;
  }

  // Read the rest of the header structure
  if (!EEPROM_Page_Read(i2c,
                        address_7bit,
                        FACTORY_INFO_START_PAGE + 1,
                        header->struct_size - EEPROM_PAGE_SIZE,
                        (uint8_t *) (param_base + EEPROM_PAGE_SIZE))) {
    return EEPROM_LOAD_STATE_DEVICE_ERROR;
  }

  uint16_t crc_eeprom = header->crc;
  header->crc         = 0U;

  // Compute CRC of the EEPROM structure so use EEPROM size!!
  uint16_t crc = 0xFFFFU;

  for (int i = 0; i < header->struct_size; ++i) {
    crc = CRC_16_Add_Byte(crc, param_base[i]);
  }

  header->crc = crc;

  // Final CRC verification
  return (crc_eeprom == crc) ? EEPROM_LOAD_STATE_ALL_OK : EEPROM_LOAD_STATE_ALL_DEFAULTS_LOADED;
}

/**
 * This will compute the CRC and save the structure
 */
bool FactoryInfo_Save(I2C_TypeDef *i2c,
                      uint8_t      address_7bit,
                      size_t       struct_size,
                      uint8_t     *param_base) {

  // First thing in the Factory Info should be the header
  factory_info_header_t *header = (factory_info_header_t *) param_base;

  header->magic          = FACTORY_INFO_MAGIC;
  header->struct_version = FACTORY_INFO_VERSION;
  header->struct_size    = struct_size;
  header->crc            = 0;

  // Compute CRC
  uint16_t crc = 0xFFFFU;

  for (int i = 0; i < struct_size; ++i) {
    crc = CRC_16_Add_Byte(crc, param_base[i]);
  }

  header->crc = crc;

  // Save structure
  return EEPROM_Page_Write(i2c,
                           address_7bit,
                           0,
                           struct_size,
                           param_base);
}

int Request_Queue_Add(request_queue_t *q,
                      uint16_t         type_id,
                      uint8_t          source_node_id,
                      uint8_t          transfer_id,
                      uint8_t          priority,
                      uint8_t          data) {

  int ix = q->end;

  q->request[ix].node_id     = source_node_id;
  q->request[ix].pending     = true;
  q->request[ix].priority    = priority;
  q->request[ix].transfer_id = transfer_id;
  q->request[ix].type_id     = type_id;
  q->request[ix].data        = data;

  q->end  = (q->end + 1) % REQUEST_QUEUE_CAPACITY;
  q->size = MIN(q->size + 1, REQUEST_QUEUE_CAPACITY);

  return ix;
}

/**
 *
 * Add to request queue only if the data type is not already in the queue.
 * Otherwise, overwrite.
 *
 *
 * @param type_id
 * @param source_node_id
 * @param transfer_id
 * @param priority
 * @param data
 * @return
 */
int Request_Queue_Add_Unique(request_queue_t *q,
                             uint16_t         type_id,
                             uint8_t          source_node_id,
                             uint8_t          transfer_id,
                             uint8_t          priority,
                             uint8_t          data) {

  for (int i = 0; i < q->size; ++i) {

    int ix = (q->top + i) % REQUEST_QUEUE_CAPACITY;

    if (q->request[ix].type_id == type_id) {
      q->request[ix].node_id     = source_node_id;
      q->request[ix].priority    = priority;
      q->request[ix].transfer_id = transfer_id;
      q->request[ix].data        = data;

      return ix;
    }
  }

  return Request_Queue_Add(q,
                           type_id,
                           source_node_id,
                           transfer_id,
                           priority,
                           data);
}

/**
 * Remove the top event in the queue
 *
 * @param q
 */
void Request_Queue_Pop(request_queue_t *q) {
  // Update queue pointers
  q->request[q->top].pending = false;
  q->size                    = MAX(0, (q->size - 1));
  q->top                     = (q->top + 1) % REQUEST_QUEUE_CAPACITY;
}
