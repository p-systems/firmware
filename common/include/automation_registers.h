//
// Created by stou on 5/5/21.
//

#ifndef P_SYSTEMS_AUTOMATION_REGISTERS_H
#define P_SYSTEMS_AUTOMATION_REGISTERS_H


/**
 * Register Configuration
 */

#define REGISTER_DRIVE_STATUS                                         100

// Is the drive powered on
#define REGISTER_DRIVE_ENABLED                                        101

// [RO]
#define REGISTER_DRIVE_CURRENT_STEP                                   110
#define REGISTER_DRIVE_CURRENT_ANGLE                                  111
#define REGISTER_DRIVE_CURRENT_POSITION                               112

#define REGISTER_DRIVE_CURRENT_VELOCITY_STEP_S                        120
#define REGISTER_DRIVE_CURRENT_VELOCITY_DEG_MIN                       121
#define REGISTER_DRIVE_CURRENT_VELOCITY_MM_MIN                        122

#define REGISTER_DRIVE_CURRENT_ACCELERATION_STEP_S_S                  130
#define REGISTER_DRIVE_CURRENT_ACCELERATION_DEG_MIN_S                 131
#define REGISTER_DRIVE_CURRENT_ACCELERATION_MM_MIN_S                  132

#define REGISTER_SOLENOID_VALVE_OPEN                                  150

// [RW]
#define REGISTER_DRIVE_SETPOINT_STEP                                  210
#define REGISTER_DRIVE_SETPOINT_ANGLE_DEG                             211
// Units: 1/10,000 of mm
#define REGISTER_DRIVE_SETPOINT_POSITION                              212

#define REGISTER_DRIVE_SETPOINT_VELOCITY_STEP_S                       220
#define REGISTER_DRIVE_SETPOINT_VELOCITY_DEG_MIN                      221
#define REGISTER_DRIVE_SETPOINT_VELOCITY_MM_MIN                       222

#define REGISTER_DRIVE_SETPOINT_ACCELERATION_STEP_S_S                 230
#define REGISTER_DRIVE_SETPOINT_ACCELERATION_DEG_MIN_S                231
#define REGISTER_DRIVE_SETPOINT_ACCELERATION_MM_MIN_S                 232

/**
 * Feeders
 */
#define REGISTER_FEEDER_ID                                            160
#define REGISTER_FEEDER_FIRMWARE                                      161
#define REGISTER_FEEDER_EEPROM                                        162
#define REGISTER_FEEDER_PITCH                                         163

// Write 1 to open pick-up window, 2 to advance, 100 to run self test, 0 to stop self test
#define REGISTER_FEEDER_ACTION                                        165

#define REGISTER_FEEDER_STATUS                                        166

#define REGISTER_FEEDER_RESET_COUNT                                   171
#define REGISTER_FEEDER_FEED_COUNT                                    172
#define REGISTER_FEEDER_ERROR_42_COUNT                                173
#define REGISTER_FEEDER_ERROR_43_COUNT                                174
#define REGISTER_FEEDER_ERROR_44_COUNT                                175

// Misc

// PWM Output (0.0 to 1.0)
#define REGISTER_PWM_OUTPUT_VALUE                                      42



#endif //P_SYSTEMS_AUTOMATION_REGISTERS_H
