/**
 * @file
 * @brief stm32 interface to INA219
 * @author Stou Sandalski <stou@p-systems.io>
 * @license MIT
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#if defined(STM32F0)
#include "stm32f0xx_ll_i2c.h"
#elif defined(STM32F3)
#include "stm32f3xx_ll_i2c.h"
#elif defined(STM32F4)
#include "stm32f4xx_ll_i2c.h"
#endif


// Configuration
static const uint8_t  INA219_BASE_ADDRESS          = 0x40;
static const uint16_t INA219_CONFIGURATION_DEFAULT = 0x399F;

// Registers
static const uint8_t INA219_REGISTER_CONFIGURATION = 0x00;
static const uint8_t INA219_REGISTER_VOLTAGE_SHUNT = 0x01;
static const uint8_t INA219_REGISTER_VOLTAGE_BUS   = 0x02;
static const uint8_t INA219_REGISTER_POWER         = 0x03;
static const uint8_t INA219_REGISTER_CURRENT       = 0x04;
static const uint8_t INA219_REGISTER_CALIBRATION   = 0x05;

// Constants for Configuration register
static const uint16_t INA219_CONFIG_BRNG_16V  = 0x0000;
static const uint16_t INA219_CONFIG_BRNG_32V  = 0x2000;
static const uint16_t INA219_CONFIG_PGA_40mV  = 0x0000;
static const uint16_t INA219_CONFIG_PGA_80mV  = 0x0800;
static const uint16_t INA219_CONFIG_PGA_160mV = 0x1000;
static const uint16_t INA219_CONFIG_PGA_320mV = 0x8800;

// Constants for BUS ADC resolution
static const uint16_t INA219_CONFIG_BADC_9bit   = 0x0000;
static const uint16_t INA219_CONFIG_BADC_10bit  = 0x0080;
static const uint16_t INA219_CONFIG_BADC_11bit  = 0x0100;
static const uint16_t INA219_CONFIG_BADC_12bit  = 0x0180;
static const uint16_t INA219_CONFIG_BADC_1spl   = 0x0400;
static const uint16_t INA219_CONFIG_BADC_2spl   = 0x0480;
static const uint16_t INA219_CONFIG_BADC_4spl   = 0x0500;
static const uint16_t INA219_CONFIG_BADC_8spl   = 0x0580;
static const uint16_t INA219_CONFIG_BADC_16spl  = 0x0600;
static const uint16_t INA219_CONFIG_BADC_32spl  = 0x0680;
static const uint16_t INA219_CONFIG_BADC_64spl  = 0x0700;
static const uint16_t INA219_CONFIG_BADC_128spl = 0x0780;

// Constants for Shunt ADC resolution
static const uint16_t INA219_CONFIG_SADC_9bit   = 0x0000;
static const uint16_t INA219_CONFIG_SADC_10bit  = 0x0008;
static const uint16_t INA219_CONFIG_SADC_11bit  = 0x0010;
static const uint16_t INA219_CONFIG_SADC_12bit  = 0x0018;
static const uint16_t INA219_CONFIG_SADC_1spl   = 0x0040;
static const uint16_t INA219_CONFIG_SADC_2spl   = 0x0048;
static const uint16_t INA219_CONFIG_SADC_4spl   = 0x0050;
static const uint16_t INA219_CONFIG_SADC_8spl   = 0x0058;
static const uint16_t INA219_CONFIG_SADC_16spl  = 0x0060;
static const uint16_t INA219_CONFIG_SADC_32spl  = 0x0068;
static const uint16_t INA219_CONFIG_SADC_64spl  = 0x0070;
static const uint16_t INA219_CONFIG_SADC_128spl = 0x0078;

static const uint16_t INA219_CONFIG_MODE_POWER_DOWN     = 0x0001;
static const uint16_t INA219_CONFIG_MODE_SHUNT_TRIG     = 0x0002;
static const uint16_t INA219_CONFIG_MODE_BUS_TRIG       = 0x0003;
static const uint16_t INA219_CONFIG_MODE_SHUNT_BUS_TRIG = 0x0004;
static const uint16_t INA219_CONFIG_MODE_SHUNT_CONT     = 0x0005;
static const uint16_t INA219_CONFIG_MODE_BUS_CONT       = 0x0006;
static const uint16_t INA219_CONFIG_MODE_SHUNT_BUS_CONT = 0x0007;

/**
 * Access Functions
 */

bool INA219_Config(I2C_TypeDef *hi2c,
                   uint16_t     address_7_bit,
                   uint16_t     config);

bool INA219_Readout(I2C_TypeDef *hi2c,
                    uint16_t     address_7_bit,
                    int32_t     *V_shunt,
                    int32_t     *V_bus);

bool INA219_Test(I2C_TypeDef *i2c,
                 uint16_t     address_7_bit);
