/**
 * @brief Simplified MCP9808 access library
 * @copyright 2023 Pomegranate Systems LLC
 * @copyright 2016 Michael Blouin <michaelblouin.ca>
 * @license Apache 2.0
 * @see https://github.com/Mobius5150/mcp9808
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#if defined(STM32F0)
#include "stm32f0xx_ll_i2c.h"
#elif defined(STM32F3)
#include "stm32f3xx_ll_i2c.h"
#elif defined(STM32F4)
#include "stm32f4xx_ll_i2c.h"
#endif


/**
 * Constants
 */

// Configuration
static const uint8_t  MCP9808_BASE_ADDRESS          = 0x18;
static const uint8_t  MCP9808_CONFIGURATION_DEFAULT = 0x1F;
static const uint16_t MCP9808_DEVICE_ID             = 0x0400;
static const uint16_t MCP9808_MANUFACTURER_ID       = 0x54;

// Registers
static const uint8_t MCP9808_REGISTER_CONFIGURATION   = 0x01;
static const uint8_t MCP9808_REGISTER_T_UPPER         = 0x02;
static const uint8_t MCP9808_REGISTER_T_LOWER         = 0x03;
static const uint8_t MCP9808_REGISTER_T_CRIT          = 0x04;
static const uint8_t MCP9808_REGISTER_T               = 0x05;
static const uint8_t MCP9808_REGISTER_MANUFACTURER_ID = 0x06;
static const uint8_t MCP9808_REGISTER_DEVICE_ID       = 0x07;
static const uint8_t MCP9808_REGISTER_RESOLUTION      = 0x08;

// Configuration Constants

// Resolution(30, 65, 130, 250 ms conversion time)
static const uint8_t MCP9808_RESOLUTION_05   = 0x00;
static const uint8_t MCP9808_RESOLUTION_025  = 0x01;
static const uint8_t MCP9808_RESOLUTION_0125 = 0x02;
static const uint8_t MCP9808_RESOLUTION_0625 = 0x03;

// Hystersis for T_upper and T_Lower (0C, 1.5C, 3C, 6C)
static const uint8_t MCP9808_T_HYST_0   = 0x00;
static const uint8_t MCP9808_T_HYST_1_5 = 0x01;
static const uint8_t MCP9808_T_HYST_3_0 = 0x02;
static const uint8_t MCP9808_T_HYST_6_0 = 0x03;

/**
 * API
 */
bool MCP9808_Readout(I2C_TypeDef *hi2c,
                     uint8_t      address_7_bit,
                     float       *output);
