/**
 * @file
 * @brief DroneCAN v0 Serialization and Transmission Code
 * @version 20
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @copyright (c) 2019 - STMicroelectronics
 * @copyright (c) 2017 - UAVCAN Team
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 *
 * @note This is mostly written from scratch except for some of the low-level data encoding / decoding routines.
 */

#pragma once

#include "stm32_util.h"

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/**
 * Global Definitions
 */

// Number of packet reassembly buffers to reserve. Each buffer needs a bit more than UAVCAN_MAX_PAYLOAD_SIZE bytes
#ifndef REASSEMBLY_BUFFERS
#define REASSEMBLY_BUFFERS                                            16
#endif

// Discard packets with life longer than this [ms]
#ifndef REASSEMBLY_TIMEOUT_MS
#define REASSEMBLY_TIMEOUT_MS                                         10
#endif

#define UAVCAN_CAN_FRAME_MAX_DATA_LEN                                 8U

/// Transfer priority definitions
#define UAVCAN_TRANSFER_PRIORITY_HIGHEST                              0
#define UAVCAN_TRANSFER_PRIORITY_HIGH                                 8
#define UAVCAN_TRANSFER_PRIORITY_MEDIUM                               16
#define UAVCAN_TRANSFER_PRIORITY_LOW                                  24
#define UAVCAN_TRANSFER_PRIORITY_LOWEST                               31

/**
 * UAVCAN Message Definitions
 *
 * Run https://raw.githubusercontent.com/UAVCAN/libcanard/master/show_data_type_info.py
 *
 * For the filter ID mask look at the can_id structure in uavcan. Bytes 1,2 are the message type_id
 *
 */

// uavcan.equipment.actuator

// uavcan.equipment.actuator.array_command
#define UAVCAN_EQUIPMENT_ACTUATOR_ARRAY_COMMAND_ID                    1010
#define UAVCAN_EQUIPMENT_ACTUATOR_ARRAY_COMMAND_SIGNATURE             0xd8a7486238ec3af3
#define UAVCAN_EQUIPMENT_ACTUATOR_ARRAY_COMMAND_MAX_SIZE              ((484 + 7) / 8)
#define UAVCAN_EQUIPMENT_ACTUATOR_ARRAY_COMMAND_FILTER_ID             0x0003F200
#define UAVCAN_EQUIPMENT_ACTUATOR_ARRAY_COMMAND_FILTER_MASK           0x00FFFF80

// uavcan.equipment.actuator.Command.command_type
#define UAVCAN_EQUIPMENT_ACTUATOR_COMMAND_TYPE_UNITLESS               0
#define UAVCAN_EQUIPMENT_ACTUATOR_COMMAND_TYPE_POSITION               1
#define UAVCAN_EQUIPMENT_ACTUATOR_COMMAND_TYPE_FORCE                  2
#define UAVCAN_EQUIPMENT_ACTUATOR_COMMAND_TYPE_SPEED                  3

// uavcan.equipment.actuator.status
#define UAVCAN_EQUIPMENT_ACTUATOR_STATUS_ID                           1011
#define UAVCAN_EQUIPMENT_ACTUATOR_STATUS_SIGNATURE                    0x5e9bba44faf1ea04
#define UAVCAN_EQUIPMENT_ACTUATOR_STATUS_MAX_SIZE                     ((64 + 7) / 8)

// uavcan.equipment.air_data

// uavcan.equipment.air_data.StaticPressure messages
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_ID                  1028
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_SIGNATURE           0xcdc7c43412bdc89a
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_PRESSURE_MAX_SIZE            ((48 + 7) / 8)

// uavcan.equipment.air_data.StaticTemperature messages
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_ID               1029
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_SIGNATURE        0x49272a6477d96271
#define UAVCAN_EQUIPMENT_AIR_DATA_STATIC_TEMPERATURE_MAX_SIZE         ((32 + 7) / 8)

// uavcan.equipment.ahrs

// uavcan.equipment.ahrs.MagneticFieldStrength2
#define UAVCAN_EQUIPMENT_AHRS_MAGNETIC_FIELD_STRENGTH2_ID             1002
#define UAVCAN_EQUIPMENT_AHRS_MAGNETIC_FIELD_STRENGTH2_SIGNATURE      0xb6ac0c442430297e
#define UAVCAN_EQUIPMENT_AHRS_MAGNETIC_FIELD_STRENGTH2_MAX_SIZE       ((204 + 7) / 8)

// uavcan.equipment.device

// uavcan.equipment.device.temperature
#define UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_ID                        1110
#define UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_SIGNATURE                 0x70261c28a94144c6
#define UAVCAN_EQUIPMENT_DEVICE_TEMPERATURE_MAX_SIZE                  ((40 + 7) / 8)

#define UAVCAN_DEVICE_TEMPERATURE_ERROR_FLAG_OVERHEATING              1U
#define UAVCAN_DEVICE_TEMPERATURE_ERROR_FLAG_OVERCOOLING              2U


// uavcan.equipment.esc

// uavcan.equipment.esc.RAWCommand
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_ID                           1030
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_SIGNATURE                    0x217f5c87d7ec951d
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_MAX_SIZE                     ((285 + 7) / 8)
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_MAX_VALUE                    8191
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_FILTER_ID                    0x00040600
#define UAVCAN_EQUIPMENT_ESC_RAW_COMMAND_FILTER_MASK                  0x00FFFF80

// uavcan.equipment.esc.RPMCommand
#define UAVCAN_EQUIPMENT_ESC_RPM_COMMAND_ID                           1031
#define UAVCAN_EQUIPMENT_ESC_RPM_COMMAND_SIGNATURE                    0xce0f9f621cf7e70b
#define UAVCAN_EQUIPMENT_ESC_RPM_COMMAND_MAX_SIZE                     ((365 + 7) / 8)
#define UAVCAN_EQUIPMENT_ESC_RPM_COMMAND_FILTER_ID                    0x00040700
#define UAVCAN_EQUIPMENT_ESC_RPM_COMMAND_FILTER_MASK                  0x00FFFF80

// Filter Mask / Id for ESC command
#define UAVCAN_EQUIPMENT_ESC_COMMAND_FILTER_ID                        0x00040600
#define UAVCAN_EQUIPMENT_ESC_COMMAND_FILTER_MASK                      0x00FFFE80

// uavcan.equipment.esc.Status
#define UAVCAN_EQUIPMENT_ESC_STATUS_ID                                1034
#define UAVCAN_EQUIPMENT_ESC_STATUS_SIGNATURE                         0xa9af28aea2fbb254
#define UAVCAN_EQUIPMENT_ESC_STATUS_MAX_SIZE                          ((110 + 7) / 8)

// uavcan.equipment.hardpoint

// uavcan.equipment.hardpoint.Command
#define UAVCAN_EQUIPMENT_HARDPOINT_COMMAND_ID                         1070
#define UAVCAN_EQUIPMENT_HARDPOINT_COMMAND_SIGNATURE                  0xa1a036268b0c3455
#define UAVCAN_EQUIPMENT_HARDPOINT_COMMAND_MAX_SIZE                   ((24 + 7) / 8)
#define UAVCAN_EQUIPMENT_HARDPOINT_COMMAND_FILTER_ID                  0x00042E00
#define UAVCAN_EQUIPMENT_HARDPOINT_COMMAND_FILTER_MASK                0x00FFFF80

// uavcan.equipment.hardpoint.Status
#define UAVCAN_EQUIPMENT_HARDPOINT_STATUS_ID                          1071
#define UAVCAN_EQUIPMENT_HARDPOINT_STATUS_SIGNATURE                   0x624a519d42553d82
#define UAVCAN_EQUIPMENT_HARDPOINT_STATUS_MAX_SIZE                    ((56 + 7) / 8)

// uavcan.equipment.indication

// uavcan.equipment.indication.LightsCommand
#define UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_ID                 1081
#define UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_SIGNATURE          0x2031d93c8bdd1ec4
#define UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_MAX_SIZE           ((485 + 7) / 8)
#define UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_FILTER_ID          0x00043900
#define UAVCAN_EQUIPMENT_INDICATION_LIGHTS_COMMAND_FILTER_MASK        0x00FFFF80

// uavcan.equipment.power

// uavcan.equipment.power.BatteryInfo
#define UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_ID                        1092
#define UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_SIGNATURE                 0x249c26548a711966
#define UAVCAN_EQUIPMENT_POWER_BATTERY_INFO_MAX_SIZE                  ((437 + 7) / 8)

// Battery Status Flags
#define UAVCAN_BATTERY_STATUS_FLAG_IN_USE                             1U
#define UAVCAN_BATTERY_STATUS_FLAG_CHARGING                           2U
#define UAVCAN_BATTERY_STATUS_FLAG_CHARGED                            4U
#define UAVCAN_BATTERY_STATUS_FLAG_TEMP_HOT                           8U
#define UAVCAN_BATTERY_STATUS_FLAG_TEMP_COLD                          16U
#define UAVCAN_BATTERY_STATUS_FLAG_OVERLOAD                           32U
#define UAVCAN_BATTERY_STATUS_FLAG_BAD_BATTERY                        64U
#define UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE                       128U
#define UAVCAN_BATTERY_STATUS_FLAG_BMS_ERROR                          256U
#define UAVCAN_BATTERY_STATUS_FLAG_RESERVED_A                         512U
#define UAVCAN_BATTERY_STATUS_FLAG_RESERVED_B                         1024U


// uavcan.equipment.power.CircuitStatus
#define UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_ID                      1091
#define UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_SIGNATURE               0x8313d33d0ddda115
#define UAVCAN_EQUIPMENT_POWER_CIRCUIT_STATUS_MAX_SIZE                ((56 + 7) / 8)


#define UAVCAN_CIRCUIT_STATUS_FLAG_OVER_VOLTAGE                       0x01U
#define UAVCAN_CIRCUIT_STATUS_FLAG_UNDER_VOLTAGE                      0x02U
#define UAVCAN_CIRCUIT_STATUS_FLAG_OVER_CURRENT                       0x04U
#define UAVCAN_CIRCUIT_STATUS_FLAG_UNDER_CURRENT                      0x08U

// uavcan.equipment.power.PrimaryPowerSupplyStatus
#define UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_ID         1090
#define UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_SIGNATURE  0xbba05074ad757480
#define UAVCAN_EQUIPMENT_POWER_PRIMARY_POWER_SUPPLY_STATUS_MAX_SIZE   ((47 + 7) / 8)

// uavcan.equipment.range_sensor

// uavcan.equipment.range_sensor.Measurement
#define UAVCAN_EQUIPMENT_RANGE_SENSOR_MEASUREMENT_ID                  1050
#define UAVCAN_EQUIPMENT_RANGE_SENSOR_MEASUREMENT_SIGNATURE           0x68fffe70fc771952
#define UAVCAN_EQUIPMENT_RANGE_SENSOR_MEASUREMENT_MAX_SIZE            ((120 + 7) / 8)

// uavcan.equipment.safety

// uavcan.equipment.safety.ArmingStatus
#define UAVCAN_EQUIPMENT_SAFETY_ARMING_STATUS_ID                      1100
#define UAVCAN_EQUIPMENT_SAFETY_ARMING_STATUS_SIGNATURE               0x8700f375556a8003
#define UAVCAN_EQUIPMENT_SAFETY_ARMING_STATUS_MAX_SIZE                1

// uavcan.protocol

// uavcan.protocol.GetNodeInfo
#define UAVCAN_PROTOCOL_GET_NODE_INFO_ID                              1
#define UAVCAN_PROTOCOL_GET_NODE_INFO_SIGNATURE                       0xee468a8121c46a9e
#define UAVCAN_PROTOCOL_GET_NODE_INFO_MAX_SIZE                        ((3015 + 7) / 8)
#define UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_ID                       0x00018080
#define UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_MASK                     0x00FF8080
// uavcan.protocol

// uavcan.protocol.GetTransportStats
#define UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_ID                        4
#define UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_SIGNATURE                 0xbe6f76a7ec312b04
#define UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_MAX_SIZE                  ((578 + 7) / 8)
#define UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_ID                 0x00048080
#define UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_MASK               0x00FF8080

// uavcan.protocol.NodeStatus
#define UAVCAN_PROTOCOL_NODE_STATUS_ID                                341
#define UAVCAN_PROTOCOL_NODE_STATUS_SIGNATURE                         0x0f0868d0c1a7c6f1
#define UAVCAN_PROTOCOL_NODE_STATUS_MAX_SIZE                          7
#define UAVCAN_PROTOCOL_NODE_STATUS_FILTER_ID                         0x00015500
#define UAVCAN_PROTOCOL_NODE_STATUS_FILTER_MASK                       0x00FFFF80

// Health
#define UAVCAN_NODE_HEALTH_OK                                         0
#define UAVCAN_NODE_HEALTH_WARNING                                    1
#define UAVCAN_NODE_HEALTH_ERROR                                      2
#define UAVCAN_NODE_HEALTH_CRITICAL                                   3

// Mode
#define UAVCAN_NODE_MODE_OPERATIONAL                                  0
#define UAVCAN_NODE_MODE_INITIALIZATION                               1
#define UAVCAN_NODE_MODE_MAINTENANCE                                  2
#define UAVCAN_NODE_MODE_SOFTWARE_UPDATE                              3
#define UAVCAN_NODE_MODE_OFFLINE                                      7

// Vendor Specific Status (these are not uavcan_v0)
#define UAVCAN_VSC_UPDATE_TIMEOUT                                     0x01
#define UAVCAN_VSC_INVALID_IMAGE                                      0x02
#define UAVCAN_VSC_FLASH_WRITE_FAIL                                   0x04
#define UAVCAN_VSC_FILE_READ_FAIL                                     0x08
#define UAVCAN_VSC_DEFAULTS_LOADED                                    0x0F
#define UAVCAN_VSC_EEPROM_LOAD_FAIL                                   0x10

// uavcan.protocol.RestartNode
#define UAVCAN_PROTOCOL_RESTART_NODE_ID                               5
#define UAVCAN_PROTOCOL_RESTART_NODE_SIGNATURE                        0x569e05394a3017f0
#define UAVCAN_PROTOCOL_RESTART_NODE_MAX_SIZE                         ((40 + 7) / 8)
#define UAVCAN_PROTOCOL_RESTART_NODE_MAGIC                            0xACCE551B1E
#define UAVCAN_PROTOCOL_RESTART_NODE_FILTER_ID                        0x00058080
#define UAVCAN_PROTOCOL_RESTART_NODE_FILTER_MASK                      0x00FF8080

// uavcan.protocol.debug

// uavcan.protocol.debug.keyvalue
#define UAVCAN_PROTOCOL_DEBUG_KEY_VALUE_ID                            16370
#define UAVCAN_PROTOCOL_DEBUG_KEY_VALUE_SIGNATURE                     0xe02f25d6e0c98ae0
#define UAVCAN_PROTOCOL_DEBUG_KEY_VALUE_MAX_SIZE                      ((502 + 7) / 8)

// uavcan.protocol.debug.logmessage
#define UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_ID                           16383
#define UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_SIGNATURE                    0xd654a48e0c049d75
#define UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_MAX_SIZE                     ((983 + 7) / 8)
#define UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_MAX_SOURCE                   31
#define UAVCAN_PROTOCOL_DEBUG_LOGMESSAGE_MAX_TEXT                     90

// uavcan.protocol.dynamic_node_id

// uavcan.protocol.dynamic_node_id.Allocation
#define UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_ID                         1
#define UAVCAN_PROTOCOL_NODE_ID_ALLOCATION_SIGNATURE                  0x0b2a812620a11d40
#define UAVCAN_PROTOCOL_DYNAMIC_NODE_ID_ALLOCATION_FILTER_ID          0x00000100
#define UAVCAN_PROTOCOL_DYNAMIC_NODE_ID_ALLOCATION_FILTER_MASK        0x00FFFF00

// uavcan.protocol.file

// uavcan.protocol.file.BeginFirmwareUpdate
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID                 40
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_SIGNATURE          0xb7d725df72724126
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_MAX_SIZE           ((1616 + 7) / 8)
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_ID          0x00288080
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_MASK        0x00FF8080

#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_OK           0
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_INVALID_MODE 1
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_IN_PROGRESS  2
#define UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_UNKNOWN      255

// uavcan.protocol.file.read
#define UAVCAN_PROTOCOL_FILE_READ_ID                                  48
#define UAVCAN_PROTOCOL_FILE_READ_SIGNATURE                           0x8dcdca939f33f678
#define UAVCAN_PROTOCOL_FILE_READ_MAX_SIZE                            ((2073 + 7) / 8)
#define UAVCAN_PROTOCOL_FILE_READ_FILTER_ID                           0x00300080
#define UAVCAN_PROTOCOL_FILE_READ_FILTER_MASK                         0x00FF8080

// uavcan.protocol.file.write
#define UAVCAN_PROTOCOL_FILE_WRITE_ID                                 49
#define UAVCAN_PROTOCOL_FILE_WRITE_SIGNATURE                          0x515aa1dc77e58429
#define UAVCAN_PROTOCOL_FILE_WRITE_MAX_SIZE                           ((3192 + 7) / 8)

// uavcan.protocol.file.Error
#define UAVCAN_FILE_ERROR_OK                                          0
#define UAVCAN_FILE_ERROR_UNKNOWN                                     32767
#define UAVCAN_FILE_ERROR_NOT_FOUND                                   2
#define UAVCAN_FILE_ERROR_IO_ERROR                                    5
#define UAVCAN_FILE_ERROR_ACCESS_DENIED                               13
#define UAVCAN_FILE_ERROR_IS_DIRECTORY                                21
#define UAVCAN_FILE_ERROR_INVALID_VALUE                               22
#define UAVCAN_FILE_ERROR_FILE_TOO_LARGE                              27
#define UAVCAN_FILE_ERROR_OUT_OF_SPACE                                28
#define UAVCAN_FILE_ERROR_NOT_IMPLEMENTED                             38


// uavcan.protocol.panic
#define UAVCAN_PROTOCOL_PANIC_ID                                      5
#define UAVCAN_PROTOCOL_PANIC_SIGNATURE                               0x0
#define UAVCAN_PROTOCOL_PANIC_MAX_SIZE                                ((7 + 7) / 8)
#define UAVCAN_PROTOCOL_PANIC_FILTER_ID                               0x0
#define UAVCAN_PROTOCOL_PANIC_FILTER_MASK                             0x0

#define UAVCAN_PROTOCOL_PANIC_MIN_MESSAGES                            3
#define UAVCAN_PROTOCOL_PANIC_MAX_INTERVAL_MS                         500

// .. predefined messages
#define UAVCAN_PROTOCOL_PANIC_MESSAGE_UNKNOWN                         "unknown"
#define UAVCAN_PROTOCOL_PANIC_MESSAGE_E_STOP                          "estop"
#define UAVCAN_PROTOCOL_PANIC_MESSAGE_MCU_CRASH                       "crash"
#define UAVCAN_PROTOCOL_PANIC_MESSAGE_MOTOR_FAULT                     "motor"

// uavcan.protocol.param
#define UAVCAN_PROTOCOL_PARAM_FILTER_ID                               0x000A8080
#define UAVCAN_PROTOCOL_PARAM_FILTER_MASK                             0x00FE8080

// uavcan.protocol.param.ExecuteOpcode
#define UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_ID                       10
#define UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_SIGNATURE                0x3b131ac5eb69d2cd
#define UAVCAN_PROTOCOL_PARAM_EXECUTE_OPCODE_MAX_SIZE                 ((56 + 7) / 8)

#define UAVCAN_PROTOCOL_PARAM_OPCODE_SAVE                             0
#define UAVCAN_PROTOCOL_PARAM_OPCODE_ERASE                            1

// uavcan.protocol.param.GetSet
#define UAVCAN_PROTOCOL_PARAM_GET_SET_ID                              11
#define UAVCAN_PROTOCOL_PARAM_GET_SET_SIGNATURE                       0xa7b622f939d1a4d5
#define UAVCAN_PROTOCOL_PARAM_GET_SET_MAX_SIZE                        ((2967 + 7) / 8)
#define UAVCAN_PROTOCOL_PARAM_NAME_MAX_SIZE                           92

// Value tags
#define UAVCAN_PROTOCOL_PARAM_VALUE_TAG_EMPTY                         0x00
#define UAVCAN_PROTOCOL_PARAM_VALUE_TAG_INT                           0x01
#define UAVCAN_PROTOCOL_PARAM_VALUE_TAG_REAL                          0x02
#define UAVCAN_PROTOCOL_PARAM_VALUE_TAG_BOOL                          0x03
#define UAVCAN_PROTOCOL_PARAM_VALUE_TAG_STRING                        0x04


/**
 * Vendor Extensions
 */

// io.p-systems.device

// io.p-systems.device.WriteOptionBytes
#define IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID                     220
#define IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_SIGNATURE              0x014ab53932cfb5d5
#define IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_MAX_SIZE               ((16 + 7) / 8)
#define IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_FILTER_ID              0x00DC8080
#define IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_FILTER_MASK            0x00FF8080

// io.p-systems.device.I2C*
#define IO_P_SYSTEMS_DEVICE_I2C_FILTER_ID                             0x00DC8080
#define IO_P_SYSTEMS_DEVICE_I2C_FILTER_MASK                           0x00FC8080

// io.p-systems.device.I2CRead
#define IO_P_SYSTEMS_DEVICE_I2C_READ_ID                               221
#define IO_P_SYSTEMS_DEVICE_I2C_READ_SIGNATURE                        0xe55eecbe8ef1a6f7
#define IO_P_SYSTEMS_DEVICE_I2C_READ_MAX_SIZE                         ((1040 + 7) / 8)

// io.p-systems.device.I2CWrite
#define IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID                              222
#define IO_P_SYSTEMS_DEVICE_I2C_WRITE_SIGNATURE                       0x2d9e6301bb3613a1
#define IO_P_SYSTEMS_DEVICE_I2C_WRITE_MAX_SIZE                        ((173 + 7) / 8)

/**
 * Universal Test Fixture Messages
 */

// io.p-systems.utf

// io.p-systems.utf.ConfigPin
#define IO_P_SYSTEMS_UTF_CONFIG_PIN_ID                                230
#define IO_P_SYSTEMS_UTF_CONFIG_PIN_SIGNATURE                         0x7C992DBF6F88F2EEULL
#define IO_P_SYSTEMS_UTF_CONFIG_PIN_REQUEST_MAX_SIZE                  ((24 + 7) / 8)
#define IO_P_SYSTEMS_UTF_CONFIG_PIN_FILTER_ID                         0x00E68080
#define IO_P_SYSTEMS_UTF_CONFIG_PIN_FILTER_MASK                       0x00FF8080

// io.p-systems.utf.SetPinState
#define IO_P_SYSTEMS_UTF_SET_PIN_STATE_ID                             231
#define IO_P_SYSTEMS_UTF_SET_PIN_STATE_SIGNATURE                      0xBC16A07D52E347C6ULL
#define IO_P_SYSTEMS_UTF_SET_PIN_STATE_REQUEST_MAX_SIZE               ((24 + 7) / 8)
#define IO_P_SYSTEMS_UTF_SET_PIN_STATE_FILTER_ID                      0x00E78080
#define IO_P_SYSTEMS_UTF_SET_PIN_STATE_FILTER_MASK                    0x00FF8080

//  io.p-systems.utf.RestartDUT
#define IO_P_SYSTEMS_UTF_RESTART_DUT_ID                               232
#define IO_P_SYSTEMS_UTF_RESTART_DUT_SIGNATURE                        0x5802915592263324ULL
#define IO_P_SYSTEMS_UTF_RESTART_DUT_REQUEST_MAX_SIZE                 ((40 + 7) / 8)
#define IO_P_SYSTEMS_UTF_RESTART_DUT_RESPONSE_MAX_SIZE                ((8 + 7) / 8)
#define IO_P_SYSTEMS_UTF_RESTART_DUT_MAGIC                            0xEDDABFFE
#define IO_P_SYSTEMS_UTF_RESTART_DUT_FILTER_ID                        0x00E88080
#define IO_P_SYSTEMS_UTF_RESTART_DUT_FILTER_MASK                      0x00FF8080


/**
 * Other
 */
// Converting from C to K
#define C_TO_K_OFFSET                                                 273.15f

/**
 * Data Structures
 */

/**
 * Battery information structure
 */
typedef struct {

  // UAVCAN Fields
  float temperature;
  float voltage_V;
  float current_A;
  float average_power_10sec;
  float remaining_capacity_Wh;
  float full_charge_capacity_Wh;
  float hours_to_full_charge;

  uint8_t state_of_health_pct;
  uint8_t state_of_charge_pct;
  uint8_t state_of_charge_pct_stdev;

  uint16_t status_flags;

  uint8_t battery_id;
  uint32_t model_instance_id;
  char model_name[32];

  // Internal Transfer Variables
  uint8_t transfer_id;
  uint32_t timestamp;

} battery_info_t;

typedef struct {

  // UAVCAN Fields
  uint16_t circuit_id;
  float voltage;
  float current;
  uint8_t error_flags;

  // Internal Transfer Variables
  uint8_t transfer_id;
  uint32_t timestamp;

} circuit_status_t;

/**
 * uavcan.equipment.device.Temperature
 */
typedef struct {

  // UAVCAN Fields
  uint16_t device_id;
  float temperature_K;
  uint8_t error_flags;

  // Internal Transfer Variables
  uint8_t transfer_id;
  uint32_t timestamp;

} device_temperature_t;

void DeviceTemperature_Set(float temperature_C,
                           float temp_limit_min_C,
                           float temp_limit_max_C,
                           device_temperature_t *device_temperature);

/**
 * Power Supply Info
 */
typedef struct {
  float hours_to_empty_at_10sec_avg_power;
  float hours_to_empty_at_10sec_avg_power_variance;

  bool external_power_available;

  uint8_t remaining_energy_pct;
  uint8_t remaining_energy_pct_stdev;

} psu_info_t;


typedef enum {
  LogLevel_DEBUG = 0,
  LogLevel_INFO = 1,
  LogLevel_WARNING = 2,
  LogLevel_ERROR = 3
} LogLevel;

/**
 * Internal structures
 */

typedef struct {
  request_t request;

  uint16_t index;

  char name[UAVCAN_PROTOCOL_PARAM_NAME_MAX_SIZE];
  uint8_t name_len;

  union {
    int64_t i;
    char s[128];
    float f;
    bool b;
  } value;

  uint8_t value_len;

  uint8_t type;

} parameter_getset_t;

typedef struct {
  request_t request;
  uint8_t opcode;

} parameter_exec_opcode_t;


typedef struct __attribute__ ((aligned (4))) {
  // Parameters
  uint32_t default_baud_kbps;
  bool bus_termination_enable;
  bool auto_retransmit_enable;
  uint8_t default_node_id;

  bool auto_baud_enable;
  uint32_t auto_baud_max_retries;
  uint32_t auto_baud_retry_time_ms;

  // UAVCAN Dynamic Node ID allocation
  bool dynamic_id_allocation_enable;
  uint32_t dynamic_id_allocation_max_retries;
  uint32_t dynamic_id_allocation_retry_time_ms;

} can_params_t;


typedef struct {
  CAN_HANDLE *can_if;
  CAN_HANDLE *can_filter_if;
  uint32_t *can_filters;
  int filter_count;

  // Flags and counters
  bool node_status_received;
  uint8_t next_baud_ix;

  uint32_t auto_baud_retry_timestamp;
  int auto_baud_retry_count;

  int dynamic_id_allocation_retry_count;
  uint32_t dynamic_id_allocation_timestamp;

} uavcan_init_t;


/**
 * Low Level routines
 */

/**
 *
 * @param boot_info
 * @param node
 * @param can_init
 * @return
 */
bool UAVCAN_Init(boot_info_t *boot_info,
                 node_info_t *node,
                 can_params_t *params,
                 uavcan_init_t *can_init);

/**
 *
 * @param can_id
 * @param data_type_signature
 * @param transfer_id
 * @param payload
 * @param payload_len
 * @return
 */
bool UAVCAN_TX_Frames(CAN_HANDLE *can,
                      uint32_t can_id,
                      uint64_t data_type_signature,
                      uint8_t transfer_id,
                      const uint8_t *payload,
                      uint16_t payload_len);

bool UAVCAN_Send_Anonymous_Frame(uint16_t data_type_id,
                                 uint8_t priority,
                                 uint8_t transfer_id,
                                 const uint8_t *payload,
                                 uint16_t payload_len);

// Broadcast message
bool UAVCAN_Send_Broadcast_Frame(uint8_t source_node_id,
                                 uint64_t data_type_signature,
                                 uint16_t data_type_id,
                                 uint8_t transfer_id,
                                 uint8_t priority,
                                 const uint8_t *payload,
                                 uint16_t payload_len);


// Service
bool UAVCAN_Send_Service_Frame(bool request,
                               uint8_t source_node_id,
                               uint8_t destination_node_id,
                               uint64_t data_type_signature,
                               uint16_t data_type_id,
                               uint8_t transfer_id,
                               uint8_t priority,
                               const void *payload,
                               uint16_t payload_len);

/**
 * This handles fully assembled messages
 *
 * This is called by the process_CAN_callbacks
 *
 * Implement this in your program code
 *
 * @param node
 * @param data_type_id
 * @param source_node_id
 * @param destination_node_id
 * @param priority
 * @param transfer_id
 * @param payload
 * @param payload_len
 */
void UAVCAN_Handle_Message(uint16_t data_type_id,
                           __attribute__((unused)) bool is_service,
                           uint8_t source_node_id,
                           uint8_t priority,
                           uint8_t transfer_id,
                           uint8_t *payload,
                           uint32_t payload_len);

/**
 *
 * Process UAVCAN messages... like reassembly
 *
 */
void UAVCAN_Process_Frame(uint8_t node_id_local,
                          uint32_t can_id,
                          uint8_t *payload,
                          size_t payload_len);
/**
 * Message generating routines
 */
/**
 *
 *
 *
 * @param buffer
 * @param buffer_len
 * @param actuator_id
 * @param command_type
 * @param command_value
 * @return
 */
bool Deserialize_ActuatorArrayCommand_Message(const uint8_t *buffer,
                                              uint32_t buffer_len,
                                              uint8_t actuator_id,
                                              uint8_t *command_type,
                                              float *command_value);
/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#status
 *
 *  uavcan.equipment.actuator.Status
 *
 * @param source_node_id
 * @param transfer_id
 * @param actuator_id
 * @param position
 * @param force
 * @param speed
 * @param power_rating_pct
 * @return
 */
bool Send_ActuatorStatus_Message(uint8_t node_id,
                                 uint8_t actuator_id,
                                 float position,
                                 float force,
                                 float speed,
                                 uint8_t power_rating_pct);

bool Send_StaticPressure_Message(uint8_t node_id,
                                 float pressure,
                                 float variance);

bool Send_StaticTemperature_Message(uint8_t node_id,
                                    float temperature,
                                    float variance);

/**
 * uavcan.equipment.device.Temperature
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#temperature
 *
 * @param source_node_id
 * @param temperature
 * @return
 */
bool Send_DeviceTemperature_Message(uint8_t node_id,
                                    device_temperature_t *device_temperature);

/**
 * uavcan.equipment.esc
 */

/**
 *
 * @param buffer
 * @param buffer_len
 * @param esc_vals
 * @return
 */
int Deserialize_ESC_RawCommand_Message(const uint8_t *buffer,
                                       uint32_t buffer_len,
                                       int16_t *esc_vals);

int Deserialize_ESC_RpmCommand_Message(const uint8_t *buffer,
                                       uint32_t buffer_len,
                                       int32_t *esc_vals);

/**
 * uavcan.equipment.hardpoint
 */
void Deserialize_Hardpoint_Command_Message(const uint8_t *buffer,
                                           uint8_t *hardpoint_id,
                                           uint16_t *command);

bool Send_Hardpoint_Status_Message(uint8_t node_id,
                                   uint8_t transfer_id,
                                   uint8_t hardpoint_id,
                                   float payload_weight,
                                   float payload_weight_variance,
                                   uint16_t status);
/**
 * uavcan.equipment.indication.LightsCommand
 *
 */

bool Deserialize_LightsCommand_Message(const uint8_t *buffer,
                                       uint32_t buffer_len,
                                       uint8_t light_id,
                                       uint16_t *rgb565);

bool Deserialize_LightsCommand_Message_Single(const uint8_t *buffer,
                                       uint32_t buffer_len,
                                       uint8_t light_id,
                                       uint16_t *single);
/**
 *  uavcan.equipment.power.BatteryInfo
 *
 * @param transfer_id
 * @param node
 * @param info
 */
bool Send_BatteryInfo_Message(uint8_t node_id,
                              battery_info_t *info);

/**
 * uavcan.equipment.power.CircuitStatus
 *
 */
bool Send_CircuitStatus_Message(uint8_t node_id,
                                circuit_status_t *circuit_status);

/**
 * uavcan.equipment.power.PrimaryPowerSupplyStatus
 *
 */
bool Send_PrimaryPowerSupplyStatus_Message(uint8_t node_id,
                                           uint8_t transfer_id,
                                           psu_info_t *psu_status);

// Send GetNodeInfo service response
bool Send_GetNodeInfo_Response(node_info_t *node,
                               request_t request);

// Create a NodeStatus message
void Serialize_NodeStatus_Message(uint32_t uptime_sec,
                                  uint8_t node_health,
                                  uint8_t node_mode,
                                  uint16_t vendor_specific_status_code,
                                  uint8_t *buffer);

// Send NodeStatus message
bool Send_NodeStatus_Message(node_info_t *node);

bool Send_GetTransportStats_Response(node_info_t *node,
                                     request_t request);

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#restartnode
 * uavcan.protocol.RestartNode
 * Check if the restart request is valid
 */
bool Check_NodeRestart_Request(const uint8_t *buffer,
                               uint32_t buffer_len);

bool Send_NodeRestart_Response(uint8_t node_id,
                               bool ok,
                               request_t request);

/**
 * uavcan.protocol.debug.KeyValue
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#keyvalue
 *
 * @param node_id
 * @param key
 * @param text
 */
bool Send_DebugKeyValue_Message(uint8_t node_id,
                                const char *key,
                                float value);

/**
 * uavcan.protocol.debug.LogMessage
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#logmessage
 *
 * @param node_id
 * @param level
 * @param source
 * @param text
 * @return
 */
bool Send_DebugLog_Message(uint8_t node_id,
                           LogLevel level,
                           const char *source,
                           const char *text);

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#allocation
 *
 * uavcan.protocol.dynamic_node_id.Allocation
 *
 * @param buffer
 * @param buffer_len
 * @param uuid
 * @return
 */
uint8_t Deserialize_Node_ID_Allocation_Response(const uint8_t *buffer,
                                                uint8_t buffer_len,
                                                const uint8_t *uuid);

// Send an allocation request
bool Send_Node_ID_Allocation_Request(const uint8_t *uuid,
                                     uint8_t uuid_len,
                                     uint8_t transfer_id,
                                     bool first);

/**
 * Deserialize the BeginFirmwareUpdate request payload
 * uavcan.protocol.file.BeginFirmwareUpdate
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#beginfirmwareupdate
 *
 * @param buffer
 * @param buffer_len
 * @param node_id
 * @param path
 */
void Deserialize_BeginFirmwareUpdate_Request(const uint8_t *buffer,
                                             uint32_t buffer_len,
                                             uint8_t *node_id,
                                             size_t *path_len,
                                             char *path);

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#beginfirmwareupdate
 *
 * @param node
 * @param destination_node_id
 * @param transfer_id
 * @param priority
 * @param error_code
 * @param error_message
 */
bool Send_BeginFirmwareUpdate_Response(uint8_t node_id,
                                       request_t request,
                                       uint8_t error_code,
                                       const char *error_message);

/**
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#read
 * uavcan.protocol.file.Read
 *
 * @param node
 * @param destination_node_id
 * @param inout_transfer_id
 * @param priority
 * @param offset
 * @param path
 */
bool Send_FileRead_Request(uint8_t node_id,
                           uint8_t server_node_id,
                           uint8_t transfer_id,
                           uint64_t offset,
                           size_t path_len,
                           const char *path);

/**
 * uavcan.protocol.file.Read
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#read
 *
 * @param buffer
 * @return error code
 */
int16_t Check_FileRead_Response(const uint8_t *buffer);

/**
 * uavcan.protocol.param.ExecuteOpcode
 *
 * https://uavcan.org/Specification/7._List_of_standard_data_types/#executeopcode
 *
 */
bool Send_ExecuteOpcode_Response(uint8_t node_id,
                                 request_t request,
                                 int32_t error_code,
                                 bool ok);

void Deserialize_GetSet_Request(const uint8_t *buffer,
                                uint32_t buffer_len,
                                parameter_getset_t *param);

bool Send_GetSet_Empty_Response(uint8_t node_id,
                                request_t request);

bool Send_GetSet_Bool_Response(uint8_t node_id,
                               request_t request,
                               bool value,
                               bool default_value,
                               const char *name);

bool Send_GetSet_Int_Response(uint8_t node_id,
                              request_t request,
                              int64_t value,
                              int64_t default_value,
                              int64_t min_value,
                              int64_t max_value,
                              const char *name);

bool Send_GetSet_Real_Response(uint8_t node_id,
                               request_t request,
                               float value,
                               float default_value,
                               float min_value,
                               float max_value,
                               const char *name);

bool Send_GetSet_String_Response(uint8_t node_id,
                                 request_t request,
                                 size_t value_len,
                                 const uint8_t *value,
                                 size_t default_value_len,
                                 const uint8_t *default_value,
                                 const char *name);

bool Params_Handle_GetSet(uint8_t node_id,
                          uint32_t parameter_count,
                          const parameter_info_t *info_table,
                          const uint8_t *factory_info_base,
                          bool *dirty_table,
                          parameter_getset_t *param,
                          request_t request,
                          uint8_t *param_base);

/**
 *
 * Vendor Specific Routines
 *
 */


/**
 * I2C Routines
 */

/**
 *
 * @param node_id
 * @param request
 * @param hi2c
 * @param i2c_if_count
 * @param payload
 * @param payload_len
 * @return
 */
bool Handle_I2C_Read_Request(uint8_t node_id,
                             request_t request,
                             I2C_TypeDef *hi2c,
                             uint8_t i2c_address,
                             uint8_t register_address,
                             uint8_t read_length,
                             uint32_t timeout_ms);

bool Handle_I2C_Write_Request(uint8_t node_id,
                              request_t request,
                              I2C_TypeDef *hi2c,
                              uint8_t i2c_address,
                              uint8_t register_address,
                              const uint8_t *payload,
                              size_t payload_len,
                              uint32_t timeout_ms);

/**
 * Option Byte Writing (probably also remove)
 *
 */
bool Handle_Write_Option_Bytes_Request(uint8_t node_id,
                                       request_t request,
                                       bool enabled,
                                       const uint8_t *payload);

/**
 * UTF
 */

void Deserialize_ConfigPin_Request(const uint8_t *buffer,
                                   uint16_t *pin_id,
                                   uint8_t *mode,
                                   uint8_t *push_pull);

bool Send_ConfigPin_Response(uint8_t node_id,
                             request_t request,
                             uint16_t pin_id,
                             uint8_t error_code);

void Deserialize_SetPinState_Request(const uint8_t *buffer,
                                     uint16_t *pin_id,
                                     bool *pin_state);

bool Send_SetPinState_Response(uint8_t node_id,
                               request_t request,
                               uint16_t pin_id,
                               uint8_t error_code,
                               bool state);

/**
 * io.p-systems.utf.ResetDUT
 * @param buffer
 * @return
 */
bool Check_ResetDUT_Request(const uint8_t *buffer);

bool Send_ResetDUT_Response(uint8_t node_id,
                            bool ok,
                            request_t request);

/**
 * Misc high-level routines
 */
void Check_Circuit_Limits(circuit_status_t *circuit,
                          float V_min,
                          float V_max,
                          float I_min,
                          float I_max);

/**
 * "User" implemented stuff
 */

bool Params_Call_Handle_GetSet(int node_id,
                               parameter_getset_t *param_get_set,
                               request_t request);

void Params_Load_Default_Values();

bool Params_Load();

bool Params_Save();


bool Decode_Device_I2C_ReadWrite(uint8_t *payload, size_t payload_len);

void Handle_Device_I2C_Read(request_t *request);
void Handle_Device_I2C_Write(request_t *request);

void Handle_Param_GetSet_Special(const char *name);

void Hardware_DeInit();

void Process_UAVCAN_Message(uint16_t data_type_id,
                            bool is_service,
                            uint8_t source_node_id,
                            uint8_t priority,
                            uint8_t transfer_id,
                            uint8_t *payload,
                            uint32_t payload_len,
                            node_info_t *node,
                            uavcan_init_t *uavcan_init,
                            request_queue_t *queue,
                            boot_info_t *boot_info);

void Service_Request_Begin_Firmware_Update(request_t *request);

void Service_UAVCAN_Requests(node_info_t *node,
                             uavcan_init_t *uavcan_init,
                             request_queue_t *queue,
                             boot_info_t *boot_info,
                             bool enable_firmware_updates,
                             can_params_t *can);

bool Service_Request_File_Read();
