/**
 * @file
 * @brief STM32 Utilities
 * @version 9
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @copyright (c) 2019 - STMicroelectronics
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 *
 * @details :
 *
 *   This file is a collection of routines for dealing with GPIO, bxCAN, and Timers for several STM32 MCUs. The
 *   code mostly comes from ST's HAL and Reference Manuals for F0, F3, F4 but has been specialized and flattened out
 *   for simplicity. This is not a library and only supports a small number of MCUs that share similar hardware.
 *
 *   It also contains routines for interacting with the Pomegranate Systems STM32 bootloader.
 *
 * Quick Start:
 *  - Setup and define uint32_t System_Tick that is incremented every ms
 *  - Setup CAN device
 *  - For F0 pins PB8 and PB9 are used unless REMAP_PA11_PA12 is defined then PA11 and PA12 are used
 */

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

/**
 * Headers
 */
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

// clang-format off

/**
 * Configuration
 */

#ifndef CANBUS_TIMEOUT_MS
#define CANBUS_TIMEOUT_MS                                             1000000
#endif

// How many elements are in a rolling buffers
#ifndef ROLLING_BUFFER_MAX_SIZE
#define ROLLING_BUFFER_MAX_SIZE                                       10
#endif

// Enable generating of transportation stats for uavcan.protocol.GetTransportStats messages
#ifndef ENABLE_TRANSPORT_STATS
#define ENABLE_TRANSPORT_STATS                                        1
#endif

// From uavcan v0 library.
#define UAVCAN_UNIQUE_ID_LENGTH_BYTES                                 16
#define UAVCAN_HW_CERT_LENGTH_BYTES                                   255
#define UAVCAN_MAX_PAYLOAD_SIZE                                       360

#if defined(STM32F0)

#define CAN_IFACE_COUNT 1

//#include "stm32f0xx_hal.h"
#include "stm32f0xx_hal_can.h"
#include "stm32f0xx_hal_dma.h"
#include "stm32f0xx_hal_i2c.h"

#if 0
/**
  * @brief  CAN Tx message header structure definition
  */
typedef struct {
  uint32_t StdId;
  uint32_t ExtId;
  uint32_t IDE;
  uint32_t RTR;
  uint32_t DLC;
  FunctionalState TransmitGlobalTime;

} CAN_TxHeaderTypeDef;

typedef struct {
  uint32_t StdId;
  uint32_t ExtId;
  uint32_t IDE;
  uint32_t RTR;
  uint32_t DLC;
  uint32_t Timestamp;
  uint32_t FilterMatchIndex;
} CAN_RxHeaderTypeDef;
#endif

#elif defined(STM32F3)

#include "stm32f3xx_hal_can.h"

#define CAN_IFACE_COUNT 1

// From _hal_gpio_ex.h
#define GPIO_AF1_TIM2                                                 ((uint8_t)0x01U)
#define GPIO_AF4_I2C1                                                 ((uint8_t)0x04U)
#define GPIO_AF4_I2C2                                                 ((uint8_t)0x04U)
#define GPIO_AF8_I2C3                                                 ((uint8_t)0x08U)

#define GPIO_AF7_CAN                                                  ((uint8_t)0x07U)
#define GPIO_AF9_CAN                                                  ((uint8_t)0x09U)

#elif defined(STM32F4)

#include "stm32f4xx_hal_can.h"

#define CAN_IFACE_COUNT 3

#if 1
#define GPIO_AF8_CAN1                                                 ((uint8_t)0x08)
#define GPIO_AF9_CAN2                                                 ((uint8_t)0x09)
#define GPIO_AF11_CAN3                                                ((uint8_t)0x0B)
#endif

#elif defined(STM32G431xx) || defined(STM32G491xx)

#define CAN_IFACE_COUNT 3

#include "stm32g4xx_hal_fdcan.h"
#define USE_FDCAN                                                     1

#endif

#ifndef GPIO_MODE_INPUT

// from stm32f3/4xx_hal_gpio.h
#define GPIO_PIN_MASK                                                 0x0000FFFFU

#define GPIO_MODE_INPUT                                               0x00000000U
#define GPIO_MODE_OUTPUT_PP                                           0x00000001U
#define GPIO_MODE_OUTPUT_OD                                           0x00000011U
#define GPIO_MODE_AF_PP                                               0x00000002U
#define GPIO_MODE_AF_OD                                               0x00000012U

#define GPIO_MODE_ANALOG                                              0x00000003U

#define GPIO_MODE_IT_RISING                                           0x10110000U
#define GPIO_MODE_IT_FALLING                                          0x10210000U
#define GPIO_MODE_IT_RISING_FALLING                                   0x10310000U

#define GPIO_MODE_EVT_RISING                                          0x10120000U
#define GPIO_MODE_EVT_FALLING                                         0x10220000U
#define GPIO_MODE_EVT_RISING_FALLING                                  0x10320000U

#define GPIO_SPEED_FREQ_LOW                                           0x00000000U
#define GPIO_SPEED_FREQ_MEDIUM                                        0x00000001U

#if defined(STM32F3)
#define GPIO_SPEED_FREQ_HIGH                                          0x00000003U
#elif defined(STM32F4) || defined(STM32F0)
#define  GPIO_SPEED_FREQ_HIGH                                         0x00000002U
#define  GPIO_SPEED_FREQ_VERY_HIGH                                    0x00000003U
#endif

#define GPIO_NOPULL                                                   0x00000000U
#define GPIO_PULLUP                                                   0x00000001U
#define GPIO_PULLDOWN                                                 0x00000002U
#endif

#ifndef GPIO_PIN_0
#define GPIO_PIN_0                                                    ((uint16_t)0x0001U)
#define GPIO_PIN_1                                                    ((uint16_t)0x0002U)
#define GPIO_PIN_2                                                    ((uint16_t)0x0004U)
#define GPIO_PIN_3                                                    ((uint16_t)0x0008U)
#define GPIO_PIN_4                                                    ((uint16_t)0x0010U)
#define GPIO_PIN_5                                                    ((uint16_t)0x0020U)
#define GPIO_PIN_6                                                    ((uint16_t)0x0040U)
#define GPIO_PIN_7                                                    ((uint16_t)0x0080U)
#define GPIO_PIN_8                                                    ((uint16_t)0x0100U)
#define GPIO_PIN_9                                                    ((uint16_t)0x0200U)
#define GPIO_PIN_10                                                   ((uint16_t)0x0400U)
#define GPIO_PIN_11                                                   ((uint16_t)0x0800U)
#define GPIO_PIN_12                                                   ((uint16_t)0x1000U)
#define GPIO_PIN_13                                                   ((uint16_t)0x2000U)
#define GPIO_PIN_14                                                   ((uint16_t)0x4000U)
#define GPIO_PIN_15                                                   ((uint16_t)0x8000U)
#endif

// From some hal
#ifndef NVIC_PRIORITYGROUP_0
#define NVIC_PRIORITYGROUP_0                                          ((uint32_t)0x00000007U)
#define NVIC_PRIORITYGROUP_1                                          ((uint32_t)0x00000006U)
#define NVIC_PRIORITYGROUP_2                                          ((uint32_t)0x00000005U)
#define NVIC_PRIORITYGROUP_3                                          ((uint32_t)0x00000004U)
#define NVIC_PRIORITYGROUP_4                                          ((uint32_t)0x00000003U)
#endif

#undef MIN
#define MIN(a, b)   (((a) < (b)) ? (a) : (b))

#undef MAX
#define MAX(a, b)   (((a) > (b)) ? (a) : (b))

/**
 * CANbus
 */
#define CANBUS_PRESCALAR_8MHZ                                         1
//#define CANBUS_PRESCALAR_12MHZ                                      2
#define CANBUS_PRESCALAR_16MHZ                                        3
#define CANBUS_PRESCALAR_48MHZ                                        4

#if defined(STM32F0)
#define CANBUS_PRESCALAR CANBUS_PRESCALAR_48MHZ
#elif defined(STM32F302x8)
#define CANBUS_PRESCALAR CANBUS_PRESCALAR_8MHZ
#elif defined(STM32F413xx)
#define CANBUS_PRESCALAR CANBUS_PRESCALAR_16MHZ
#elif defined(STM32G431xx) || defined(STM32G491xx)
#define CANBUS_PRESCALAR CANBUS_PRESCALAR_16MHZ
#else
#error "Specify CANBUS_PRESCALAR"
#endif

/**
 * Flash
 */
#if 1
#if defined(STM32F0)

#elif defined(STM32F3)

#elif defined(STM32F4)

#ifndef RDP_KEY
#define RDP_KEY                                                       0x00A5U
#define FLASH_KEY1                                                    0x45670123U
#define FLASH_KEY2                                                    0xCDEF89ABU
#define FLASH_OPT_KEY1                                                0x08192A3BU
#define FLASH_OPT_KEY2                                                0x4C5D6E7FU
#endif

// Flash program parallelism
#define FLASH_PSIZE_BYTE                                              0x00000000U
#define FLASH_PSIZE_HALF_WORD                                         0x00000100U
#define FLASH_PSIZE_WORD                                              0x00000200U
#define FLASH_PSIZE_DOUBLE_WORD                                       0x00000300U

#elif defined(STM32G4)

#ifndef RDP_KEY
#define RDP_KEY                                                       0x00A5U
#define FLASH_KEY1                                                    0x45670123U
#define FLASH_KEY2                                                    0xCDEF89ABU
#define FLASH_OPT_KEY1                                                0x08192A3BU
#define FLASH_OPT_KEY2                                                0x4C5D6E7FU
#endif

#endif

#endif
// clang-format on

/**
 * Interface Statistics
 *
 * uavcan.protocol.GetTransportStats
 *
 */
typedef struct {
  uint64_t frames_tx;
  uint64_t frames_rx;
  uint64_t errors;

} iface_stats_t;

/**
 * Node info struct
 */
typedef struct {

  // Status
  uint8_t id;

  // Has the id been allocated
  bool id_valid;

  uint32_t uptime_sec;
  uint8_t  health;
  uint8_t  mode;
  uint16_t vendor_status_code;

  char app_name[80];

  // Hardware version
  uint8_t hw_version_major;
  uint8_t hw_version_minor;
  uint8_t uuid[UAVCAN_UNIQUE_ID_LENGTH_BYTES];
  uint8_t hw_cert[UAVCAN_HW_CERT_LENGTH_BYTES];

  // Sofware version
  uint8_t sw_version_major;
  uint8_t sw_version_minor;

  // git short hash
  uint32_t vcs_commit;
  uint64_t image_crc;

  // Simple hash that can be used for basic randomness
  uint8_t hardware_hash;

  /**
   * _RESET = CAN was just turned on
   * _AUTO_BAUD = On and trying to figure out proper
   * _SEND_ONLY = Up with the correct baud but filters not configured.
   * _OPERATIONAL = CANbus is up but have no UAVCAN Node ID
   * _ONLINE = Connected and have obtained ID
   *
   */
  enum {
    CAN_STATUS_RESET = 0,
    CAN_STATUS_AUTO_BAUD,
    CAN_STATUS_SEND_ONLY,
    CAN_STATUS_OPERATIONAL,
    CAN_STATUS_ONLINE,
  } can_status;

  // Transfer ID for the node status packet
  uint8_t transfer_id;

  // Last time a nodestatus was sent
  uint32_t timestamp;

} node_info_t;

/**
 * Store data for packet reassembly
 */
typedef struct {

  uint8_t  transfer_id;
  uint16_t data_type_id;
  uint8_t  source_node_id;

  uint8_t payload[UAVCAN_MAX_PAYLOAD_SIZE];

  uint16_t payload_len;
  bool     in_use;

  //  to remove stale packets
  uint32_t timestamp;

  // The CRC from the payload (i.e. target CRC)
  uint16_t crc_payload;

  // Computed CRC
  uint16_t crc_computed;

} packet_reassembly_t;

/**
 * Message processing status flags
 */
typedef struct {

  bool     pending;
  uint16_t type_id;
  uint8_t  node_id;
  uint8_t  transfer_id;
  uint8_t  priority;

  // Extra custom data
  uint8_t data;

} request_t;

#ifndef REQUEST_QUEUE_CAPACITY
#define REQUEST_QUEUE_CAPACITY 128
#endif

typedef struct {

  // The queue
  request_t request[REQUEST_QUEUE_CAPACITY];

  // The position of messages that need to be processed
  int top;

  // The position at which things are currently added
  int end;

  // Total number of messages in the queue
  int size;
} request_queue_t;

int Request_Queue_Add(request_queue_t *q,
                      uint16_t         type_id,
                      uint8_t          source_node_id,
                      uint8_t          transfer_id,
                      uint8_t          priority,
                      uint8_t          data);

int Request_Queue_Add_Unique(request_queue_t *q,
                             uint16_t         type_id,
                             uint8_t          source_node_id,
                             uint8_t          transfer_id,
                             uint8_t          priority,
                             uint8_t          data);

void Request_Queue_Pop(request_queue_t *q);

/**
 * Bootloader
 */

/**
 * App descriptor struct
 */
typedef struct __attribute__((packed)) {

  uint8_t  signature[8];
  uint64_t image_crc;
  uint32_t image_size;
  uint32_t vcs_commit;
  uint8_t  major_version;
  uint8_t  minor_version;
  uint8_t  reserved[6];

} app_descriptor_t;

/**
 * Bootloader information struct
 */
// clang-format off
#define BOOT_INFO_SIGNATURE                                           0x12344321

// clang-format on

typedef struct __attribute__((packed)) {

  // Boot Info identifier
  uint32_t signature;

  // Node config info
  uint32_t can_baud;
  uint8_t  node_id;
  uint8_t  enable_term;

  // Firmware update info
  uint8_t update_req_node_id;
  uint8_t update_req_transfer_id;
  uint8_t update_req_priority;

  uint8_t server_node_id;
  uint8_t firmware_file_path_len;
  char    firmware_file_path[200];

  // Checksum to make sure the data is valid
  uint16_t crc;

} boot_info_t;

/**
 * Factory Info
 */
// clang-format off
#define FACTORY_INFO_MAGIC                                            0x10224589U
#define FACTORY_INFO_VERSION                                          1

#define CAL_NAME_HSI                                                  "cal.hsi"
#define CAL_DEFAULT_HSI                                               16
#define CAL_MAX_HSI                                                   32
#define CAL_MIN_HSI                                                   0

#define CAL_NAME_DEVICE_TYPE                                          "device.type"
#define CAL_MAX_DEVICE_TYPE                                           255
#define CAL_MIN_DEVICE_TYPE                                           0

#define CAL_NAME_DEVICE_VERSION_MAJOR                                 "device.version.major"
#define CAL_MAX_DEVICE_VERSION_MAJOR                                  255
#define CAL_MIN_DEVICE_VERSION_MAJOR                                  0

#define CAL_NAME_DEVICE_VERSION_MINOR                                 "device.version.minor"
#define CAL_MAX_DEVICE_VERSION_MINOR                                  255
#define CAL_MIN_DEVICE_VERSION_MINOR                                  0

#define CAL_NAME_DEVICE_SERIAL                                        "device.serial"
#define CAL_DEFAULT_DEVICE_SERIAL                                     ""

#define CAL_NAME_HASH                                                 "device.hash"
#define CAL_DEFAULT_HASH                                              ""

#define CAL_NAME_UNLOCK_CODE                                          "parameter_unlock_code"
#define CAL_MAX_UNLOCK_CODE                                           UINT32_MAX
#define PARAMETER_UNLOCK_CODE                                         7868

#define CAL_NAME_PRODUCTION_TIMESTAMP                                 "production.timestamp"
#define CAL_MAX_PRODUCTION_TIMESTAMP                                  2147483647
#define CAL_MIN_PRODUCTION_TIMESTAMP                                  0

// Need string support
#define CAL_NAME_PRODUCTION_QA                                        "production.qa"
#define CAL_DEFAULT_PRODUCTION_QA                                     ""

// clang-format on

// This is located at EEPROM OFFSET = 0 and should only be written by the bootloader
// It wastes some memory but makes IO simpler
typedef struct __attribute__((aligned(4))) {

  uint32_t magic;
  uint8_t  struct_version;

  // Size of the entire structure (header + device-specific stuff)
  uint8_t struct_size;

  // Set this to zero when computing the checksum
  uint16_t crc;

  // Device Type
  uint32_t device_type;

  // Hardware version
  uint32_t device_version_major;
  uint32_t device_version_minor;

  // Basic serial number
  uint8_t device_serial[10];

  // UNIX timestamp for when the device was tested and initialized
  uint32_t production_timestamp;

  // QA Operator
  uint8_t production_qa[8];

  // Authenticity hash
  uint8_t crypto_hash[64];

  // Single 32bit clock trim variable that can be internally split if necessary
  uint32_t clock_trim;

  uint8_t reserved[18];

} factory_info_header_t;

/*
 * Parameter Info
 */
// clang-format off
#define PARAMETER_HEADER_MAGIC                                        0x70314301U

// clang-format on

// This is located at EEPROM OFFSET = 0
typedef struct __attribute__((packed)) {

  uint32_t magic;

  /*
   * On startup we check if the parameter version is the same as the
   * current parameter version and if not we load defaults into the correct
   * places on the eepprom and if necessary upgrade any variables
   */
  uint16_t version;

  // Take up an entire page
  uint8_t reserved[10];

} parameter_header_t;

/**
 * Parameter Information Struct used for saving / loading to / from eeprom
 */
typedef struct {

  char *name;

  uint8_t type;

  // First version that included this variable
  uint8_t version;

  // Variable offset in the parameter struct
  size_t struct_offset;

  // Actual variable size from sizeof
  uint8_t variable_size;

  // Variable offset in the EEPROM
  size_t eeprom_offset;

  // Variable size when stored on the EEPROM
  uint8_t eeprom_size;

  // Offset in the factory calibration struct
  size_t calibration_offset;

  // For Integers / Booleans
  int64_t i_default;
  int64_t i_max;
  int64_t i_min;

  // For Real
  float f_default;
  float f_max;
  float f_min;

} parameter_info_t;

// clang-format off

// Value tag types
#define PARAM_VALUE_EMPTY                                             0
#define PARAM_VALUE_INT                                               1
#define PARAM_VALUE_REAL                                              2
#define PARAM_VALUE_BOOL                                              3
#define PARAM_VALUE_STRING                                            4

// Macros for building factory info table
#define ADD_CAL_BOOL(NAME, STR, VAR)              {CAL_NAME_ ## NAME,\
                                                   PARAM_VALUE_BOOL, \
                                                   0,                \
                                                   offsetof(STR, VAR),\
                                                   sizeof(((STR *)0)->VAR)}

#define ADD_CAL_INT(NAME, STR, VAR, DEFAULT)      {CAL_NAME_ ## NAME,\
                                                   PARAM_VALUE_INT,           \
                                                   0,                         \
                                                   offsetof(STR, VAR),        \
                                                   sizeof(((STR *)0)->VAR),   \
                                                    0,0,0,\
                                                   DEFAULT,\
                                                   (int64_t) CAL_MAX_ ## NAME,\
                                                   (int64_t) CAL_MIN_ ## NAME}

#define ADD_CAL_REAL(NAME, STR, VAR, DEFAULT)     {CAL_NAME_ ## NAME,\
                                                   PARAM_VALUE_REAL, \
                                                   0,                \
                                                   offsetof(STR, VAR),\
                                                   sizeof(((STR *)0)->VAR),\
                                                   0,0,0,            \
                                                   0,0,0,            \
                                                   DEFAULT,          \
                                                   (float) CAL_MAX_ ## NAME,\
                                                   (float) CAL_MIN_ ## NAME}

#define ADD_CAL_STRING(NAME, STR, VAR)            {CAL_NAME_ ## NAME,\
                                                   PARAM_VALUE_STRING, \
                                                   0,                \
                                                   offsetof(STR, VAR),\
                                                   sizeof(((STR *)0)->VAR) \
                                                   }

// Macros for building parameter info table
#define ADD_PARAM_BOOL(NAME, STR, VAR, CAL){ PARAM_NAME_ ## NAME,\
                                             PARAM_VALUE_BOOL,\
                                             PARAM_VERSION_## NAME,\
                                             offsetof(STR, VAR),\
                                             sizeof(((STR *)0)->VAR),\
                                             PARAM_OFFSET_## NAME,\
                                             PARAM_SIZE_## NAME, \
                                             CAL, \
                                             (int64_t)PARAM_DEFAULT_ ## NAME}

#define ADD_PARAM_INT(NAME, STR, VAR, CAL){ PARAM_NAME_ ## NAME,\
                                            PARAM_VALUE_INT,\
                                            PARAM_VERSION_## NAME,\
                                            offsetof(STR, VAR),\
                                            sizeof(((STR *)0)->VAR),\
                                            PARAM_OFFSET_## NAME,\
                                            PARAM_SIZE_## NAME,\
                                            CAL,\
                                            (int64_t) PARAM_DEFAULT_ ## NAME,\
                                            (int64_t) PARAM_MAX_ ## NAME,\
                                            (int64_t) PARAM_MIN_ ## NAME}

#define ADD_PARAM_REAL(NAME, STR, VAR, CAL){ PARAM_NAME_ ## NAME,\
                                             PARAM_VALUE_REAL,\
                                             PARAM_VERSION_## NAME,\
                                             offsetof(STR, VAR),\
                                             sizeof(((STR *)0)->VAR),\
                                             PARAM_OFFSET_## NAME,\
                                             PARAM_SIZE_## NAME,\
                                             CAL,\
                                             0,0,0,\
                                             (float) PARAM_DEFAULT_ ## NAME,\
                                             (float) PARAM_MAX_ ## NAME,\
                                             (float) PARAM_MIN_ ## NAME}

// clang-format on
enum EepromLoadState {
  EEPROM_LOAD_STATE_ALL_OK               = 0,
  EEPROM_LOAD_STATE_DEVICE_ERROR         = 1,
  EEPROM_LOAD_STATE_SOME_PARAMS_UPGRADED = 2,
  EEPROM_LOAD_STATE_ALL_DEFAULTS_LOADED  = 3,
};

/**
 * Rolling Buffers
 */
typedef struct {

  int32_t  data[ROLLING_BUFFER_MAX_SIZE];
  uint32_t timestamp[ROLLING_BUFFER_MAX_SIZE];
  uint16_t count;
  uint16_t index;
  uint16_t index_prev;
} rolling_buffer_t;

float compute_average(rolling_buffer_t b);

void rolling_buffer_add(rolling_buffer_t *b, int32_t value);

/**
 * Systems and Clock stuff
 */

void Busy_Wait(uint32_t delay_ms);

/**
 * CRC 16 - Used for packets reassembly
 */
uint16_t CRC_16_Add_Byte(uint16_t crc_val,
                         uint8_t  byte);

uint16_t CRC_16_Add_Signature(uint16_t crc,
                              uint64_t data_type_signature);

/**
 * Compute a CRC_16 signature
 *
 * @param ptr
 * @param count
 * @return
 */
uint16_t CRC_16_Compute(uint16_t    crc,
                        const void *ptr,
                        size_t      count);

/**
 * CRC 64 - Used for image verification
 */
// clang-format off
#define CRC_64_WE_POLY                                               0x42F0E1EBA9EA3693
#define CRC_64_WE_INIT                                               0xFFFFFFFFFFFFFFFF
#define CRC_64_WE_MASK                                               0xFFFFFFFFFFFFFFFF
// clang-format on

uint64_t CRC_64_Compute(const uint64_t *table,
                        uint64_t        input_crc,
                        size_t          length,
                        const uint8_t  *data);

void CRC_64_Generate_Table(uint64_t  polynomial,
                           uint64_t *output_table);

// clang-format off
#if USE_FDCAN
#define FDCAN                                                        1
#define CAN_HANDLE                                                   FDCAN_GlobalTypeDef
#define CAN_RX_HEADER                                                FDCAN_RxHeaderTypeDef
#define CAN_TX_HEADER                                                FDCAN_TxHeaderTypeDef
#else
#define CAN_HANDLE                                                   CAN_TypeDef
#define CAN_RX_HEADER                                                CAN_RxHeaderTypeDef
#define CAN_TX_HEADER                                                CAN_TxHeaderTypeDef
#endif
// clang-format on

/**
 * FDCAN / bxCAN Functions
 */

#if FDCAN == 0
bool bxCAN_Init(CAN_HANDLE *can,
                uint32_t    baud_kbps,
                uint32_t    timeout,
                bool        enable_ART);

void bxCAN_Config_Filter(CAN_HANDLE *can,
                         uint8_t     FilterNumber,
                         uint32_t    FilterIdHigh,
                         uint32_t    FilterIdLow,
                         uint32_t    FilterMaskIdHigh,
                         uint32_t    FilterMaskIdLow);

void bxCAN_Config_Filters(CAN_HANDLE     *can,
                          uint8_t         filter_count,
                          const uint32_t *filters);

bool bxCAN_Receive(CAN_HANDLE    *can,
                   uint32_t       fifo,
                   CAN_RX_HEADER *header,
                   uint8_t        payload[]);

bool bxCAN_Transmit(CAN_HANDLE    *can,
                    CAN_TX_HEADER *header,
                    const uint8_t  payload[],
                    uint32_t       timeout);
#else


void FDCAN_Config_Filter(FDCAN_GlobalTypeDef *can,
                         uint8_t              FilterNumber,
                         uint32_t             FilterIdHigh,
                         uint32_t             FilterIdLow,
                         uint32_t             FilterMaskIdHigh,
                         uint32_t             FilterMaskIdLow);

void FDCAN_Config_Filters(FDCAN_GlobalTypeDef *can,
                          uint8_t              filter_count,
                          const uint32_t      *filters);

typedef enum {
  FDCAN_INIT_MODE_NORMAL = 0,
  FDCAN_INIT_MODE_RESTRICTED_OPERATION,
  FDCAN_INIT_MODE_BUS_MONITORING,
  FDCAN_INIT_MODE_INTERNAL_LOOPBACK,
  FFDCAN_INIT_MODE_EXTERNAL_LOOPBACK,
} fdcan_init_mode;

bool FDCAN_Init(FDCAN_GlobalTypeDef *can,
                fdcan_init_mode      mode,
                uint32_t             baud_kbps,
                bool                 disable_automatic_retransmission,
                bool                 transmit_pause,
                bool                 protocol_exception_handling,
                uint32_t             timeout);

bool FDCAN_GetReceivedMessage(FDCAN_GlobalTypeDef *can,
                              uint32_t             fifo,
                              CAN_RX_HEADER       *header,
                              uint8_t              payload[]);

bool FDCAN_QueueTransmitMessage(FDCAN_GlobalTypeDef *can,
                                uint32_t             can_id,
                                const uint8_t        payload[],
                                uint32_t             payload_len);

#endif

#if !defined(USE_LL_GPIO)
/**
 * GPIO Routines
 */

void GPIO_Config_Pins(GPIO_TypeDef *GPIOx,
                      uint32_t      Pin,
                      uint32_t      Mode,
                      uint32_t      Type,
                      uint32_t      Pull,
                      uint32_t      Speed);

void GPIO_Config_Pins_EXTI(GPIO_TypeDef *GPIOx,
                           uint32_t      Pin,
                           uint32_t      Mode);

void GPIO_DeConfig_Pins(GPIO_TypeDef *GPIOx,
                        uint32_t      GPIO_Pin);
#endif

/**
 * FLASH Interface
 */

#if defined(FIRMWARE_PAGE_COUNT)

bool Flash_Erase_Pages(uint32_t base_address,
                       uint8_t  page_count);

#endif

#if defined(FIRMWARE_SECTOR_COUNT)

bool Flash_Erase_Sectors(uint32_t sector_start,
                         uint8_t  sector_count);

#endif

bool Flash_Write_Block(uint32_t       memory_offset,
                       const uint8_t *buffer,
                       uint32_t       buffer_len);

/**
 * I2C
 *
 *  - I2C_Write_Data - Write 'raw' data to the i2c bus (I2C_Transmit)
 *  - I2C_Read/Write_Register - For chips that need the register address "written" before reads
 *
 */

/**
 *
 * @param hi2c
 * @param address_7bit
 * @param length
 * @param data
 * @return
 */
bool I2C_Write(I2C_TypeDef   *i2c,
               uint8_t        address_7bit,
               size_t         length,
               const uint8_t *data,
               uint32_t       timeout_ms);

bool I2C_Read_Register(I2C_TypeDef *i2c,
                       uint8_t      address_7bit,
                       uint8_t      register_address,
                       size_t       data_length,
                       uint8_t     *data,
                       uint32_t     timeout_ms);

bool I2C_Write_Register(I2C_TypeDef   *i2c,
                        uint8_t        address_7bit,
                        uint8_t        register_address,
                        size_t         data_length,
                        const uint8_t *data,
                        uint32_t       timeout_ms);

/**
 * EEPROM Function
 *
 *  - EEPROM_Read/Write -> Read write
 *  - EEPROM_Bulk_Read/Write ->
 *
 */
bool EEPROM_Page_Read(I2C_TypeDef *i2c,
                      uint8_t      chip_address_7bit,
                      uint8_t      start_page,
                      size_t       byte_count,
                      uint8_t     *p);

bool EEPROM_Page_Write(I2C_TypeDef *i2c,
                       uint8_t      address_7bit,
                       uint8_t      start_page,
                       size_t       byte_count,
                       uint8_t     *p);

bool EEPROM_Short_Read(I2C_TypeDef *i2c,
                       uint8_t      chip_address_7bit,
                       uint16_t     address,
                       size_t       byte_count,
                       uint8_t     *p);

/**
 * Higher Level Routines
 */

/**
 * Initialize a uavcan node.
 *
 * @param node
 * @param health
 * @param mode
 * @param default_node_id
 * @param sw_ver_major
 * @param sw_ver_minor
 * @param vcs_commit
 * @param image_crc
 * @param app_name
 */

void Node_Init(node_info_t           *node,
               uint8_t                health,
               uint8_t                mode,
               uint8_t                default_node_id,
               uint8_t                sw_ver_major,
               uint8_t                sw_ver_minor,
               uint32_t               vcs_commit,
               uint64_t               image_crc,
               const char            *app_name,
               factory_info_header_t *factory_info);

/**
 *
 * Jump to the Pomegranate Systems STM32 Micro-Bootloader
 *
 * Rough Steps:
 *
 * 1. Fill out boot_info data structure which should live at the beginning of RAM
 * 2. Disable all devices, DMA transfers, interrupts.
 * 3. Call Bootloader_Jump(FLASH_BASE)
 *
 */
void Bootloader_Jump(uint32_t flash_base);

/**
 * Factory Info
 */
#define FACTORY_INFO_START_PAGE 0


/**
 * Load the factory info block form EEPROM.
 *
 *
 * @param hi2c
 * @param address_7bit
 * @param param_base
 */
enum EepromLoadState FactoryInfo_Load(I2C_TypeDef *i2c,
                         uint8_t      address_7bit,
                         size_t       struct_size,
                         uint8_t     *param_base);

/**
 * This will compute the CRC and save the structure
 */
bool FactoryInfo_Save(I2C_TypeDef *i2c,
                      uint8_t      address_7bit,
                      size_t       struct_size,
                      uint8_t     *param_base);

/**
 * Parameter Handling
 */


void Params_Load_Defaults(size_t                  parameter_count,
                          const parameter_info_t *info_table,
                          const uint8_t          *factory_info_base,
                          bool                   *dirty_table,
                          uint8_t                *param_base);

enum EepromLoadState Params_EEPROM_Load(I2C_TypeDef            *i2c,
                           uint8_t                 address_7bit,
                           uint8_t                 header_start_page,
                           uint8_t                 parameter_start_page,
                           int                     parameter_version,
                           size_t                  parameter_count,
                           const parameter_info_t *info_table,
                           const uint8_t          *factory_info_base,
                           bool                   *dirty_table,
                           uint8_t                *param_base);

bool Params_EEPROM_Save(I2C_TypeDef            *i2c,
                        uint8_t                 address_7bit,
                        uint8_t                 header_start_page,
                        uint8_t                 parameter_start_page,
                        int                     parameter_version,
                        size_t                  parameter_count,
                        const parameter_info_t *info_table,
                        bool                   *dirty_table,
                        const uint8_t          *param_base);

#ifdef __cplusplus
}
#endif
