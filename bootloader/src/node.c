/**
* @file
* @brief Handle "network traffic" from DroneCAN
*
* @author Stou Sandalski <stou@p-systems.io>
* @copyright (c) 2023 - Pomegranate Systems LLC
* @license MIT and 3-Clause BSD (See LICENSE.md)
*/


#include "node.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "bootloader.h"
#include "main.h"

/**
 * Globals
 */

extern uint32_t System_Tick;

extern boot_info_t    boot_info;
extern node_info_t    node;
extern factory_info_t factory_info;

/**
 * CANbus Reception Filters {id, mask}
 */

uint32_t can_filters[][2] = {
    {UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_ID, UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_MASK},
    {UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_ID, UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_MASK},
    {UAVCAN_PROTOCOL_PARAM_FILTER_ID, UAVCAN_PROTOCOL_PARAM_FILTER_MASK},
    {UAVCAN_PROTOCOL_RESTART_NODE_FILTER_ID, UAVCAN_PROTOCOL_RESTART_NODE_FILTER_MASK},
    {UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_ID, UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_MASK},
    {UAVCAN_PROTOCOL_FILE_READ_FILTER_ID, UAVCAN_PROTOCOL_FILE_READ_FILTER_MASK}
#if ENABLE_OPTION_BYTE_WRITE
    , {IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_FILTER_ID, IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_FILTER_MASK}
#endif
};

/**
 * Request Handling
 */

request_queue_t queue;

// Parameter handling
parameter_getset_t param_get_set;

extern can_params_t  can_params;
extern uavcan_init_t uavcan_init;

// File IO
uint8_t  file_read_last_error;
uint32_t file_buffer_length = 0;
uint8_t  file_read_buffer[FILE_BUFFER_SIZE];
uint32_t firmware_update_write_offset;
bool     flash_memory_erased;
uint64_t file_remote_offset = 0;

uint32_t file_last_read_timestamp = 0;
uint8_t  file_update_retry_count  = 0;

// Option byte writer
#if ENABLE_OPTION_BYTE_WRITE
uint8_t obwrite_data[2];
bool    enable_option_byte_writer;
#endif

extern uint32_t timestamp_led_off_time;

/**
 * Initialize the can interface
 *
 *
 * @param can
 * @param bus_speed
 * @return
 */
bool CANbus_Init() {

  uavcan_init.can_if        = CAN;
  uavcan_init.can_filter_if = CAN_FILTER_IF;
  uavcan_init.filter_count  = (sizeof(can_filters) / (2 * sizeof(uint32_t)));
  uavcan_init.can_filters   = &can_filters[0][0];

  return UAVCAN_Init(&boot_info,
                     &node,
                     &can_params,
                     &uavcan_init);
}

void Service_Request_Begin_Firmware_Update(request_t *request) {
  if (node.mode == UAVCAN_NODE_MODE_SOFTWARE_UPDATE) {
    Send_BeginFirmwareUpdate_Response(node.id,
                                      *request,
                                      UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_IN_PROGRESS,
                                      "");
  } else {
    node.mode               = UAVCAN_NODE_MODE_SOFTWARE_UPDATE;
    node.health             = UAVCAN_NODE_HEALTH_OK;
    node.vendor_status_code = 0;

    Send_BeginFirmwareUpdate_Response(node.id,
                                      *request,
                                      UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_OK,
                                      "");

    // Start writing firmware... at the beginning
    firmware_update_write_offset = FIRMWARE_BASE_ADDRESS;

    // Trigger erase if file read is successful so that the firmware isn't accidentally
    // erased by a stray firmware update call.
    flash_memory_erased = false;

    file_buffer_length      = 0;
    file_remote_offset      = 0;
    file_update_retry_count = 0;

    // Setup a file read request
    Send_FileRead_Request(node.id,
                          boot_info.server_node_id,
                          0,
                          file_remote_offset,
                          boot_info.firmware_file_path_len,
                          boot_info.firmware_file_path);
    file_last_read_timestamp = System_Tick;
  }

  // Trigger a new node status message
  node.timestamp = 0;
}

bool Service_Request_File_Read() {

  // No data was sent
  if (file_buffer_length == 0) {
#if ENABLE_IMAGE_VERIFY
    if (Verify_Application_Image(FIRMWARE_BASE_ADDRESS)) {
      // Transfer is over and the image is good... reboooot!
      NVIC_SystemReset();
    } else {
      node.health             = UAVCAN_NODE_HEALTH_ERROR;
      node.vendor_status_code = UAVCAN_VSC_INVALID_IMAGE;
      return false;
    }
#else
    // Otherwise whatever...
    NVIC_SystemReset();
#endif
  }

  // Erase the memory if we have data but memory has not been erased yet.
  if (!flash_memory_erased) {
#if defined(FIRMWARE_PAGE_COUNT)
    flash_memory_erased = Flash_Erase_Pages(FIRMWARE_BASE_ADDRESS, FIRMWARE_PAGE_COUNT);
#elif defined(FIRMWARE_BASE_SECTOR) && defined(FIRMWARE_SECTOR_COUNT)
    flash_memory_erased = Flash_Erase_Sectors(FIRMWARE_BASE_SECTOR, FIRMWARE_SECTOR_COUNT);
#endif
  }

#if defined(LED_RGB_B_GPIO_Port)
  // Indicate YELLOW for software update
  LL_GPIO_ResetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
  LL_GPIO_ResetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
  LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
#elif defined(LED_ACT_GPIO_Port)
  LL_GPIO_ResetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
#endif
  timestamp_led_off_time = 0;

  if (!Flash_Write_Block(firmware_update_write_offset,
                         file_read_buffer,
                         file_buffer_length)) {

    firmware_update_write_offset = 0;
    node.health                  = UAVCAN_NODE_HEALTH_ERROR;
    node.vendor_status_code      = UAVCAN_VSC_FLASH_WRITE_FAIL;
    return false;
  }

  // Advance the firmware offset
  firmware_update_write_offset += file_buffer_length;
  file_remote_offset += file_buffer_length;

  // If there is more data, send a request for new data
  Busy_Wait(10);

  Send_FileRead_Request(node.id,
                        boot_info.server_node_id,
                        0,
                        file_remote_offset,
                        boot_info.firmware_file_path_len,
                        boot_info.firmware_file_path);
  file_last_read_timestamp = System_Tick;
  file_update_retry_count  = 0;
  return true;
}

void Service_Requests() {

  // If there are no requests pending, return
  if (!queue.request[queue.top].pending) {
    return;
  }

  // Dispatch message
  switch (queue.request[queue.top].type_id) {

#if ENABLE_OPTION_BYTE_WRITE
    case IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID:
      Handle_Write_Option_Bytes_Request(node.id,
                                        queue.request[queue.top],
                                        enable_option_byte_writer,
                                        obwrite_data);

      Busy_Wait(100);
      NVIC_SystemReset();
#endif

    default:
      // Dispatch all the rest
      Service_UAVCAN_Requests(&node,
                              &uavcan_init,
                              &queue,
                              &boot_info,
                              true,
                              &can_params);
      return;
  }

  Request_Queue_Pop(&queue);
}

/**
 *
 * @param data_type_id
 * @param is_service
 * @param source_node_id
 * @param priority
 * @param transfer_id
 * @param payload
 * @param payload_len
 */
void UAVCAN_Handle_Message(const uint16_t                     data_type_id,
                           __attribute__((unused)) const bool is_service,
                           const uint8_t                      source_node_id,
                           const uint8_t                      priority,
                           uint8_t                            transfer_id,
                           uint8_t                           *payload,
                           uint32_t                           payload_len) {

  // Dispatch data
  switch (data_type_id) {

    case UAVCAN_PROTOCOL_FILE_READ_ID:
      // uavcan.protocol.file.Read
      // https://uavcan.org/Specification/7._List_of_standard_data_types/#read

      // File error code is the first 2 byte
      file_read_last_error = (payload[0] | (payload[1] << 8));

      if (file_read_last_error == 0) {
        // Copy the new data into the main buffer
        memcpy(file_read_buffer, &payload[2], payload_len - 2);
        file_buffer_length = payload_len - 2;

        // Queue up a request to write the data
        Request_Queue_Add_Unique(&queue,
                                 data_type_id,
                                 source_node_id,
                                 transfer_id,
                                 priority,
                                 0);

      } else {
        file_buffer_length      = 0;
        node.health             = UAVCAN_NODE_HEALTH_WARNING;
        node.vendor_status_code = UAVCAN_VSC_FILE_READ_FAIL;
      }

      break;

#if ENABLE_OPTION_BYTE_WRITE
    case IO_P_SYSTEMS_DEVICE_WRITE_OPTION_BYTES_ID:
      // io.p-systems.OptionByteWrite
      Request_Queue_Add_Unique(&queue,
                               data_type_id,
                               source_node_id,
                               transfer_id,
                               priority,
                               0);

      obwrite_data[0] = payload[0];
      obwrite_data[1] = payload[1];

      break;
#endif


    default:
      Process_UAVCAN_Message(data_type_id,
                             is_service,
                             source_node_id,
                             priority,
                             transfer_id,
                             payload,
                             payload_len,
                             &node,
                             &uavcan_init,
                             &queue,
                             &boot_info);
      break;
  }
}
