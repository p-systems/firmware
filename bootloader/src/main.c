/**
 * @file
 * @brief Hardware init and main entry for the bootloader
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @copyright (c) 2021 STMicroelectronics
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

/**
 * Includes
 */
#include "main.h"

#include <stdlib.h>
#include <string.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "bootloader.h"

/**
 * Globals
 */

// Clock tick
uint32_t       System_Tick    = 0;
const uint32_t Tick_Frequency = 1;

// App descriptor for the bootloader itself (tools/make_can_boot_descriptor.py)
__attribute__((section(".app_descriptor"))) app_descriptor_t app_descriptor = {
    .signature     = {'A', 'P', 'D', 'e', 's', 'c'},
    .image_crc     = 0,
    .image_size    = 0,
    .vcs_commit    = 0,
    .major_version = SOFTWARE_VERSION_MAJOR,
    .minor_version = SOFTWARE_VERSION_MINOR,
    .reserved      = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff}};

// Shared memory section with firmware used for passing info between bootloader and firmware
// leave this alone...
__attribute__((section(".boot_info_section"))) boot_info_t boot_info;

factory_info_t  factory_info;
device_status_t status;

// UAVCAN node
node_info_t node;

// CAN init structures
can_params_t  can_params;
uavcan_init_t uavcan_init;

// CAN device used by the "library"
CAN_TypeDef *CAN_device = CAN;

// The request queue
extern request_queue_t queue;

// Option byte writer
#if ENABLE_OPTION_BYTE_WRITE
extern bool enable_option_byte_writer;
#endif


void Error_Handler() {
  while (1) {
#if defined(LED_FAULT_Pin)
    LL_GPIO_SetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
    Busy_Wait(900);

    LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
    Busy_Wait(100);
#endif
  }
}

/**
 * IRQ Handlers
 */

#if defined(STM32F0)

void CEC_CAN_IRQHandler() {
#error Unimplemented CAN handler
}

#elif defined(STM32F3)

void USB_LP_CAN_RX0_IRQHandler() {

  if (((CAN->IER & CAN_IT_RX_FIFO0_MSG_PENDING) == 0U)
      || ((CAN->RF0R & CAN_RF0R_FMP0) == 0U)) {
    return;
  }

  CAN_RX_HEADER header = {};
  uint8_t       payload[8];

  if (!bxCAN_Receive(CAN, CAN_RX_FIFO0, &header, payload)) {
    Error_Handler();
    return;
  }

  const uint32_t can_id = header.ExtId & 0x1FFFFFFFU;
  UAVCAN_Process_Frame(node.id, can_id, payload, header.DLC);
}

#elif defined(STM32F4)

#if CAN_UNIT == 1

void CAN1_RX0_IRQHandler() {
#elif CAN_UNIT == 2

void CAN2_RX0_IRQHandler() {
#endif

  CAN_RX_HEADER header = {};
  uint8_t       payload[8];

  if (!bxCAN_Receive(CAN, CAN_RX_FIFO0, &header, payload)) {
    Error_Handler();
    return;
  }

  const uint32_t can_id = header.ExtId & 0x1FFFFFFFU;
  UAVCAN_Process_Frame(node.id, can_id, payload, header.DLC);
}

/**
 *  CAN1/2 SCE (Status-Change / Error ) Interrupt
 */
#if CAN_UNIT == 1

void CAN1_SCE_IRQHandler() {
#elif CAN_UNIT == 2

void CAN2_SCE_IRQHandler() {
#endif

#if defined(LED_FAULT_Pin)
  LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif
}

#endif

void SysTick_Handler() {
  System_Tick += Tick_Frequency;
}

/**
 * Hardware Init Functions (ST)
 *
 */

void CAN_Init() {

#if defined(STM32F0)
#exception("Not Implemented")
#elif defined(STM32F3)
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_CAN);

  // Enable CAN Interrupts
  NVIC_SetPriority(USB_LP_CAN_RX0_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_EnableIRQ(USB_LP_CAN_RX0_IRQn);
#elif defined(STM32F4)

#if (CAN_UNIT == 1) || (CAN_UNIT == 2)
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_CAN1);
#endif

#if CAN_UNIT == 1
  NVIC_SetPriority(CAN1_RX0_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(CAN1_SCE_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));

  NVIC_EnableIRQ(CAN1_RX0_IRQn);
  NVIC_EnableIRQ(CAN1_SCE_IRQn);
#elif CAN_UNIT == 2
  LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_CAN2);
  NVIC_SetPriority(CAN2_RX0_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(CAN2_SCE_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));

  NVIC_EnableIRQ(CAN2_RX0_IRQn);
  NVIC_EnableIRQ(CAN2_SCE_IRQn);
#endif

#endif
}

/**
 * Deinit the CAN hardware clocks and interrupts
 */
void CAN_DeInit() {

#if defined(STM32F0)

#elif defined(STM32F3)

  // Disable all CAN attributes
  NVIC_DisableIRQ(USB_HP_CAN_TX_IRQn);
  NVIC_DisableIRQ(USB_LP_CAN_RX0_IRQn);
  NVIC_DisableIRQ(CAN_RX1_IRQn);
  NVIC_DisableIRQ(CAN_SCE_IRQn);

  // Clock
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_CAN);

#elif defined(STM32F4)

  // CAN1 and CAN2 need the CAN1 clock
#if (CAN_UNIT == 1) || (CAN_UNIT == 2)
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_CAN1);
#endif

#if CAN_UNIT == 1
  NVIC_DisableIRQ(CAN1_TX_IRQn);
  NVIC_DisableIRQ(CAN1_RX0_IRQn);
  NVIC_DisableIRQ(CAN1_SCE_IRQn);
#elif CAN_UNIT == 2
  NVIC_DisableIRQ(CAN2_TX_IRQn);
  NVIC_DisableIRQ(CAN2_RX0_IRQn);
  NVIC_DisableIRQ(CAN2_SCE_IRQn);
  LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_CAN2);
#endif

#endif
}

void GPIO_Init() {

  // Enable clocks
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOF);

#if defined(STM32F4)
  LL_AHB1_GRP1_EnableClock(LL_AHB1_GRP1_PERIPH_GPIOH);
#endif

  // Configure Pins
#ifdef BOOT_GPIO_Port
  GPIO_Config_Pins(BOOT_GPIO_Port,
                   BOOT_Pin,
                   GPIO_MODE_INPUT,
                   GPIO_MODE_INPUT,
                   BOOT_PIN_ACTIVE_LOW ? GPIO_PULLUP : GPIO_PULLDOWN,
                   GPIO_SPEED_FREQ_LOW);
#endif

#if defined(CAN_RX_Pin)
  GPIO_Config_Pins(CAN_RX_GPIO_Port,
                   CAN_RX_Pin,
                   GPIO_MODE_AF_PP,
                   GPIO_MODE_AF_PP,
                   GPIO_PULLUP,
                   CAN_PIN_SPEED);
  LL_GPIO_SetAFPin_8_15(CAN_RX_GPIO_Port, CAN_RX_Pin, CAN_RX_AF_MODE);

  GPIO_Config_Pins(CAN_TX_GPIO_Port,
                   CAN_TX_Pin,
                   GPIO_MODE_AF_PP,
                   GPIO_MODE_AF_PP,
                   GPIO_PULLUP,
                   CAN_PIN_SPEED);
  LL_GPIO_SetAFPin_8_15(CAN_TX_GPIO_Port, CAN_TX_Pin, CAN_TX_AF_MODE);
#endif

#ifdef CAN_SILENT_Pin
  LL_GPIO_ResetOutputPin(CAN_SILENT_GPIO_Port, CAN_SILENT_Pin);
  GPIO_Config_Pins(CAN_SILENT_GPIO_Port,
                   CAN_SILENT_Pin,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_PULLDOWN,
                   GPIO_SPEED_FREQ_LOW);
#endif

#ifdef CAN_TERM_Pin
  LL_GPIO_ResetOutputPin(CAN_TERM_GPIO_Port, CAN_TERM_Pin);

  GPIO_Config_Pins(CAN_TERM_GPIO_Port,
                   CAN_TERM_Pin,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_PULLUP,
                   GPIO_SPEED_FREQ_LOW);
#endif

#ifdef EEPROM_WP_GPIO_Port
  // EEPROM WP
  LL_GPIO_SetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
  GPIO_Config_Pins(EEPROM_WP_GPIO_Port,
                   EEPROM_WP_Pin,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_MODE_OUTPUT_PP,
                   GPIO_PULLUP,
                   GPIO_SPEED_FREQ_LOW);
#endif

#if defined(LED_RGB_R_Pin)
  LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
  GPIO_Config_Pins(LED_RGB_R_GPIO_Port,
                   LED_RGB_R_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
  LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);

  LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
  GPIO_Config_Pins(LED_RGB_G_GPIO_Port,
                   LED_RGB_G_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
  LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);

  LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
  GPIO_Config_Pins(LED_RGB_B_GPIO_Port,
                   LED_RGB_B_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
  LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);

#else
#if defined(LED_ACT_Pin)
  LL_GPIO_SetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
  GPIO_Config_Pins(LED_ACT_GPIO_Port,
                   LED_ACT_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
#endif

#if defined(LED_FAULT_Pin)
  LL_GPIO_SetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
  GPIO_Config_Pins(LED_FAULT_GPIO_Port,
                   LED_FAULT_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
#endif
#endif

#if defined(TEST_MODE_ENTER_Pin)
  LL_GPIO_ResetOutputPin(TEST_MODE_ENTER_GPIO_Port, TEST_MODE_ENTER_Pin);
  GPIO_Config_Pins(TEST_MODE_ENTER_GPIO_Port,
                   TEST_MODE_ENTER_Pin,
                   GPIO_MODE_INPUT,
                   GPIO_MODE_INPUT,
                   GPIO_PULLUP,
                   GPIO_SPEED_FREQ_LOW);
#endif

#if defined(TEST_MODE_CLOCK_INPUT_Pin)
  LL_GPIO_ResetOutputPin(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin);
  GPIO_Config_Pins(TEST_MODE_CLOCK_INPUT_GPIO_Port,
                   TEST_MODE_CLOCK_INPUT_Pin,
                   GPIO_MODE_INPUT,
                   GPIO_MODE_INPUT,
                   GPIO_PULLUP,
                   GPIO_SPEED_FREQ_LOW);
#endif

#if defined(TEST_MODE_CLOCK_OUTPUT_Pin)
  LL_GPIO_SetOutputPin(TEST_MODE_CLOCK_OUTPUT_GPIO_Port, TEST_MODE_CLOCK_OUTPUT_Pin);
  GPIO_Config_Pins(TEST_MODE_CLOCK_OUTPUT_GPIO_Port,
                   TEST_MODE_CLOCK_OUTPUT_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_PULLUP,
                   GPIO_SPEED_FREQ_LOW);
#endif
}

void GPIO_DeInit() {

#ifdef BOOT_Pin
  GPIO_DeConfig_Pins(BOOT_GPIO_Port, BOOT_Pin);
#endif

#ifdef CAN_RX_Pin
  GPIO_DeConfig_Pins(CAN_RX_GPIO_Port, CAN_RX_Pin);
#endif

#ifdef CAN_TX_Pin
  GPIO_DeConfig_Pins(CAN_TX_GPIO_Port, CAN_TX_Pin);
#endif

#ifdef CAN_FAULT_Pin
  GPIO_DeConfig_Pins(CAN_FAULT_GPIO_Port, CAN_FAULT_Pin);
#endif

#ifdef CAN_SILENT_Pin
  GPIO_DeConfig_Pins(CAN_SILENT_GPIO_Port, CAN_SILENT_Pin);
#endif

#ifdef CAN_TERM_Pin
  GPIO_DeConfig_Pins(CAN_TERM_GPIO_Port, CAN_TERM_Pin);
#endif

#ifdef LED_Pin
  GPIO_DeConfig_Pins(LED_GPIO_Port, LED_Pin);
#endif

#ifdef LED_RGB_R_Pin
  GPIO_DeConfig_Pins(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
  GPIO_DeConfig_Pins(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
  GPIO_DeConfig_Pins(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
#endif

#ifdef TEST_MODE_CLOCK_INPUT_Pin
  GPIO_DeConfig_Pins(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin);
#endif

#ifdef TEST_MODE_CLOCK_OUTPUT_Pin
  GPIO_DeConfig_Pins(TEST_MODE_CLOCK_OUTPUT_GPIO_Port, TEST_MODE_CLOCK_OUTPUT_Pin);
#endif

  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOA);
  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOB);
  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOC);
  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOF);

#if defined(STM32F4)
  LL_AHB1_GRP1_DisableClock(LL_AHB1_GRP1_PERIPH_GPIOH);
#endif
}

#if ENABLE_I2C

void I2C_Init() {

  // GPIO Configuration
  // SCL
  LL_GPIO_SetPinMode(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinSpeed(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin, LL_GPIO_OUTPUT_OPENDRAIN);
  LL_GPIO_SetPinPull(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin, LL_GPIO_PULL_UP);
  I2C_INTERNAL_SCL_SET_AFPIN(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin, I2C_INTERNAL_SCL_AF_MODE);

  // SDA
  LL_GPIO_SetPinMode(I2C_INTERNAL_SDA_GPIO_Port, I2C_INTERNAL_SDA_Pin, LL_GPIO_MODE_ALTERNATE);
  LL_GPIO_SetPinSpeed(I2C_INTERNAL_SDA_GPIO_Port, I2C_INTERNAL_SDA_Pin, LL_GPIO_SPEED_FREQ_HIGH);
  LL_GPIO_SetPinOutputType(I2C_INTERNAL_SDA_GPIO_Port, I2C_INTERNAL_SDA_Pin, LL_GPIO_OUTPUT_OPENDRAIN);
  LL_GPIO_SetPinPull(I2C_INTERNAL_SDA_GPIO_Port, I2C_INTERNAL_SDA_Pin, LL_GPIO_PULL_UP);
  I2C_INTERNAL_SDA_SET_AFPIN(I2C_INTERNAL_SDA_GPIO_Port, I2C_INTERNAL_SDA_Pin, I2C_INTERNAL_SDA_AF_MODE);

  // Clock
  LL_APB1_GRP1_EnableClock(I2C_INTERNAL_CLOCK_PERIPH);

  // Disable device to make changes
  LL_I2C_Disable(I2C_INTERNAL);

  // Peripheral
  LL_I2C_DisableOwnAddress2(I2C_INTERNAL);
  LL_I2C_DisableGeneralCall(I2C_INTERNAL);
  LL_I2C_EnableClockStretching(I2C_INTERNAL);
  LL_I2C_SetMode(I2C_INTERNAL, LL_I2C_MODE_I2C);

#if defined(STM32F3)

  LL_I2C_ConfigFilters(I2C_INTERNAL, 0, 0);
  LL_I2C_SetTiming(I2C_INTERNAL, I2C_INTERNAL_TIMING);

  LL_I2C_DisableOwnAddress1(I2C_INTERNAL);
  LL_I2C_SetOwnAddress1(I2C_INTERNAL, 0, LL_I2C_OWNADDRESS1_7BIT);
  LL_I2C_SetOwnAddress2(I2C_INTERNAL, 0, LL_I2C_OWNADDRESS2_NOMASK);

#elif defined(STM32F4)
  LL_RCC_ClocksTypeDef rcc_clocks;
  LL_RCC_GetSystemClocksFreq(&rcc_clocks);
  LL_I2C_ConfigSpeed(I2C_INTERNAL, rcc_clocks.PCLK1_Frequency, I2C_INTERNAL_CLOCK, LL_I2C_DUTYCYCLE_2);
  LL_I2C_SetOwnAddress2(I2C_INTERNAL, 0);
#endif

  // Enable Device
  LL_I2C_Enable(I2C_INTERNAL);

  // Interrupt
  NVIC_SetPriority(I2C_INTERNAL_EV_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(I2C_INTERNAL_ER_IRQ, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));

  NVIC_EnableIRQ(I2C_INTERNAL_EV_IRQ);
  NVIC_EnableIRQ(I2C_INTERNAL_ER_IRQ);
}

void I2C_DeInit() {

  // Interrupts
  NVIC_DisableIRQ(I2C_INTERNAL_EV_IRQ);
  NVIC_DisableIRQ(I2C_INTERNAL_ER_IRQ);

  // Device
  LL_I2C_Disable(I2C_INTERNAL);

  // Clock
  LL_APB1_GRP1_DisableClock(I2C_INTERNAL_CLOCK_PERIPH);

  // Pins
  GPIO_DeConfig_Pins(I2C_INTERNAL_SCL_GPIO_Port, I2C_INTERNAL_SCL_Pin | I2C_INTERNAL_SDA_Pin);
}

#if ENABLE_I2C_DEVICE_CHECK
void I2C_DeviceCheck() {

#if defined(I2C_BUS_ERROR_MCP_TEMP)
  float temperature;
  if (!MCP9808_Readout(I2C_INTERNAL, MCP_PCB_ADDRESS_7B, &temperature)
      || (temperature > 100.0f)
      || (temperature < -20.0f)) {
    status.i2c_device_error |= I2C_BUS_ERROR_MCP_TEMP;
  }
#endif

#if defined(I2C_BUS_ERROR_INA_MAIN)
  if (!INA219_Test(I2C_INTERNAL, INA_MAIN_ADDRESS_7B)) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_MAIN;
  }
#endif

#if defined(I2C_BUS_ERROR_INA_AUX)
  if (!INA219_Test(I2C_INTERNAL, INA_AUX_ADDRESS_7B)) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_AUX;
  }
#endif

#if defined(STEPPER_CONTROLLER)
  float temperature_A;
  float temperature_B;
  uint8_t data_a;
  uint8_t data_b;

  status.i2c_device_error = status.i2c_device_error
                            | (INA219_Test(I2C_INTERNAL, DRIVER_A_INA_ADDRESS_7B) ? 0u : I2C_BUS_ERROR_INA_A)
                            | (INA219_Test(I2C_INTERNAL, DRIVER_B_INA_ADDRESS_7B) ? 0u : I2C_BUS_ERROR_INA_B)
                            | (I2C_Read_Register(I2C_INTERNAL, DRIVER_A_GPIO_ADDRESS_7B, PCA6408_REGISTER_CONFIGURATION, 1, &data_a, 100) ? 0u : I2C_BUS_ERROR_GPIO_A)
                            | (I2C_Read_Register(I2C_INTERNAL, DRIVER_B_GPIO_ADDRESS_7B, PCA6408_REGISTER_CONFIGURATION, 1, &data_b, 100) ? 0u : I2C_BUS_ERROR_GPIO_B)
                            | ((MCP9808_Readout(I2C_INTERNAL, DRIVER_A_MCP_ADDRESS_7B, &temperature_A)
                                && (temperature_A < 100.0f)
                                && (temperature_A > -20.0f))
                                   ? 0u
                                   : I2C_BUS_ERROR_MCP_A)
                            | ((MCP9808_Readout(I2C_INTERNAL, DRIVER_B_MCP_ADDRESS_7B, &temperature_B)
                                && (temperature_B < 100.0f)
                                && (temperature_B > -20.0f))
                                   ? 0u
                                   : I2C_BUS_ERROR_MCP_B);

#endif
}
#endif

#endif

void System_Clock_Init() {

#if defined(STM32F0)

  __HAL_RCC_HSI48_ENABLE();
  while (__HAL_RCC_GET_FLAG(RCC_FLAG_HSI48RDY) == RESET)
    ;

  MODIFY_REG(RCC->CFGR, RCC_CFGR_HPRE, RCC_SYSCLK_DIV1);
  __HAL_RCC_GET_FLAG(RCC_FLAG_HSI48RDY);
  __HAL_RCC_SYSCLK_CONFIG(RCC_SYSCLKSOURCE_HSI48);
  while (__HAL_RCC_GET_SYSCLK_SOURCE() != RCC_SYSCLKSOURCE_STATUS_HSI48)
    ;

  MODIFY_REG(RCC->CFGR, RCC_CFGR_PPRE, RCC_HCLK_DIV1);
  SystemCoreClock = HSI48_VALUE >> AHBPrescTable[(RCC->CFGR & RCC_CFGR_HPRE) >> RCC_CFGR_HPRE_BITNUMBER];

  // Configure the SysTick to have interrupt in 1ms time basis
  SysTick_Config(SystemCoreClock / 1000U);

  NVIC_SetPriority(SVC_IRQn, 0);
  NVIC_SetPriority(PendSV_IRQn, 0);

#elif defined(STM32F3)

  // Setup the HSI
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_HSI_Enable();

  while (!LL_RCC_HSI_IsReady()) {}

  // And the PLL
  LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSI_DIV_2, LL_RCC_PLL_MUL_4);
  LL_RCC_PLL_Enable();

  while (!LL_RCC_PLL_IsReady()) {}

  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB1_DIV_1);

  // SYSCLK
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);

#if ENABLE_I2C
  LL_RCC_SetI2CClockSource(LL_RCC_I2C1_CLKSOURCE_HSI);
  LL_RCC_SetI2CClockSource(LL_RCC_I2C2_CLKSOURCE_HSI);
#endif

#elif defined(STM32F4)

  LL_FLASH_SetLatency(LL_FLASH_LATENCY_0);
  while (LL_FLASH_GetLatency() != LL_FLASH_LATENCY_0) {}

  LL_PWR_SetRegulVoltageScaling(LL_PWR_REGU_VOLTAGE_SCALE1);
  LL_RCC_HSI_SetCalibTrimming(16);
  LL_RCC_HSI_Enable();

  while (!LL_RCC_HSI_IsReady()) {}

  LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
  LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_1);
  LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
  LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_HSI);

  // Wait until the system clock is ready
  while (LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_HSI) {}
  LL_SetSystemCoreClock(16000000);

  LL_RCC_SetTIMPrescaler(LL_RCC_TIM_PRESCALER_TWICE);

  MODIFY_REG(PWR->CR, PWR_CR_VOS, PWR_CR_VOS);
#endif

  SysTick_Config(SystemCoreClock / (1000U / Tick_Frequency));

  System_Tick = 0;
  SysTick_Config(SystemCoreClock / 1000);
  SysTick->CTRL |= LL_SYSTICK_CLKSOURCE_HCLK;

  NVIC_SetPriority(SysTick_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
}

void Hardware_DeInit() {

  CAN_DeInit();

#if ENABLE_I2C
  I2C_DeInit();
#endif

  // Disable peripheral hardware
  GPIO_DeInit();

  LL_RCC_DeInit();

#if 1
  // Reset Peripherals
#if defined(STM32F0)
  RCC->APB1RSTR = 0xFFFFFFFFU;
  RCC->APB1RSTR = 0x00000000U;

  RCC->APB2RSTR = 0xFFFFFFFFU;
  RCC->APB2RSTR = 0x00000000U;

  RCC->AHBRSTR = 0xFFFFFFFFU;
  RCC->AHBRSTR = 0x00000000U;
#elif defined(STM32F3)
  RCC->APB1RSTR = 0xFFFFFFFFU;
  RCC->APB1RSTR = 0x00000000U;

  RCC->APB2RSTR = 0xFFFFFFFFU;
  RCC->APB2RSTR = 0x00000000U;

  RCC->AHBRSTR = 0xFFFFFFFFU;
  RCC->AHBRSTR = 0x00000000U;
#elif defined(STM32F4)
  RCC->APB1RSTR = 0xFFFFFFFFU;
  RCC->APB1RSTR = 0x00U;

  RCC->APB2RSTR = 0xFFFFFFFFU;
  RCC->APB2RSTR = 0x00U;

  RCC->AHB1RSTR = 0xFFFFFFFFU;
  RCC->AHB1RSTR = 0x00U;

  RCC->AHB2RSTR = 0xFFFFFFFFU;
  RCC->AHB2RSTR = 0x00U;

  RCC->AHB3RSTR = 0xFFFFFFFFU;
  RCC->AHB3RSTR = 0x00U;
#endif
#endif

#if 1
  // Disable oscillator
  RCC->APB1ENR &= ~(RCC_APB1ENR_PWREN);
  RCC->APB2ENR &= ~(RCC_APB2ENR_SYSCFGEN);

  // HSI
  LL_RCC_HSI_Disable();

  // Disable interrupts
  NVIC->ICER[0] = 0xFFFFFFFF;
  NVIC->ICER[1] = 0xFFFFFFFF;
  NVIC->ICER[2] = 0xFFFFFFFF;

  // and clear pending
  NVIC->ICPR[0] = 0xFFFFFFFF;
  NVIC->ICPR[1] = 0xFFFFFFFF;
  NVIC->ICPR[2] = 0xFFFFFFFF;
#endif

  // Disable global interrupt
  __disable_irq();

  // Disable SisTick
#if defined(STM32F3) || defined(STM32F4)
  SysTick->CTRL = 0;
  SysTick->LOAD = 0;
  SysTick->VAL  = 0;
#endif
}


#if defined(TEST_MODE_CLOCK_INPUT_Pin)

/**
 * This waits for a low to high transition to start the clock
 * then stops the clock at the high to low transition
 *
 * Input clock should have a period of 1000 ms (or 1s)
 *
 */

#if IS_COMPREHENSIVE_TEST_MODE

void Test_Mode_Calibrate_Clock() {

  int32_t difference_lowest = 1000;
  int32_t trim_lowest       = 16;

  int32_t target_ticks = 1000;

  // Go through the entire parameter space, takes longer but it's simpler and
  // we don't get stuck in weird pseud-minima.
  for (int i = 0; i < 32; ++i) {
    LL_RCC_HSI_SetCalibTrimming(i);

#if defined(LED_FAULT_GPIO_Port)
    LL_GPIO_TogglePin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif

    // Wait for a high on clock input
    while (LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {
      __ASM volatile("nop");
    }

    // Then wait for a low
    while (!LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {
      __ASM volatile("nop");
    }

    // Start the clock
    uint32_t start_tick = System_Tick;

    // Go through one full wave
    while (LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {
      __ASM volatile("nop");
    }

    while (!LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {
      __ASM volatile("nop");
    }

    uint32_t stop_tick = System_Tick;

    int32_t actual_ticks = (int32_t) (stop_tick - start_tick);
    int32_t difference   = abs(actual_ticks - target_ticks);

    if ((i == 0) || (difference < difference_lowest)) {
      difference_lowest = difference;
      trim_lowest       = i;
    }
  }

  // Set final trim value
  LL_RCC_HSI_SetCalibTrimming(trim_lowest);

  // Store the value in the factory region
  factory_info.header.clock_trim = trim_lowest;

#if IS_AUTO_SAVE_CALIBRATION
  // Save the factory info table at the top of the EEPROM
#ifdef EEPROM_WP_GPIO_Port
  // Disable Write Protection
  LL_GPIO_ResetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
  FactoryInfo_Save(I2C_INTERNAL,
                   EEPROM_ADDRESS_7B,
                   sizeof(factory_info_t),
                   (uint8_t *) &factory_info);

#ifdef EEPROM_WP_GPIO_Port
  // Enable Write Protection
  LL_GPIO_SetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
#endif
}

#else

void Test_Mode_Calibrate_Clock(uint8_t start_trim) {

  int32_t target_ticks    = 1000;
  uint8_t trim            = start_trim;
  uint8_t prev_trim       = start_trim;
  int32_t prev_difference = 0;

  // Fixed loop to prevent some kind of lockup?
  for (int i = 0; i < 32; ++i) {

#if defined(LED_FAULT_GPIO_Port)
    LL_GPIO_TogglePin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif

    // Wait for a high on clock input
    while (LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {}

    // Then wait for a low
    while (!LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {}

    // Start the clock
    uint32_t start_tick = System_Tick;

    // Go through one full wave
    while (LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {}

    while (!LL_GPIO_IsInputPinSet(TEST_MODE_CLOCK_INPUT_GPIO_Port, TEST_MODE_CLOCK_INPUT_Pin)) {}

    uint32_t stop_tick = System_Tick;

    int32_t actual_ticks = (int32_t) (stop_tick - start_tick);
    int32_t difference   = actual_ticks - target_ticks;

    // Here we know that <trim> results in <difference>
    // After the first iteration compare to previous value and revert if it's better.

    if (i > 0) {
      // Check if the last time had smaller difference and if so, go back to that trim level
      if (abs(prev_difference) < abs(difference)) {
        trim = prev_trim;
        break;
      }
    }

    prev_trim       = trim;
    prev_difference = difference;

    // If the difference is positive then frequency is too high so increase trim... I guess
    if (difference < 0) {
      if (trim < 32) {
        trim = trim + 1;
      }
    } else if (difference > 0) {
      if (trim > 0) {
        trim = trim - 1;
      }
    } else {
      break;
    }

    // Set test trim value
    LL_RCC_HSI_SetCalibTrimming(trim);
  }

  // Set final trim value
  LL_RCC_HSI_SetCalibTrimming(trim);

  // Store the value in the factory region
  factory_info.header.clock_trim = trim;

#if IS_AUTO_SAVE_CALIBRATION
  // Save the factory info table at the top of the EEPROM
#ifdef EEPROM_WP_GPIO_Port
  // Disable Write Protection
  LL_GPIO_ResetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
  FactoryInfo_Save(I2C_INTERNAL,
                   EEPROM_ADDRESS_7B,
                   sizeof(factory_info_t),
                   (uint8_t *) &factory_info);

#ifdef EEPROM_WP_GPIO_Port
  // Enable Write Protection
  LL_GPIO_SetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
#endif
}
#endif

void Run_Clock_Calibration_Mode() {

#if defined(TEST_MODE_ENTER_Pin)
  // While the Clock Input pin is low we send out a clock to test timing
  while (!LL_GPIO_IsInputPinSet(TEST_MODE_ENTER_GPIO_Port, TEST_MODE_ENTER_Pin)) {
#if defined(TEST_MODE_CLOCK_OUTPUT_GPIO_Port)
    LL_GPIO_TogglePin(TEST_MODE_CLOCK_OUTPUT_GPIO_Port, TEST_MODE_CLOCK_OUTPUT_Pin);
#endif
    LL_GPIO_TogglePin(LED_ACT_GPIO_Port, LED_ACT_Pin);
    Busy_Wait(250);
  }
#endif
  LL_GPIO_ResetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);

  // This allows the calibrator to abort the procedure if clock timing is within spec already
  if (LL_GPIO_IsInputPinSet(BOOT_GPIO_Port, BOOT_Pin) == BOOT_PIN_ACTIVE_LOW) {
    return;
  }
  LL_GPIO_SetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);

  // Run clock calibration and save trim value into the EEPROM
#if IS_COMPREHENSIVE_TEST_MODE
  Test_Mode_Calibrate_Clock();
#else
  Test_Mode_Calibrate_Clock(factory_info.header.clock_trim);
#endif

#if defined(BOOT_Pin)
  // After calibration send out pulses while the boot pin is active
  while (LL_GPIO_IsInputPinSet(BOOT_GPIO_Port, BOOT_Pin) != BOOT_PIN_ACTIVE_LOW) {
#if defined(TEST_MODE_CLOCK_OUTPUT_GPIO_Port)
    LL_GPIO_TogglePin(TEST_MODE_CLOCK_OUTPUT_GPIO_Port, TEST_MODE_CLOCK_OUTPUT_Pin);
#endif
    LL_GPIO_TogglePin(LED_ACT_GPIO_Port, LED_ACT_Pin);
    Busy_Wait(250);
  }
#endif
}

#endif

#if ENABLE_IMAGE_VERIFY

bool Verify_Application_Image(const uint32_t base_address) {

  const uint32_t max_search = 4096;

  // This table is pretty huge but bootloader does one thing
  uint64_t crc_64_table[256];

  uint8_t *app_descriptor_address = (uint8_t *) base_address;
  uint8_t  signature[]            = {'A', 'P', 'D', 'e', 's', 'c'};

  // App-Descriptor Offset
  bool found = false;

  for (int i = 0; i < max_search; ++i) {
    found = true;

    for (int j = 0; j < sizeof(signature); ++j) {
      if (signature[j] != *(app_descriptor_address + j)) {
        ++app_descriptor_address;
        found = false;
      }
    }

    if (found) {
      break;
    }
  }

  if (!found) {
    return found;
  }

  uint32_t ad_offset = (uint32_t) app_descriptor_address - base_address;

  // Generate CRC 64 Table
  CRC_64_Generate_Table(CRC_64_WE_POLY, crc_64_table);

  // Load application image
  const app_descriptor_t *firmware_app_descriptor = (app_descriptor_t *) app_descriptor_address;

  size_t image_size = firmware_app_descriptor->image_size;

  if (image_size == 0) {
    return false;
  }

  // Need to zero-out thee image_crc in the description so create a separate 'clean copy'.
  uint8_t clean_buffer[ad_offset + sizeof(app_descriptor_t)];

  // Copy over the first chunk of firmware upto the end of th app_descriptor_t
  memcpy(clean_buffer, (uint32_t *) FIRMWARE_BASE_ADDRESS, sizeof(clean_buffer));

  // Clear 64bit CRC value from 'header'
  for (int i = 0; i < 8; ++i) {
    clean_buffer[ad_offset + 8 + i] = 0;
  }

  // compute CRC on the first half
  uint64_t memory_image_crc = CRC_64_Compute(crc_64_table,
                                             CRC_64_WE_INIT,
                                             sizeof(clean_buffer),
                                             clean_buffer);

  // add the rest of the image and since firmware is filled with FF there should be no problem
  // reading past the image size a few bytes.
  memory_image_crc = CRC_64_Compute(crc_64_table,
                                    memory_image_crc,
                                    image_size - sizeof(clean_buffer),
                                    (uint8_t *) FIRMWARE_BASE_ADDRESS + sizeof(clean_buffer));

  memory_image_crc ^= CRC_64_WE_MASK;

  return memory_image_crc == firmware_app_descriptor->image_crc;
}

#endif


__attribute__((naked)) static void start_app(__attribute__((unused)) uint32_t pc,
                                             __attribute__((unused)) uint32_t sp) {
  // sp is in r1
  __asm("msr msp, r1"); // sp
  // pc is in r0
  __asm("bx r0");
}

/**
 * Start the User Application
 */
void Run_Application() {

  Hardware_DeInit();

#if defined(STM32F0)
  // Copy vector table to memory
  volatile uint32_t *VectorTable = (volatile uint32_t *) 0x20000000;

  for (uint32_t ix_vec = 0; ix_vec < 48; ++ix_vec) {
    VectorTable[ix_vec] = *(volatile uint32_t *) (FIRMWARE_BASE_ADDRESS + (ix_vec << 2));
  }

  // Remap 0x00000000 to point to SRAM
  __HAL_SYSCFG_REMAPMEMORY_SRAM();
  //  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x4000)

#elif defined(STM32F3) || defined(STM32F4)
  SCB->VTOR = FIRMWARE_BASE_ADDRESS;
#endif

  // Jump to application
  uint32_t *app_code = (uint32_t *) FIRMWARE_BASE_ADDRESS;

  uint32_t app_sp    = app_code[0];
  uint32_t app_start = app_code[1];

  start_app(app_start, app_sp);
}

int main() {

  __enable_irq();

#if defined(STM32F3)
  LL_FLASH_DisablePrefetch();

#elif defined(STM32F4)
#if (INSTRUCTION_CACHE_ENABLE != 0U)
  FLASH->ACR |= FLASH_ACR_ICEN;
#endif

#if (DATA_CACHE_ENABLE != 0U)
  FLASH->ACR |= FLASH_ACR_DCEN;
#endif
#endif

#if IS_BLINK_TEST == 0
  // If the flash is empty at the base address then there is no program loaded so enter bootloader
  bool enter_bootloader = ((*(uint32_t *) FIRMWARE_BASE_ADDRESS) == 0xFFFFFFFF);
#endif

  System_Clock_Init();

#if defined(STM32F3) || defined(STM32F4)
  // Set Interrupt Group Priority
  NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  // System Interrupts
  NVIC_SetPriority(MemoryManagement_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(BusFault_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(UsageFault_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(SVCall_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(DebugMonitor_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
  NVIC_SetPriority(PendSV_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(), 0, 0));
#endif

  GPIO_Init();

#if IS_BLINK_TEST == 1

#ifdef LED_RGB_R_GPIO_Port
  GPIO_Config_Pins(LED_RGB_R_GPIO_Port,
                   LED_RGB_B_Pin | LED_RGB_R_Pin | LED_RGB_G_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);
  LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
  LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
  LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);

  for (;;) {

    LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
    Busy_Wait(1000);

    LL_GPIO_ResetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
    Busy_Wait(1000);

    LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
    LL_GPIO_ResetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
    Busy_Wait(1000);

    LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
    LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
    LL_GPIO_ResetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
    Busy_Wait(1000);
  }
#else

  GPIO_Config_Pins(LED_ACT_GPIO_Port,
                   LED_ACT_Pin,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_MODE_OUTPUT_OD,
                   GPIO_NOPULL,
                   GPIO_SPEED_FREQ_LOW);

  for (;;) {
    LL_GPIO_TogglePin(LED_ACT_GPIO_Port, LED_ACT_Pin);
#if defined(TEST_MODE_CLOCK_OUTPUT_GPIO_Port)
    LL_GPIO_TogglePin(TEST_MODE_CLOCK_OUTPUT_GPIO_Port, TEST_MODE_CLOCK_OUTPUT_Pin);
#endif
    Busy_Wait(500);
    LL_GPIO_ResetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
    Busy_Wait(500);
  }
#endif

#else

#if ENABLE_I2C
  I2C_Init();
#endif

#if ENABLE_FACTORY_INFO && ENABLE_I2C
  uint16_t factory_load_result = FactoryInfo_Load(I2C_INTERNAL,
                                                  EEPROM_ADDRESS_7B,
                                                  sizeof(factory_info_t),
                                                  (uint8_t *) &factory_info);
  // Load factory info from EEPROM
  if (factory_load_result != EEPROM_LOAD_STATE_ALL_OK) {
    Params_Load_Default_Values();

#if ENABLE_I2C_DEVICE_CHECK
    if (factory_load_result == EEPROM_LOAD_STATE_DEVICE_ERROR) {
      status.i2c_device_error |= I2C_BUS_ERROR_EEPROM;
    }
#endif
  }
#else
  Params_Load_Default_Values();
#endif

#if ENABLE_I2C && ENABLE_I2C_DEVICE_CHECK
  I2C_DeviceCheck();
#endif

  LL_RCC_HSI_SetCalibTrimming(factory_info.header.clock_trim);

#if defined(BOOT_GPIO_Port)
  // Check if the user wants to enter the bootloader
  bool boot_pin_enabled = true;

  for (int i = 0; i < BOOT_PIN_LOOP_COUNT; ++i) {

    if (LL_GPIO_IsInputPinSet(BOOT_GPIO_Port, BOOT_Pin) == BOOT_PIN_ACTIVE_LOW) {
      boot_pin_enabled = false;
      break;
    }

    LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
    Busy_Wait(BOOT_PIN_LOOP_DELAY_MS);
    LL_GPIO_SetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
    Busy_Wait(BOOT_PIN_LOOP_DELAY_MS);
  }
  enter_bootloader |= boot_pin_enabled;

#endif

#if defined(TEST_MODE_ENTER_Pin) && defined(BOOT_GPIO_Port)
  // Boot pin is enabled and the TEST MODE pin is low
  if (boot_pin_enabled && (!LL_GPIO_IsInputPinSet(TEST_MODE_ENTER_GPIO_Port, TEST_MODE_ENTER_Pin))) {
    Run_Clock_Calibration_Mode();
  }
#endif

  // Initialize node information
#if ENABLE_APP_DESCRIPTOR
  Node_Init(&node,
            UAVCAN_NODE_HEALTH_OK,
            UAVCAN_NODE_MODE_MAINTENANCE,
            CAN_DEFAULT_NODE_ID,
            app_descriptor.major_version,
            app_descriptor.minor_version,
            app_descriptor.vcs_commit,
            app_descriptor.image_crc,
            APP_NAME,
            &factory_info.header);
#else
  Node_Init(&node,
            UAVCAN_NODE_HEALTH_OK,
            UAVCAN_NODE_MODE_MAINTENANCE,
            CAN_DEFAULT_NODE_ID,
            SOFTWARE_VERSION_MAJOR,
            SOFTWARE_VERSION_MINOR,
            0,
            0,
            APP_NAME,
            &factory_info.header);
#endif

  // Check if the bootloader was entered because of a firmware update
  if (boot_info.signature == BOOT_INFO_SIGNATURE) {
    enter_bootloader  = true;
    uint16_t crc_save = boot_info.crc;
    boot_info.crc     = 0;
    uint16_t new_crc  = CRC_16_Compute(0xFFFFU, (void *) &boot_info, sizeof(boot_info_t));
    // Clear signature to prevent weirdness
    boot_info.signature = 0;

    if (new_crc == crc_save) {
      enter_bootloader = true;

      // Node id is known
      node.id       = boot_info.node_id;
      node.id_valid = node.id != 0;

      // Baud and name are known so go to fully operational status...
      node.can_status = CAN_STATUS_OPERATIONAL;

      // Queue up a firmware message so bootloader can send a proper response
      Request_Queue_Add(&queue,
                        UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ID,
                        boot_info.update_req_node_id,
                        boot_info.update_req_transfer_id,
                        boot_info.update_req_priority,
                        0);
    }
  } else {
    // Set a default
    boot_info.enable_term = true;
  }
  can_params.bus_termination_enable = boot_info.enable_term;

#if ENABLE_IMAGE_VERIFY
  if (!enter_bootloader) {
    // Go into bootloader if the image is invalid
    enter_bootloader = !Verify_Application_Image(FIRMWARE_BASE_ADDRESS);

    // These only take effect if enter_bootloader is true so there is no need for an if statement... I guess
    node.health             = UAVCAN_NODE_HEALTH_CRITICAL;
    node.vendor_status_code = UAVCAN_VSC_INVALID_IMAGE;
  }
#endif

#if ENABLE_OPTION_BYTE_WRITE
  // Only allow option byte writing if there is no firmware... for safety.
  enable_option_byte_writer |= ((*(uint32_t *) FIRMWARE_BASE_ADDRESS) == 0xFFFFFFFF);
#endif

  if (enter_bootloader) {
    CAN_Init();

#ifdef CAN_TERM_Pin
    if (can_params.bus_termination_enable) {
      LL_GPIO_ResetOutputPin(CAN_TERM_GPIO_Port, CAN_TERM_Pin);
    } else {
      LL_GPIO_SetOutputPin(CAN_TERM_GPIO_Port, CAN_TERM_Pin);
    }
#endif

    Run_Bootloader();
  } else {
    Run_Application();
  }
#endif
}
