/**
 * @file
 * @brief Bootloader event loop
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

/**
 * Includes
 */
#include "bootloader.h"

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "node.h"

/**
 * Globals
 */
extern uint32_t        System_Tick;
extern boot_info_t     boot_info;
extern device_status_t status;
extern node_info_t     node;
extern can_params_t    can_params;

// Firmware update temporaries
extern uint32_t file_last_read_timestamp;
extern uint8_t  file_update_retry_count;
extern uint64_t file_remote_offset;

uint32_t timestamp_led_off_time = 0;

/**
 *
 */
void Run_Bootloader() {

  // Setup CAN
  can_params.default_baud_kbps                   = CAN_DEFAULT_BAUD;
  can_params.auto_baud_enable                    = (bool) ENABLE_AUTO_BAUD;
  can_params.auto_baud_retry_time_ms             = CAN_AUTO_BAUD_RETRY_TIMEOUT_MS;
  can_params.dynamic_id_allocation_enable        = (bool) ENABLE_DYNAMIC_ID_ALLOCATION;
  can_params.dynamic_id_allocation_retry_time_ms = DYNAMIC_ID_ALLOCATION_RETRY_TIME_MS;
  can_params.dynamic_id_allocation_max_retries   = DYNAMIC_ID_ALLOCATION_MAX_RETRIES;

  // Start an infinite loop and wait for a firmware update.
  for (;;) {
    node.uptime_sec = System_Tick / 1000;

    if (System_Tick > timestamp_led_off_time) {
#if defined(LED_RGB_B_GPIO_Port)
      LL_GPIO_SetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
      LL_GPIO_SetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
      LL_GPIO_SetOutputPin(LED_RGB_B_GPIO_Port, LED_RGB_B_Pin);
#elif defined(LED_ACT_Pin)
      LL_GPIO_SetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
#endif
    }

    if (node.can_status != CAN_STATUS_ONLINE) {

      if (!CANbus_Init()) {
#if defined(LED_FAULT_Pin)
        LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif
        Busy_Wait(100);
      }
      continue;
    }

    /**
     * Request Processing
     */

    // Process one message from the queue
    Service_Requests();

    // Software Update Timeout
    if ((node.mode == UAVCAN_NODE_MODE_SOFTWARE_UPDATE)
        && (file_last_read_timestamp > 0)
        && (System_Tick - file_last_read_timestamp > SOFTWARE_UPDATE_TIMEOUT_MS)) {

      node.health = UAVCAN_NODE_HEALTH_WARNING;

      if (file_update_retry_count < FILE_GET_MAX_RETRIES) {
        Send_FileRead_Request(node.id,
                              boot_info.server_node_id,
                              0,
                              file_remote_offset,
                              boot_info.firmware_file_path_len,
                              boot_info.firmware_file_path);
        file_last_read_timestamp = System_Tick;
        file_update_retry_count += 1;
      } else {
        node.mode               = UAVCAN_NODE_MODE_MAINTENANCE;
        node.vendor_status_code = UAVCAN_VSC_UPDATE_TIMEOUT;
      }
    }

#if ENABLE_I2C_DEVICE_CHECK
    if (status.i2c_device_error) {
      node.vendor_status_code |= (status.i2c_device_error << 8);
      node.health = UAVCAN_NODE_HEALTH_ERROR;
    }
#endif

    // Send out node Status Message
    if ((System_Tick - node.timestamp) > 1000) {

      // Indicate mode
      if (node.health != 0) {
#if defined(LED_FAULT_Pin)
        LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif
      } else if (node.mode == UAVCAN_NODE_MODE_SOFTWARE_UPDATE) {
#if defined(LED_RGB_B_GPIO_Port)
        // Yellow!
        LL_GPIO_ResetOutputPin(LED_RGB_R_GPIO_Port, LED_RGB_R_Pin);
        LL_GPIO_ResetOutputPin(LED_RGB_G_GPIO_Port, LED_RGB_G_Pin);
#elif defined(LED_ACT_Pin)
        LL_GPIO_ResetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
#endif
      } else {
#if defined(LED_ACT_Pin)
        LL_GPIO_ResetOutputPin(LED_ACT_GPIO_Port, LED_ACT_Pin);
#endif
      }

      if (!Send_NodeStatus_Message(&node)) {
#if defined(LED_FAULT_Pin)
        LL_GPIO_ResetOutputPin(LED_FAULT_GPIO_Port, LED_FAULT_Pin);
#endif
      }

      timestamp_led_off_time = node.timestamp + LED_ACT_ON_TIME_MS;
    }
  }
}
