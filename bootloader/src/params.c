/**
 * @file
 * @brief Parameter setup
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "bootloader.h"

extern factory_info_t factory_info;

// Write lock
bool parameters_unlocked;

static const parameter_info_t parameter_info_table[] = {

    {
        CAL_NAME_UNLOCK_CODE,
        PARAM_VALUE_INT,
        0, 0, 0, 0, 0, 0,
        (int32_t) CAL_MAX_UNLOCK_CODE, 0,
    },

    // Factory Header
    ADD_CAL_INT(DEVICE_TYPE, factory_info_t, header.device_type, DEVICE_TYPE),
    ADD_CAL_INT(DEVICE_VERSION_MAJOR, factory_info_t, header.device_version_major, HARDWARE_VERSION_MAJOR),
    ADD_CAL_INT(DEVICE_VERSION_MINOR, factory_info_t, header.device_version_minor, HARDWARE_VERSION_MINOR),
    ADD_CAL_STRING(DEVICE_SERIAL, factory_info_t, header.device_serial),
    ADD_CAL_STRING(HASH, factory_info_t, header.crypto_hash),
    ADD_CAL_INT(PRODUCTION_TIMESTAMP, factory_info_t, header.production_timestamp, 0),
    ADD_CAL_STRING(PRODUCTION_QA, factory_info_t, header.production_qa),

    ADD_CAL_INT(HSI, factory_info_t, header.clock_trim, CAL_DEFAULT_HSI),

// Hardware Specific Parameters
#if defined(POWER_MODULE) || defined(POWER_MONITOR)
    ADD_CAL_REAL(R_SHUNT_MAIN, factory_info_t, bus_main_R_shunt, CAL_DEFAULT_R_SHUNT_MAIN),
#endif
#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR > 2 || defined(POWER_MONITOR)
    ADD_CAL_REAL(I_OFFSET_MAIN, factory_info_t, bus_main_I_offset_ApV, CAL_DEFAULT_I_OFFSET_MAIN),
#endif

#if defined(POWER_MODULE)
    ADD_CAL_REAL(R_SHUNT_AUX, factory_info_t, bus_aux_R_shunt, CAL_DEFAULT_R_SHUNT_AUX),
#endif

#if defined(POWER_MONITOR)
    ADD_CAL_REAL(AUX_VOLTAGE_SCALE, factory_info_t, bus_aux_V_scale, CAL_DEFAULT_AUX_VOLTAGE_SCALE),
#endif

#if defined(POWER_MODULE) || defined(POWER_MONITOR)
    ADD_CAL_INT(LED_BRIGHTNESS_SCALE_R, factory_info_t, led_brightness[0], CAL_DEFAULT_LED_BRIGHTNESS_SCALE_R),
    ADD_CAL_INT(LED_BRIGHTNESS_SCALE_G, factory_info_t, led_brightness[1], CAL_DEFAULT_LED_BRIGHTNESS_SCALE_G),
    ADD_CAL_INT(LED_BRIGHTNESS_SCALE_B, factory_info_t, led_brightness[2], CAL_DEFAULT_LED_BRIGHTNESS_SCALE_B),
#endif

#if defined(STEPPER_CONTROLLER)
    ADD_CAL_REAL(R_SHUNT_A, factory_info_t, R_shunt_A, CAL_DEFAULT_R_SHUNT_A),
    ADD_CAL_REAL(R_SHUNT_B, factory_info_t, R_shunt_B, CAL_DEFAULT_R_SHUNT_B),
#endif

#if defined(SOLENOID_CONTROLLER)
    ADD_CAL_REAL(R_SHUNT, factory_info_t, R_shunt, CAL_DEFAULT_R_SHUNT),
#endif

#if defined(STEPPER_CONTROLLER) || defined(SOLENOID_CONTROLLER)
    //    ADD_CAL_INT(R_SHUNT_A, factory_info_t, led_brightness[0]),
#endif
};

bool dirty_table[sizeof(parameter_info_table) / sizeof(parameter_info_t)];

void Handle_Param_GetSet_Special(const char *name) {
//  if (strcmp(name, PARAM_NAME_CAN_ENABLE_TERM) == 0) {
//    CAN_Set_Termination();
//  } else if ((strcmp(name, PARAM_NAME_BUS_MAIN_R_SHUNT) == 0)
//             || (strcmp(name, PARAM_NAME_BATTERY_CAPACITY) == 0)
//             || (strcmp(name, PARAM_NAME_BUS_MAIN_CURRENT_FILTER_CONST) == 0)) {
//    Battery_Capacity_Calc();
//  } else if ((strcmp(name, PARAM_NAME_LED_ENABLED) == 0)) {
//    if (parameters.led_enabled) {
//      LED_Init();
//    } else {
//      LED_DeInit();
//    }
//  } else {
//    // Otherwise update the globals
//    Update_Globals();
//  }
}

/**
 * Parameter function
 */

bool Params_Call_Handle_GetSet(int                 node_id,
                               parameter_getset_t *param,
                               request_t           request) {

  if ((strcmp(param->name, CAL_NAME_UNLOCK_CODE) == 0)
      || ((param->name_len == 0)
          && strcmp(parameter_info_table[param->index].name, CAL_NAME_UNLOCK_CODE) == 0)) {

    if (param->type == PARAM_VALUE_INT) {
      parameters_unlocked = (param->value.i == PARAMETER_UNLOCK_CODE);
    }

    return Send_GetSet_Int_Response(node_id,
                                    request,
                                    parameters_unlocked ? PARAMETER_UNLOCK_CODE : 0,
                                    0,
                                    0,
                                    CAL_MAX_UNLOCK_CODE,
                                    CAL_NAME_UNLOCK_CODE);
  } else if ((strcmp(param->name, CAL_NAME_DEVICE_SERIAL) == 0)
             || ((param->name_len == 0) && strcmp(parameter_info_table[param->index].name, CAL_NAME_DEVICE_SERIAL) == 0)) {

    if (param->type == PARAM_VALUE_STRING) {
      memcpy(&factory_info.header.device_serial, param->value.s,
             MIN(sizeof(factory_info.header.device_serial), param->value_len));
      dirty_table[param->index] = true;
    }

    return Send_GetSet_String_Response(node_id,
                                       request,
                                       sizeof(factory_info.header.device_serial),
                                       factory_info.header.device_serial,
                                       sizeof(CAL_DEFAULT_DEVICE_SERIAL),
                                       (uint8_t *) CAL_DEFAULT_DEVICE_SERIAL,
                                       CAL_NAME_DEVICE_SERIAL);

  } else if ((strcmp(param->name, CAL_NAME_HASH) == 0)
             || ((param->name_len == 0) && strcmp(parameter_info_table[param->index].name, CAL_NAME_HASH) == 0)) {

    if (param->type == PARAM_VALUE_STRING) {
      memcpy(&factory_info.header.crypto_hash, param->value.s,
             MIN(sizeof(factory_info.header.crypto_hash), param->value_len));
      dirty_table[param->index] = true;
    }

    return Send_GetSet_String_Response(node_id,
                                       request,
                                       sizeof(factory_info.header.crypto_hash),
                                       factory_info.header.crypto_hash,
                                       sizeof(CAL_DEFAULT_HASH),
                                       (uint8_t *) CAL_DEFAULT_HASH,
                                       CAL_NAME_HASH);

  } else if ((strcmp(param->name, CAL_NAME_PRODUCTION_QA) == 0)
             || ((param->name_len == 0) && strcmp(parameter_info_table[param->index].name, CAL_NAME_PRODUCTION_QA) == 0)) {

    if (param->type == PARAM_VALUE_STRING) {
      // Set value
      strncpy((char *) factory_info.header.production_qa, param->value.s,
              MIN(sizeof(factory_info.header.production_qa), sizeof(param->value.s)));
      dirty_table[param->index] = true;
    }

    return Send_GetSet_String_Response(node_id,
                                       request,
                                       strlen((char *) factory_info.header.production_qa),
                                       factory_info.header.production_qa,
                                       sizeof(CAL_DEFAULT_PRODUCTION_QA),
                                       (uint8_t *) CAL_DEFAULT_PRODUCTION_QA,
                                       CAL_NAME_PRODUCTION_QA);
  } else {
    return Params_Handle_GetSet(node_id,
                                sizeof(parameter_info_table) / sizeof(parameter_info_t),
                                parameter_info_table,
                                0,
                                dirty_table,
                                param,
                                request,
                                (uint8_t *) &factory_info);
  }
}

void Params_Load_Default_Values() {
  const size_t parameter_count = sizeof(parameter_info_table) / sizeof(parameter_info_t);

  Params_Load_Defaults(parameter_count,
                       parameter_info_table,
                       0,
                       dirty_table,
                       (uint8_t *) &factory_info);
}

bool Params_Save() {

  // Make sure the parameters are unlocked for writing.
  if (!parameters_unlocked) {
    return false;
  }

  // Disable Write Protection if present
#ifdef EEPROM_WP_GPIO_Port
  LL_GPIO_ResetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
  // Save the data
  bool param_ok = FactoryInfo_Save(I2C_INTERNAL,
                                   EEPROM_ADDRESS_7B,
                                   sizeof(factory_info_t),
                                   (uint8_t *) &factory_info);

  // Enable Write Protection if we have it
#ifdef EEPROM_WP_GPIO_Port
  LL_GPIO_SetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif

  return param_ok;
}
