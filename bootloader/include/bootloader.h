/**
 * @file
 * @brief Bootloader configuration file
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */


#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "main.h"

/**
 * Compile Options
 */

// Basic tests for checking hardware or porting
#ifndef IS_BLINK_TEST
#define IS_BLINK_TEST                                                 0
#endif

// Test all calibration parameters (brute force but prevents getting stuck in local minima)
#define IS_COMPREHENSIVE_TEST_MODE                                    1

// Automatically save the factory calibration data after a timing test... not safe
#define IS_AUTO_SAVE_CALIBRATION                                      0

// CANbus Configuration
#define CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                                3000
#define CAN_AUTO_BAUD_MAX_RETRIES                                     2

#define CAN_DEFAULT_BAUD                                              1000
#define CAN_DEFAULT_NODE_ID                                           0

// Boot pin Check boot pin is up over n loops of x ms each
#define BOOT_PIN_LOOP_DELAY_MS                                        50
#define BOOT_PIN_LOOP_COUNT                                           5

// Dynamic Node ID Allocation
#define DYNAMIC_ID_ALLOCATION_RETRY_TIME_MS                           600
#define DYNAMIC_ID_ALLOCATION_MAX_RETRIES                             3

// Size of firmware update file read buffer, max file chunk is about 256
// so this can be reduced if necessary
#define FILE_BUFFER_SIZE                                              512

// How many times to retry update
#define FILE_GET_MAX_RETRIES                                          10

// How long the activity LED will be on each cycle
#define LED_ACT_ON_TIME_MS                                            50

// Wait this long for file before giving up on a software update
#define SOFTWARE_UPDATE_TIMEOUT_MS                                    500


/**
 * Device Specific Bootloader Configuration
 */
#if defined(ESC_GATEWAY)

#include "core.h"

// Boot pin (TP1 - PA13)
#define BOOT_Pin                                                      GPIO_PIN_5
#define BOOT_GPIO_Port                                                GPIOA
#define BOOT_PIN_ACTIVE_LOW                                           true

#define LED_FAULT_Pin                                                 LED_Pin
#define LED_FAULT_GPIO_Port                                           LED_GPIO_Port

// Enable Calibration Parameters
#define ENABLE_FACTORY_INFO                                           1
#define ENABLE_PARAMS                                                 1

#elif defined(FEEDER_CONTROLLER)

#include "core.h"

#elif defined(INDICATION_CONTROLLER)

#include "core.h"

#elif defined(RETRACT)

#include "retract_controller.h"

// Boot pin
#define BOOT_Pin                                                      GPIO_PIN_7
#define BOOT_GPIO_Port                                                GPIOB
#define BOOT_PIN_ACTIVE_LOW                                           false

// Status LED
#define LED_FAULT_Pin                                                 LED_PIN
#define LED_FAULT_GPIO_Port                                           LED_GPIO_Port

#elif defined(POWER_MODULE)

#include "core.h"

#elif defined(POWER_MONITOR)

#include "core.h"

#elif defined(SOLENOID_CONTROLLER)

#include "core.h"

#elif defined(STEPPER_CONTROLLER)

#include "core.h"

#elif defined(UNIVERSAL_TEST_FIXTURE)

#include "core.h"

#else

#include "stm32_util.h"

typedef struct __attribute__ ((aligned(4))) {
  factory_info_header_t header;
} factory_info_t;

#define DEVICE_TYPE                                                   0

// Generic bootloaders
#if defined(STM32F042x6)

#define APP_NAME                                                      "io.p-systems.bootloader.f042x6"

#elif defined(STM32F072xB)

#define APP_NAME                                                      "io.p-systems.bootloader.f072xB"

// Boot pin
#define BOOT_Pin                                                      GPIO_PIN_7
#define BOOT_GPIO_Port                                                GPIOB
#define BOOT_PIN_ACTIVE_LOW                                           false

// Status LED
#define LED_FAULT_Pin                                                 GPIO_PIN_10
#define LED_FAULT_GPIO_Port                                           GPIOA

// CAN terminate pin
#define CAN_TERM_Pin                                                  GPIO_PIN_4
#define CAN_TERM_GPIO_Port                                            GPIOB

#define STATUS_LED_ON                                                 GPIO_PIN_RESET
#define STATUS_LED_OFF                                                GPIO_PIN_SET

#elif defined(STM32F302x8)

#define APP_NAME                                                      "io.p-systems.bootloader.f302x8"

#elif defined(STM32F413xx)

#define APP_NAME                                                      "io.p-systems.bootloader.f413xx"
#define CAN                                                           CAN1
#define CAN_UNIT                                                      1

#endif

#endif


#if !defined(LED_ACT_Pin) && defined(LED_RGB_B_Pin)
#define LED_ACT_Pin                                                   LED_RGB_B_Pin
#define LED_ACT_GPIO_Port                                             LED_RGB_B_GPIO_Port
#endif

#if !defined(LED_FAULT_Pin)
#if defined(LED_RGB_R_Pin)
#define LED_FAULT_Pin                                                 LED_RGB_R_Pin
#define LED_FAULT_GPIO_Port                                           LED_RGB_R_GPIO_Port
#elif defined(LED_ACT_Pin)
#define LED_FAULT_Pin                                                 LED_ACT_Pin
#define LED_FAULT_GPIO_Port                                           LED_ACT_GPIO_Port
#endif
#endif


/**
 * Platform specific settings
 */
#if defined(STM32F042x6)

// Location of system bootloader is architecture specific
#define SYSTEM_BOOTLOADER_ADDRESS                                     0x1FFFC400

#ifndef FLASH_PAGE_SIZE
#define FLASH_PAGE_SIZE                                               0x400U
#endif

#elif defined(STM32F072xB)

// Location of system bootloader is architecture specific
#define SYSTEM_BOOTLOADER_ADDRESS                                     0x1FFFC800

#ifndef FLASH_PAGE_SIZE
#define FLASH_PAGE_SIZE                                               0x800U
#endif

#elif defined(STM32F302x8)

// Location of system bootloader is architecture specific
#define SYSTEM_BOOTLOADER_ADDRESS                                     0x1FFFD800

#define CAN_PIN_SPEED                                                 GPIO_SPEED_FREQ_HIGH
#define I2C_PIN_SPEED                                                 GPIO_SPEED_FREQ_HIGH
#define CAN_FILTER_IF                                                 CAN

#elif defined(STM32F413xx)

#define CAN_PIN_SPEED                                                 GPIO_SPEED_FREQ_VERY_HIGH
#define I2C_PIN_SPEED                                                 GPIO_SPEED_FREQ_VERY_HIGH

// CAN2 filters are (for some fucking reason) set through CAN1 so if using CAN2...
#ifndef CAN_FILTER_IF
#define CAN_FILTER_IF                                                 CAN1
#endif

#endif

#ifndef FIRMWARE_BASE_ADDRESS
#define FIRMWARE_BASE_ADDRESS                                         0x08004000
#endif

#ifndef HARDWARE_VERSION_MAJOR
#define HARDWARE_VERSION_MAJOR                                        0
#endif

#ifndef HARDWARE_VERSION_MINOR
#define HARDWARE_VERSION_MINOR                                        0
#endif

// Hardware Features
#ifndef ENABLE_I2C
#define ENABLE_I2C                                                    0
#endif

#ifndef ENABLE_I2C_DEVICE_CHECK
#define ENABLE_I2C_DEVICE_CHECK                                       0
#endif

// Software Features
// (Note: turn these on/off in make file or hardware specific define)
#ifndef ENABLE_APP_DESCRIPTOR
#define ENABLE_APP_DESCRIPTOR                                         1
#endif

#ifndef ENABLE_AUTO_BAUD
#define ENABLE_AUTO_BAUD                                              1
#endif

#ifndef ENABLE_DYNAMIC_ID_ALLOCATION
#define ENABLE_DYNAMIC_ID_ALLOCATION                                  1
#endif

#ifndef ENABLE_FACTORY_INFO
#define ENABLE_FACTORY_INFO                                           1
#endif

#ifndef ENABLE_IMAGE_VERIFY
#define ENABLE_IMAGE_VERIFY                                           1
#endif

#ifndef ENABLE_OPTION_BYTE_WRITE
#define ENABLE_OPTION_BYTE_WRITE                                      1
#endif

#ifndef ENABLE_PARAMS
#define ENABLE_PARAMS                                                 0
#endif

#ifndef ENABLE_TRANSPORT_STATS
#define ENABLE_TRANSPORT_STATS                                        1
#endif

/**
 * @brief Device status block
 */
typedef struct __attribute__((aligned(4))) {

#if ENABLE_I2C_DEVICE_CHECK
  /**
   * @brief if i2c devices are ok
   */
  uint8_t i2c_device_error;
#endif

} device_status_t;

/**
 * Subroutines
 */

/**
 * @brief Start bootloader
 */
void Run_Bootloader();
