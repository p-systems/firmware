/**
 * @file
 * @brief Hardware init and main entry for the bootloader
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2021 - Pomegranate Systems LLC
 * @copyright (c) 2021 STMicroelectronics
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#pragma once

#include <stdbool.h>

#if defined(STM32F0)

#elif defined(STM32F3)

#include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_cortex.h"
#include "stm32f3xx_ll_exti.h"
#include "stm32f3xx_ll_gpio.h"
#include "stm32f3xx_ll_i2c.h"
#include "stm32f3xx_ll_rcc.h"
#include "stm32f3xx_ll_system.h"
#include "stm32f3xx_ll_tim.h"
#include "stm32f3xx_ll_utils.h"

#elif defined(STM32F4)

#include "stm32f4xx_ll_bus.h"
#include "stm32f4xx_ll_cortex.h"
#include "stm32f4xx_ll_gpio.h"
#include "stm32f4xx_ll_i2c.h"
#include "stm32f4xx_ll_pwr.h"
#include "stm32f4xx_ll_rcc.h"
#include "stm32f4xx_ll_system.h"
#include "stm32f4xx_ll_tim.h"
#include "stm32f4xx_ll_utils.h"

#endif

/**
 * @brief De-initialize all the hardware and peripherals in preparation for jumping
 * into the application code.
 *
 */
void Hardware_DeInit();

bool Verify_Application_Image(uint32_t base_address);
