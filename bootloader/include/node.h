/**
 * @file
 * @brief Handle "network traffic" from DroneCAN
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#pragma once

#include "stm32_util.h"

/**
 * @brief Bring up the CAN interface
 */
bool CANbus_Init();

/**
 * @brief Process messages in the request queue
 */
void Service_Requests();

/**
 *  @brief Request file contents for firmware upgrades
 */
void Start_File_Read();
