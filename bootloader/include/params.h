/**
 *
 * Author: Stou Sandalski <stou@p-systems.io>
 * Copyright (c) 2021 - Pomegranate Systems LLC
 *
 * License: MIT and 3-Clause BSD (See LICENSE.md)
 *
 * Description: Bootloader parameters
 *
 */

#ifndef P_SYSTEMS_BOOTLOADER_PARAMS_H
#define P_SYSTEMS_BOOTLOADER_PARAMS_H

#include <stdbool.h>
#include <stdint.h>

#include <stm32_util.h>


/**
 * Param Handling subroutines
 */

void Params_Load_Default_Values();

bool Params_Call_Handle_GetSet(int node_id,
                               parameter_getset_t *param_get_set,
                               request_t request);

void Params_Save();

#endif // P_SYSTEMS_BOOTLOADER_PARAMS_H

