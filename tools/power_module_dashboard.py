#! /usr/bin/env python3
#
# License: Public Domain
#
# Basic program to display real time info from the power module
#
# @version: 14
#
#
# Run:
#
#   ./power_module_dashboard.py -d can0 -n 45

import argparse
import datetime
import numpy
import time
import uavcan
import logging

from bokeh.layouts import column, row
from bokeh.models import Button, Span
from bokeh.palettes import RdYlBu3
from bokeh.plotting import figure, curdoc
from bokeh.server.server import Server

from bokeh.io import output_file, show
from bokeh.plotting import figure
from bokeh.models import LinearAxis, Range1d

node = None

item_count = 100

DEFAULT_NODE_ID = 65
DEFAULT_CANBUS_DEVICE = "can0"

battery_range = (12, 17)
battery_warning = 15.20

# Globals
circuit_status = {}
battery_info = {}
device_temperature = {}

armed_sample_id = 0
battery_info_sample_id = 0
circuit_status_sample_id = 0
temperature_sample_id = 0

ds_bat_I = None
ds_bat_I_var = None
ds_bat_V = None
ds_bat_soc = None
ds_can_I = None
ds_can_V = None
ds_vdd_V = None

ds_T_mcp = None
ds_T_stm = None

f_armed = None
f_bat = None
f_can = None
f_vdd = None

f_metadata = None
f_T = None

write_enable = True

LOG_FORMAT = "%(levelname)s %(asctime)s - %(message)s"
# file_handler = logging.FileHandler(filename='test.log', mode='w')
# file_handler.setFormatter(logging.Formatter(LOG_FORMAT))
logger = logging.getLogger(__name__)
# logger.addHandler(file_handler)
logger.setLevel(logging.DEBUG)

node_map = {}


def node_info_callback(event):
  global node_map

  print(event.__dict__)


def node_status_callback(event):
  global node_map

  # print(event.__dict__)
  if event.transfer.source_node_id not in node_map:
    node_map[event.transfer.source_node_id] = {}
    print(event.transfer.source_node_id)


def armed_callback(event):
  global armed, armed_sample_id

  armed = False if event.message.status == 0 else True
  timestamp_s = (datetime.datetime.now() - start_time).total_seconds()
  timestamp_ms = int(timestamp_s * 1000)

  f_armed.write('{0:8} {1:12} {2:4d}\n'.format(armed_sample_id, timestamp_ms, event.message.status))
  f_armed.flush()

  armed_sample_id = armed_sample_id + 1


def battery_info_callback(event):
  global battery_info, battery_info_sample_id, ds_bat_I, ds_bat_I_var, ds_bat_V

  timestamp_s = (datetime.datetime.now() - start_time).total_seconds()
  timestamp_ms = int(timestamp_s * 1000)

  # Battery Current
  new_I_data = dict(x=ds_bat_I.data['x'][-item_count:] + [timestamp_s],
                    y=ds_bat_I.data['y'][-item_count:] + [event.message.current])
  ds_bat_I.data = new_I_data

  # Battery Voltage
  new_V_data = dict(x=ds_bat_V.data['x'][-item_count:] + [timestamp_s],
                    y=ds_bat_V.data['y'][-item_count:] + [event.message.voltage])
  ds_bat_V.data = new_V_data

  # State of Charge
  new_soc_data = dict(x=ds_bat_soc.data['x'][-item_count:] + [timestamp_s],
                      y=ds_bat_soc.data['y'][-item_count:] + [event.message.state_of_charge_pct])
  ds_bat_soc.data = new_soc_data

  # Current Variance
  I_variance = numpy.array(new_I_data['y']).var()

  ds_bat_I_var.data = dict(x=ds_bat_I_var.data['x'][-item_count:] + [timestamp_s],
                           y=ds_bat_I_var.data['y'][-item_count:] + [I_variance])

  battery_info[event.message.battery_id] = dict(sample_id=battery_info_sample_id,
                                                timestamp_ms=timestamp_ms,
                                                timestamp_s=timestamp_s,
                                                battery_id=event.message.battery_id,
                                                current=event.message.current,
                                                voltage=event.message.voltage,
                                                average_power_10sec=event.message.average_power_10sec,
                                                remaining_capacity_wh=event.message.remaining_capacity_wh,
                                                full_charge_capacity_wh=event.message.full_charge_capacity_wh,
                                                state_of_health_pct=event.message.state_of_health_pct,
                                                state_of_charge_pct=event.message.state_of_charge_pct,
                                                state_of_charge_pct_stdev=event.message.state_of_charge_pct_stdev,
                                                remaining_pct=int(100 * event.message.remaining_capacity_wh / event.message.full_charge_capacity_wh),
                                                current_var=I_variance)

  # print('{sample_id:8} {timestamp_ms:12} {current:03.4f} {voltage:03.2f} {average_power_10sec:03.2f} {state_of_charge_pct:4d}'.format(**battery_info[0]))
  # logger.info('{sample_id:8} {timestamp_ms:12} {current:03.4f} {voltage:03.2f} {average_power_10sec:03.2f} {state_of_charge_pct:4d}'.format(**battery_info[0]))

  print(
    '{sample_id:8} {timestamp_ms:12} {current:03.4f} {voltage:03.2f} {remaining_capacity_wh:03.2f} {remaining_pct:4d} {state_of_charge_pct:4d} {current_var:03.6f}'.format(
      **battery_info[0]))

  if not write_enable: return

  f_bat.write(
    '{sample_id:8} {timestamp_ms:12} {current:03.4f} {voltage:03.2f} {remaining_capacity_wh:03.2f} {state_of_charge_pct:4d} {current_var:03.6f}\n'.format(
      **battery_info[0]))
  f_bat.flush()

  battery_info_sample_id = battery_info_sample_id + 1


def circuit_status_callback(event):
  global circuit_status, circuit_status_sample_id, ds_can_I, ds_can_I_var, ds_can_V, ds_vdd_V

  if event.message.circuit_id not in [1, 2]:
    return

  timestamp_s = (datetime.datetime.now() - start_time).total_seconds()
  timestamp_ms = int(timestamp_s * 1000)

  if event.message.circuit_id == 1:

    new_I_data = dict(x=ds_can_I.data['x'][-item_count:] + [timestamp_s],
                      y=ds_can_I.data['y'][-item_count:] + [event.message.current])

    new_V_data = dict(x=ds_can_V.data['x'][-item_count:] + [timestamp_s],
                      y=ds_can_V.data['y'][-item_count:] + [event.message.voltage])

    ds_can_I.data = new_I_data
    ds_can_V.data = new_V_data
    I_variance = numpy.array(new_I_data['y']).var()
    ds_can_I_var.data = dict(x=ds_can_I_var.data['x'][-item_count:] + [timestamp_s],
                             y=ds_can_I_var.data['y'][-item_count:] + [I_variance])

  elif event.message.circuit_id == 2:
    new_V_data = dict(x=ds_vdd_V.data['x'][-item_count:] + [timestamp_s],
                      y=ds_vdd_V.data['y'][-item_count:] + [event.message.voltage])
    ds_vdd_V.data = new_V_data
    I_variance = 0

  circuit_status[event.message.circuit_id] = dict(sample_id=circuit_status_sample_id,
                                                  timestamp_s=timestamp_s,
                                                  timestamp_ms=timestamp_ms,
                                                  circuit_id=event.message.circuit_id,
                                                  current=event.message.current,
                                                  voltage=event.message.voltage,
                                                  variance=I_variance)

  # print('{sample_id:8d} {timestamp_ms:12d} {current:03.2f} {voltage:03.2f}'.format(**circuit_status[1]))

  if event.message.circuit_id == 1:
    f_can.write(
      '{sample_id:8d} {timestamp_ms:12d} {current:03.2f} {voltage:03.2f} {variance:03.7f}\n'.format(
        **circuit_status[event.message.circuit_id]))
    f_can.flush()
  elif event.message.circuit_id == 2:
    f_vdd.write(
      '{sample_id:8d} {timestamp_ms:12d} {current:03.2f} {voltage:03.2f} {variance:03.7f}\n'.format(
        **circuit_status[event.message.circuit_id]))
    f_vdd.flush()

  if not write_enable: return

  circuit_status_sample_id = circuit_status_sample_id + 1


def device_temperature_callback(event):
  global temperature_sample_id, ds_T_mcp, ds_T_stm, f_T

  timestamp_s = (datetime.datetime.now() - start_time).total_seconds()
  timestamp_ms = int(timestamp_s * 1000)

  new_data = dict()

  if event.message.device_id == 0:
    new_data['x'] = ds_T_mcp.data['x'][-item_count:] + [timestamp_s]
    new_data['y'] = ds_T_mcp.data['y'][-item_count:] + [event.message.temperature]
    ds_T_mcp.data = new_data
  elif event.message.device_id == 1:
    new_data['x'] = ds_T_stm.data['x'][-item_count:] + [timestamp_s]
    new_data['y'] = ds_T_stm.data['y'][-item_count:] + [event.message.temperature]
    ds_T_stm.data = new_data
  else:
    return

  if not write_enable: return

  f_T.write("%3d %12d %12d %3.2f\n" % (event.message.device_id,
                                       temperature_sample_id,
                                       timestamp_ms,
                                       event.message.temperature))
  f_T.flush()

  temperature_sample_id = temperature_sample_id + 1


def run_uavcan(can_device, node_id):
  global node, f_armed, f_bat, f_can, f_vdd, f_T, f_metadata, start_time

  node_info = uavcan.protocol.GetNodeInfo.Response()
  node_info.name = 'io.p-systems.power.dashboard'
  node_info.software_version.major = 1

  node = uavcan.make_node(can_device,
                          node_id=node_id,
                          node_info=node_info, mode=uavcan.protocol.NodeStatus().MODE_OPERATIONAL)

  if write_enable:
    # Open file
    prefix = "power_monitor_data"
    timestr = time.strftime("%Y%m%d-%H_%M_%S")

    f_armed = open("%s_%s_armed.csv" % (prefix, timestr), "w")
    header = "index timestamp armed_status"
    f_armed.write(header + "\n")

    f_bat = open("%s_%s_bat.csv" % (prefix, timestr), "w")
    header = "index timestamp I_bat V_bat P_avg state_of_charge I_var"
    f_bat.write(header + "\n")

    f_can = open("%s_%s_can.csv" % (prefix, timestr), "w")
    header = "index timestamp I_can V_can I_var"
    f_can.write(header + "\n")

    f_vdd = open("%s_%s_vdd.csv" % (prefix, timestr), "w")
    header = "index timestamp I_vdd V_vdd I_var"
    f_vdd.write(header + "\n")

    f_T = open("%s_%s_T.csv" % (prefix, timestr), "w")
    header = "device index timestamp temperature"
    f_T.write(header + "\n")

    f_metadata = open("%s_%s_metadata.json" % (prefix, timestr), "w")

  start_time = datetime.datetime.now()

  node.add_handler(uavcan.equipment.device.Temperature, device_temperature_callback)
  node.add_handler(uavcan.equipment.power.BatteryInfo, battery_info_callback)
  node.add_handler(uavcan.equipment.power.CircuitStatus, circuit_status_callback)
  node.add_handler(uavcan.equipment.safety.ArmingStatus, armed_callback)

  node.add_handler(uavcan.protocol.NodeStatus, node_status_callback)
  node.add_handler(uavcan.protocol.GetNodeInfo, node_info_callback)


def update_uavcan_msgs():
  timestamp_ms = int((datetime.datetime.now() - start_time).total_seconds() * 1000)

  #  print("Tick %i" % timestamp_ms)

  try:
    node.spin(0.1)
  except uavcan.UAVCANException as ex:
    pass


def bokeh_dashboard(doc):
  global fig_T, fig_bat, fig_can, ds_T_mcp, ds_T_stm, ds_bat_I, ds_bat_V, ds_bat_soc, ds_can_I, ds_can_I_var, ds_can_V, \
    ds_vdd_V, ds_bat_I_var

  # create a plot and style its properties
  # p.border_fill_color = 'black'
  # p.background_fill_color = 'black'
  # p.outline_line_color = None
  # p.grid.grid_line_color = None

  # Temperaturef
  fig_T = figure(y_range=(0, 155), toolbar_location=None, title="Temperature", plot_width=400, plot_height=300)
  plot_T_mcp = fig_T.line(x=[], y=[], line_width=5, legend="MCP")
  ds_T_mcp = plot_T_mcp.data_source

  plot_T_stm = fig_T.line(x=[], y=[], line_width=5, line_color='gray', legend="STM")
  ds_T_stm = plot_T_stm.data_source

  fig_T.yaxis.axis_label = 'Temperature [C]'

  # Battery Current
  fig_bat_I = figure(y_range=(0, 200), toolbar_location=None, title="Battery Current", plot_width=800, plot_height=300)
  plot_bat_I = fig_bat_I.line(x=[], y=[], line_width=3, line_alpha=0.6)
  ds_bat_I = plot_bat_I.data_source
  fig_bat_I.xaxis.axis_label = 'Time [s]'
  fig_bat_I.yaxis.axis_label = 'Current [A]'

  hline1 = Span(location=150, dimension='width', line_color='red', line_width=3, line_dash='dotted')
  hline2 = Span(location=100, dimension='width', line_color='gray', line_width=3, line_dash='dotted')
  hline3 = Span(location=50, dimension='width', line_color='green', line_width=3, line_dash='dotted')
  fig_bat_I.renderers.extend([hline1, hline2, hline3])

  # fig_bat.extra_y_ranges = {"V": Range1d(start=9, end=14)}
  # fig_bat.add_layout(LinearAxis(y_range_name="V"), 'right')
  # fig_hist_bat = figure(toolbar_location=None, title="Battery", plot_width=400, plot_height=300)

  # Battery Voltage
  fig_bat_V = figure(y_range=battery_range, toolbar_location=None, title="Battery Voltage", plot_width=400, plot_height=300)
  plot_bat_V = fig_bat_V.line(x=[], y=[], line_width=3, line_alpha=0.6)
  ds_bat_V = plot_bat_V.data_source
  fig_bat_V.yaxis.axis_label = 'Voltage [V]'

  hline = Span(location=battery_warning, dimension='width', line_color='red', line_width=3, line_dash='dotted')
  fig_bat_V.renderers.extend([hline])

  # Battery State of Charge
  fig_soc = figure(y_range=(0, 100), toolbar_location=None, title="Battery State of Charge", plot_width=400,
                   plot_height=300)
  plot_soc = fig_soc.line(x=[], y=[], line_width=3, line_alpha=0.6)
  ds_bat_soc = plot_soc.data_source
  fig_soc.yaxis.axis_label = 'State of Charge [%]'

  hline1 = Span(location=50, dimension='width', line_color='red', line_width=3, line_dash='dotted')
  hline2 = Span(location=75, dimension='width', line_color='green', line_width=3, line_dash='dotted')
  fig_soc.renderers.extend([hline1, hline2])

  # CANbus
  fig_can = figure(y_range=(0, 6), toolbar_location=None, title="CANbus", plot_width=800, plot_height=300)
  fig_can.extra_y_ranges = {"I": Range1d(start=0, end=3)}
  fig_can.yaxis.axis_label = 'Voltage [V]'
  fig_can.add_layout(LinearAxis(y_range_name="I", axis_label='Current [A]'), 'right')

  plot_can_I = fig_can.line(x=[], y=[], line_width=3, line_alpha=0.6, line_color="red", legend="CAN Current [A]",
                            y_range_name="I")
  plot_can_V = fig_can.line(x=[], y=[], line_width=3, line_alpha=0.6, line_color="green", legend="CAN Voltage [V]")
  plot_vdd_V = fig_can.line(x=[], y=[], line_width=3, line_alpha=0.6, line_color="blue", legend="VDD Voltage [V]")

  ds_can_I = plot_can_I.data_source
  ds_can_V = plot_can_V.data_source
  ds_vdd_V = plot_vdd_V.data_source

  # Battery Variance
  fig_I_var = figure(y_range=(1e-8, 1e-1), y_axis_type="log", toolbar_location=None, title="Battery Variance",
                     plot_width=400, plot_height=300)

  plot_bat_I_var = fig_I_var.line(x=[], y=[], line_width=3, line_alpha=0.6, line_color="brown", legend="Battery")
  ds_bat_I_var = plot_bat_I_var.data_source

  plot_can_I_var = fig_I_var.line(x=[], y=[], line_width=3, line_alpha=0.6, line_color="gray", legend="5V")
  ds_can_I_var = plot_can_I_var.data_source

  # add a text renderer to our plot (no data yet)
  #  r = p.text(x=[], y=[], text=[], text_color=[], text_font_size="20pt",
  #            text_baseline="middle", text_align="center")

  # add a button widget and configure with the call back
  # button = Button(label="Reset")
  # button.on_click(callback)

  doc.add_periodic_callback(update_uavcan_msgs, 100)
  doc.add_root(column(row(fig_bat_I), row(fig_bat_V, fig_T, fig_soc), fig_can))


def main():
  from bokeh.util.browser import view

  parser = argparse.ArgumentParser("Show realtime power module telemetry")
  parser.add_argument("-d", "--device", dest="device", default=DEFAULT_CANBUS_DEVICE,
                      help="CANbus device to use")

  parser.add_argument("-n", "--node-id", dest="node_id", default=DEFAULT_NODE_ID,
                      help="CANbus device to use")

  args = parser.parse_args()

  # Initialize UAVCAN 
  run_uavcan(args.device, args.node_id)

  server = Server(dict({'/pm': bokeh_dashboard}))
  server.start()

  # Run Server
  server.io_loop.add_callback(view, "http://localhost:5006/")
  server.io_loop.start()


if __name__ == "__main__":
  main()
