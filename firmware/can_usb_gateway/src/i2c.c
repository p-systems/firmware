/*

The MIT License (MIT)

Copyright (c) 2020 Stou Sandalski
Copyright (c) 2016 Hubert Denkmair

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#include "config.h"
#include "stm32f0xx_hal.h"

I2C_HandleTypeDef hi2c2;


void i2c_init(){
  uint8_t buffer[8];

  GPIO_InitTypeDef GPIO_InitStruct = {0};

  __HAL_RCC_GPIOB_CLK_ENABLE();

  GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_11;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF1_I2C2;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  __HAL_RCC_I2C2_CLK_ENABLE();

  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x00303D5B;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;

  HAL_I2C_Init(&hi2c2);

  HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE);
  HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0);


  // Configure the TUSB320 device
  uint8_t tusb_address_7bit = (uint8_t) (TUSB_I2C_ADDRESS << 1U);

  // Read Device ID
  buffer[0] = 0;
  HAL_I2C_Master_Transmit(&hi2c2, tusb_address_7bit, buffer, 1, 100);
  HAL_I2C_Master_Receive(&hi2c2, tusb_address_7bit, buffer, 8, 100);

  // Read Revision
  buffer[0] = 0xA0;
  HAL_I2C_Master_Transmit(&hi2c2, tusb_address_7bit, buffer, 1, 100);
  HAL_I2C_Master_Receive(&hi2c2, tusb_address_7bit, buffer, 1, 100);

  // Quick revision Check
  if(buffer[0] != 0x02){
    HAL_GPIO_WritePin(LED_FAULT_GPIO_Port, LED_FAULT_Pin, GPIO_PIN_RESET);
  }

#if 1
  // Configure 1.5A mode (TSUB datasheet sec 7.5.2)
  buffer[0] = 0x08;
  buffer[1] = 0x04;

  HAL_I2C_Master_Transmit(&hi2c2, tusb_address_7bit, buffer, 2, 100);
#endif
}
