/*

The MIT License (MIT)

Copyright (c) 2016 Hubert Denkmair

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*/

#pragma once

#define CAN_QUEUE_SIZE 64

#define USBD_VID                     0x1d50U
#define USBD_PID_FS                  0x606fU
#define USBD_LANGID_STRING           1033U
#define USBD_CONFIGURATION_STRING_FS (uint8_t*) "gs_usb config"
#define USBD_INTERFACE_STRING_FS     (uint8_t*) "gs_usb interface"

#define USE_APP_DESCRIPTOR           1

#define BOARD_candleLight 1
#define BOARD_cantact     2
#define BOARD_canable     3
#define BOARD_usb2can     4
#define BOARD_cug         5


#if BOARD == BOARD_candleLight
#define USBD_PRODUCT_STRING_FS		(uint8_t*) "candleLight USB to CAN adapter"
#define USBD_MANUFACTURER_STRING	(uint8_t*) "bytewerk"
#define DFU_INTERFACE_STRING_FS		(uint8_t*) "candleLight firmware upgrade interface"
#define CAN_SILENT_Pin GPIO_PIN_13
#define CAN_SILENT_GPIO_Port GPIOC

#define LED1_Pin GPIO_PIN_0
#define LED1_Mode GPIO_MODE_OUTPUT_OD
#define LED1_GPIO_Port GPIOA
#define LED1_Active_Low true

#define LED2_GPIO_Port GPIOA
#define LED2_Pin GPIO_PIN_1
#define LED2_Mode GPIO_MODE_OUTPUT_OD
#define LED2_Active_Low true

#elif BOARD == BOARD_cantact
#define USBD_PRODUCT_STRING_FS		(uint8_t*) "cantact gs_usb"
#define USBD_MANUFACTURER_STRING	(uint8_t*) "cantact.io"
#define DFU_INTERFACE_STRING_FS		(uint8_t*) "cantact firmware upgrade interface"

// SILENT pin not connected

#define LED1_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_0	/* green */
#define LED1_Mode GPIO_MODE_OUTPUT_PP
#define LED1_Active_Low false

#define LED2_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_1	/* red */
#define LED2_Mode GPIO_MODE_OUTPUT_PP
#define LED2_Active_Low false

#elif BOARD == BOARD_canable
#define USBD_PRODUCT_STRING_FS			(uint8_t*) "canable gs_usb"
#define USBD_MANUFACTURER_STRING		(uint8_t*) "canable.io"
#define DFU_INTERFACE_STRING_FS			(uint8_t*) "canble firmware upgrade interface"

// SILENT pin not connected

#define LED1_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_0	/* green */
#define LED1_Mode GPIO_MODE_OUTPUT_PP
#define LED1_Active_Low false

#define LED2_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_1	/* blue */
#define LED2_Mode GPIO_MODE_OUTPUT_PP
#define LED1_Active_Low false

#elif BOARD == BOARD_usb2can
#define USBD_PRODUCT_STRING_FS		(uint8_t*) "USB2CAN RCA gs_usb"
#define USBD_MANUFACTURER_STRING	(uint8_t*) "Roboter Club Aachen"
#define DFU_INTERFACE_STRING_FS		(uint8_t*) "usb2can firmware upgrade interface"

// SILENT pin not connected

#define LED4_GPIO_Port GPIOA
#define LED4_Pin GPIO_PIN_0	/* white */
#define LED4_Mode GPIO_MODE_OUTPUT_OD
#define LED4_Active_Low true

#define LED2_GPIO_Port GPIOA
#define LED2_Pin GPIO_PIN_1	/* blue */
#define LED2_Mode GPIO_MODE_OUTPUT_OD
#define LED2_Active_Low true

#define LED_FAULT_GPIO_Port GPIOA
#define LED_FAULT_Pin GPIO_PIN_2	/* red */
#define LED_FAULT_Mode GPIO_MODE_OUTPUT_OD
#define LED_FAULT_Active_Low true

#define LED1_GPIO_Port GPIOB
#define LED1_Pin GPIO_PIN_3	/* green */
#define LED1_Mode GPIO_MODE_OUTPUT_OD
#define LED1_Active_Low true

#elif BOARD == BOARD_cug

#define USBD_PRODUCT_STRING_FS    (uint8_t*) "CAN-USB Gateway gs_usb"
#define USBD_MANUFACTURER_STRING  (uint8_t*) "Pomegranate Systems LLC"
#define DFU_INTERFACE_STRING_FS    (uint8_t*) "CUG firmware upgrade interface"

// External Oscillator Control
#define HSE_ENABLE_Pin GPIO_PIN_15
#define HSE_ENABLE_GPIO_Port GPIOC

// Power Bus Control
#define POWER_BUS_ENABLE_Pin GPIO_PIN_12
#define POWER_BUS_ENABLE_GPIO_Port GPIOB

// Transceiver Silent and Fault
#define CAN_SILENT_Pin GPIO_PIN_7
#define CAN_SILENT_GPIO_Port GPIOB

#if HARDWARE_VERSION > 2
#define CAN_FAULT_Pin GPIO_PIN_6
#define CAN_FAULT_GPIO_Port GPIOB
#endif

// Termination DIP and Switch
#define CAN_TERM_Pin GPIO_PIN_14
#define CAN_TERM_GPIO_Port GPIOC

# if 0
// r1
#define DIP_TERM_Pin GPIO_PIN_13
#define DIP_TERM_GPIO_Port GPIOB
#else
#define DIP_TERM_Pin GPIO_PIN_1
#define DIP_TERM_GPIO_Port GPIOA
#endif

// RX
#define LED1_GPIO_Port GPIOA
#define LED1_Pin GPIO_PIN_15
#define LED1_Mode GPIO_MODE_OUTPUT_OD
#define LED1_Active_Low   true

// TX
#define LED2_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_3
#define LED2_Mode GPIO_MODE_OUTPUT_OD
#define LED2_Active_Low   true

// FAULT
#define LED_FAULT_GPIO_Port GPIOB
#define LED_FAULT_Pin GPIO_PIN_4
#define LED_FAULT_Mode GPIO_MODE_OUTPUT_OD
#define LED_FAULT_Active_Low true

// Power
#define LED_POWER_GPIO_Port GPIOB
#define LED_POWER_Pin GPIO_PIN_5

// USB power control chip
#define TUSB_I2C_ADDRESS 0x47U

#else
#error please define BOARD
#endif