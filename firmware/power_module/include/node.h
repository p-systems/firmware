/**
 * @file
 * @brief Handle "network traffic" from DroneCAN
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */


#pragma once

#include "stm32_util.h"

/**
 * Bring-up CANbus interface (including autobaud and uavcan id)
 */
bool CANbus_Init();

/**
 * Process UAVCAN requests
 */
void Service_Requests();
