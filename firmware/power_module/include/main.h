/**
 * @file
 * @brief Hardware setup and firmware entry
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#pragma once

#include "stm32f3xx.h"
#include "stm32f3xx_ll_adc.h"
#include "stm32f3xx_ll_bus.h"
#include "stm32f3xx_ll_cortex.h"
#include "stm32f3xx_ll_dma.h"
#include "stm32f3xx_ll_exti.h"
#include "stm32f3xx_ll_gpio.h"
#include "stm32f3xx_ll_i2c.h"
#include "stm32f3xx_ll_iwdg.h"
#include "stm32f3xx_ll_pwr.h"
#include "stm32f3xx_ll_rcc.h"
#include "stm32f3xx_ll_system.h"
#include "stm32f3xx_ll_tim.h"
#include "stm32f3xx_ll_utils.h"

#include "stm32_util.h"

#include "params.h"

// Device priorities
#define PrioritySystem 0
#define PriorityADC 1
#define PriorityCAN 2
#define PriorityI2C 3

/**
 * IRQ Handlers
 */
void HardFault_Handler(void);

void SysTick_Handler(void);

void Error_Handler();


/**
 * Hardware Management Functions
 */

void ADC_DeInit();

void CAN_DeInit();

/**
 * Enable or disable the can termination. We should always be using an NC switch.
 */
void CAN_Set_Termination();

void GPIO_DeInit();

void I2C_Internal_Init();

void I2C_Internal_DeInit();

#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
void I2C_External_Init();

void I2C_External_DeInit();
#endif

void LED_Add_State(led_state_t state);

void LED_DeInit();

void LED_Init();

void LED_Remove_State(led_state_t state);

uint32_t LED_Set_Values(uint8_t  red,
                        uint8_t  green,
                        uint8_t  blue,
                        uint32_t off_time);

void LED_Update();

void SystemClock_DeInit();

void TIM_DeInit();

void Enable_Interrupts(bool enabled);
