/**
 * @file
 * @brief DroneCAN firmware parameters setup
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#define PARAMETERS_CURRENT_VERSION 4

/**
 * Configuration Parameters
 */


/**
 * CAN (can.)
 *
 *  Start of parameters block. (These are not in alphabetic order)
 *  Reserve 16 bytes at the front for no reason
 */

// clang-format off
#define PARAM_NAME_CAN_ENABLE_AUTO_BAUD                               "can.enable_auto_baud"
#define PARAM_DEFAULT_CAN_ENABLE_AUTO_BAUD                            true
#define PARAM_OFFSET_CAN_ENABLE_AUTO_BAUD                             16
#define PARAM_SIZE_CAN_ENABLE_AUTO_BAUD                               1
#define PARAM_VERSION_CAN_ENABLE_AUTO_BAUD                            1
#define PARAM_HELP_CAN_ENABLE_AUTO_BAUD                               "Enable automatic detection of the CANbus baud rate."

#define PARAM_NAME_CAN_ENABLE_TERM                                    "can.enable_term"
#define PARAM_DEFAULT_CAN_ENABLE_TERM                                 true
#define PARAM_OFFSET_CAN_ENABLE_TERM                                  (PARAM_OFFSET_CAN_ENABLE_AUTO_BAUD + PARAM_SIZE_CAN_ENABLE_AUTO_BAUD)
#define PARAM_SIZE_CAN_ENABLE_TERM                                    1
#define PARAM_VERSION_CAN_ENABLE_TERM                                 1
#define PARAM_HELP_CAN_ENABLE_TERM                                    "Enable the 120 Ohm termination resistor."

#define PARAM_NAME_CAN_DEFAULT_BAUD_KBPS                              "can.baud_kbps"
#define PARAM_DEFAULT_CAN_DEFAULT_BAUD_KBPS                           1000
#define PARAM_MAX_CAN_DEFAULT_BAUD_KBPS                               1000
#define PARAM_MIN_CAN_DEFAULT_BAUD_KBPS                               1
#define PARAM_OFFSET_CAN_DEFAULT_BAUD_KBPS                            (PARAM_OFFSET_CAN_ENABLE_TERM + PARAM_SIZE_CAN_ENABLE_TERM + 2)
#define PARAM_SIZE_CAN_DEFAULT_BAUD_KBPS                              2
#define PARAM_VERSION_CAN_DEFAULT_BAUD_KBPS                           1
#define PARAM_HELP_CAN_DEFAULT_BAUD_KBPS                              "Default CANbus baud rate [kbps]."

#define PARAM_NAME_CAN_AUTO_BAUD_MAX_RETRIES                          "can.auto_baud.max_retries"
#define PARAM_DEFAULT_CAN_AUTO_BAUD_MAX_RETRIES                       0
#define PARAM_MAX_CAN_AUTO_BAUD_MAX_RETRIES                           1000
#define PARAM_MIN_CAN_AUTO_BAUD_MAX_RETRIES                           0
#define PARAM_OFFSET_CAN_AUTO_BAUD_MAX_RETRIES                        (PARAM_OFFSET_CAN_DEFAULT_BAUD_KBPS + PARAM_SIZE_CAN_DEFAULT_BAUD_KBPS)
#define PARAM_SIZE_CAN_AUTO_BAUD_MAX_RETRIES                          2
#define PARAM_VERSION_CAN_AUTO_BAUD_MAX_RETRIES                       1
#define PARAM_HELP_CAN_AUTO_BAUD_MAX_RETRIES                          "How many times to cycle through the list of baud rates before giving up and using the default. Set to zero to retry forever."

#define PARAM_NAME_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                     "can.auto_baud.retry_timout_ms"
#define PARAM_DEFAULT_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                  1500
#define PARAM_MAX_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                      15000
#define PARAM_MIN_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                      0
#define PARAM_OFFSET_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                   (PARAM_OFFSET_CAN_AUTO_BAUD_MAX_RETRIES + PARAM_SIZE_CAN_AUTO_BAUD_MAX_RETRIES)
#define PARAM_SIZE_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                     2
#define PARAM_VERSION_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                  1
#define PARAM_HELP_CAN_AUTO_BAUD_RETRY_TIMEOUT_MS                     "How long to wait before trying next baud."

/**
 * UAVCAN (uavcan.)
 */

#define PARAM_NAME_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE                "uavcan.dynamic_id_allocation.enable"
#define PARAM_DEFAULT_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE             true
#define PARAM_OFFSET_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE              48
#define PARAM_SIZE_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE                1
#define PARAM_VERSION_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE             1
#define PARAM_HELP_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE                "Enable node ID allocation requests."

#define PARAM_NAME_UAVCAN_DEFAULT_NODE_ID                             "uavcan.default_node_id"
#define PARAM_DEFAULT_UAVCAN_DEFAULT_NODE_ID                          0
#define PARAM_MAX_UAVCAN_DEFAULT_NODE_ID                              127
#define PARAM_MIN_UAVCAN_DEFAULT_NODE_ID                              0
#define PARAM_OFFSET_UAVCAN_DEFAULT_NODE_ID                           (PARAM_OFFSET_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE + PARAM_SIZE_UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE + 2)
#define PARAM_SIZE_UAVCAN_DEFAULT_NODE_ID                             1
#define PARAM_VERSION_UAVCAN_DEFAULT_NODE_ID                          1
#define PARAM_HELP_UAVCAN_DEFAULT_NODE_ID                             "Default Node ID, leave 0 to use uuid derived id."

#define PARAM_NAME_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS      "uavcan.dynamic_id_allocation.retry_time_ms"
#define PARAM_DEFAULT_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS   1500
#define PARAM_MAX_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS       15000
#define PARAM_MIN_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS       0
#define PARAM_OFFSET_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS    (PARAM_OFFSET_UAVCAN_DEFAULT_NODE_ID + PARAM_SIZE_UAVCAN_DEFAULT_NODE_ID)
#define PARAM_SIZE_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS      2
#define PARAM_VERSION_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS   1
#define PARAM_HELP_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS      "How long to wait between name resolution retries."

#define PARAM_NAME_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES           "uavcan.dynamic_id_allocation.max_retries"
#define PARAM_DEFAULT_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES        5
#define PARAM_MAX_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES            1000
#define PARAM_MIN_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES            0
#define PARAM_OFFSET_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES         (PARAM_OFFSET_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS + PARAM_SIZE_UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS)
#define PARAM_SIZE_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES           2
#define PARAM_VERSION_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES        1
#define PARAM_HELP_UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES           "How many times to try to get a name before using the default."

/**
 * System (system.)
 */

// Flags
#define PARAM_NAME_SYSTEM_ENABLE_FIRMWARE_UPDATES                     "system.enable_firmware_updates"
#define PARAM_DEFAULT_SYSTEM_ENABLE_FIRMWARE_UPDATES                  true
#define PARAM_OFFSET_SYSTEM_ENABLE_FIRMWARE_UPDATES                   64
#define PARAM_SIZE_SYSTEM_ENABLE_FIRMWARE_UPDATES                     1
#define PARAM_VERSION_SYSTEM_ENABLE_FIRMWARE_UPDATES                  1
#define PARAM_HELP_SYSTEM_ENABLE_FIRMWARE_UPDATES                     "Allow firmware updates without manually rebooting into the bootloader."

#define PARAM_NAME_SYSTEM_ENABLE_EXTERNAL_I2C                         "system.enable_external_i2c"
#define PARAM_DEFAULT_SYSTEM_ENABLE_EXTERNAL_I2C                      false
#define PARAM_OFFSET_SYSTEM_ENABLE_EXTERNAL_I2C                       (PARAM_OFFSET_SYSTEM_ENABLE_FIRMWARE_UPDATES + PARAM_SIZE_SYSTEM_ENABLE_FIRMWARE_UPDATES)
#define PARAM_SIZE_SYSTEM_ENABLE_EXTERNAL_I2C                         1
#define PARAM_VERSION_SYSTEM_ENABLE_EXTERNAL_I2C                      1
#define PARAM_HELP_SYSTEM_ENABLE_EXTERNAL_I2C                         "Enable the external i2c bus."

// Timing
#define PARAM_NAME_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                   "system.emergency_msg_interval_ms"
#define PARAM_DEFAULT_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                200
#define PARAM_MAX_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                    60000
#define PARAM_MIN_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                    0
#define PARAM_OFFSET_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                 (PARAM_OFFSET_SYSTEM_ENABLE_FIRMWARE_UPDATES + 16)
#define PARAM_SIZE_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                   2
#define PARAM_VERSION_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                1
#define PARAM_HELP_SYSTEM_EMERGENCY_MSG_INTERVAL_MS                   "How often to send status messages under emergency conditions such as over current/voltage/temp."

#define PARAM_NAME_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS                 "system.node_status_message_interval_ms"
#define PARAM_DEFAULT_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS              1000
#define PARAM_MAX_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS                  1500
#define PARAM_MIN_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS                  100
#define PARAM_OFFSET_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS               (PARAM_OFFSET_SYSTEM_EMERGENCY_MSG_INTERVAL_MS + PARAM_SIZE_SYSTEM_EMERGENCY_MSG_INTERVAL_MS)
#define PARAM_SIZE_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS                 2
#define PARAM_VERSION_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS              1
#define PARAM_HELP_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS                 "How often to send node status messages."

#define PARAM_NAME_SYSTEM_SENSOR_READOUT_INTERVAL_MS                  "system.sensor_readout_interval_ms"
#define PARAM_DEFAULT_SYSTEM_SENSOR_READOUT_INTERVAL_MS               10
#define PARAM_MAX_SYSTEM_SENSOR_READOUT_INTERVAL_MS                   1000
#define PARAM_MIN_SYSTEM_SENSOR_READOUT_INTERVAL_MS                   0
#define PARAM_OFFSET_SYSTEM_SENSOR_READOUT_INTERVAL_MS                (PARAM_OFFSET_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS + PARAM_SIZE_SYSTEM_NODE_STATUS_MSG_INTERVAL_MS)
#define PARAM_SIZE_SYSTEM_SENSOR_READOUT_INTERVAL_MS                  2
#define PARAM_VERSION_SYSTEM_SENSOR_READOUT_INTERVAL_MS               1
#define PARAM_HELP_SYSTEM_SENSOR_READOUT_INTERVAL_MS                  "How often to query the current and temperature sensors."

// Temperature Device ID

// MCU Temperature Sensor
#define PARAM_NAME_MCU_TEMP_DEVICE_ID                                 "system.temp.MCU.device_id"
#define PARAM_DEFAULT_MCU_TEMP_DEVICE_ID                              0
#define PARAM_MAX_MCU_TEMP_DEVICE_ID                                  UINT16_MAX
#define PARAM_MIN_MCU_TEMP_DEVICE_ID                                  0
#define PARAM_OFFSET_MCU_TEMP_DEVICE_ID                               (PARAM_OFFSET_SYSTEM_EMERGENCY_MSG_INTERVAL_MS + 16)
#define PARAM_SIZE_MCU_TEMP_DEVICE_ID                                 2
#define PARAM_VERSION_MCU_TEMP_DEVICE_ID                              1
#define PARAM_HELP_MCU_TEMP_DEVICE_ID                                 "Device ID for MCU Temperature Sensor."

#define PARAM_NAME_MCU_TEMP_MSG_INTERVAL_MS                           "system.temp.MCU.msg_interval_ms"
#define PARAM_DEFAULT_MCU_TEMP_MSG_INTERVAL_MS                        500
#define PARAM_MAX_MCU_TEMP_MSG_INTERVAL_MS                            60000
#define PARAM_MIN_MCU_TEMP_MSG_INTERVAL_MS                            0
#define PARAM_OFFSET_MCU_TEMP_MSG_INTERVAL_MS                         (PARAM_OFFSET_MCU_TEMP_DEVICE_ID + PARAM_SIZE_MCU_TEMP_DEVICE_ID)
#define PARAM_SIZE_MCU_TEMP_MSG_INTERVAL_MS                           2
#define PARAM_VERSION_MCU_TEMP_MSG_INTERVAL_MS                        1
#define PARAM_HELP_MCU_TEMP_MSG_INTERVAL_MS                           "How often to send MCU device temperature messages."

#define PARAM_NAME_MCU_TEMP_LIMIT_MAX_C                               "system.temp.MCU.limit.max_C"
#define PARAM_DEFAULT_MCU_TEMP_LIMIT_MAX_C                            60.0f
#define PARAM_MAX_MCU_TEMP_LIMIT_MAX_C                                200.0f
#define PARAM_MIN_MCU_TEMP_LIMIT_MAX_C                                (-200.0f)
#define PARAM_OFFSET_MCU_TEMP_LIMIT_MAX_C                             (PARAM_OFFSET_MCU_TEMP_MSG_INTERVAL_MS + PARAM_SIZE_MCU_TEMP_MSG_INTERVAL_MS + 4)
#define PARAM_SIZE_MCU_TEMP_LIMIT_MAX_C                               4
#define PARAM_VERSION_MCU_TEMP_LIMIT_MAX_C                            1
#define PARAM_HELP_MCU_TEMP_LIMIT_MAX_C                               "Temperatures above this value will cause over-temperature warnings."

#define PARAM_NAME_MCU_TEMP_LIMIT_MIN_C                               "system.temp.MCU.limit.min_C"
#define PARAM_DEFAULT_MCU_TEMP_LIMIT_MIN_C                            (-20.0f)
#define PARAM_MAX_MCU_TEMP_LIMIT_MIN_C                                200.0f
#define PARAM_MIN_MCU_TEMP_LIMIT_MIN_C                                (-200.0f)
#define PARAM_OFFSET_MCU_TEMP_LIMIT_MIN_C                             (PARAM_OFFSET_MCU_TEMP_LIMIT_MAX_C + PARAM_SIZE_MCU_TEMP_LIMIT_MAX_C)
#define PARAM_SIZE_MCU_TEMP_LIMIT_MIN_C                               PARAM_SIZE_MCU_TEMP_LIMIT_MAX_C
#define PARAM_VERSION_MCU_TEMP_LIMIT_MIN_C                            PARAM_VERSION_MCU_TEMP_LIMIT_MAX_C
#define PARAM_HELP_MCU_TEMP_LIMIT_MIN_C                               "Temperatures below this value will cause under-temperature warnings."


// PCB Temperature Sensor
#define PARAM_NAME_PCB_TEMP_DEVICE_ID                                 "system.temp.PCB.device_id"
#define PARAM_DEFAULT_PCB_TEMP_DEVICE_ID                              1
#define PARAM_MAX_PCB_TEMP_DEVICE_ID                                  PARAM_MAX_MCU_TEMP_DEVICE_ID
#define PARAM_MIN_PCB_TEMP_DEVICE_ID                                  PARAM_MIN_MCU_TEMP_DEVICE_ID
#define PARAM_OFFSET_PCB_TEMP_DEVICE_ID                               (PARAM_OFFSET_MCU_TEMP_DEVICE_ID + 32)
#define PARAM_SIZE_PCB_TEMP_DEVICE_ID                                 PARAM_SIZE_MCU_TEMP_DEVICE_ID
#define PARAM_VERSION_PCB_TEMP_DEVICE_ID                              PARAM_VERSION_MCU_TEMP_DEVICE_ID
#define PARAM_HELP_PCB_TEMP_DEVICE_ID                                 "Device ID for PCB Temperature Sensor."

#define PARAM_NAME_PCB_TEMP_MSG_INTERVAL_MS                           "system.temp.PCB.msg_interval_ms"
#define PARAM_DEFAULT_PCB_TEMP_MSG_INTERVAL_MS                        500
#define PARAM_MAX_PCB_TEMP_MSG_INTERVAL_MS                            PARAM_MAX_MCU_TEMP_MSG_INTERVAL_MS
#define PARAM_MIN_PCB_TEMP_MSG_INTERVAL_MS                            PARAM_MIN_MCU_TEMP_MSG_INTERVAL_MS
#define PARAM_OFFSET_PCB_TEMP_MSG_INTERVAL_MS                         (PARAM_OFFSET_PCB_TEMP_DEVICE_ID + PARAM_SIZE_PCB_TEMP_DEVICE_ID)
#define PARAM_SIZE_PCB_TEMP_MSG_INTERVAL_MS                           PARAM_SIZE_MCU_TEMP_MSG_INTERVAL_MS
#define PARAM_VERSION_PCB_TEMP_MSG_INTERVAL_MS                        PARAM_VERSION_MCU_TEMP_MSG_INTERVAL_MS
#define PARAM_HELP_PCB_TEMP_MSG_INTERVAL_MS                           "How often to send PCB device temperature messages."

#define PARAM_NAME_PCB_TEMP_LIMIT_MAX_C                               "system.temp.PCB.limit.max_C"
#define PARAM_DEFAULT_PCB_TEMP_LIMIT_MAX_C                            75.0f
#define PARAM_MAX_PCB_TEMP_LIMIT_MAX_C                                200.0f
#define PARAM_MIN_PCB_TEMP_LIMIT_MAX_C                                (-200.0f)
#define PARAM_OFFSET_PCB_TEMP_LIMIT_MAX_C                             (PARAM_OFFSET_PCB_TEMP_MSG_INTERVAL_MS + PARAM_SIZE_PCB_TEMP_MSG_INTERVAL_MS + 4)
#define PARAM_SIZE_PCB_TEMP_LIMIT_MAX_C                               4
#define PARAM_VERSION_PCB_TEMP_LIMIT_MAX_C                            1
#define PARAM_HELP_PCB_TEMP_LIMIT_MAX_C                               "Temperatures above this value will cause over-temperature warnings."

#define PARAM_NAME_PCB_TEMP_LIMIT_MIN_C                               "system.temp.PCB.limit.min_C"
#define PARAM_DEFAULT_PCB_TEMP_LIMIT_MIN_C                            (-20.0f)
#define PARAM_MAX_PCB_TEMP_LIMIT_MIN_C                                200.0f
#define PARAM_MIN_PCB_TEMP_LIMIT_MIN_C                                (-200.0f)
#define PARAM_OFFSET_PCB_TEMP_LIMIT_MIN_C                             (PARAM_OFFSET_PCB_TEMP_LIMIT_MAX_C + PARAM_SIZE_PCB_TEMP_LIMIT_MAX_C)
#define PARAM_SIZE_PCB_TEMP_LIMIT_MIN_C                               PARAM_SIZE_PCB_TEMP_LIMIT_MAX_C
#define PARAM_VERSION_PCB_TEMP_LIMIT_MIN_C                            PARAM_VERSION_PCB_TEMP_LIMIT_MAX_C
#define PARAM_HELP_PCB_TEMP_LIMIT_MIN_C                               "Temperatures below this value will cause under-temperature warnings."

/**
 * LED (led.)
 */

#define PARAM_NAME_LED_ENABLED                                        "system.led.enabled"
#define PARAM_DEFAULT_LED_ENABLED                                     true
#define PARAM_OFFSET_LED_ENABLED                                      (PARAM_OFFSET_PCB_TEMP_DEVICE_ID + 32)
#define PARAM_SIZE_LED_ENABLED                                        1
#define PARAM_VERSION_LED_ENABLED                                     1
#define PARAM_HELP_LED_ENABLED                                        "Enable the RGB LED."

#define PARAM_NAME_LED_IDENTIFY                                       "system.led.identify"
#define PARAM_DEFAULT_LED_IDENTIFY                                    false
#define PARAM_OFFSET_LED_IDENTIFY                                     (PARAM_OFFSET_LED_ENABLED + PARAM_SIZE_LED_ENABLED)
#define PARAM_SIZE_LED_IDENTIFY                                       1
#define PARAM_VERSION_LED_IDENTIFY                                    1
#define PARAM_HELP_LED_IDENTIFY                                       "Flash the LED Red - Green - Blue to help identify this board in a collection."

// Calibration values for the RGB LED so they have all the same intensity
#define PARAM_NAME_LED_BRIGHTNESS_SCALE_R                             "system.led.scale.r"
#define PARAM_DEFAULT_LED_BRIGHTNESS_SCALE_R                          175
#define PARAM_MAX_LED_BRIGHTNESS_SCALE_R                              255
#define PARAM_MIN_LED_BRIGHTNESS_SCALE_R                              0
#define PARAM_OFFSET_LED_BRIGHTNESS_SCALE_R                           (PARAM_OFFSET_LED_ENABLED + 16)
#define PARAM_SIZE_LED_BRIGHTNESS_SCALE_R                             2
#define PARAM_VERSION_LED_BRIGHTNESS_SCALE_R                          1
#define PARAM_HELP_LED_BRIGHTNESS_SCALE_R                             "Status LED red channel brightness level."

#define PARAM_NAME_LED_BRIGHTNESS_SCALE_G                             "system.led.scale.g"
#define PARAM_DEFAULT_LED_BRIGHTNESS_SCALE_G                          255
#define PARAM_MAX_LED_BRIGHTNESS_SCALE_G                              PARAM_MAX_LED_BRIGHTNESS_SCALE_R
#define PARAM_MIN_LED_BRIGHTNESS_SCALE_G                              PARAM_MIN_LED_BRIGHTNESS_SCALE_R
#define PARAM_OFFSET_LED_BRIGHTNESS_SCALE_G                           (PARAM_OFFSET_LED_BRIGHTNESS_SCALE_R + PARAM_SIZE_LED_BRIGHTNESS_SCALE_R)
#define PARAM_SIZE_LED_BRIGHTNESS_SCALE_G                             PARAM_SIZE_LED_BRIGHTNESS_SCALE_R
#define PARAM_VERSION_LED_BRIGHTNESS_SCALE_G                          PARAM_VERSION_LED_BRIGHTNESS_SCALE_R
#define PARAM_HELP_LED_BRIGHTNESS_SCALE_G                             "Status LED green channel brightness level."

#define PARAM_NAME_LED_BRIGHTNESS_SCALE_B                             "system.led.scale.b"
#define PARAM_DEFAULT_LED_BRIGHTNESS_SCALE_B                          PARAM_DEFAULT_LED_BRIGHTNESS_SCALE_G
#define PARAM_MAX_LED_BRIGHTNESS_SCALE_B                              PARAM_MAX_LED_BRIGHTNESS_SCALE_R
#define PARAM_MIN_LED_BRIGHTNESS_SCALE_B                              PARAM_MIN_LED_BRIGHTNESS_SCALE_R
#define PARAM_OFFSET_LED_BRIGHTNESS_SCALE_B                           (PARAM_OFFSET_LED_BRIGHTNESS_SCALE_G + PARAM_SIZE_LED_BRIGHTNESS_SCALE_G)
#define PARAM_SIZE_LED_BRIGHTNESS_SCALE_B                             PARAM_SIZE_LED_BRIGHTNESS_SCALE_R
#define PARAM_VERSION_LED_BRIGHTNESS_SCALE_B                          PARAM_VERSION_LED_BRIGHTNESS_SCALE_R
#define PARAM_HELP_LED_BRIGHTNESS_SCALE_B                             "Status LED blue channel brightness level."



/**
 * VDD or System Bus
 */

#define PARAM_NAME_BUS_SYS_CIRCUIT_ID                                 "bus.sys.circuit_id"
#define PARAM_DEFAULT_BUS_SYS_CIRCUIT_ID                              2
#define PARAM_MAX_BUS_SYS_CIRCUIT_ID                                  UINT16_MAX
#define PARAM_MIN_BUS_SYS_CIRCUIT_ID                                  0
#define PARAM_OFFSET_BUS_SYS_CIRCUIT_ID                               (PARAM_OFFSET_LED_BRIGHTNESS_SCALE_R + 16)
#define PARAM_SIZE_BUS_SYS_CIRCUIT_ID                                 2
#define PARAM_VERSION_BUS_SYS_CIRCUIT_ID                              1
#define PARAM_HELP_BUS_SYS_CIRCUIT_ID                                 "Circuit ID for the 3V/VDD bus."

// Message Interval for Circuit Status
#define PARAM_NAME_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS             "bus.sys.circuit_status_msg_interval_ms"
#define PARAM_DEFAULT_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS          0
#define PARAM_MAX_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS              60000
#define PARAM_MIN_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS              0
#define PARAM_OFFSET_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS           (PARAM_OFFSET_BUS_SYS_CIRCUIT_ID + PARAM_SIZE_BUS_SYS_CIRCUIT_ID)
#define PARAM_SIZE_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS             2
#define PARAM_VERSION_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS          1
#define PARAM_HELP_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS             "How often to send circuit status messages for the 3V/VDD bus."


#define PARAM_NAME_BUS_SYS_VOLTAGE_LIMIT_MAX                          "bus.sys.V_limit.max_V"
#define PARAM_DEFAULT_BUS_SYS_VOLTAGE_LIMIT_MAX                       3.50f
#define PARAM_MAX_BUS_SYS_VOLTAGE_LIMIT_MAX                           3.60f
#define PARAM_MIN_BUS_SYS_VOLTAGE_LIMIT_MAX                           1.60f
#define PARAM_OFFSET_BUS_SYS_VOLTAGE_LIMIT_MAX                        (PARAM_OFFSET_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS + 4)
#define PARAM_SIZE_BUS_SYS_VOLTAGE_LIMIT_MAX                          4
#define PARAM_VERSION_BUS_SYS_VOLTAGE_LIMIT_MAX                       1
#define PARAM_HELP_BUS_SYS_VOLTAGE_LIMIT_MAX                          "Voltage threshold that activates Over-Voltage warnings for primary/battery bus."

#define PARAM_NAME_BUS_SYS_VOLTAGE_LIMIT_MIN                          "bus.sys.V_limit.min_V"
#define PARAM_DEFAULT_BUS_SYS_VOLTAGE_LIMIT_MIN                       3.10f
#define PARAM_MAX_BUS_SYS_VOLTAGE_LIMIT_MIN                           3.60f
#define PARAM_MIN_BUS_SYS_VOLTAGE_LIMIT_MIN                           1.60f
#define PARAM_OFFSET_BUS_SYS_VOLTAGE_LIMIT_MIN                        (PARAM_OFFSET_BUS_SYS_VOLTAGE_LIMIT_MAX + PARAM_SIZE_BUS_SYS_VOLTAGE_LIMIT_MAX)
#define PARAM_SIZE_BUS_SYS_VOLTAGE_LIMIT_MIN                          PARAM_SIZE_BUS_SYS_VOLTAGE_LIMIT_MAX
#define PARAM_VERSION_BUS_SYS_VOLTAGE_LIMIT_MIN                       1
#define PARAM_HELP_BUS_SYS_VOLTAGE_LIMIT_MIN                          "Voltage threshold that activates Under-Voltage warnings for Main bus."



/**
 * Primary Battery Connection Block (bus.main.)
 */
#define PARAM_NAME_BUS_MAIN_CIRCUIT_ID                                "bus.main.circuit_id"
#define PARAM_DEFAULT_BUS_MAIN_CIRCUIT_ID                             0
#define PARAM_MAX_BUS_MAIN_CIRCUIT_ID                                 PARAM_MAX_BUS_SYS_CIRCUIT_ID
#define PARAM_MIN_BUS_MAIN_CIRCUIT_ID                                 PARAM_MIN_BUS_SYS_CIRCUIT_ID
#define PARAM_OFFSET_BUS_MAIN_CIRCUIT_ID                              (PARAM_OFFSET_BUS_SYS_CIRCUIT_ID + 4 * 16)
#define PARAM_SIZE_BUS_MAIN_CIRCUIT_ID                                PARAM_SIZE_BUS_SYS_CIRCUIT_ID
#define PARAM_VERSION_BUS_MAIN_CIRCUIT_ID                             PARAM_VERSION_BUS_SYS_CIRCUIT_ID
#define PARAM_HELP_BUS_MAIN_CIRCUIT_ID                                "Circuit ID for the main/primary/battery bus."

#define PARAM_NAME_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS            "bus.main.circuit_status_msg_interval_ms"
#define PARAM_DEFAULT_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS         0
#define PARAM_MAX_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS             60000
#define PARAM_MIN_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS             0
#define PARAM_OFFSET_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS          (PARAM_OFFSET_BUS_MAIN_CIRCUIT_ID + PARAM_SIZE_BUS_MAIN_CIRCUIT_ID)
#define PARAM_SIZE_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS            2
#define PARAM_VERSION_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS         1
#define PARAM_HELP_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS            "How often to send circuit status messages for the primary/battery bus."

// Filter configuration
#define PARAM_NAME_BUS_MAIN_CURRENT_FILTER_ENABLE                     "bus.main.current_filter.enable"
#define PARAM_DEFAULT_BUS_MAIN_CURRENT_FILTER_ENABLE                  false
#define PARAM_OFFSET_BUS_MAIN_CURRENT_FILTER_ENABLE                   (PARAM_OFFSET_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS + PARAM_SIZE_BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS)
#define PARAM_SIZE_BUS_MAIN_CURRENT_FILTER_ENABLE                     1
#define PARAM_VERSION_BUS_MAIN_CURRENT_FILTER_ENABLE                  1
#define PARAM_HELP_BUS_MAIN_CURRENT_FILTER_ENABLE                     "Enable the main bus current filter."

#define PARAM_NAME_BUS_MAIN_CURRENT_FILTER_CONST                      "bus.main.current_filter.coefficient"
#define PARAM_DEFAULT_BUS_MAIN_CURRENT_FILTER_CONST                   1.2f
#define PARAM_MAX_BUS_MAIN_CURRENT_FILTER_CONST                       10.0f
#define PARAM_MIN_BUS_MAIN_CURRENT_FILTER_CONST                       0.0f
#define PARAM_OFFSET_BUS_MAIN_CURRENT_FILTER_CONST                    (PARAM_OFFSET_BUS_MAIN_CURRENT_FILTER_ENABLE + PARAM_SIZE_BUS_MAIN_CURRENT_FILTER_ENABLE + 3)
#define PARAM_SIZE_BUS_MAIN_CURRENT_FILTER_CONST                      4
#define PARAM_VERSION_BUS_MAIN_CURRENT_FILTER_CONST                   1
#define PARAM_HELP_BUS_MAIN_CURRENT_FILTER_CONST                      "Coefficient for the main bus current filter."

#define PARAM_NAME_BUS_MAIN_R_SHUNT                                   "bus.main.r_shunt_Ohm"
#define PARAM_DEFAULT_BUS_MAIN_R_SHUNT                                0.0005f
#define PARAM_MAX_BUS_MAIN_R_SHUNT                                    1.0f
#define PARAM_MIN_BUS_MAIN_R_SHUNT                                    0.0000f
#define PARAM_OFFSET_BUS_MAIN_R_SHUNT                                 (PARAM_OFFSET_BUS_MAIN_CIRCUIT_ID + 24)
#define PARAM_SIZE_BUS_MAIN_R_SHUNT                                   4
#define PARAM_VERSION_BUS_MAIN_R_SHUNT                                1
#define PARAM_HELP_BUS_MAIN_R_SHUNT                                   "Value of the shunt resistor for the primary/battery bus."

#define PARAM_NAME_BUS_MAIN_CURRENT_LIMIT_MAX                         "bus.main.I_limit.max_A"
#define PARAM_DEFAULT_BUS_MAIN_CURRENT_LIMIT_MAX                      75.00f
#define PARAM_MAX_BUS_MAIN_CURRENT_LIMIT_MAX                          200.00f
#define PARAM_MIN_BUS_MAIN_CURRENT_LIMIT_MAX                          (-200.00f)
#define PARAM_OFFSET_BUS_MAIN_CURRENT_LIMIT_MAX                       (PARAM_OFFSET_BUS_MAIN_R_SHUNT + PARAM_SIZE_BUS_MAIN_R_SHUNT)
#define PARAM_SIZE_BUS_MAIN_CURRENT_LIMIT_MAX                         4
#define PARAM_VERSION_BUS_MAIN_CURRENT_LIMIT_MAX                      4
#define PARAM_HELP_BUS_MAIN_CURRENT_LIMIT_MAX                         "Current threshold that activates Over-Current warnings for primary/battery bus."

#define PARAM_NAME_BUS_MAIN_CURRENT_LIMIT_MIN                         "bus.main.I_limit.min_A"
#define PARAM_DEFAULT_BUS_MAIN_CURRENT_LIMIT_MIN                      (-75.00f)
#define PARAM_MAX_BUS_MAIN_CURRENT_LIMIT_MIN                          PARAM_MAX_BUS_MAIN_CURRENT_LIMIT_MAX
#define PARAM_MIN_BUS_MAIN_CURRENT_LIMIT_MIN                          PARAM_MAX_BUS_MAIN_CURRENT_LIMIT_MAX
#define PARAM_OFFSET_BUS_MAIN_CURRENT_LIMIT_MIN                       (PARAM_OFFSET_BUS_MAIN_CURRENT_LIMIT_MAX + PARAM_SIZE_BUS_MAIN_CURRENT_LIMIT_MAX)
#define PARAM_SIZE_BUS_MAIN_CURRENT_LIMIT_MIN                         PARAM_SIZE_BUS_MAIN_CURRENT_LIMIT_MAX
#define PARAM_VERSION_BUS_MAIN_CURRENT_LIMIT_MIN                      4
#define PARAM_HELP_BUS_MAIN_CURRENT_LIMIT_MIN                         "Current threshold that activates Under-Current warnings for primary/battery bus."

#define PARAM_NAME_BUS_MAIN_VOLTAGE_LIMIT_MAX                         "bus.main.V_limit.max_V"
#define PARAM_DEFAULT_BUS_MAIN_VOLTAGE_LIMIT_MAX                      26.0f
#define PARAM_MAX_BUS_MAIN_VOLTAGE_LIMIT_MAX                          26.00f
#define PARAM_MIN_BUS_MAIN_VOLTAGE_LIMIT_MAX                          0.00f
#define PARAM_OFFSET_BUS_MAIN_VOLTAGE_LIMIT_MAX                       (PARAM_OFFSET_BUS_MAIN_CURRENT_LIMIT_MIN + PARAM_SIZE_BUS_MAIN_CURRENT_LIMIT_MIN)
#define PARAM_SIZE_BUS_MAIN_VOLTAGE_LIMIT_MAX                         4
#define PARAM_VERSION_BUS_MAIN_VOLTAGE_LIMIT_MAX                      4
#define PARAM_HELP_BUS_MAIN_VOLTAGE_LIMIT_MAX                         "Voltage threshold that activates Over-Voltage warnings for primary/battery bus."

#define PARAM_NAME_BUS_MAIN_VOLTAGE_LIMIT_MIN                         "bus.main.V_limit.min_V"
#define PARAM_DEFAULT_BUS_MAIN_VOLTAGE_LIMIT_MIN                      0.00f
#define PARAM_MAX_BUS_MAIN_VOLTAGE_LIMIT_MIN                          26.00f
#define PARAM_MIN_BUS_MAIN_VOLTAGE_LIMIT_MIN                          0.00f
#define PARAM_OFFSET_BUS_MAIN_VOLTAGE_LIMIT_MIN                       (PARAM_OFFSET_BUS_MAIN_VOLTAGE_LIMIT_MAX + PARAM_SIZE_BUS_MAIN_VOLTAGE_LIMIT_MAX)
#define PARAM_SIZE_BUS_MAIN_VOLTAGE_LIMIT_MIN                         PARAM_SIZE_BUS_MAIN_VOLTAGE_LIMIT_MAX
#define PARAM_VERSION_BUS_MAIN_VOLTAGE_LIMIT_MIN                      4
#define PARAM_HELP_BUS_MAIN_VOLTAGE_LIMIT_MIN                         "Voltage threshold that activates Under-Voltage warnings for Main bus."

#define PARAM_NAME_BUS_MAIN_I_OFFSET                                  "bus.main.I_offset_ApV"
#define PARAM_DEFAULT_BUS_MAIN_I_OFFSET                               0.17f
#define PARAM_MAX_BUS_MAIN_I_OFFSET                                   1.0f
#define PARAM_MIN_BUS_MAIN_I_OFFSET                                   0.0f
#define PARAM_OFFSET_BUS_MAIN_I_OFFSET                                (PARAM_OFFSET_BUS_MAIN_VOLTAGE_LIMIT_MIN + PARAM_SIZE_BUS_MAIN_VOLTAGE_LIMIT_MIN)
#define PARAM_SIZE_BUS_MAIN_I_OFFSET                                  4
#define PARAM_VERSION_BUS_MAIN_I_OFFSET                               2
#define PARAM_HELP_BUS_MAIN_I_OFFSET                                  "Main bus current offset (due to filters) slope I_offset(V) = V *  bus.main.I_offset_ApV"

/**
 * Auxiliary Bus Settings (bus.aux.)
 */

// 5V BUS
#define PARAM_NAME_BUS_AUX_CIRCUIT_ID                                 "bus.aux.circuit_id"
#define PARAM_DEFAULT_BUS_AUX_CIRCUIT_ID                              1
#define PARAM_MAX_BUS_AUX_CIRCUIT_ID                                  PARAM_MAX_BUS_SYS_CIRCUIT_ID
#define PARAM_MIN_BUS_AUX_CIRCUIT_ID                                  PARAM_MIN_BUS_SYS_CIRCUIT_ID
#define PARAM_OFFSET_BUS_AUX_CIRCUIT_ID                               (PARAM_OFFSET_BUS_MAIN_CIRCUIT_ID + 4 * 16)
#define PARAM_SIZE_BUS_AUX_CIRCUIT_ID                                 PARAM_SIZE_BUS_SYS_CIRCUIT_ID
#define PARAM_VERSION_BUS_AUX_CIRCUIT_ID                              PARAM_VERSION_BUS_SYS_CIRCUIT_ID
#define PARAM_HELP_BUS_AUX_CIRCUIT_ID                                 "Circuit ID for the 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS             "bus.aux.circuit_status_msg_interval_ms"
#define PARAM_DEFAULT_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS          0
#define PARAM_MAX_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS              PARAM_MAX_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS
#define PARAM_MIN_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS              PARAM_MIN_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS
#define PARAM_OFFSET_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS           (PARAM_OFFSET_BUS_AUX_CIRCUIT_ID + PARAM_SIZE_BUS_AUX_CIRCUIT_ID)
#define PARAM_SIZE_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS             PARAM_SIZE_BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS
#define PARAM_VERSION_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS          1
#define PARAM_HELP_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS             "How often to send circuit status messages for the 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_R_SHUNT                                    "bus.aux.r_shunt_Ohm"
#define PARAM_DEFAULT_BUS_AUX_R_SHUNT                                 0.01f
#define PARAM_MAX_BUS_AUX_R_SHUNT                                     1.0f
#define PARAM_MIN_BUS_AUX_R_SHUNT                                     0.0000f
#define PARAM_OFFSET_BUS_AUX_R_SHUNT                                  (PARAM_OFFSET_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS + PARAM_SIZE_BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS)
#define PARAM_SIZE_BUS_AUX_R_SHUNT                                    4
#define PARAM_VERSION_BUS_AUX_R_SHUNT                                 1
#define PARAM_HELP_BUS_AUX_R_SHUNT                                    "Value of the shunt resistor for the 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_CURRENT_LIMIT_MAX                          "bus.aux.I_limit.max_A"
#define PARAM_DEFAULT_BUS_AUX_CURRENT_LIMIT_MAX                       2.00f
#define PARAM_MAX_BUS_AUX_CURRENT_LIMIT_MAX                           3.00f
#define PARAM_MIN_BUS_AUX_CURRENT_LIMIT_MAX                           0.00f
#define PARAM_OFFSET_BUS_AUX_CURRENT_LIMIT_MAX                        (PARAM_OFFSET_BUS_AUX_R_SHUNT + PARAM_SIZE_BUS_AUX_R_SHUNT)
#define PARAM_SIZE_BUS_AUX_CURRENT_LIMIT_MAX                          4
#define PARAM_VERSION_BUS_AUX_CURRENT_LIMIT_MAX                       1
#define PARAM_HELP_BUS_AUX_CURRENT_LIMIT_MAX                          "Current threshold that activates Over-Current warnings for 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_CURRENT_LIMIT_MIN                          "bus.aux.I_limit.min_A"
#define PARAM_DEFAULT_BUS_AUX_CURRENT_LIMIT_MIN                       0.00f
#define PARAM_MAX_BUS_AUX_CURRENT_LIMIT_MIN                           PARAM_MAX_BUS_AUX_CURRENT_LIMIT_MAX
#define PARAM_MIN_BUS_AUX_CURRENT_LIMIT_MIN                           PARAM_MIN_BUS_AUX_CURRENT_LIMIT_MAX
#define PARAM_OFFSET_BUS_AUX_CURRENT_LIMIT_MIN                        (PARAM_OFFSET_BUS_AUX_CURRENT_LIMIT_MAX + PARAM_SIZE_BUS_AUX_CURRENT_LIMIT_MAX)
#define PARAM_SIZE_BUS_AUX_CURRENT_LIMIT_MIN                          4
#define PARAM_VERSION_BUS_AUX_CURRENT_LIMIT_MIN                       3
#define PARAM_HELP_BUS_AUX_CURRENT_LIMIT_MIN                          "Current threshold that activates Over-Current warnings for 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_VOLTAGE_LIMIT_MAX                          "bus.aux.V_limit.max_V"
#define PARAM_DEFAULT_BUS_AUX_VOLTAGE_LIMIT_MAX                       5.50f
#define PARAM_MAX_BUS_AUX_VOLTAGE_LIMIT_MAX                           6.00f
#define PARAM_MIN_BUS_AUX_VOLTAGE_LIMIT_MAX                           10.00f
#define PARAM_OFFSET_BUS_AUX_VOLTAGE_LIMIT_MAX                        (PARAM_OFFSET_BUS_AUX_CIRCUIT_ID + 16)
#define PARAM_SIZE_BUS_AUX_VOLTAGE_LIMIT_MAX                          4
#define PARAM_VERSION_BUS_AUX_VOLTAGE_LIMIT_MAX                       1
#define PARAM_HELP_BUS_AUX_VOLTAGE_LIMIT_MAX                          "Voltage threshold that activates Over-Voltage warnings for 5V/CAN bus."

#define PARAM_NAME_BUS_AUX_VOLTAGE_LIMIT_MIN                          "bus.aux.V_limit.min_V"
#define PARAM_DEFAULT_BUS_AUX_VOLTAGE_LIMIT_MIN                       4.80f
#define PARAM_MAX_BUS_AUX_VOLTAGE_LIMIT_MIN                           6.00f
#define PARAM_MIN_BUS_AUX_VOLTAGE_LIMIT_MIN                           0.00f
#define PARAM_OFFSET_BUS_AUX_VOLTAGE_LIMIT_MIN                        (PARAM_OFFSET_BUS_AUX_VOLTAGE_LIMIT_MAX + PARAM_SIZE_BUS_AUX_VOLTAGE_LIMIT_MAX)
#define PARAM_SIZE_BUS_AUX_VOLTAGE_LIMIT_MIN                          PARAM_SIZE_BUS_AUX_VOLTAGE_LIMIT_MAX
#define PARAM_VERSION_BUS_AUX_VOLTAGE_LIMIT_MIN                       1
#define PARAM_HELP_BUS_AUX_VOLTAGE_LIMIT_MIN                          "Voltage threshold that activates Under-Voltage warnings for 5V/CAN bus."

#if defined(POWER_MONITOR)
#define PARAM_NAME_BUS_AUX_VOLTAGE_SCALE                              "bus.aux.V_scale"
#define PARAM_DEFAULT_BUS_AUX_VOLTAGE_SCALE                           0.002f
#define PARAM_MAX_BUS_AUX_VOLTAGE_SCALE                               1.00f
#define PARAM_MIN_BUS_AUX_VOLTAGE_SCALE                               0.00f
#define PARAM_OFFSET_BUS_AUX_VOLTAGE_SCALE                            (PARAM_OFFSET_BUS_AUX_VOLTAGE_LIMIT_MIN + PARAM_SIZE_BUS_AUX_VOLTAGE_LIMIT_MIN + 16)
#define PARAM_SIZE_BUS_AUX_VOLTAGE_SCALE                              4
#define PARAM_VERSION_BUS_AUX_VOLTAGE_SCALE                           1
#define PARAM_HELP_BUS_AUX_VOLTAGE_SCALE                              "Calibration parameter for the AUX bus voltage measurement"
#endif

/**
 * Battery
 */
#define PARAM_NAME_BATTERY_ENABLE_CURRENT_TRACK                        "battery.enable_current_track"
#define PARAM_DEFAULT_BATTERY_ENABLE_CURRENT_TRACK                     true
#define PARAM_OFFSET_BATTERY_ENABLE_CURRENT_TRACK                      (PARAM_OFFSET_BUS_AUX_CIRCUIT_ID + 4 * 16)
#define PARAM_SIZE_BATTERY_ENABLE_CURRENT_TRACK                        1
#define PARAM_VERSION_BATTERY_ENABLE_CURRENT_TRACK                     1
#define PARAM_HELP_BATTERY_ENABLE_CURRENT_TRACK                        "Use current integration to determine battery state of charge."

#define PARAM_NAME_BATTERY_STATUS_MSG_INTERVAL_MS                     "battery.status_msg_interval_ms"
#define PARAM_DEFAULT_BATTERY_STATUS_MSG_INTERVAL_MS                  500
#define PARAM_MAX_BATTERY_STATUS_MSG_INTERVAL_MS                      60000
#define PARAM_MIN_BATTERY_STATUS_MSG_INTERVAL_MS                      0
#define PARAM_OFFSET_BATTERY_STATUS_MSG_INTERVAL_MS                   (PARAM_OFFSET_BATTERY_ENABLE_CURRENT_TRACK + 8)
#define PARAM_SIZE_BATTERY_STATUS_MSG_INTERVAL_MS                     2
#define PARAM_VERSION_BATTERY_STATUS_MSG_INTERVAL_MS                  1
#define PARAM_HELP_BATTERY_STATUS_MSG_INTERVAL_MS                     "How often to send BatteryStatus messages."

#define PARAM_NAME_BATTERY_ID                                         "battery.slot_id"
#define PARAM_DEFAULT_BATTERY_ID                                      0
#define PARAM_MAX_BATTERY_ID                                          255
#define PARAM_MIN_BATTERY_ID                                          0
#define PARAM_OFFSET_BATTERY_ID                                       (PARAM_OFFSET_BATTERY_STATUS_MSG_INTERVAL_MS + PARAM_SIZE_BATTERY_STATUS_MSG_INTERVAL_MS)
#define PARAM_SIZE_BATTERY_ID                                         1
#define PARAM_VERSION_BATTERY_ID                                      1
#define PARAM_HELP_BATTERY_ID                                         "Battery within vehicle."

#define PARAM_NAME_BATTERY_INSTANCE_ID                                "battery.instance_id"
#define PARAM_DEFAULT_BATTERY_INSTANCE_ID                             0
#define PARAM_MAX_BATTERY_INSTANCE_ID                                 UINT32_MAX
#define PARAM_MIN_BATTERY_INSTANCE_ID                                 0
#define PARAM_OFFSET_BATTERY_INSTANCE_ID                              (PARAM_OFFSET_BATTERY_ID + PARAM_SIZE_BATTERY_ID + 1)
#define PARAM_SIZE_BATTERY_INSTANCE_ID                                4
#define PARAM_VERSION_BATTERY_INSTANCE_ID                             1
#define PARAM_HELP_BATTERY_INSTANCE_ID                                "Global battery identifier."

#define PARAM_NAME_BATTERY_CAPACITY                                   "battery.capacity_mAh"
#define PARAM_DEFAULT_BATTERY_CAPACITY                                5000
#define PARAM_MAX_BATTERY_CAPACITY                                    1000000
#define PARAM_MIN_BATTERY_CAPACITY                                    0
#define PARAM_OFFSET_BATTERY_CAPACITY                                 (PARAM_OFFSET_BATTERY_INSTANCE_ID + PARAM_SIZE_BATTERY_INSTANCE_ID)
#define PARAM_SIZE_BATTERY_CAPACITY                                   4
#define PARAM_VERSION_BATTERY_CAPACITY                                1
#define PARAM_HELP_BATTERY_CAPACITY                                   "Battery capacity used for current tracking."

#define PARAM_NAME_BATTERY_V_FULL                                     "battery.full_V"
#define PARAM_DEFAULT_BATTERY_V_FULL                                  25.00f
#define PARAM_MAX_BATTERY_V_FULL                                      26.50f
#define PARAM_MIN_BATTERY_V_FULL                                      0.0f
#define PARAM_OFFSET_BATTERY_V_FULL                                   (PARAM_OFFSET_BATTERY_CAPACITY + PARAM_SIZE_BATTERY_CAPACITY)
#define PARAM_SIZE_BATTERY_V_FULL                                     4
#define PARAM_VERSION_BATTERY_V_FULL                                  1
#define PARAM_HELP_BATTERY_V_FULL                                     "Voltage above which the pack is considered charged."

#define PARAM_NAME_BATTERY_V_EMPTY                                    "battery.empty_V"
#define PARAM_DEFAULT_BATTERY_V_EMPTY                                 23.50f
#define PARAM_MAX_BATTERY_V_EMPTY                                     PARAM_MAX_BATTERY_V_FULL
#define PARAM_MIN_BATTERY_V_EMPTY                                     PARAM_MIN_BATTERY_V_FULL
#define PARAM_OFFSET_BATTERY_V_EMPTY                                  (PARAM_OFFSET_BATTERY_V_FULL + PARAM_SIZE_BATTERY_V_FULL)
#define PARAM_SIZE_BATTERY_V_EMPTY                                    PARAM_SIZE_BATTERY_V_FULL
#define PARAM_VERSION_BATTERY_V_EMPTY                                 PARAM_VERSION_BATTERY_V_FULL
#define PARAM_HELP_BATTERY_V_EMPTY                                    "Voltage below which the pack is considered discharged."

#define PARAM_NAME_BATTERY_CURRENT_LIMIT_MAX                          "battery.I_limit.max_A"
#define PARAM_DEFAULT_BATTERY_CURRENT_LIMIT_MAX                       75.00f
#define PARAM_MAX_BATTERY_CURRENT_LIMIT_MAX                           200.00f
#define PARAM_MIN_BATTERY_CURRENT_LIMIT_MAX                           0.0f
#define PARAM_OFFSET_BATTERY_CURRENT_LIMIT_MAX                        (PARAM_OFFSET_BATTERY_V_EMPTY + PARAM_SIZE_BATTERY_V_EMPTY)
#define PARAM_SIZE_BATTERY_CURRENT_LIMIT_MAX                          4
#define PARAM_VERSION_BATTERY_CURRENT_LIMIT_MAX                       1
#define PARAM_HELP_BATTERY_CURRENT_LIMIT_MAX                          "Current threshold that activates Over-Current warnings for primary/battery bus."

// clang-format on

/**
 * Settings data structure
 */
typedef struct __attribute__((aligned(4))) {

  /**
   * CANbus parameters
   */
  can_params_t can;

  /**
   * System Stuff
   */

  // Enable firmware updates directly from firmware
  bool system_enable_firmware_updates;

  // Enable the I2C bus.
  bool system_enable_external_i2c;

  uint32_t system_emergency_msg_interval_ms;
  uint32_t system_node_status_msg_interval_ms;
  uint32_t system_sensor_readout_interval_ms;

  // Alert threshold hysteresis (default should be like 10 degrees)
  uint32_t over_temp_alert_threshold_hist;

  /**
   * Device Temperature
   */

  // Device ID For PCB and MCU sensors
  uint16_t mcu_temp_device_id;
  uint32_t mcu_temp_msg_interval_ms;
  float    mcu_temp_limit_min_C;
  float    mcu_temp_limit_max_C;

  uint16_t pcb_temp_device_id;
  uint32_t pcb_temp_msg_interval_ms;
  float    pcb_temp_limit_min_C;
  float    pcb_temp_limit_max_C;

  /**
   * LED
   */
  // Enable status LED [true]
  bool led_enabled;

  // Enable the identify function
  bool led_enable_identify;

  // Brightness calibration for each LED
  uint8_t led_brightness_scale[3];

  // The mode of the RGB LED (UNUSED)
  // ? - Map to Battery voltage
  // ? - Map to current/power draw
  // ? - Map to internal / external temp
  // ? - Disabled
  // ? - Manual / External control
  uint32_t led_rgb_mode;

  // How long the LED is activate when displaying status (default is 800ms)
  uint32_t led_status_on_time_ms;

  /**
   * System Bus
   */

  uint16_t bus_sys_circuit_id;
  uint32_t bus_sys_circuit_status_msg_interval_ms;
  float    bus_sys_voltage_limit_max;
  float    bus_sys_voltage_limit_min;

  /**
   * Auxiliary (i.e CAN)
   */

  uint16_t bus_aux_circuit_id;
  uint32_t bus_aux_circuit_status_msg_interval_ms;

#if defined(POWER_MODULE)
  float bus_aux_R_shunt_Ohm;
  float bus_aux_current_limit_min;
  float bus_aux_current_limit_max;
#elif defined(POWER_MONITOR)
  float bus_aux_voltage_scale;
#endif

  float bus_aux_voltage_limit_max;
  float bus_aux_voltage_limit_min;

  /**
   * Main Bus
   */

  bool bus_main_enable_current_filter;

  uint16_t bus_main_circuit_id;
  uint32_t bus_main_circuit_status_msg_interval_ms;

  // Shunt value
  float bus_main_R_shunt_Ohm;

  // Current offset I(V)
  float bus_main_I_offset_ApV;

  // Current and voltage limits for both buses
  float bus_main_current_limit_min;
  float bus_main_current_limit_max;
  float bus_main_voltage_limit_max;
  float bus_main_voltage_limit_min;

  // Filter settings
  float bus_main_filter_coefficient;

  // Battery on vehicle id
  uint32_t battery_id;

  // Battery unit id (from same models)
  uint32_t battery_instance_id;

  // Battery capacity in mAh
  uint32_t battery_capacity_mAh;

  // Max current for this specific battery
  float battery_current_limit_max;

  // Battery voltages
  float battery_v_full;
  float battery_v_empty;

  // Track capacity through current integration [true]
  bool battery_enable_current_integration;
  uint32_t battery_status_msg_interval_ms;

} parameters_t;

typedef enum led_state_t {
  LED_STATE_BATTERY_LEVEL = 1,
  LED_STATE_AUTO_BAUD     = 2,
  LED_STATE_FAULT         = 3,
} led_state_t;

#define LED_MAX_STATES 4


/**
 * @brief Device status block
 */
typedef struct __attribute__((aligned(4))) {

  /**
   * @brief if i2c devices are ok
   */
  uint8_t i2c_device_error;

#if defined(POWER_MODULE)
  struct {
    float    shunt_scale;
    uint32_t last_readout_bat_tick;

    // Previous filtered value used for IIR
    int32_t V_shunt_filtered_prev;
    float   filter_scale;

    // Units of 10uV
    rolling_buffer_t V_shunt;
    rolling_buffer_t V_bus;
  } aux_bus;
#endif

  struct {
    int32_t charge_consumed_mWs;
    float inverse_capacity_100overWh;
    bool integrator_running;
  } battery;

  struct {
    float shunt_scale_1over100kOhm;
    float consumed_mWms;

    // Previous filtered value used for IIR
    int32_t V_shunt_filtered_prev;
    float   filter_scale;

    rolling_buffer_t V_shunt_10uV;
    rolling_buffer_t V_bus_4mV;

    float V_shunt_offset_10uV_p_4mV;
  } main_bus;

  struct {

    // LED
    led_state_t states[LED_MAX_STATES];

    uint8_t  state_index;
    uint8_t  sub_state;
    uint8_t  state_count;
    uint32_t mode_switch_time;

    // LED turn off time for R, G, B
    uint32_t timestamp_off_time[3];

  } led;

} device_status_t;
