/**
 *
 * Author: Stou Sandalski <stou@p-systems.io>
 * Copyright (c) 2021 - Pomegranate Systems LLC
 *
 * Portions of this code are:
 *
 * Copyright (c) 2019 STMicroelectronics
 * Copyright (c) 2017 UAVCAN Team
 *
 * License: MIT and 3-Clause BSD (See LICENSE.md)
 *
 * Description:
 *
 *
 */

#ifndef P_SYSTEMS_POWER_MODULE_POWER_MODULE_H
#define P_SYSTEMS_POWER_MODULE_POWER_MODULE_H

#include <stdbool.h>
#include <stdint.h>

#include <stm32_util.h>

// Application constants
#define APP_NAME                                                      "io.p-systems.power"
#define DEVICE_TYPE                                                   8

/**
 * Feature Flags
 */

// Experimental

/**
 * Constants
 */

// I2C Addresses
#define INA_CAN_ADDR_7B                                               ((INA219_BASE_ADDRESS | 0x0U) << 1)
#define INA_MAIN_ADDR_7B                                              ((INA219_BASE_ADDRESS | 0xAU) << 1)
#define TEMP_PCB_ADDR_7B                                              (MCP9808_BASE_ADDRESS << 1)

#define EEPROM_ADDR_7B                                                (0x50 << 1)
#define EEPROM_PAGE_SIZE                                              16

#define EEPROM_PARAM_HEADER_START_PAGE                                20
#define EEPROM_PARAM_START_PAGE                                       21

/**
 * Hardware Configuration (Pins, etc.)
 */

// LED
#define LED_GPIO_Port                                                 GPIOA
#define LED_RGB_R_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_R_Pin                                                 GPIO_PIN_0
#define LED_RGB_B_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_B_Pin                                                 GPIO_PIN_1
#define LED_RGB_G_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_G_Pin                                                 GPIO_PIN_2

// Test Points
#define TP_GPIO_1_GPIO_Port                                           GPIOA
#define TP_GPIO_1_Pin                                                 GPIO_PIN_3
#define TP_GPIO_2_GPIO_Port                                           GPIOA
#define TP_GPIO_2_Pin                                                 GPIO_PIN_4
#define EEPROM_WP_GPIO_Port                                           GPIOA
#define EEPROM_WP_Pin                                                 GPIO_PIN_6

// CAN
#define CAN_RX_GPIO_Port                                              GPIOA
#define CAN_RX_Pin                                                    GPIO_PIN_11
#define CAN_RX_AF_MODE                                                GPIO_AF9_CAN

#define CAN_TX_GPIO_Port                                              GPIOA
#define CAN_TX_Pin                                                    GPIO_PIN_12
#define CAN_TX_AF_MODE                                                GPIO_AF9_CAN

#define CAN_FAULT_GPIO_Port                                           GPIOA
#define CAN_FAULT_Pin                                                 GPIO_PIN_8
#define CAN_SILENT_GPIO_Port                                          GPIOA
#define CAN_SILENT_Pin                                                GPIO_PIN_15
#define CAN_TERM_GPIO_Port                                            GPIOB
#define CAN_TERM_Pin                                                  GPIO_PIN_4

// I2C
#define I2C_INTERNAL                                                  I2C2
#define I2C_INTERNAL_CLOCK_PERIPH                                     LL_APB1_GRP1_PERIPH_I2C2
#define I2C_INTERNAL_SCL_GPIO_Port                                    GPIOA
#define I2C_INTERNAL_SCL_Pin                                          GPIO_PIN_9
#define I2C_INTERNAL_SCL_AF_MODE                                      GPIO_AF4_I2C2
#define I2C_INTERNAL_SCL_SET_AFPIN                                    LL_GPIO_SetAFPin_8_15

#define I2C_INTERNAL_SDA_GPIO_Port                                    GPIOA
#define I2C_INTERNAL_SDA_Pin                                          GPIO_PIN_10
#define I2C_INTERNAL_SDA_AF_MODE                                      I2C_INTERNAL_SCL_AF_MODE
#define I2C_INTERNAL_SDA_SET_AFPIN                                    I2C_INTERNAL_SCL_SET_AFPIN
#define I2C_INTERNAL_EV_IRQ                                           I2C2_EV_IRQn
#define I2C_INTERNAL_ER_IRQ                                           I2C2_ER_IRQn

#define I2C_EXTERNAL                                                  I2C1
#define I2C_EXTERNAL_CLOCK_PERIPH                                     LL_APB1_GRP1_PERIPH_I2C1
#define I2C_EXTERNAL_SCL_GPIO_Port                                    GPIOB
#define I2C_EXTERNAL_SCL_Pin                                          GPIO_PIN_6
#define I2C_EXTERNAL_SDA_GPIO_Port                                    GPIOB
#define I2C_EXTERNAL_SDA_Pin                                          GPIO_PIN_7
#define I2C_EXTERNAL_AF_MODE                                          GPIO_AF4_I2C1
#define I2C_EXTERNAL_EV_IRQ                                           I2C1_EV_IRQn
#define I2C_EXTERNAL_ER_IRQ                                           I2C1_ER_IRQn

/**
 * Other Hardware Configuration
 */

// DMA - ADC buffer size
#define ADC_VALUES                                                    2
#define ADC_AVERAGE_COUNT                                             10

// Timer Configuration for the RGB LED
#define LED_TIM_PRESCALAR                                             7
#define LED_TIM_PERIOD                                                499

// LED mode timing
#define LED_AUTO_BAUD_ON_MS                                           200
#define LED_AUTO_BAUD_OFF_MS                                          500
#define LED_FAULT_ON_MS                                               150
#define LED_FAULT_OFF_MS                                              150
#define LED_IDENTIFY_ON_MS                                            500
#define LED_IDENTIFY_OFF_MS                                           50
#define LED_BATTERY_LEVEL_OFF_MS                                      50
#define LED_RESET_DEFAULT_ON_MS                                       2000


/**
 *  System Calibration data written by Bootloader (only!!)
 */
#define CAL_NAME_R_SHUNT_MAIN                                         "cal.R_shunt_main_ohm"
#define CAL_DEFAULT_R_SHUNT_MAIN                                      0.0005f
#define CAL_MAX_R_SHUNT_MAIN                                          0.1f
#define CAL_MIN_R_SHUNT_MAIN                                          0.0f
#define CAL_OFFSET_R_SHUNT_MAIN                                       16
#define CAL_SIZE_R_SHUNT_MAIN                                         4
#define CAL_VERSION_R_SHUNT_MAIN                                      1
#define CAL_HELP_R_SHUNT_MAIN                                         ""

#define CAL_NAME_R_SHUNT_AUX                                          "cal.R_shunt_aux_ohm"
#define CAL_DEFAULT_R_SHUNT_AUX                                       0.01f
#define CAL_MAX_R_SHUNT_AUX                                           1.0f
#define CAL_MIN_R_SHUNT_AUX                                           0.0f
#define CAL_OFFSET_R_SHUNT_AUX                                        (CAL_OFFSET_R_SHUNT_MAIN + CAL_SIZE_R_SHUNT_MAIN)
#define CAL_SIZE_R_SHUNT_AUX                                          CAL_SIZE_R_SHUNT_MAIN
#define CAL_VERSION_R_SHUNT_AUX                                       CAL_VERSION_R_SHUNT_MAIN
#define CAL_HELP_R_SHUNT_AUX                                          ""

// Calibration values for the RGB LED so they have all the same intensity
#define CAL_NAME_LED_BRIGHTNESS_SCALE_R                               "cal.led.r_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_R                            255
#define CAL_MAX_LED_BRIGHTNESS_SCALE_R                                255
#define CAL_MIN_LED_BRIGHTNESS_SCALE_R                                0
#define CAL_OFFSET_LED_BRIGHTNESS_SCALE_R                             (CAL_OFFSET_R_SHUNT_AUX + CAL_SIZE_R_SHUNT_AUX + 8)
#define CAL_SIZE_LED_BRIGHTNESS_SCALE_R                               1
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_R                            1
#define CAL_HELP_LED_BRIGHTNESS_SCALE_R                               ""

#define CAL_NAME_LED_BRIGHTNESS_SCALE_G                               "cal.led.g_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_G                            CAL_DEFAULT_LED_BRIGHTNESS_SCALE_R
#define CAL_MAX_LED_BRIGHTNESS_SCALE_G                                CAL_MAX_LED_BRIGHTNESS_SCALE_R
#define CAL_MIN_LED_BRIGHTNESS_SCALE_G                                CAL_MIN_LED_BRIGHTNESS_SCALE_R
#define CAL_OFFSET_LED_BRIGHTNESS_SCALE_G                             (CAL_OFFSET_LED_BRIGHTNESS_SCALE_R + CAL_SIZE_LED_BRIGHTNESS_SCALE_R)
#define CAL_SIZE_LED_BRIGHTNESS_SCALE_G                               CAL_SIZE_LED_BRIGHTNESS_SCALE_R
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_G                            CAL_VERSION_LED_BRIGHTNESS_SCALE_R
#define CAL_HELP_LED_BRIGHTNESS_SCALE_G                               ""

#define CAL_NAME_LED_BRIGHTNESS_SCALE_B                               "cal.led.b_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_B                            CAL_DEFAULT_LED_BRIGHTNESS_SCALE_R
#define CAL_MAX_LED_BRIGHTNESS_SCALE_B                                CAL_MAX_LED_BRIGHTNESS_SCALE_R
#define CAL_MIN_LED_BRIGHTNESS_SCALE_B                                CAL_MIN_LED_BRIGHTNESS_SCALE_R
#define CAL_OFFSET_LED_BRIGHTNESS_SCALE_B                             (CAL_OFFSET_LED_BRIGHTNESS_SCALE_G + CAL_SIZE_LED_BRIGHTNESS_SCALE_G)
#define CAL_SIZE_LED_BRIGHTNESS_SCALE_B                               CAL_SIZE_LED_BRIGHTNESS_SCALE_R
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_B                            CAL_VERSION_LED_BRIGHTNESS_SCALE_R
#define CAL_HELP_LED_BRIGHTNESS_SCALE_B                               ""


/**
 * Requirements
 *
 * - Size of these must be identical to the ones in
 *
 */
typedef struct __attribute__ ((aligned(4))) {

  // The general factory info stuff
  factory_info_header_t header;

  float R_shunt_aux;
  float R_shunt_main;

  uint8_t led_brightness[3];

} factory_info_t;

/**
 * Subroutines
 */
void Battery_Capacity_Calc();

void Run_Event_Loop();

void Update_Globals();

#endif // P_SYSTEMS_POWER_MODULE_POWER_MODULE_H
