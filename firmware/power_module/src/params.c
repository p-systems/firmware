/**
 * @file
 * @brief DroneCAN firmware parameters setup
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#include "params.h"

#include <stddef.h>
#include <stdint.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "core.h"
#include "main.h"

extern factory_info_t  factory_info;
extern parameters_t    parameters;
extern device_status_t status;

static const parameter_info_t parameter_info_table[] = {

    ADD_PARAM_INT(BATTERY_ID, parameters_t, battery_id, 0),
    ADD_PARAM_INT(BATTERY_INSTANCE_ID, parameters_t, battery_instance_id, 0),
    ADD_PARAM_INT(BATTERY_CAPACITY, parameters_t, battery_capacity_mAh, 0),
    ADD_PARAM_REAL(BATTERY_V_EMPTY, parameters_t, battery_v_empty, 0),
    ADD_PARAM_REAL(BATTERY_V_FULL, parameters_t, battery_v_full, 0),
    ADD_PARAM_REAL(BATTERY_CURRENT_LIMIT_MAX, parameters_t, battery_current_limit_max, 0),
    ADD_PARAM_INT(BATTERY_STATUS_MSG_INTERVAL_MS, parameters_t, battery_status_msg_interval_ms, 0),
    ADD_PARAM_BOOL(BATTERY_ENABLE_CURRENT_TRACK, parameters_t, battery_enable_current_integration, 0),

    // can
    ADD_PARAM_BOOL(CAN_ENABLE_TERM, parameters_t, can.bus_termination_enable, 0),
    ADD_PARAM_INT(CAN_DEFAULT_BAUD_KBPS, parameters_t, can.default_baud_kbps, 0),
    ADD_PARAM_BOOL(CAN_ENABLE_AUTO_BAUD, parameters_t, can.auto_baud_enable, 0),
    ADD_PARAM_INT(CAN_AUTO_BAUD_RETRY_TIMEOUT_MS, parameters_t, can.auto_baud_retry_time_ms, 0),
    ADD_PARAM_INT(CAN_AUTO_BAUD_MAX_RETRIES, parameters_t, can.auto_baud_max_retries, 0),

    ADD_PARAM_INT(UAVCAN_DEFAULT_NODE_ID, parameters_t, can.default_node_id, 0),
    ADD_PARAM_BOOL(UAVCAN_DYNAMIC_ID_ALLOCATION_ENABLE, parameters_t, can.dynamic_id_allocation_enable, 0),
    ADD_PARAM_INT(UAVCAN_DYNAMIC_ID_ALLOCATION_RETRY_TIMEOUT_MS, parameters_t, can.dynamic_id_allocation_retry_time_ms, 0),
    ADD_PARAM_INT(UAVCAN_DYNAMIC_ID_ALLOCATION_MAX_RETRIES, parameters_t, can.dynamic_id_allocation_max_retries, 0),

    // bus.main
    ADD_PARAM_INT(BUS_MAIN_CIRCUIT_ID, parameters_t, bus_main_circuit_id, 0),
    ADD_PARAM_INT(BUS_MAIN_CIRCUIT_STATUS_MSG_INTERVAL_MS, parameters_t, bus_main_circuit_status_msg_interval_ms, 0),
    ADD_PARAM_BOOL(BUS_MAIN_CURRENT_FILTER_ENABLE, parameters_t, bus_main_enable_current_filter, 0),
    ADD_PARAM_REAL(BUS_MAIN_CURRENT_FILTER_CONST, parameters_t, bus_main_filter_coefficient, 0),
    ADD_PARAM_REAL(BUS_MAIN_R_SHUNT, parameters_t, bus_main_R_shunt_Ohm, offsetof(factory_info_t, bus_main_R_shunt)),
#if defined(POWER_MONITOR) || (defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 3)
    ADD_PARAM_REAL(BUS_MAIN_I_OFFSET, parameters_t, bus_main_I_offset_ApV, offsetof(factory_info_t, bus_main_I_offset_ApV)),
#endif
    ADD_PARAM_REAL(BUS_MAIN_CURRENT_LIMIT_MIN, parameters_t, bus_main_current_limit_min, 0),
    ADD_PARAM_REAL(BUS_MAIN_CURRENT_LIMIT_MAX, parameters_t, bus_main_current_limit_max, 0),
    ADD_PARAM_REAL(BUS_MAIN_VOLTAGE_LIMIT_MIN, parameters_t, bus_main_voltage_limit_min, 0),
    ADD_PARAM_REAL(BUS_MAIN_VOLTAGE_LIMIT_MAX, parameters_t, bus_main_voltage_limit_max, 0),

    // bus.aux
    ADD_PARAM_INT(BUS_AUX_CIRCUIT_ID, parameters_t, bus_aux_circuit_id, 0),
    ADD_PARAM_INT(BUS_AUX_CIRCUIT_STATUS_MSG_INTERVAL_MS, parameters_t, bus_aux_circuit_status_msg_interval_ms, 0),
#if defined(POWER_MODULE)
    ADD_PARAM_REAL(BUS_AUX_R_SHUNT, parameters_t, bus_aux_R_shunt_Ohm, offsetof(factory_info_t, bus_aux_R_shunt)),
    ADD_PARAM_REAL(BUS_AUX_CURRENT_LIMIT_MIN, parameters_t, bus_aux_current_limit_min, 0),
    ADD_PARAM_REAL(BUS_AUX_CURRENT_LIMIT_MAX, parameters_t, bus_aux_current_limit_max, 0),
#elif defined(POWER_MONITOR)
    ADD_PARAM_REAL(BUS_AUX_VOLTAGE_SCALE, parameters_t, bus_aux_voltage_scale, offsetof(factory_info_t, bus_aux_V_scale)),
#endif
    ADD_PARAM_REAL(BUS_AUX_VOLTAGE_LIMIT_MIN, parameters_t, bus_aux_voltage_limit_min, 0),
    ADD_PARAM_REAL(BUS_AUX_VOLTAGE_LIMIT_MAX, parameters_t, bus_aux_voltage_limit_max, 0),

    // bus.sys
    ADD_PARAM_INT(BUS_SYS_CIRCUIT_ID, parameters_t, bus_sys_circuit_id, 0),
    ADD_PARAM_INT(BUS_SYS_CIRCUIT_STATUS_MSG_INTERVAL_MS, parameters_t, bus_sys_circuit_status_msg_interval_ms, 0),

    // mcu.temp
    ADD_PARAM_INT(MCU_TEMP_DEVICE_ID, parameters_t, mcu_temp_device_id, 0),
    ADD_PARAM_INT(MCU_TEMP_MSG_INTERVAL_MS, parameters_t, mcu_temp_msg_interval_ms, 0),
    ADD_PARAM_REAL(MCU_TEMP_LIMIT_MIN_C, parameters_t, mcu_temp_limit_min_C, 0),
    ADD_PARAM_REAL(MCU_TEMP_LIMIT_MAX_C, parameters_t, mcu_temp_limit_max_C, 0),

    ADD_PARAM_INT(PCB_TEMP_DEVICE_ID, parameters_t, pcb_temp_device_id, 0),
    ADD_PARAM_INT(PCB_TEMP_MSG_INTERVAL_MS, parameters_t, pcb_temp_msg_interval_ms, 0),
    ADD_PARAM_REAL(PCB_TEMP_LIMIT_MIN_C, parameters_t, pcb_temp_limit_min_C, 0),
    ADD_PARAM_REAL(PCB_TEMP_LIMIT_MAX_C, parameters_t, pcb_temp_limit_max_C, 0),

    ADD_PARAM_BOOL(LED_ENABLED, parameters_t, led_enabled, 0),
    ADD_PARAM_BOOL(LED_IDENTIFY, parameters_t, led_enable_identify, 0),

    ADD_PARAM_INT(LED_BRIGHTNESS_SCALE_R, parameters_t, led_brightness_scale[0], offsetof(factory_info_t, led_brightness[0])),
    ADD_PARAM_INT(LED_BRIGHTNESS_SCALE_G, parameters_t, led_brightness_scale[1], offsetof(factory_info_t, led_brightness[1])),
    ADD_PARAM_INT(LED_BRIGHTNESS_SCALE_B, parameters_t, led_brightness_scale[2], offsetof(factory_info_t, led_brightness[2])),
#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
    ADD_PARAM_BOOL(SYSTEM_ENABLE_EXTERNAL_I2C, parameters_t, system_enable_external_i2c, 0),
#endif
    ADD_PARAM_BOOL(SYSTEM_ENABLE_FIRMWARE_UPDATES, parameters_t, system_enable_firmware_updates, 0),
    ADD_PARAM_INT(SYSTEM_EMERGENCY_MSG_INTERVAL_MS, parameters_t, system_emergency_msg_interval_ms, 0),
    ADD_PARAM_INT(SYSTEM_NODE_STATUS_MSG_INTERVAL_MS, parameters_t, system_node_status_msg_interval_ms, 0),
    ADD_PARAM_INT(SYSTEM_SENSOR_READOUT_INTERVAL_MS, parameters_t, system_sensor_readout_interval_ms, 0)};

bool dirty_table[sizeof(parameter_info_table) / sizeof(parameter_info_t)];

/**
 * Parameter function
 */
bool Params_Call_Handle_GetSet(int                 node_id,
                               parameter_getset_t *param_get_set,
                               request_t           request) {

  return Params_Handle_GetSet(node_id,
                              sizeof(parameter_info_table) / sizeof(parameter_info_t),
                              parameter_info_table,
                              (uint8_t *) &factory_info,
                              dirty_table,
                              param_get_set,
                              request,
                              (uint8_t *) &parameters);
}

void Params_Load_Default_Values() {

  Params_Load_Defaults(sizeof(parameter_info_table) / sizeof(parameter_info_t),
                       parameter_info_table,
                       (uint8_t *) &factory_info,
                       dirty_table,
                       (uint8_t *) &parameters);

  // Set the LED to pure blue for some time to indicate a settings reset
  LED_Set_Values(0, 0, 255, LED_RESET_DEFAULT_ON_MS);
}

/**
 * Load parameters from the EEPROM
 *
 * return true if parameters are loaded from eeprom and false if defaults were used
 */
bool Params_Load() {

  uint8_t result = Params_EEPROM_Load(I2C_INTERNAL,
                                      EEPROM_ADDRESS_7B,
                                      EEPROM_PARAM_HEADER_START_PAGE,
                                      EEPROM_PARAM_START_PAGE,
                                      PARAMETERS_CURRENT_VERSION,
                                      sizeof(parameter_info_table) / sizeof(parameter_info_t),
                                      parameter_info_table,
                                      (uint8_t *) &factory_info,
                                      dirty_table,
                                      (uint8_t *) &parameters);

  if (result == EEPROM_LOAD_STATE_DEVICE_ERROR) {
    status.i2c_device_error |= I2C_BUS_ERROR_EEPROM;
    Params_Load_Default_Values();
  } else if ((result == EEPROM_LOAD_STATE_SOME_PARAMS_UPGRADED)
             || (result == EEPROM_LOAD_STATE_ALL_DEFAULTS_LOADED)) {
    // parameters changed during loading so save them
    Params_Save();
  }

  return (result == EEPROM_LOAD_STATE_ALL_OK);
}

bool Params_Save() {
  int parameter_count = sizeof(parameter_info_table) / sizeof(parameter_info_t);

#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
  // Disable Write Protection
  LL_GPIO_ResetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
  uint8_t result = Params_EEPROM_Save(I2C_INTERNAL,
                                      EEPROM_ADDRESS_7B,
                                      EEPROM_PARAM_HEADER_START_PAGE,
                                      EEPROM_PARAM_START_PAGE,
                                      PARAMETERS_CURRENT_VERSION,
                                      parameter_count,
                                      parameter_info_table,
                                      dirty_table,
                                      (uint8_t *) &parameters);

  if (result == EEPROM_LOAD_STATE_DEVICE_ERROR) {
    status.i2c_device_error |= I2C_BUS_ERROR_EEPROM;
  }

#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
  // Enable Write Protection
  LL_GPIO_SetOutputPin(EEPROM_WP_GPIO_Port, EEPROM_WP_Pin);
#endif
  return (result == EEPROM_LOAD_STATE_ALL_OK);
}
