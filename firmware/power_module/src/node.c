/**
 * @file
 * @brief Handle "network traffic" from DroneCAN
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#include "node.h"

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "stm32_util.h"
#include "uavcan_v0.h"

#include "core.h"
#include "main.h"
#include "params.h"

/**
 * Globals
 */

extern factory_info_t factory_info;
extern boot_info_t    boot_info;
extern node_info_t    node;
extern parameters_t   parameters;

/**
 * CANbus Filters
 */
uint32_t can_filters[][2] = {
    {UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_ID, UAVCAN_PROTOCOL_GET_NODE_INFO_FILTER_MASK},
    {UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_ID, UAVCAN_PROTOCOL_GET_TRANSPORT_STATS_FILTER_MASK},
    {UAVCAN_PROTOCOL_PARAM_FILTER_ID, UAVCAN_PROTOCOL_PARAM_FILTER_MASK},
    {UAVCAN_PROTOCOL_RESTART_NODE_FILTER_ID, UAVCAN_PROTOCOL_RESTART_NODE_FILTER_MASK},
    {UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_ID, UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_FILTER_MASK},
#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
    {IO_P_SYSTEMS_DEVICE_I2C_FILTER_ID, IO_P_SYSTEMS_DEVICE_I2C_FILTER_MASK},
#endif
};

/**
 * Request Handling
 */

request_queue_t queue;

// Parameter handling
parameter_getset_t param_get_set;

// I2C
uint8_t i2c_buffer[IO_P_SYSTEMS_DEVICE_I2C_READ_MAX_SIZE];
uint8_t i2c_buffer_len;

uavcan_init_t uavcan_init;

/**
 * Initialize the can interface
 *
 * @return
 */
bool CANbus_Init() {

  uavcan_init.can_if = uavcan_init.can_filter_if = CAN;
  uavcan_init.can_filters                        = &can_filters[0][0];
  uavcan_init.filter_count                       = (sizeof(can_filters) / (2 * sizeof(uint32_t)));

  UAVCAN_Init(&boot_info, &node, &parameters.can, &uavcan_init);

  // Check if we left the auto_baud state
  if (node.can_status != CAN_STATUS_AUTO_BAUD) {
    LED_Remove_State(LED_STATE_AUTO_BAUD);
  }

  return true;
}

void Handle_Param_GetSet_Special(const char *name) {
  // Perform any extra init work here
  if (strcmp(name, PARAM_NAME_CAN_ENABLE_TERM) == 0) {
    CAN_Set_Termination();
  } else if ((strcmp(name, PARAM_NAME_BUS_MAIN_R_SHUNT) == 0)
             || (strcmp(name, PARAM_NAME_BATTERY_CAPACITY) == 0)
             || (strcmp(name, PARAM_NAME_BUS_MAIN_CURRENT_FILTER_CONST) == 0)) {
    Battery_Capacity_Calc();

  } else if ((strcmp(name, PARAM_NAME_LED_ENABLED) == 0)) {
    if (parameters.led_enabled) {
      LED_Init();
    } else {
      LED_DeInit();
    }
#if defined(POWER_MODULE)
  } else if (strcmp(name, PARAM_NAME_SYSTEM_ENABLE_EXTERNAL_I2C) == 0) {
    if (parameters.system_enable_external_i2c) {
      I2C_External_Init();
    } else {
      I2C_External_DeInit();
    }
#endif
  } else {
    // Otherwise update the globals
    Update_Globals();
  }
}

void Service_Request_Begin_Firmware_Update(request_t *request) {

  // uavcan.protocol.file.BeginFirmwareUpdate
  if (parameters.system_enable_firmware_updates) {

    // Bootloader handles response message
    boot_info.signature              = BOOT_INFO_SIGNATURE;
    boot_info.enable_term            = parameters.can.bus_termination_enable;
    boot_info.node_id                = node.id;
    boot_info.update_req_node_id     = request->node_id;
    boot_info.update_req_priority    = request->priority;
    boot_info.update_req_transfer_id = request->transfer_id;
    boot_info.crc                    = 0;
    boot_info.crc                    = CRC_16_Compute(0xFFFFU, &boot_info, sizeof(boot_info_t));

    // De-Init Hardware
    CAN_DeInit();

    I2C_Internal_DeInit();

#if defined(POWER_MODULE)
    if (parameters.system_enable_external_i2c) {
      I2C_External_DeInit();
    }
#endif

    TIM_DeInit();

    ADC_DeInit();

    GPIO_DeInit();

    SystemClock_DeInit();

#if defined(POWER_MODULE)
    // If the magic is not present we are on a 1.x bootloader
    if (factory_info.header.magic == FACTORY_INFO_MAGIC) {
      __disable_irq();
    }
#endif

    LL_FLASH_DisablePrefetch();

    // Jump to bootloader
    Bootloader_Jump(FLASH_BASE);
  } else {
    Send_BeginFirmwareUpdate_Response(node.id,
                                      *request,
                                      UAVCAN_PROTOCOL_FILE_BEGIN_FIRMWARE_UPDATE_ERROR_INVALID_MODE,
                                      "Only the bootloader can update firmware.");
  }
}

/**
 * Called from the main thread to respond to various messages
 */
void Service_Requests() {
#if defined(POWER_MODULE)
  const uint32_t i2c_timeout_ms = 100;
#endif

  // If there are no requests pending, return
  if (!queue.request[queue.top].pending) {
    return;
  }

  // Dispatch custom messages
  switch (queue.request[queue.top].type_id) {
#if defined(POWER_MODULE)

    case IO_P_SYSTEMS_DEVICE_I2C_READ_ID:
      // io.p-systems.i2c.Read
      Handle_I2C_Read_Request(node.id,
                              queue.request[queue.top],
                              I2C_EXTERNAL,
                              i2c_buffer[2],
                              i2c_buffer[3],
                              i2c_buffer[1],
                              i2c_timeout_ms);
      break;

    case IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID:
      // io.p-systems.i2c.Write
      Handle_I2C_Write_Request(node.id,
                               queue.request[queue.top],
                               I2C_EXTERNAL,
                               i2c_buffer[1],
                               i2c_buffer[2],
                               &i2c_buffer[3],
                               i2c_buffer_len - 3,
                               i2c_timeout_ms);
      break;
#endif
    default:
      // Dispatch all the rest
      Service_UAVCAN_Requests(&node,
                              &uavcan_init,
                              &queue,
                              &boot_info,
                              parameters.system_enable_firmware_updates,
                              &parameters.can);
      return;
  }
}

/**
 *
 *
 * @param data_type_id
 * @param is_service
 * @param source_node_id
 * @param priority
 * @param transfer_id
 * @param payload
 * @param payload_len
 */
void UAVCAN_Handle_Message(const uint16_t data_type_id,
                           const bool     is_service,
                           const uint8_t  source_node_id,
                           const uint8_t  priority,
                           uint8_t        transfer_id,
                           uint8_t       *payload,
                           uint32_t       payload_len) {

  // Dispatch data
  switch (data_type_id) {
#if defined(POWER_MODULE)
    case IO_P_SYSTEMS_DEVICE_I2C_WRITE_ID:
      // io.psystems.i2c.Write

    case IO_P_SYSTEMS_DEVICE_I2C_READ_ID:
      // io.psystems.i2c.Read/.Write

      // Save the payload so it can be written
      memcpy(i2c_buffer, payload, MIN(IO_P_SYSTEMS_DEVICE_I2C_READ_MAX_SIZE, payload_len));
      i2c_buffer_len = (uint8_t) payload_len;

      if (parameters.system_enable_external_i2c) {
        Request_Queue_Add_Unique(&queue,
                                 data_type_id,
                                 source_node_id,
                                 transfer_id,
                                 priority,
                                 0);
      }
      break;
#endif
    default:
      Process_UAVCAN_Message(data_type_id,
                             is_service,
                             source_node_id,
                             priority,
                             transfer_id,
                             payload,
                             payload_len,
                             &node,
                             &uavcan_init,
                             &queue,
                             &boot_info);
      break;
  }
}
