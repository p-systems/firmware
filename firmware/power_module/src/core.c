/**
 * @file
 * @brief Contains main event loop, sensor readout, and calculations
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#include "core.h"

#include <math.h>

#include "ina219.h"
#include "mcp9808.h"
#include "stm32_util.h"
#include "uavcan_v0.h"

#include "main.h"
#include "node.h"
#include "params.h"

/**
 * Globals
 */

extern uint32_t System_Tick;

// Firmware image app descriptor
extern app_descriptor_t app_descriptor;

// Factory calibration data
extern factory_info_t factory_info;

// The UAVCAN node
extern node_info_t node;

// Operational parameters
extern parameters_t parameters;

// ADC DMA Values
extern uint32_t adc_values[ADC_AVERAGE_COUNT * ADC_VALUES];

/**
 * System Status
 */
extern device_status_t status;

float          ina_levels[4] = {32.0f, 64.0f, 128.0f};
battery_info_t battery_info;

// Circuit status messages for the three buses
circuit_status_t aux_circuit_status;
circuit_status_t main_circuit_status;
circuit_status_t sys_circuit_status;

// Device Temperature Messages from the temperature sensor and the internal MCU sensor
device_temperature_t device_temperature_pcb;
device_temperature_t device_temperature_mcu;

void Sensors_Primary_Config(float V_shunt_mA);

/**
 * Compute / Set the battery capacity max. This is used on startup and
 * when the user changes R_shunt_primary or battery_capacity.
 */
void Battery_Capacity_Calc() {
  // Hackish... but we don't actually know how many Wh are in the battery so use average between full and empty
  battery_info.full_charge_capacity_Wh = 5.0e-4f * (parameters.battery_v_full + parameters.battery_v_empty) * (float) parameters.battery_capacity_mAh;

  // Save these so we can multiply and not divide...
  status.main_bus.filter_scale              = 1.0f / (parameters.bus_main_filter_coefficient + 1.0f);
  status.main_bus.shunt_scale_1over100kOhm  = 0.00001f / parameters.bus_main_R_shunt_Ohm;
  status.battery.inverse_capacity_100overWh = battery_info.full_charge_capacity_Wh > 0.0f ? 100.0f / battery_info.full_charge_capacity_Wh : 0.0f;

#if defined(POWER_MODULE)
  status.aux_bus.shunt_scale = 0.00001f / parameters.bus_aux_R_shunt_Ohm;
#endif

  /**
   * I_reported = I_measured - I_offset
   *            = V_shunt / R_shunt - k[A/V] V_bus
   *
   * I_reported * R_shunt = V_shunt - k[A/V] V_bus R_shunt
   *
   * (100,000) 10uV / 1 V_s
   * (250) 4mV / 1 V_b
   *  1 V_b / (250) 4mV
   *
   *  [V_s / V_bus] = 400 [10uV / 4mV]
   *
   */
  status.main_bus.V_shunt_offset_10uV_p_4mV = 400.0f * parameters.bus_main_I_offset_ApV * parameters.bus_main_R_shunt_Ohm;
}

/**
 * Process the ADC DMA data
 */
void Sensors_ADC_Compute() {

  /**
   * MCU Temperature Sensor
   */

  // Temperature Sensor is in Section 15.3.30 of the reference manual
  // this can be made more accurate using the internal calibration data See 6.3.22 in data sheet
  // This is from STM32Cube_FW_F3_V1.10.0/Projects/STM32F302R8-Nucleo/Examples/ADC/ADC_Sequencer
  const float TEMP_SCALE = (float) (TEMPSENSOR_CAL2_TEMP - TEMPSENSOR_CAL1_TEMP) / (float) (*TEMPSENSOR_CAL2_ADDR - *TEMPSENSOR_CAL1_ADDR);

  int32_t adc_average[ADC_VALUES] = {0};

  int offset = 0;
  for (int j = 0; j < ADC_AVERAGE_COUNT; ++j) {
    for (int i = 0; i < ADC_VALUES; ++i) {
      adc_average[i] += (int32_t) adc_values[offset++];
    }
  }

  // Normalize average
  for (int i = 0; i < ADC_VALUES; ++i) {
    adc_average[i] /= ADC_AVERAGE_COUNT;
  }

  int32_t t_raw = (adc_average[0] - (int32_t) *TEMPSENSOR_CAL1_ADDR);

  // Compute the actual temperature
  float mcu_temp_C = (float) TEMPSENSOR_CAL1_TEMP + (float) t_raw * TEMP_SCALE;

  DeviceTemperature_Set(mcu_temp_C,
                        parameters.mcu_temp_limit_min_C,
                        parameters.mcu_temp_limit_max_C,
                        &device_temperature_mcu);

  /**
   * VDD (3.3V) Bus
   */
  float vdd_scale = adc_average[1] > 0 ? 1.0f / (float) adc_average[1] : 0.0f;

  sys_circuit_status.voltage = 3.3f * (float) *VREFINT_CAL_ADDR * vdd_scale;
  sys_circuit_status.current = 0.0f;

  Check_Circuit_Limits(&sys_circuit_status, 3.2f, 3.5f, 0.0f, 1.0f);

#if defined(POWER_MONITOR)
  /**
   * 5V Bus
   */
  aux_circuit_status.voltage     = parameters.bus_aux_voltage_scale * (float) adc_average[2];
  aux_circuit_status.current     = 0.0f;
  aux_circuit_status.error_flags = 0;

  Check_Circuit_Limits(&aux_circuit_status,
                       parameters.bus_aux_voltage_limit_min,
                       parameters.bus_aux_voltage_limit_max,
                       0.0f,
                       1.0f);
#endif
}

#if defined(POWER_MODULE)

void Sensors_Aux_Config() {
  // Configure INA219 - CAN
  // - Set bus voltage range to 16V
  // - Set PGA gain and range: 80mV for CAN on PMv1 and 40mV for PMv2
  // - Bus SADC resolution: ???
  // - Bus BADC resolution: ???
  // - Operating Mode is 0x7 - Shunt, bus, continuous (default)
  uint16_t ina_can_config = INA219_CONFIG_BRNG_16V
                            | INA219_CONFIG_PGA_40mV
                            | INA219_CONFIG_BADC_2spl
                            | INA219_CONFIG_SADC_16spl
                            | INA219_CONFIG_MODE_SHUNT_BUS_CONT;

  if (!INA219_Config(I2C_INTERNAL, INA_AUX_ADDRESS_7B, ina_can_config)) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_AUX;
  }
}

/**
 * 5V Circuit
 */
void Sensors_Aux_Compute() {
  aux_circuit_status.current = compute_average(status.aux_bus.V_shunt) * status.aux_bus.shunt_scale;
  aux_circuit_status.voltage = (float) (0.004f * compute_average(status.aux_bus.V_bus));

  Check_Circuit_Limits(&aux_circuit_status,
                       parameters.bus_aux_voltage_limit_min,
                       parameters.bus_aux_voltage_limit_max,
                       parameters.bus_aux_current_limit_min,
                       parameters.bus_aux_current_limit_max);
}

/**
 * Read sensor data from AUX INA
 */
void Sensors_Aux_Readout() {

  if (status.i2c_device_error & I2C_BUS_ERROR_INA_AUX) {
    return;
  }

  // Auxiliary circuit
  if (!INA219_Readout(I2C_INTERNAL,
                      INA_AUX_ADDRESS_7B,
                      &status.aux_bus.V_shunt.data[status.aux_bus.V_shunt.index],
                      &status.aux_bus.V_bus.data[status.aux_bus.V_bus.index])) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_AUX;
  }
  //
  //  // Increment buffer (TODO: Verify that this is the right thing to do here)
  //  rolling_buffer_inc(&status.aux_bus.V_bus);
  //  rolling_buffer_inc(&status.aux_bus.V_shunt);
}
#endif


/**
 * Primary / Battery Circuit
 */
void Sensors_Primary_Compute() {

  /* Current compensation
   * shunt_scale = 0.00001f / parameters.bus_main_R_shunt_Ohm;
   *
   * const float current_offset_A =  0.004f * parameters.bus_main_I_offset_ApV * compute_average(status.main_bus.V_bus);
   *
   * main_circuit_status.current_ = status.main_bus.shunt_scale * (V_shunt_avg - current_offset_A / status.main_bus.shunt_scale);
   * main_circuit_status.current_ = status.main_bus.shunt_scale * (V_shunt_avg - current_offset_A * parameters.bus_main_R_shunt_Ohm / 0.00001f);
   * main_circuit_status.current_ = status.main_bus.shunt_scale * (V_shunt_comp);
   *
   * V_shunt_comp = V_shunt_avg - 0.004f * parameters.bus_main_I_offset_ApV * compute_average(status.main_bus.V_bus) * parameters.bus_main_R_shunt_Ohm / 0.00001f;
   * V_shunt_comp = V_shunt_avg - 400.0f * parameters.bus_main_I_offset_ApV * compute_average(status.main_bus.V_bus) * parameters.bus_main_R_shunt_Ohm;
   *
   * main_circuit_status.current = V_shunt_avg * status.main_bus.shunt_scale - parameters.bus_main_I_offset_ApV * 0.004f * compute_average(status.main_bus.V_bus);
   */

  // Bus voltage is in units of 4mV
  float V_bus_mV              = 4.0f * compute_average(status.main_bus.V_bus_4mV);
  main_circuit_status.voltage = 0.001f * V_bus_mV;

  /**
   *
   * The LSB for the INA219 shunt voltage is 10uV or 0.01mV, so for a 0.0005 Ohm shunt resistor
   * the smallest detectable current is 20mA.
   */

  /* Filter current */
  int32_t V_shunt = status.main_bus.V_shunt_10uV.data[status.main_bus.V_shunt_10uV.index];

  // Supress currents around 1V since they produce undesired numeric effects
  if (main_circuit_status.voltage < 1.0f) {
    V_shunt = 0;
  }

  if (parameters.bus_main_enable_current_filter) {
    V_shunt = (int32_t) (status.main_bus.filter_scale * ((float) status.main_bus.V_shunt_filtered_prev * parameters.bus_main_filter_coefficient + (float) V_shunt));

    status.main_bus.V_shunt_filtered_prev = V_shunt;
  }

  // not super clean, but we write this back into the rolling buffer, so it can be used in the filtered state later.
  status.main_bus.V_shunt_10uV.data[status.main_bus.V_shunt_10uV.index] = V_shunt;

  const float V_shunt_avg_10uV = compute_average(status.main_bus.V_shunt_10uV);
  main_circuit_status.current  = status.main_bus.shunt_scale_1over100kOhm * V_shunt_avg_10uV;

  // Check current and voltage limits
  Check_Circuit_Limits(&main_circuit_status,
                       parameters.bus_main_voltage_limit_min,
                       parameters.bus_main_voltage_limit_max,
                       parameters.bus_main_current_limit_min,
                       fminf(parameters.battery_current_limit_max,
                             parameters.bus_main_current_limit_max));

  /* Battery Info */
  battery_info.current_A           = main_circuit_status.current;
  battery_info.voltage_V           = main_circuit_status.voltage;
  battery_info.battery_id          = parameters.battery_id;
  battery_info.model_instance_id   = parameters.battery_instance_id;
  battery_info.state_of_health_pct = 127;

  // Current Integration or Voltage-based SOC computation by
  if (status.battery.integrator_running) {

    /* Integrate */
    const uint32_t dt = status.main_bus.V_bus_4mV.timestamp[status.main_bus.V_bus_4mV.index] - status.main_bus.V_bus_4mV.timestamp[status.main_bus.V_bus_4mV.index_prev];

    // INA is averaging on a scale of dt so to get quantity discharged can just multiply V_shunt * dt
    status.main_bus.consumed_mWms += 4.0e-3f * (float) status.main_bus.V_bus_4mV.data[status.main_bus.V_bus_4mV.index] * (float) (dt * V_shunt);
    status.battery.charge_consumed_mWs = (int32_t) ((float) status.main_bus.consumed_mWms * status.main_bus.shunt_scale_1over100kOhm);

    /**
     * Add the large and small number's contribution.
     */
    battery_info.remaining_capacity_Wh = battery_info.full_charge_capacity_Wh - ((float) status.battery.charge_consumed_mWs / 3.6e6f);

    // If this gets weird it can make the uint8_t > 100 which will erroneously indicate full battery.
    const float state_of_charge_pct = status.battery.inverse_capacity_100overWh * battery_info.remaining_capacity_Wh;
    // Clamp
    battery_info.state_of_charge_pct = ((state_of_charge_pct < 0.0f) || (battery_info.voltage_V < parameters.battery_v_empty))  ? 0 : (uint8_t) (state_of_charge_pct);
  } else {
    float v_delta    = parameters.battery_v_full - parameters.battery_v_empty;
    float v_scale    = v_delta > 0.0f ? 100.0f / v_delta : 0.0f;
    float charge_pct = ((battery_info.voltage_V - parameters.battery_v_empty) * v_scale);

    battery_info.state_of_charge_pct = (uint8_t) (charge_pct > 0 ? charge_pct : 0);
  }

  if (battery_info.state_of_charge_pct > 100) {
    battery_info.state_of_charge_pct = 100;
  }

  // Check and adjust sensor range setting
  Sensors_Primary_Config(fabsf(V_shunt_avg_10uV));
}

/**
 * Sensor functions
 *
 * @param current_range_high If true the INA219 scale is set for currents > 100A
 *
 */
void Sensors_Primary_Config(float V_shunt_mA) {

  // Configure INA219 - Main Battery
  // - Set bus voltage range to 32V
  // - Bus SADC resolution: 16 samples (8.5ms readout)
  // - Bus BADC resolution: 2 samples (1.06ms readout)
  // - Operating Mode is 0x7 - Shunt, bus, continuous (default)
  uint16_t ina_bus_config = INA219_CONFIG_BRNG_32V
                            | INA219_CONFIG_BADC_2spl
                            | INA219_CONFIG_SADC_16spl
                            | INA219_CONFIG_MODE_SHUNT_BUS_CONT;

  // - Set PGA gain and range: 40mV-320mV for MAIN
  if (V_shunt_mA < ina_levels[0]) {
    ina_bus_config |= INA219_CONFIG_PGA_40mV;
  } else if (V_shunt_mA < ina_levels[1]) {
    ina_bus_config |= INA219_CONFIG_PGA_80mV;
  } else if (V_shunt_mA < ina_levels[2]) {
    ina_bus_config |= INA219_CONFIG_PGA_160mV;
  } else {
    ina_bus_config |= INA219_CONFIG_PGA_320mV;
  }

  if (!INA219_Config(I2C_INTERNAL, INA_MAIN_ADDRESS_7B, ina_bus_config)) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_MAIN;
  }
}

/**
 * Read sensor data from Main INA
 */
void Sensors_Primary_Readout() {

  if (status.i2c_device_error & I2C_BUS_ERROR_INA_MAIN) {
    return;
  }

  int32_t V_shunt_10uV;
  int32_t V_bus_4mV;

  // Main battery
  if (!INA219_Readout(I2C_INTERNAL,
                      INA_MAIN_ADDRESS_7B,
                      &V_shunt_10uV,
                      &V_bus_4mV)) {
    status.i2c_device_error |= I2C_BUS_ERROR_INA_MAIN;
    return;
  }

  /* Compensate for input filter induced bus-voltage dependant current offset */
  const int32_t V_shunt_offset_10uV = (int32_t) (status.main_bus.V_shunt_offset_10uV_p_4mV * (float) V_bus_4mV);

  // Add values
  rolling_buffer_add(&status.main_bus.V_shunt_10uV, V_shunt_10uV - V_shunt_offset_10uV);
  rolling_buffer_add(&status.main_bus.V_bus_4mV, V_bus_4mV);
}

/**
 * PCB Temperature Sensor (MCP9808)
 */
void Sensors_Temperature_Readout() {

  float temperature_C = 0.0f;

  if (status.i2c_device_error & I2C_BUS_ERROR_MCP_TEMP) {
    return;
  }

  if (!MCP9808_Readout(I2C_INTERNAL, MCP_PCB_ADDRESS_7B, &temperature_C)) {
    status.i2c_device_error |= I2C_BUS_ERROR_MCP_TEMP;
    return;
  }

  DeviceTemperature_Set(temperature_C,
                        parameters.pcb_temp_limit_min_C,
                        parameters.pcb_temp_limit_max_C,
                        &device_temperature_pcb);
}

/**
 * Copy parameters into various cached structures
 */
void Update_Globals() {

  // Temperature Device IDs
  device_temperature_mcu.device_id = parameters.mcu_temp_device_id;
  device_temperature_pcb.device_id = parameters.pcb_temp_device_id;

  // Circuit IDs
  aux_circuit_status.circuit_id  = parameters.bus_aux_circuit_id;
  main_circuit_status.circuit_id = parameters.bus_main_circuit_id;
  sys_circuit_status.circuit_id  = parameters.bus_sys_circuit_id;
}

/**
 * This is essentially the main entry point for the firmware after all the hardware has been initialized.
 */
void Run_Event_Loop() {

  uint32_t timestamp_next_sensor_readout = 0;
  uint32_t temp;
  status.battery.integrator_running = false;

  // and load settings
  bool param_load_success = true;

  // Somewhat hard coded parameter
  parameters.led_status_on_time_ms = 950;

  // TODO: Distinguish between defaults being loaded and failure

  // If TP2 is pulled down then the user wants to load defaults
  if (!LL_GPIO_IsInputPinSet(TP_DEFAULTS_GPIO_Port, TP_DEFAULTS_Pin)) {
    Params_Load_Default_Values();
    Params_Save();

    Busy_Wait(100);
    param_load_success = false;
  } else {
    param_load_success = Params_Load();
  }

  Update_Globals();

  // Load node information
  Node_Init(&node,
            UAVCAN_NODE_HEALTH_OK,
            UAVCAN_NODE_MODE_OPERATIONAL,
            parameters.can.default_node_id,
            app_descriptor.major_version,
            app_descriptor.minor_version,
            app_descriptor.vcs_commit,
            app_descriptor.image_crc,
            APP_NAME,
            &factory_info.header);

  node.id_valid           = !parameters.can.dynamic_id_allocation_enable;
  node.vendor_status_code = param_load_success ? 0 : UAVCAN_VSC_DEFAULTS_LOADED;

  Battery_Capacity_Calc();

  // Configure INAs
  Sensors_Primary_Config(0.0f);

#if defined(POWER_MODULE)
  Sensors_Aux_Config();
#endif

  // Initialize the CAN termination
  CAN_Set_Termination();

#if defined(POWER_MODULE) && HARDWARE_VERSION_MAJOR == 2
  // and the external I2C bus
  if (parameters.system_enable_external_i2c) {
    I2C_External_Init();
  }
#endif

  // and the LED if it's on
  if (parameters.led_enabled) {
    LED_Init();
  }

  // Setup LED states
  LED_Add_State(LED_STATE_BATTERY_LEVEL);

  if (parameters.can.auto_baud_enable) {
    LED_Add_State(LED_STATE_AUTO_BAUD);
  }

  // The main loop
  for (;;) {

    // Turn LEDs on/off and switch states
    LED_Update();

    // Read-out sensors (TODO: Breakup into reading and integration loops)
    if (System_Tick > timestamp_next_sensor_readout) {

      // Disable most interrupts during readout
      Enable_Interrupts(false);

      Sensors_Temperature_Readout();
      Sensors_Primary_Readout();

#if defined(POWER_MODULE)
      Sensors_Aux_Readout();
#endif

      Enable_Interrupts(true);

      // Compute
      Sensors_ADC_Compute();
      Sensors_Primary_Compute();

#if defined(POWER_MODULE)
      Sensors_Aux_Compute();
#endif

      // Only start integrator when a full battery is actually plugged in
      if (parameters.battery_enable_current_integration && !status.battery.integrator_running) {
        if (battery_info.voltage_V >= parameters.battery_v_full) {
          status.battery.integrator_running = true;
        } else {
          battery_info.status_flags |= UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE;
        }
      }

      timestamp_next_sensor_readout = System_Tick + parameters.system_sensor_readout_interval_ms;

      // Is there a problem?
      if ((aux_circuit_status.error_flags != 0)
          || (main_circuit_status.error_flags != 0)
          || ((battery_info.status_flags & UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE) != 0)
          || (device_temperature_pcb.error_flags != 0)
          || (device_temperature_mcu.error_flags != 0)
          || (status.i2c_device_error != 0)) {

        // Clear error flag if the integrator has been started
        if (status.battery.integrator_running) {
          battery_info.status_flags &= ~UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE;
        }

        if (status.i2c_device_error != 0) {
          node.vendor_status_code |= (status.i2c_device_error << 8);
        }

        node.health = UAVCAN_NODE_HEALTH_ERROR;
        LED_Add_State(LED_STATE_FAULT);
      } else {
        node.health = UAVCAN_NODE_HEALTH_OK;
        LED_Remove_State(LED_STATE_FAULT);
      }
    }

    node.uptime_sec = System_Tick / 1000;

    // We need CAN beyond this point so if it's not online activate it
    if (node.can_status != CAN_STATUS_ONLINE) {
      CANbus_Init();
      continue;
    }

    // Process any pending requests
    Service_Requests();

    /**
     * Send Status Messages
     */

    // Node Status Message
    if ((System_Tick - node.timestamp) > parameters.system_node_status_msg_interval_ms) {
      Send_NodeStatus_Message(&node);
    }

    // Battery Status Message
    temp = System_Tick - battery_info.timestamp;
    if ((parameters.battery_status_msg_interval_ms > 0) && (temp > parameters.battery_status_msg_interval_ms)) {
      // Send out battery info data
      Send_BatteryInfo_Message(node.id, &battery_info);
    }

    // Circuit Status for CANbus
    temp = System_Tick - aux_circuit_status.timestamp;
    if (((parameters.bus_aux_circuit_status_msg_interval_ms > 0) && (temp > parameters.bus_aux_circuit_status_msg_interval_ms))
        || ((aux_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))) {

      Send_CircuitStatus_Message(node.id, &aux_circuit_status);
    }

    // Circuit Status for Primary
    temp = System_Tick - main_circuit_status.timestamp;
    if (((parameters.bus_main_circuit_status_msg_interval_ms > 0) && (temp > parameters.bus_main_circuit_status_msg_interval_ms))
        || ((main_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))) {

      Send_CircuitStatus_Message(node.id, &main_circuit_status);
    }

    // Circuit Status for VDD
    temp = System_Tick - sys_circuit_status.timestamp;
    if (((parameters.bus_sys_circuit_status_msg_interval_ms > 0) && (temp > parameters.bus_sys_circuit_status_msg_interval_ms))
        || ((sys_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))) {

      Send_CircuitStatus_Message(node.id, &sys_circuit_status);
    }

    // Device Temperature for MCU
    temp = System_Tick - device_temperature_mcu.timestamp;
    if (((parameters.mcu_temp_msg_interval_ms > 0) && (temp > parameters.mcu_temp_msg_interval_ms))
        || ((device_temperature_mcu.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))) {

      Send_DeviceTemperature_Message(node.id, &device_temperature_mcu);
    }

    // Device Temperature for PCB
    temp = System_Tick - device_temperature_pcb.timestamp;
    if (((parameters.pcb_temp_msg_interval_ms > 0) && (temp > parameters.pcb_temp_msg_interval_ms))
        || ((device_temperature_pcb.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))) {

      Send_DeviceTemperature_Message(node.id, &device_temperature_pcb);
    }
  }
}
