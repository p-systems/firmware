/**
 *
 * Author: Stou Sandalski <stou@p-systems.io>
 * Copyright (c) 2021 - Pomegranate Systems LLC
 *
 * License: MIT and 3-Clause BSD (See LICENSE.md)
 *
 */

#include "power_module.h"

#include <stdint.h>

#include "ina219.h"
#include "mcp9808.h"
#include <stm32_util.h>
#include <uavcan_v0.h>

#include "main.h"
#include "node.h"
#include "params.h"

/**
 * Globals
 */

extern uint32_t System_Tick;

// Firmware image app descriptor
extern app_descriptor_t app_descriptor;

// Factory calibration data
extern factory_info_t factory_info;

// The UAVCAN node
extern node_info_t node;

// Operational parameters
extern power_module_parameters_t parameters;

// ADC DMA Values
extern uint32_t adc_values[ADC_AVERAGE_COUNT * ADC_VALUES];

/**
 * Battery Information
 */
battery_info_t battery_info;

// This is the consumed battery charge [mV s] and the remainder [10uV ms]
int64_t battery_charge_consumed_10uVms;
int32_t battery_charge_consumed_mVs;
int32_t battery_charge_total_mVs;
int32_t battery_charge_consumed_mWs;

float consumed_mWms;
float shunt_scale_primary;
float shunt_scale_aux;

float inverse_capacity_100overWh;

// Which range of the INA219 of the lower two are we using
bool battery_current_range_high = false;
bool battery_full_on_start = false;

uint32_t last_readout_bat_tick = 0;

rolling_buffer_t V_aux_shunt;
rolling_buffer_t V_aux_bus;

// Previous filtered value used for IIR
int32_t V_shunt_filtered_prev = 0;
float main_bus_filter_scale = 0.0f;

// Units of 10uV
rolling_buffer_t V_main_shunt;
rolling_buffer_t V_main_bus;

// Circuit status messages for the three buses
circuit_status_t aux_circuit_status;
circuit_status_t main_circuit_status;
circuit_status_t sys_circuit_status;

// Device Temperature Messages from the temperature sensor and the internal MCU sensor
device_temperature_t device_temperature_pcb;
device_temperature_t device_temperature_mcu;

/**
 * Compute / Set the battery capacity max. This is used on startup and
 * when the user changes R_shunt_primary or battery_capacity.
 */
void Battery_Capacity_Calc(){

  // (3600 s / 1hr) * ( capacity mA h * R_shunt Ohm)
  battery_charge_total_mVs = (int32_t) ((float) (3600U * parameters.battery_capacity_mAh) *
                                        parameters.bus_main_R_shunt);

  // Hackish... but we don't actually know how many Wh are in the battery so use average between full and empty
  battery_info.full_charge_capacity_Wh =
      5.0e-4f * (parameters.battery_v_full + parameters.battery_v_empty) * (float) parameters.battery_capacity_mAh;

  // Save this so we can multiply and not divide...
  shunt_scale_primary = 0.00001f / parameters.bus_main_R_shunt;
  shunt_scale_aux = 0.00001f / parameters.bus_aux_R_shunt;

  inverse_capacity_100overWh =
      battery_info.full_charge_capacity_Wh > 0.0f ? 100.0f / battery_info.full_charge_capacity_Wh : 0.0f;
  main_bus_filter_scale = 1.0f / (parameters.bus_main_filter_coefficient + 1.0f);
}

/**
 * Sensor functions
 *
 * @param current_range_high If true the INA219 scale is set for currents > 100A
 *
 */
void Sensors_Config(I2C_TypeDef *hi2c,
                    bool current_range_high){

  // Configure INA219 - CAN
  // - Set bus voltage range to 16V
  // - Set PGA gain and range: 80mV for CAN on PMv1 and 40mV for PMv2
  // - Bus SADC resolution: ???
  // - Bus BADC resolution: ???
  // - Operating Mode is 0x7 - Shunt, bus, continuous (default)
  uint16_t ina_can_config = INA219_CONFIG_BRNG_16V
                            | INA219_CONFIG_PGA_40mV
                            | INA219_CONFIG_BADC_2spl
                            | INA219_CONFIG_SADC_16spl
                            | INA219_CONFIG_MODE_SHUNT_BUS_CONT;

  INA219_Config(hi2c, INA_CAN_ADDR_7B, ina_can_config);

  // Configure INA219 - Main Battery
  // - Set bus voltage range to 32V
  // - Set PGA gain and range: 40mV for CAN
  // - Bus SADC resolution: 16 samples (8.5ms readout)
  // - Bus BADC resolution: 2 samples (1.06ms readout)
  // - Operating Mode is 0x7 - Shunt, bus, continuous (default)
  uint16_t ina_bus_config = INA219_CONFIG_BRNG_32V
                            | (current_range_high ? INA219_CONFIG_PGA_80mV : INA219_CONFIG_PGA_40mV)
                            | INA219_CONFIG_BADC_2spl
                            | INA219_CONFIG_SADC_16spl
                            | INA219_CONFIG_MODE_SHUNT_BUS_CONT;

  INA219_Config(hi2c, INA_MAIN_ADDR_7B, ina_bus_config);
}

/**
 * Read sensor data from the two INA
 */
void Sensors_Readout(){

  // Auxiliary circuit
  INA219_Readout(I2C_INTERNAL,
                 INA_CAN_ADDR_7B,
                 &V_aux_shunt.data[V_aux_shunt.index],
                 &V_aux_bus.data[V_aux_bus.index]);

  rolling_buffer_inc(&V_aux_bus);
  rolling_buffer_inc(&V_aux_shunt);

  // Main battery
  INA219_Readout(I2C_INTERNAL,
                 INA_MAIN_ADDR_7B,
                 &V_main_shunt.data[V_main_shunt.index],
                 &V_main_bus.data[V_main_bus.index]);

  uint32_t dt = System_Tick - last_readout_bat_tick;
  last_readout_bat_tick = System_Tick;

  /**
   * The INA is averaging on a scale of dt so to get quantity discharged can just multiply V_shunt * dt
   *
   * V_bat_shunt has units of [10muV]
   *
   * The LSB for the INA219 shunt voltage is 10uV or 0.01mV, so for a 0.0005 Ohm shunt resistor
   * the smallest detectable current is 20mA.
   *
   * We are keeping track of charge in units of [mV * s] and a remainder
   * in units of [10uV * ms]
   *
   * The sensor data is integrated in units of [10uV * ms]
   */

  int32_t V_shunt = V_main_shunt.data[V_main_shunt.index];

  if(parameters.bus_main_enable_current_filter){
    V_shunt = (int32_t) (main_bus_filter_scale * ((float) V_shunt_filtered_prev * parameters.bus_main_filter_coefficient
                                                  + (float) V_main_shunt.data[V_main_shunt.index]));
    V_shunt_filtered_prev = V_shunt;
  }

  battery_charge_consumed_10uVms += dt * V_shunt;
  int32_t consumed_mVs = (int32_t) battery_charge_consumed_10uVms / 100000;

  if(consumed_mVs > 1){
    // Consumed
    battery_charge_consumed_10uVms -= (100000 * consumed_mVs);
    battery_charge_consumed_mVs += consumed_mVs;
  }else if(consumed_mVs < -1){
    // Charged
    battery_charge_consumed_10uVms += (100000 * consumed_mVs);
    battery_charge_consumed_mVs -= consumed_mVs;
  }

  // Shunt data comes in units of 10uV so we divide by 100 to convert it into units of 1mV, V_bus is in units of 4mV
  consumed_mWms += 4.0e-3f * (float) V_main_bus.data[V_main_bus.index] * (float) (dt * V_shunt);
  battery_charge_consumed_mWs = (int32_t) ((float) consumed_mWms * shunt_scale_primary);

  // Increment buffer
  rolling_buffer_inc(&V_main_bus);
  rolling_buffer_inc(&V_main_shunt);
}

/**
 * PCB Temperature Sensor (MCP9808)
 */
void Sensors_Readout_Temp(){

  DeviceTemperature_Set(MCP9808_Readout(I2C_INTERNAL, TEMP_PCB_ADDR_7B),
                        parameters.pcb_temp_limit_min_C,
                        parameters.pcb_temp_limit_max_C,
                        &device_temperature_pcb);
}

/**
 * Primary / Battery Circuit
 */
void Sensors_Compute_Primary(){

  // Shunt voltage is in units of 10 micro V
  main_circuit_status.current = battery_info.current_A = (compute_average(V_main_shunt) * shunt_scale_primary);
  // Bus voltage is in units of 4mV
  main_circuit_status.voltage = battery_info.voltage_V = (float) (0.004f * compute_average(V_main_bus));

  // Check current and voltage limits
  Check_Circuit_Limits(&main_circuit_status,
                       parameters.bus_main_voltage_limit_min,
                       parameters.bus_main_voltage_limit_max,
                       parameters.bus_main_current_limit_max);

  /**
   * Current Integration
   *
   * Disable integration and switch over to voltage-based SOC computation by
   *
   */

  // Current integration is on if it's enabled AND (battery is full OR battery is empty but auto-disable is on)
  bool enable_integration = parameters.bus_main_enable_current_integration
                            && (battery_full_on_start || !parameters.bus_main_auto_disable_current_track);

  if(enable_integration){
    /**
     * Add the large and small number's contribution.
     */

    battery_info.remaining_capacity_Wh =
        battery_info.full_charge_capacity_Wh - ((float) battery_charge_consumed_mWs / 3.6e6f);

    // If this gets weird it can make the uint8_t > 100 which will erroneously indicate full battery.
    float state_of_charge_pct = inverse_capacity_100overWh * battery_info.remaining_capacity_Wh;

    if(state_of_charge_pct < 0.0f){
      state_of_charge_pct = 0;
    }

    battery_info.state_of_charge_pct = (uint8_t) (state_of_charge_pct);

    if((battery_info.remaining_capacity_Wh < 0.0f)
       || (battery_info.voltage_V < parameters.battery_v_empty)){
      battery_info.remaining_capacity_Wh = 0.0f;
      battery_info.state_of_charge_pct = 0;
    }

  }else{
    float v_delta = parameters.battery_v_full - parameters.battery_v_empty;
    float v_scale = v_delta > 0.0f ? 100.0f / v_delta : 0.0f;
    float charge_pct = ((battery_info.voltage_V - parameters.battery_v_empty) * v_scale);

    battery_info.state_of_charge_pct = (uint8_t) (charge_pct > 0 ? charge_pct : 0);
  }

  if(battery_info.state_of_charge_pct > 100) battery_info.state_of_charge_pct = 100;

  battery_info.battery_id = parameters.battery_id;
  battery_info.model_instance_id = parameters.battery_instance_id;
  battery_info.state_of_health_pct = 127;
  battery_info.status_flags |= UAVCAN_BATTERY_STATUS_FLAG_IN_USE;

  // Check and adjust sensor range setting
  int32_t current_mA = (int32_t) (1000.0f * main_circuit_status.current);

  if((current_mA > parameters.bus_main_sensor_range_switch_high_mA) && (!battery_current_range_high)){
    battery_current_range_high = true;
    Sensors_Config(I2C_INTERNAL, battery_current_range_high);
  }else if((current_mA < parameters.bus_main_sensor_range_switch_low_mA) && (battery_current_range_high)){
    battery_current_range_high = false;
    Sensors_Config(I2C_INTERNAL, battery_current_range_high);
  }
}


/**
 * 5V Circuit
 */
void Sensors_Compute_Aux(){

  aux_circuit_status.current = compute_average(V_aux_shunt) * shunt_scale_aux;
  aux_circuit_status.voltage = (float) (0.004f * compute_average(V_aux_bus));

  Check_Circuit_Limits(&aux_circuit_status,
                       parameters.bus_aux_voltage_limit_min,
                       parameters.bus_aux_voltage_limit_max,
                       parameters.bus_aux_current_limit_max);
}

/**
 * Process the ADC DMA data
 */
void Sensors_Compute_ADC(){

  /**
   * MCU Temperature Sensor
   */

  // Temperature Sensor is in Section 15.3.30 of the reference manual
  // this can be made more accurate using the internal calibration data See 6.3.22 in data sheet
  // This is from STM32Cube_FW_F3_V1.10.0/Projects/STM32F302R8-Nucleo/Examples/ADC/ADC_Sequencer
  const float TEMP_SCALE = 80.0f / (float) (*TEMPSENSOR_CAL2_ADDR - *TEMPSENSOR_CAL1_ADDR);

  int32_t adc_average[ADC_VALUES] = {0, 0};

  int offset = 0;
  for(int j = 0; j < ADC_AVERAGE_COUNT; ++j){
    for(int i = 0; i < ADC_VALUES; ++i){
      adc_average[i] += (int32_t) adc_values[offset++];
    }
  }

  // Normalize average
  for(int i = 0; i < ADC_VALUES; ++i){
    adc_average[i] /= ADC_AVERAGE_COUNT;
  }

  int32_t t_raw = (adc_average[0] - (int32_t) *TEMPSENSOR_CAL1_ADDR);

  // Compute the actual temperature
  float mcu_temp_C = (float) TEMPSENSOR_CAL1_TEMP + (float) t_raw * TEMP_SCALE;

  DeviceTemperature_Set(mcu_temp_C,
                        parameters.mcu_temp_limit_min_C,
                        parameters.mcu_temp_limit_max_C,
                        &device_temperature_mcu);

  /**
   * VDD (3.3V) Bus
   */
  float vdd_scale = adc_average[1] > 0 ? 1.0f / (float) adc_average[1] : 0.0f;

  sys_circuit_status.voltage = 3.3f * (float) *VREFINT_CAL_ADDR * vdd_scale;
  sys_circuit_status.current = 0.0f;

  Check_Circuit_Limits(&sys_circuit_status, 3.2f, 3.5f, 1.0f);
}

/**
 * Copy parameters into various cached structures
 */
void Update_Globals(){

  // Temperature Device IDs
  device_temperature_mcu.device_id = parameters.mcu_temp_device_id;
  device_temperature_pcb.device_id = parameters.pcb_temp_device_id;

  // Circuit IDs
  aux_circuit_status.circuit_id = parameters.bus_aux_circuit_id;
  main_circuit_status.circuit_id = parameters.bus_main_circuit_id;
  sys_circuit_status.circuit_id = parameters.bus_sys_circuit_id;
}

/**
 * This is essentially the main entry point for the firmware after all the hardware has been initialized.
 */
void Run_Event_Loop(){

  uint32_t timestamp_last_sensor_readout = 0;
  uint32_t temp;

  // and load settings
  bool param_load_success;

  // Somewhat hard coded parameter
  parameters.led_status_on_time_ms = 950;

  // If TP2 is pulled down then the user wants to load defaults
  if(!LL_GPIO_IsInputPinSet(TP_GPIO_2_GPIO_Port, TP_GPIO_2_Pin)){
    Params_Load_Default_Values(&parameters);
    Params_Save(&parameters);

    Busy_Wait(100);
    param_load_success = false;
  }else{
    param_load_success = Params_Load(&parameters);
  }

  Update_Globals();

  // Load node information
  Node_Init(&node,
            UAVCAN_NODE_HEALTH_OK,
            UAVCAN_NODE_MODE_OPERATIONAL,
            parameters.can.default_node_id,
            app_descriptor.major_version,
            app_descriptor.minor_version,
            app_descriptor.vcs_commit,
            app_descriptor.image_crc,
            APP_NAME,
            &factory_info.header);

  node.id_valid = !parameters.can.dynamic_id_allocation_enable;
  node.vendor_status_code = param_load_success ? 0 : UAVCAN_VSC_DEFAULTS_LOADED;

  Battery_Capacity_Calc();

  // Read the INA219 data at the start to determine if integrator should be on.
  Sensors_Readout();

  Sensors_Compute_Primary();

  battery_full_on_start = battery_info.voltage_V >= parameters.battery_v_full;

  // If integration is enabled but battery voltage is below full on startup and auto-disable is off then
  // the battery needs service
  if(parameters.bus_main_enable_current_integration
     && !battery_full_on_start
     && !parameters.bus_main_auto_disable_current_track){
    battery_info.status_flags |= UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE;
  }

  // Initialize the CAN termination
  CAN_Set_Termination();

  // and the external I2C bus
  if(parameters.system_enable_external_i2c){
    I2C_External_Init();
  }

  // and the LED if it's on
  if(parameters.led_enabled){
    LED_Init();
  }

  // Setup LED states
  LED_Add_State(LED_STATE_BATTERY_LEVEL);

  if(parameters.can.auto_baud_enable){
    LED_Add_State(LED_STATE_AUTO_BAUD);
  }

  // and sensors
  Sensors_Config(I2C_INTERNAL, battery_current_range_high);

  // The main loop
  for(;;){

    // Turn LEDs on/off and switch states
    LED_Update();

    // Read-out sensors (TODO: Breakup into reading and integration loops)
    if((System_Tick - timestamp_last_sensor_readout) > parameters.system_sensor_readout_interval_ms){

      Sensors_Readout();
      Sensors_Readout_Temp();

      Sensors_Compute_ADC();
      Sensors_Compute_Aux();
      Sensors_Compute_Primary();

      timestamp_last_sensor_readout = System_Tick;

      // Is there a problem?
      if((aux_circuit_status.error_flags != 0)
         || (main_circuit_status.error_flags != 0)
         || ((battery_info.status_flags & UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE) != 0)
         || (device_temperature_pcb.error_flags != 0)
         || (device_temperature_mcu.error_flags != 0)){

        // Need this for multi-battery setups where the module is first powered up by 5V
        // and then by its own primary battery
        if(battery_info.voltage_V >= parameters.battery_v_full){
          battery_info.status_flags &= ~UAVCAN_BATTERY_STATUS_FLAG_NEED_SERVICE;
          battery_full_on_start = true;
        }

        node.health = UAVCAN_NODE_HEALTH_ERROR;
        LED_Add_State(LED_STATE_FAULT);
      }else{
        node.health = UAVCAN_NODE_HEALTH_OK;
        LED_Remove_State(LED_STATE_FAULT);
      }
    }

    node.uptime_sec = System_Tick / 1000;

    // We need CAN beyond this point so if it's not online activate it
    if(node.can_status != CAN_STATUS_ONLINE){
      CANbus_Init();
      continue;
    }

    // Process any pending requests
    Service_Requests();

    /**
     * Send Status Messages
     */

    // Node Status Message
    if((System_Tick - node.timestamp) > parameters.system_node_status_msg_interval_ms){
      Send_NodeStatus_Message(&node);
    }

    // Battery Status Message
    temp = System_Tick - battery_info.timestamp;
    if((parameters.battery_status_msg_interval_ms > 0) && (temp > parameters.battery_status_msg_interval_ms)){
      // Send out battery info data
      Send_BatteryInfo_Message(node.id, &battery_info);
    }

    // Circuit Status for CANbus
    temp = System_Tick - aux_circuit_status.timestamp;
    if(((parameters.bus_aux_circuit_status_msg_interval_ms > 0) &&
        (temp > parameters.bus_aux_circuit_status_msg_interval_ms))
       || ((aux_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))){

      Send_CircuitStatus_Message(node.id, &aux_circuit_status);
    }

    // Circuit Status for Primary
    temp = System_Tick - main_circuit_status.timestamp;
    if(((parameters.bus_main_circuit_status_msg_interval_ms > 0) &&
        (temp > parameters.bus_main_circuit_status_msg_interval_ms))
       || ((main_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))){

      Send_CircuitStatus_Message(node.id, &main_circuit_status);
    }

    // Circuit Status for VDD
    temp = System_Tick - sys_circuit_status.timestamp;
    if(((parameters.bus_sys_circuit_status_msg_interval_ms > 0) &&
        (temp > parameters.bus_sys_circuit_status_msg_interval_ms))
       || ((sys_circuit_status.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))){

      Send_CircuitStatus_Message(node.id, &sys_circuit_status);
    }

    // Device Temperature for MCU
    temp = System_Tick - device_temperature_mcu.timestamp;
    if(((parameters.mcu_temp_msg_interval_ms > 0) && (temp > parameters.mcu_temp_msg_interval_ms))
       || ((device_temperature_mcu.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))){

      Send_DeviceTemperature_Message(node.id, &device_temperature_mcu);
    }

    // Device Temperature for PCB
    temp = System_Tick - device_temperature_pcb.timestamp;
    if(((parameters.pcb_temp_msg_interval_ms > 0) && (temp > parameters.pcb_temp_msg_interval_ms))
       || ((device_temperature_pcb.error_flags != 0) && (temp > parameters.system_emergency_msg_interval_ms))){

      Send_DeviceTemperature_Message(node.id, &device_temperature_pcb);
    }
  }
}
