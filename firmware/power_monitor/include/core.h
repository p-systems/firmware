/**
 * @file
 * @brief Power Monitor main event loop, sensor readout, and calculations
 *
 * @author Stou Sandalski <stou@p-systems.io>
 * @copyright (c) 2023 - Pomegranate Systems LLC
 * @license MIT and 3-Clause BSD (See LICENSE.md)
 */

#pragma once

#include <stdbool.h>
#include <stdint.h>

#include "ina219.h"
#include "mcp9808.h"
#include "stm32_util.h"


// Application constants
#define APP_NAME                                                      "io.p-systems.power-monitor"
#define DEVICE_TYPE                                                   9

/**
 * Feature Flags
 */

/**
 * Constants
 */

// I2C Addresses
#define MCP_PCB_ADDRESS_7B                                            (MCP9808_BASE_ADDRESS << 1)
#define INA_MAIN_ADDRESS_7B                                           (0b01001010 << 1)

#define EEPROM_ADDRESS_7B                                             0b10100000
#define EEPROM_PARAM_HEADER_START_PAGE                                20
#define EEPROM_PARAM_START_PAGE                                       21

/**
 * Hardware Configuration (Pins, etc.)
 */

// LED
#define LED_GPIO_Port                                                 GPIOA
#define LED_RGB_R_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_R_Pin                                                 GPIO_PIN_0
#define LED_RGB_G_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_G_Pin                                                 GPIO_PIN_1
#define LED_RGB_B_GPIO_Port                                           LED_GPIO_Port
#define LED_RGB_B_Pin                                                 GPIO_PIN_2

// Test Points
#define TP_BOOTLOADER_GPIO_Port                                       GPIOA
#define TP_BOOTLOADER_Pin                                             GPIO_PIN_3
#define TP_DEFAULTS_GPIO_Port                                         GPIOA
#define TP_DEFAULTS_Pin                                               GPIO_PIN_4

// Boot pin (TP1 - PA13)
#define BOOT_GPIO_Port                                                TP_BOOTLOADER_GPIO_Port
#define BOOT_Pin                                                      TP_BOOTLOADER_Pin
#define BOOT_PIN_ACTIVE_LOW                                           true

// Mostly for bootloader
#define TEST_MODE_ENTER_GPIO_Port                                     TP_DEFAULTS_GPIO_Port
#define TEST_MODE_ENTER_Pin                                           TP_DEFAULTS_Pin
#define TEST_MODE_CLOCK_INPUT_GPIO_Port                               GPIOF
#define TEST_MODE_CLOCK_INPUT_Pin                                     GPIO_PIN_0
#define TEST_MODE_CLOCK_OUTPUT_GPIO_Port                              GPIOF
#define TEST_MODE_CLOCK_OUTPUT_Pin                                    GPIO_PIN_1

// CAN
#define CAN_RX_GPIO_Port                                              GPIOA
#define CAN_RX_Pin                                                    GPIO_PIN_11
#define CAN_RX_AF_MODE                                                GPIO_AF9_CAN

#define CAN_TX_GPIO_Port                                              GPIOA
#define CAN_TX_Pin                                                    GPIO_PIN_12
#define CAN_TX_AF_MODE                                                GPIO_AF9_CAN

#define CAN_FAULT_GPIO_Port                                           GPIOA
#define CAN_FAULT_Pin                                                 GPIO_PIN_8
#define CAN_SILENT_GPIO_Port                                          GPIOA
#define CAN_SILENT_Pin                                                GPIO_PIN_15
#define CAN_TERM_GPIO_Port                                            GPIOB
#define CAN_TERM_Pin                                                  GPIO_PIN_4

// I2C
#define I2C_INTERNAL                                                  I2C2
#define I2C_INTERNAL_CLOCK_PERIPH                                     LL_APB1_GRP1_PERIPH_I2C2
#define I2C_INTERNAL_TIMING                                           0x2000090E
#define I2C_INTERNAL_SCL_GPIO_Port                                    GPIOA
#define I2C_INTERNAL_SCL_Pin                                          GPIO_PIN_9
#define I2C_INTERNAL_SCL_AF_MODE                                      GPIO_AF4_I2C2
#define I2C_INTERNAL_SCL_SET_AFPIN                                    LL_GPIO_SetAFPin_8_15

#define I2C_INTERNAL_SDA_GPIO_Port                                    GPIOA
#define I2C_INTERNAL_SDA_Pin                                          GPIO_PIN_10
#define I2C_INTERNAL_SDA_AF_MODE                                      I2C_INTERNAL_SCL_AF_MODE
#define I2C_INTERNAL_SDA_SET_AFPIN                                    I2C_INTERNAL_SCL_SET_AFPIN
#define I2C_INTERNAL_EV_IRQ                                           I2C2_EV_IRQn
#define I2C_INTERNAL_ER_IRQ                                           I2C2_ER_IRQn

// ADC
#define BUS_VOLTAGE_ADC_GPIO_Port                                     GPIOB
#define BUS_VOLTAGE_ADC_Pin                                           GPIO_PIN_0


/**
 * Other Hardware Configuration
 */

// DMA - ADC buffer size: MCU temp, Vbat/VDD, 5V pin
#define ADC_VALUES                                                    3
#define ADC_AVERAGE_COUNT                                             10

// Timer Configuration for the RGB LED
#define LED_TIM_PRESCALAR                                             7
#define LED_TIM_PERIOD                                                499

// LED mode timing
#define LED_AUTO_BAUD_ON_MS                                           200
#define LED_AUTO_BAUD_OFF_MS                                          500
#define LED_FAULT_ON_MS                                               150
#define LED_FAULT_OFF_MS                                              150
#define LED_IDENTIFY_ON_MS                                            500
#define LED_IDENTIFY_OFF_MS                                           50
#define LED_BATTERY_LEVEL_OFF_MS                                      50
#define LED_RESET_DEFAULT_ON_MS                                       2000


/**
 *  System Calibration data written by Bootloader (only!!)
 */
#define CAL_NAME_I_OFFSET_MAIN                                        "cal.main.I_offset_ApV"
#define CAL_DEFAULT_I_OFFSET_MAIN                                     0.17
#define CAL_MAX_I_OFFSET_MAIN                                         1.0f
#define CAL_MIN_I_OFFSET_MAIN                                         0.0f
#define CAL_VERSION_I_OFFSET_MAIN                                     1
#define CAL_HELP_I_OFFSET_MAIN                                        "Current offset"

#define CAL_NAME_R_SHUNT_MAIN                                         "cal.main.R_shunt_Ohm"
#define CAL_DEFAULT_R_SHUNT_MAIN                                      0.0005f
#define CAL_MAX_R_SHUNT_MAIN                                          0.1f
#define CAL_MIN_R_SHUNT_MAIN                                          0.0f
#define CAL_VERSION_R_SHUNT_MAIN                                      1
#define CAL_HELP_R_SHUNT_MAIN                                         "Main shunt"

#define CAL_NAME_AUX_VOLTAGE_SCALE                                    "cal.aux.V_scale"
#define CAL_DEFAULT_AUX_VOLTAGE_SCALE                                 0.002f
#define CAL_MAX_AUX_VOLTAGE_SCALE                                     1.0f
#define CAL_MIN_AUX_VOLTAGE_SCALE                                     0.0f
#define CAL_VERSION_AUX_VOLTAGE_SCALE                                 1
#define CAL_HELP_AUX_VOLTAGE_SCALE                                    "Voltage Scale"

#define CAL_NAME_LED_BRIGHTNESS_SCALE_R                               "cal.led.r_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_R                            175
#define CAL_MAX_LED_BRIGHTNESS_SCALE_R                                255
#define CAL_MIN_LED_BRIGHTNESS_SCALE_R                                0
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_R                            1
#define CAL_HELP_LED_BRIGHTNESS_SCALE_R                               "Calibration values for the RGB LED so they have all the same intensity"

#define CAL_NAME_LED_BRIGHTNESS_SCALE_G                               "cal.led.g_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_G                            255
#define CAL_MAX_LED_BRIGHTNESS_SCALE_G                                CAL_MAX_LED_BRIGHTNESS_SCALE_R
#define CAL_MIN_LED_BRIGHTNESS_SCALE_G                                CAL_MIN_LED_BRIGHTNESS_SCALE_R
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_G                            CAL_VERSION_LED_BRIGHTNESS_SCALE_R
#define CAL_HELP_LED_BRIGHTNESS_SCALE_G                               ""

#define CAL_NAME_LED_BRIGHTNESS_SCALE_B                               "cal.led.b_scale"
#define CAL_DEFAULT_LED_BRIGHTNESS_SCALE_B                            CAL_DEFAULT_LED_BRIGHTNESS_SCALE_G
#define CAL_MAX_LED_BRIGHTNESS_SCALE_B                                CAL_MAX_LED_BRIGHTNESS_SCALE_R
#define CAL_MIN_LED_BRIGHTNESS_SCALE_B                                CAL_MIN_LED_BRIGHTNESS_SCALE_R
#define CAL_VERSION_LED_BRIGHTNESS_SCALE_B                            CAL_VERSION_LED_BRIGHTNESS_SCALE_R
#define CAL_HELP_LED_BRIGHTNESS_SCALE_B                               ""

/* I2C Device error reporting */
// clang-format off
// Offsets into i2c device error register
#define I2C_BUS_ERROR_EEPROM   0x01
#define I2C_BUS_ERROR_MCP_TEMP 0x02
#define I2C_BUS_ERROR_INA_MAIN 0x04
// clang-format on

/**
 * Requirements
 *
 * - Size of these must be identical to the ones in
 *
 * @note must be aligned(4) not packed!
 */
typedef struct __attribute__ ((aligned(4))) {

  // The general factory info stuff
  factory_info_header_t header;
  uint8_t led_brightness[3];

  float bus_main_I_offset_ApV;
  float bus_main_R_shunt;

  float bus_aux_V_scale;
} factory_info_t;

/**
 * Subroutines
 */
void Battery_Capacity_Calc();

void Run_Event_Loop();

void Update_Globals();
