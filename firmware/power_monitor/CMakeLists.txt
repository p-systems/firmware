#
# CMakeList.txt file for Power Monitor firmware
#

# Configuration
set(SOFTWARE_VERSION_MAJOR 3)
set(SOFTWARE_VERSION_MINOR 1)

set(PROJECT_ROOT ${CMAKE_CURRENT_LIST_DIR})

add_compile_definitions(
        # Firmware identification
        POWER_MONITOR=1
        SOFTWARE_VERSION_MAJOR=${SOFTWARE_VERSION_MAJOR}
        SOFTWARE_VERSION_MINOR=${SOFTWARE_VERSION_MINOR}
        # Features
        ENABLE_AUTO_BAUD=1
        ENABLE_PARAMS=1
        EEPROM_ADDRESS_SIZE=2
        EEPROM_PAGE_SIZE=32)

include_directories(
        ${PROJECT_ROOT}/include
        ${PROJECTS_PREFIX}/power_module/include)

# And common source files
set(USER_SOURCES
        ${COMMON_PREFIX}/src/ina219.c
        ${COMMON_PREFIX}/src/mcp9808.c
        ${COMMON_PREFIX}/src/stm32_util.c
        ${COMMON_PREFIX}/src/uavcan_v0.c
        ${PROJECTS_PREFIX}/power_module/src/main.c
        ${PROJECTS_PREFIX}/power_module/src/core.c
        ${PROJECTS_PREFIX}/power_module/src/node.c
        ${PROJECTS_PREFIX}/power_module/src/params.c)

# Firmware for different hardware version
add_subdirectory(f302x8)
