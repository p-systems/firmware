# firmware

Firmware for Pomegranate Systems hardware

# Building

```
git clone git@bitbucket.org:p-systems/firmware.git
cd firmware
mkdir build
cd build
cmake ../
make
```

Firmware binaries will be located in `bin` directory.

By default bootloader and firmware will be built for all supported hardware. To build
firmware only for specific hardware run `cmake --build . --target help` to get a list of all targets
and then build with

```
make <target>
``` 

for example

```
make power_module_r2_bootloader.elf
```

